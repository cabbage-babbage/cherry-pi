Sources:

* https://github.com/bztsrc/raspi3-tutorial (especially `/00_crosscompiler` for the `aarch64-elf` toolchain setup, and `/03_uart1` for the UART1 'driver' implementation)
* https://raspberrypi.stackexchange.com/questions/34733/how-to-do-qemu-emulation-for-bare-metal-raspberry-pi-images (to compile QEMU for emulating RPi3)
* https://github.com/dwelch67/raspberrypi (more resources)
* https://jsandler18.github.io/ (even more resources)
