#include <std.h>
#include <dbg.h>

#define ATAG_ID_NONE        ((uint32_t) 0x00000000)
#define ATAG_ID_CORE        ((uint32_t) 0x54410001)
#define ATAG_ID_MEM         ((uint32_t) 0x54410002)
#define ATAG_ID_VIDEOTEXT   ((uint32_t) 0x54410003)
#define ATAG_ID_RAMDISK     ((uint32_t) 0x54410004)
#define ATAG_ID_INITRD2     ((uint32_t) 0x54420005)
#define ATAG_ID_SERIAL      ((uint32_t) 0x54410006)
#define ATAG_ID_REVISION    ((uint32_t) 0x54410007)
#define ATAG_ID_VIDEOLFB    ((uint32_t) 0x54410008)
#define ATAG_ID_CMDLINE     ((uint32_t) 0x54410009)

typedef struct {
    uint32_t size;
    uint32_t id;
} __attribute__((packed)) atag_header_t;

typedef struct {
    /* No fields. */
} __attribute__((packed)) atag_none_t;

typedef struct {
    uint32_t flags;
    uint32_t pagesize;
    uint32_t rootdev;
} __attribute__((packed)) atag_core_t;

typedef struct {
    uint32_t size;
    uint32_t start;
} __attribute__((packed)) atag_mem_t;

typedef struct {
    uint8_t x;
    uint8_t y;
    uint16_t video_page;
    uint8_t video_mode;
    uint8_t video_cols;
    uint16_t video_ega_bx;
    uint8_t video_lines;
    uint8_t video_isvga;
    uint16_t video_points;
} __attribute__((packed)) atag_videotext_t;

typedef struct {
    uint32_t flags;
    uint32_t size;
    uint32_t start;
} __attribute__((packed)) atag_ramdisk_t;

typedef struct {
    uint32_t start;
    uint32_t size;
} __attribute__((packed)) atag_initrd2_t;

typedef struct {
    uint32_t low;
    uint32_t high;
} __attribute__((packed)) atag_serialnr_t;

typedef struct {
    uint32_t revision;
} __attribute__((packed)) atag_revision_t;

typedef struct {
    uint16_t lfb_width;
    uint16_t lfb_height;
    uint16_t lfb_depth;
    uint16_t lfb_linelength;
    uint32_t lfb_base;
    uint32_t lfb_size;
    uint8_t red_size;
    uint8_t red_pos;
    uint8_t green_size;
    uint8_t green_pos;
    uint8_t blue_size;
    uint8_t blue_pos;
    uint8_t rsvd_size;
    uint8_t rsvd_pos;
} __attribute__((packed)) atag_videolfb_t;

typedef struct {
    /* No fields; it's a variable-length buffer. */
} __attribute__((packed)) atag_cmdline_t;

typedef struct {
    atag_header_t header;
    union {
        atag_core_t core;
        atag_mem_t mem;
        atag_videotext_t videotext;
        atag_ramdisk_t ramdisk;
        atag_initrd2_t initrd2;
        atag_serialnr_t serialnr;
        atag_revision_t revision;
        atag_videolfb_t videolfb;
        atag_cmdline_t cmdline;
    } v;
} __attribute__((packed)) atag_t;

ptr_t atags_next(atag_t *tag)
{
    return (atag_t *) ((uint32_t *) tag + tag->header.size);
}

void parse_atags(ptr_t atags)
{
    atag_t *tag = atags;
    while (tag->header.id != ATAG_ID_NONE) {
        printf("[atag:%u32] ", tag->header.size);
        switch (tag->header.id) {
            case ATAG_ID_CORE: {
                atag_core_t *core = &tag->v.core;
                printf("CORE: flags=0b%u32{2}, pagesize=0x%u32{16}, rootdev=0x%u32{16}\n",
                        core->flags, core->pagesize, core->rootdev);
                break;
            }
            case ATAG_ID_MEM: {
                atag_mem_t *mem = &tag->v.mem;
                printf("MEM: size=0x%u32{16}, start=0x%u32{16}\n",
                        mem->size, mem->start);
                break;
            }
            case ATAG_ID_VIDEOTEXT: {
                atag_videotext_t *vt = &tag->v.videotext;
                printf("VIDEOTEXT: x=%u8, y=%u8, video [page=%u16, mode=%u8, cols=%u8, ega_bx=%u16, lines=%u8, isvga=%u8, points=%u16]\n",
                        vt->x, vt->y, vt->video_page, vt->video_mode, vt->video_cols, vt->video_ega_bx, vt->video_lines, vt->video_isvga, vt->video_points);
                break;
            }
            case ATAG_ID_RAMDISK: {
                atag_ramdisk_t *rd = &tag->v.ramdisk;
                printf("RAMDISK: flags=0b%u32{2}, size=0x%u32{16}, start=0x%u32{16}\n",
                        rd->flags, rd->size, rd->start);
                break;
            }
            case ATAG_ID_INITRD2: {
                atag_initrd2_t *rd2 = &tag->v.initrd2;
                printf("INITRD2: size=0x%u32{16}, start=0x%u32{16}\n",
                        rd2->size, rd2->start);
                break;
            }
            case ATAG_ID_SERIAL: {
                atag_serialnr_t *serial = &tag->v.serialnr;
                printf("SERIALNR: 0x%u32{16} %u32{16}\n",
                        serial->high, serial->low);
                break;
            }
            case ATAG_ID_REVISION: {
                atag_revision_t *rev = &tag->v.revision;
                printf("REVISION: 0x%u32\n",
                        rev->revision);
            }
            case ATAG_ID_VIDEOLFB: {
                //atag_videolfb_t *vid = &tag->v.vid;
                printf("VIDEOLFB: ...\n");
                break;
            }
            case ATAG_ID_CMDLINE: {
                /* Null-terminated! */
                char *cmdline = (char *) &tag->v.cmdline;
                printf("CMDLINE: %qs\n", cmdline);
                break;
            }
            default:
                printf("(unknown atag id 0x%u32)\n", tag->header.id);
                break;
        }
        tag = atags_next(tag);
    }
}
