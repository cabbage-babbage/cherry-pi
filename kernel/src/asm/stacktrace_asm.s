.global print_stacktrace
print_stacktrace:
    mov x1, fp
    mov x2, lr
    b _print_stacktrace
