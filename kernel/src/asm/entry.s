.global _start
_start:
    // read cpu id, stop slave cores
    mrs     x0, mpidr_el1
    and     x0, x0, #3
    cbz     x0, 2f
    // cpu id > 0, stop
1:  wfe
    b       1b
2:  // cpu id == 0
    mov sp, KERNEL_INITIAL_SP
    mov x0, #0x3C0 // all DAIF flags: mask all interrupts
    bl switch_from_el2_to_el1
    bl initialize_basic_virtual_memory
    mov x0, #-(1 << KERNEL_VA_BITS)
    add sp, sp, x0
    mov fp, #0
    adr lr, #8
    b kernel_main
    // We should never return!

// Argument x0 contains the desired DAIF flags for our EL1 processor state,
// in bits 9-6; all other bits should be 0.
switch_from_el2_to_el1:
    // Set up the hypervisor control register (HCR_EL2) to enable AArch64 execution
    // in EL1 (bit 31 on).
    mov x1, #(1 << 31)
    msr HCR_EL2, x1
    // Load the address we will exception-return to.
    adr x1, 1f
    msr ELR_EL2, x1
    // 0x4 specifies EL1 (0b01, shifted left by 2)
    orr x0, x0, #0x4
    msr SPSR_EL2, x0
    // Store the current SP (i.e. SP_EL2h == SP_EL2) so that after exception-returning
    // to EL1, we can copy the SP to SP_EL1.
    mov x0, sp
    // Since we're in EL2, eret will jump to ELR_EL2,
    // set the program state to SPSR_EL2,
    // and our stack pointer will be SP_EL1t == SP_EL0
    eret
1:
    mov x1, #1
    msr SPSel, x1 // Now SP == SP_EL1h == SP_EL1 (instead of SP_EL1t == SP_EL0).
    mov sp, x0
    ret
