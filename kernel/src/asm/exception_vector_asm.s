.global exception_vector
.global exception_vector_entry_0
.global exception_vector_entry_1
.global exception_vector_entry_2
.global exception_vector_entry_3
.global exception_vector_entry_4
.global exception_vector_entry_5
.global exception_vector_entry_6
.global exception_vector_entry_7
.global exception_vector_entry_8
.global exception_vector_entry_9
.global exception_vector_entry_10
.global exception_vector_entry_11
.global exception_vector_entry_12
.global exception_vector_entry_13
.global exception_vector_entry_14
.global exception_vector_entry_15

// Each (non-IRQ) entry contains code to:
// 1) save all non-callee-saved registers
//    1a) optionally set x0 to point to the saved registers on the stack
// 2) call into a handler (written in C)
// 3) restore all non-callee saved registers
//    3a) optionally do _not_ restore x0, instead setting x0 to the return value
//        of the C handler function
// 5) return from the exception
//
// Note that x19-x29 and SP are callee-saved;
// we must save all other registers, i.e. x0-x18 and x30.
//
// IRQ entries are different; they save all the general-purpose registers (x0-x31),
// and restore them all on exception return.
//
// Each entry must remain within 32 instructions (32 instrs * 4 bytes/instr = 0x80 bytes).
//
// TODO: update this comment... now lower_el_aarch64_sync acts more like an IRQ handler.
// lower_el_aarch64_sync does perform 1a) and 3a), so its C handler function has
//    access to the saved registers and the exception entry 'returns' the value
//    from the handler function.

// Perform 1) above.
.macro save_registers
    stp x1, x0, [sp, #-16]!
    stp x3, x2, [sp, #-16]!
    stp x5, x4, [sp, #-16]!
    stp x7, x6, [sp, #-16]!
    stp x9, x8, [sp, #-16]!
    stp x11, x10, [sp, #-16]!
    stp x13, x12, [sp, #-16]!
    stp x15, x14, [sp, #-16]!
    stp x17, x16, [sp, #-16]!
    stp x30, x18, [sp, #-16]!
.endm

// Perform 1) and 1a) above.
.macro save_registers_ptr
    stp x1, x0, [sp, #-16]!
    add x0, sp, #8 // x0 now points to where we saved x0 on the stack.
    stp x3, x2, [sp, #-16]!
    stp x5, x4, [sp, #-16]!
    stp x7, x6, [sp, #-16]!
    stp x9, x8, [sp, #-16]!
    stp x11, x10, [sp, #-16]!
    stp x13, x12, [sp, #-16]!
    stp x15, x14, [sp, #-16]!
    stp x17, x16, [sp, #-16]!
    stp x30, x18, [sp, #-16]!
.endm

// Perform 3) above.
.macro restore_registers
    ldp x30, x18, [sp], #16
    ldp x17, x16, [sp], #16
    ldp x15, x14, [sp], #16
    ldp x13, x12, [sp], #16
    ldp x11, x10, [sp], #16
    ldp x9, x8, [sp], #16
    ldp x7, x6, [sp], #16
    ldp x5, x4, [sp], #16
    ldp x3, x2, [sp], #16
    ldp x1, x0, [sp], #16
.endm

// Perform 3) and 3a) above.
.macro restore_registers_ret
    ldp x30, x18, [sp], #16
    ldp x17, x16, [sp], #16
    ldp x15, x14, [sp], #16
    ldp x13, x12, [sp], #16
    ldp x11, x10, [sp], #16
    ldp x9, x8, [sp], #16
    ldp x7, x6, [sp], #16
    ldp x5, x4, [sp], #16
    ldp x3, x2, [sp], #16
    // instead of restoring x0, use the value that the handler gives us;
    // that is, just load x1 instead of also x0.
    ldr x1, [sp], #16
.endm

// TODO: these two macros store the registers in a different order, in
// effect pushing x31 ... x0, so in memory they appear in order x0 ... x31.
// Change the other macros to the same ordering (it makes more sense).

// This switches to the kernel stack and pushes x0 ... x30 and SP_EL0 to the stack
// (in that order in memory); the final SP points at x0 in that memory block.
.macro save_all_registers_from_el0
    // Since we're going from EL0 to EL1, our current SP (== SP_EL1) is _not_ same
    // SP that the current process was running; it was using SP_EL0. It's thus easy
    // to obtain the process's stack pointer and push it to our stack.

    // Unfortunately AAarch64 makes it (quite literally) impossible to set our SP
    // to the value we need (KERNEL_REENTRY_SP) without modifying at least one
    // general-purpose register. Since x18 is defined to be the "platform register",
    // we can use it as a temporary for loading the value we want into our SP.
    ldr x18, =KERNEL_REENTRY_SP
    mov sp, x18
    mov x18, #0 // Just for safety, always make it 0.
    stp x28, x29, [sp, #-32]!
    // Use x28 as an intermediate for SP_EL0 after we push it to the stack.
    mrs x28, SP_EL0
    stp x30, x28 /* SP_EL0 */, [sp, #16]
    stp x26, x27, [sp, #-16]!
    stp x24, x25, [sp, #-16]!
    stp x22, x23, [sp, #-16]!
    stp x20, x21, [sp, #-16]!
    stp x18, x19, [sp, #-16]!
    stp x16, x17, [sp, #-16]!
    stp x14, x15, [sp, #-16]!
    stp x12, x13, [sp, #-16]!
    stp x10, x11, [sp, #-16]!
    stp  x8,  x9, [sp, #-16]!
    stp  x6,  x7, [sp, #-16]!
    stp  x4,  x5, [sp, #-16]!
    stp  x2,  x3, [sp, #-16]!
    stp  x0,  x1, [sp, #-16]!
.endm

// This switches to the kernel stack and pushes x0 ... x30 and SP (== SP_EL1)
// to the stack (in that order in memory); the final SP points at x0 in that
// memory block.
.macro save_all_registers_from_el1
    // Since we're going from EL1 to EL1, our current SP (== SP_EL1) is the same
    // SP that the current process was running. To resolve this, we can temporarily
    // switch our SP to be SP_EL0 by resetting the SPSel system register.

    // Like above, we have to use x18 as a temporary to load the KERNEL_REENTRY_SP
    // into (any) SP.
    msr SPSel, #0
    ldr x18, =KERNEL_REENTRY_SP
    mov sp, x18
    mov x18, #0 // Just for safety, always make it 0.
    stp x28, x29, [sp, #-32]
    // Now we can copy the SP to x28 as a temporary...
    msr SPSel, #1
    mov x28, sp // set x28 to SP_ELx == SP_EL1
    msr SPSel, #0
    // and save x30 and SP_EL1 (== x28)
    stp x30, x28, [sp, #-16]!
    add sp, sp, #-16
    stp x26, x27, [sp, #-16]!
    stp x24, x25, [sp, #-16]!
    stp x22, x23, [sp, #-16]!
    stp x20, x21, [sp, #-16]!
    stp x18, x19, [sp, #-16]!
    stp x16, x17, [sp, #-16]!
    stp x14, x15, [sp, #-16]!
    stp x12, x13, [sp, #-16]!
    stp x10, x11, [sp, #-16]!
    stp  x8,  x9, [sp, #-16]!
    stp  x6,  x7, [sp, #-16]!
    stp  x4,  x5, [sp, #-16]!
    stp  x2,  x3, [sp, #-16]!
    stp  x0,  x1, [sp, #-16]!
    // Finally, set SP_EL1 to point to the top of the kernel stack,
    // since so far it's been SP_EL0.
    mov x0, sp
    msr SPSel, #1
    mov sp, x0
.endm

// return_to_process_el0(pointer to all regs):
// assigns x0-x30, and SP_EL0, to the values pointed to by
// the argument; then performs an exception return.
// (this makes no guarantee about the value of SP_EL1 at the
// moment of exception return)
.global return_to_process_el0
return_to_process_el0:
    ldp x2, x3, [x0, #8 * 2]
    ldp x4, x5, [x0, #8 * 4]
    ldp x6, x7, [x0, #8 * 6]
    ldp x8, x9, [x0, #8 * 8]
    ldp x10, x11, [x0, #8 * 10]
    ldp x12, x13, [x0, #8 * 12]
    ldp x14, x15, [x0, #8 * 14]
    ldp x16, x17, [x0, #8 * 16]
    ldp x18, x19, [x0, #8 * 18]
    ldp x20, x21, [x0, #8 * 20]
    ldp x22, x23, [x0, #8 * 22]
    ldp x24, x25, [x0, #8 * 24]
    ldp x26, x27, [x0, #8 * 26]
    ldp x28, x29, [x0, #8 * 28]
    ldr x30, [x0, #8 * 30]
    // We can use x1 as a temporary for SP_EL0.
    ldr x1, [x0, #8 * 31]
    msr SP_EL0, x1
    // And finally, we can load x0 and x1.
    ldp x0, x1, [x0]
    eret

// return_to_process_el1(pointer to all regs):
// assigns x0-x30, and SP (== SP_EL1) to the values pointed to by
// the argument; then performs an exception return
// (this makes no guarantee about the value of SP_EL0 at the
// moment of exception return)
.global return_to_process_el1
return_to_process_el1:
    ldr x30, [x0, #8 * 30] // x30 = regs[30]
    ldr x1, [x0, #8 * 31] // x1 = regs[31]
    mov sp, x1
    add x0, x0, #16
    // Now let's use x0 as a stack pointer.
    ldp  x2,  x3, [x0], #16
    ldp  x4,  x5, [x0], #16
    ldp  x6,  x7, [x0], #16
    ldp  x8,  x9, [x0], #16
    ldp x10, x11, [x0], #16
    ldp x12, x13, [x0], #16
    ldp x14, x15, [x0], #16
    ldp x16, x17, [x0], #16
    ldp x18, x19, [x0], #16
    ldp x20, x21, [x0], #16
    ldp x22, x23, [x0], #16
    ldp x24, x25, [x0], #16
    ldp x26, x27, [x0], #16
    ldp x28, x29, [x0], #16
    ldp  x0,  x1, [x0, #-240]
    eret

.macro fixup_stackframe
    mrs lr, ELR_EL1
    add lr, lr, #4
    stp fp, lr, [sp, #-16]!
    mov fp, sp
.endm

.balign 0x800
exception_vector:
    exception_vector_entry_0:
        save_registers
        fixup_stackframe
        bl curr_el_sp0_sync_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_1:
        save_registers
        fixup_stackframe
        bl curr_el_sp0_irq_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_2:
        save_registers
        fixup_stackframe
        bl curr_el_sp0_fiq_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_3:
        save_registers
        fixup_stackframe
        bl curr_el_sp0_serror_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_4:
        b exception_vector_entry_4_body
    .balign 0x80
    exception_vector_entry_5:
        b exception_vector_entry_5_body
    .balign 0x80
    exception_vector_entry_6:
        save_registers
        fixup_stackframe
        bl curr_el_spx_fiq_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_7:
        save_registers
        fixup_stackframe
        bl curr_el_spx_serror_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_8:
        save_all_registers_from_el0
        mov x0, sp // x0 now points to the saved regs
        fixup_stackframe
        // The handler must finish by jumping to either return_to_process_el0
        // or return_to_process_el1. We 'bl' instead of 'b' just to make sure
        // the stack frames look right.
        bl lower_el_aarch64_sync_handler
    .balign 0x80
    exception_vector_entry_9:
        save_all_registers_from_el0
        mov x0, sp // x0 now points to the saved regs
        fixup_stackframe
        // The handler must finish by jumping to either return_to_process_el0
        // or return_to_process_el1. We 'bl' instead of 'b' just to make sure
        // the stack frames look right.
        bl lower_el_aarch64_irq_handler
    .balign 0x80
    exception_vector_entry_10:
        save_registers
        fixup_stackframe
        bl lower_el_aarch64_fiq_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_11:
        save_registers
        fixup_stackframe
        bl lower_el_aarch64_serror_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_12:
        save_registers
        fixup_stackframe
        bl lower_el_aarch32_sync_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_13:
        save_registers
        fixup_stackframe
        bl lower_el_aarch32_irq_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_14:
        save_registers
        fixup_stackframe
        bl lower_el_aarch32_fiq_handler
        restore_registers
        eret
    .balign 0x80
    exception_vector_entry_15:
        save_registers
        fixup_stackframe
        bl lower_el_aarch32_serror_handler
        restore_registers
        eret

exception_vector_entry_4_body:
    save_all_registers_from_el1
    mov x0, sp // x0 now points to the saved regs
    fixup_stackframe
    // The handler must finish by jumping to either return_to_process_el0
    // or return_to_process_el1. We 'bl' instead of 'b' just to make sure
    // the stack frames look right.
    bl curr_el_spx_sync_handler

exception_vector_entry_5_body:
    save_all_registers_from_el1
    mov x0, sp // x0 now points to the saved regs
    fixup_stackframe
    // The handler must finish by jumping to either return_to_process_el0
    // or return_to_process_el1. We 'bl' instead of 'b' just to make sure
    // the stack frames look right.
    bl curr_el_spx_irq_handler
