.global switch_from_el1_to_el0
// Argument x0 contains the desired DAIF flags for our EL1 processor state,
// in bits 9-6; all other bits should be 0.
switch_from_el1_to_el0:
    adr x1, 1f
    msr ELR_EL1, x1
    // Since EL0 is simply 0b00 << 2 (== 0b0000), we don't need to change x0
    // at all before pushing it to SPRS_EL1.
    msr SPSR_EL1, x0
    // Store the current SP (i.e. SP_EL1h == SP_EL1) so that after exception-returning
    // to EL0, we can push the SP to SP_EL0.
    mov x0, sp
    // Since we're in EL1, eret will jump to ELR_EL1,
    // set the program state to SPSR_EL1,
    // and our stack pointer will be SP_EL0t == SP_EL0
    eret
1:
    // Now in EL0, the SP is SP_EL0.
    mov sp, x0
    ret
