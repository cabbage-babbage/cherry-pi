#include <irq.h>
#include <console.h>
#include <sysreg.h>
#include <syscall.h>
#include <proc.h>
#include <timer.h>
#include <device/uart.h>
#include <device/usb.h>
#include <syscontext.h>
#include <dbg.h>

#define DEFINE_HANDLER(entry_name) \
    void entry_name##_handler() \
    { \
        uint64_t esr; /* Exception syndrome register */ \
        load_sysreg(esr, ESR_EL1); \
        printf("got to " #entry_name "_handler: ESR=0x%u64{16} (EC=0b%u64{2}, IL=0b%u64{2}, ISS=0x%u64{16})\n", \
                esr, (esr >> 26) & 0x1F, (esr >> 25) & 0x1, esr & 0x1FFFFFF); \
        panic("exception handler not implemented\n"); \
    }

/*
 * Note that a saved_regs_base value will always point to the IRQ-saved registers on
 * the stack, where the stack looks like:
 *    x30 x18 x17 ... x1 x0
 *                        ^ saved_regs_base
 */

/*
 * The exception handler for a synchronous exception from a lower EL (AArch64), or from the
 * current EL using the current SP.
 */
void sync_handler(ptr_t irq_saved_regs_base, bool from_el1)
{
    uint64_t esr; /* Exception syndrome register */
    load_sysreg(esr, ESR_EL1);

    /*
     * exc_class specifies the type of synchronous exception that occured.
     * instr_len is a bit specifying if we trapped on a 16-bit or 32-bit instruction.
     * iss (instruction-specific syndrome) is a value specific to the exception class and instruction
     *      that caused the trap.
     */
    uint32_t exc_class = (uint32_t) ((esr >> 26) & 0x3F);
    uint32_t instr_len = (uint32_t) ((esr >> 25) & 0x1);
    uint32_t iss = (uint32_t) (esr & 0x1FFFFFF);

    /* Sanity check. */
    panic_if(instr_len != 1, "expected to trap on a 32-bit instruction\n");

    uint64_t elr, far;
    load_sysreg(elr, ELR_EL1);
    load_sysreg(far, FAR_EL1);
    proc_t *proc = current_process();
#define report_syndrome(msg) \
    panic("synchronous exception: " msg " ('%s', PID %u32: EC = 0b%u32{2}, ISS = 0x%u32{16}, ELR = 0x%p, FAR = 0x%p)\n", \
            proc ? proc->name : "(none)", proc ? proc->id : 0, exc_class, iss, elr, far)

    switch (exc_class) {
        case 0x00 /* 0b000000 */:
            report_syndrome("unknown reason");
            break;
        case 0x01 /* 0b000001 */:
            report_syndrome("trapped wfi/wfe");
            break;
        case 0x03 /* 0b000011 */:
            report_syndrome("trapped mcr/mrc access (coproc==0b1111)");
            break;
        case 0x04 /* 0b000100 */:
            report_syndrome("trapped mcrr/mrrc access (coproc==0b1111)");
            break;
        case 0x05 /* 0b000101 */:
            report_syndrome("trapped mcr/mrc access (coproc==0b1110)");
            break;
        case 0x06 /* 0b000110 */:
            report_syndrome("trapped ldc/stc access");
            break;
        case 0x07 /* 0b000111 */:
            report_syndrome("trapped SVE, advanced SIMD, or FP");
            break;
        case 0x0C /* 0b001100 */:
            report_syndrome("trapped mrrc access (coproc==0b1110)");
            break;
        case 0x0D /* 0b001101 */:
            report_syndrome("branch target exception");
            break;
        case 0x0E /* 0b001110 */:
            report_syndrome("illegal execution state");
            break;
        case 0x11 /* 0b010001 */:
            report_syndrome("svc from aarch32");
            break;
        case 0x15 /* 0b010101 */:
            /* It's a syscall! We don't use the iss for the syscall number,
             * but simply require it to be 0x1. Put the return value in x0. */
            if (iss == 1) {
                handle_process_syscall(irq_saved_regs_base);
            } else {
                panic("invalid iss %u64 for syscall (process '%qs'); should be 0x1\n",
                        iss, current_process()->name);
            }
            break;
        case 0x18 /* 0b011000 */:
            report_syndrome("trapped msr/mrs from aarch64");
            break;
        case 0x19 /* 0b011001 */:
            report_syndrome("trapped SVE functionality");
            break;
        case 0x1C /* 0b011100 */:
            report_syndrome("pointer authentication failure");
            break;
        case 0x20 /* 0b100000 */:
            report_syndrome("instruction MMU abort from lower EL");
            break;
        case 0x21 /* 0b100001 */:
            report_syndrome("instruction MMU abort from current EL");
            break;
        case 0x22 /* 0b100010 */:
            report_syndrome("PC alignment fault");
            break;
        case 0x24 /* 0b100100 */:
            report_syndrome("data MMU abort from lower EL");
            break;
        case 0x25 /* 0b100101 */:
            report_syndrome("data MMU abort from current EL");
            break;
        case 0x26 /* 0b100110 */:
            report_syndrome("SP alignment fault");
            break;
        case 0x28 /* 0b101000 */:
            report_syndrome("trapped FP exception from aarch32");
            break;
        case 0x2C /* 0b101100 */:
            report_syndrome("trapped FP exception from aarch64");
            break;
        case 0x2F /* 0b101111 */:
            report_syndrome("SError interrupt");
            break;
        case 0x30 /* 0b110000 */:
            report_syndrome("breakpoint exception from lower EL");
            break;
        case 0x31 /* 0b110001 */:
            report_syndrome("breakpoint exception from current EL");
            break;
        case 0x32 /* 0b110010 */:
            report_syndrome("software step exception from lower EL");
            break;
        case 0x33 /* 0b110011 */:
            report_syndrome("software step exception from current EL");
            break;
        case 0x34 /* 0b110100 */:
            report_syndrome("watchpoint exception from lower EL");
            break;
        case 0x35 /* 0b110101 */:
            report_syndrome("watchpoint exception from current EL");
            break;
        case 0x38 /* 0b111000 */:
            report_syndrome("bkpt executed in aarch32");
            break;
        case 0x3C /* 0b111100 */:
            report_syndrome("brk executed in aarch64");
            break;
        default:
            panic("unexpected synchronous exception class 0b%u32{2} (process '%qs')\n",
                    exc_class, current_process()->name);
    }

    return_to_process(current_process(), irq_saved_regs_base);
}

void irq_handler(ptr_t irq_saved_regs_base, bool from_el1)
{
    // TODO: turn this into a configurable list

    panic_if_not(in_context_preemptible(),
            "IRQ must be delivered during a preemptible context\n");
    syscontext_t old_context = current_syscontext();
    set_syscontext(SYSCONTEXT_IRQ);

    if (timer_irq_firing()) {
        handle_timer_irq(irq_saved_regs_base, from_el1);
    }
    if (uart_irq_firing()) {
        uart_handle_irq(irq_saved_regs_base);
    }
    if (usb_irq_firing()) {
        usb_handle_irq();
    }

    set_syscontext(old_context);
    return_to_process(current_process(), irq_saved_regs_base);
}

/* The exception handler for a synchronous exception from the current EL using SP0. */
DEFINE_HANDLER(curr_el_sp0_sync)
/* The exception handler for an IRQ exception from the current EL using SP0. */
DEFINE_HANDLER(curr_el_sp0_irq)
/* The exception handler for an FIQ exception from the current EL using SP0. */
DEFINE_HANDLER(curr_el_sp0_fiq)
/* The exception handler for the system error exception from the current EL using SP0. */
DEFINE_HANDLER(curr_el_sp0_serror)

/* The exception handler for a synchronous exception from the current EL using the current SP. */
void curr_el_spx_sync_handler(ptr_t irq_saved_regs_base)
{
    sync_handler(irq_saved_regs_base, true /* from EL1 */);
}

/* The exception handler for an IRQ exception from the current EL using the current SP. */
void curr_el_spx_irq_handler(ptr_t irq_saved_regs_base)
{
    irq_handler(irq_saved_regs_base, true /* from El1 */);
}

/* The exception handler for an FIQ from the current EL using the current SP. */
DEFINE_HANDLER(curr_el_spx_fiq)
/* The exception handler for a System Error exception from the current EL using the current SP. */
DEFINE_HANDLER(curr_el_spx_serror)

/*
 * The exception handler for a synchronous exception from a lower EL (AArch64).
 * @param saved_regs_base   Points to the saved x0 on the stack, where the stack looks like:
 *                          x30 x18 x17 ... x1 x0
 *                                              ^ saved_regs_base
 * @return  The value that will end up in x0 as we return from the exception.
 */
void lower_el_aarch64_sync_handler(ptr_t irq_saved_regs_base)
{
    sync_handler(irq_saved_regs_base, false /* from EL0 */);
}

/* The exception handler for an IRQ from a lower EL (AArch64). */
void lower_el_aarch64_irq_handler(ptr_t irq_saved_regs_base)
{
    //printf_nl("got to lower_el_aarch64_irq_handler()\n");
    //uint64_t timer_control;
    //load_sysreg(timer_control, CNTP_CTL_EL0);
    //printf("timer status: %u64{2}\n", timer_control);
    //printf("core0 irq source: %u32{2}\n",
    //        *(uint32_t*)0x40000060);
    irq_handler(irq_saved_regs_base, false /* from EL0 */);
}

/* The exception handler for an FIQ from a lower EL (AArch64). */
DEFINE_HANDLER(lower_el_aarch64_fiq)
/* The exception handler for a System Error exception from a lower EL(AArch64). */
DEFINE_HANDLER(lower_el_aarch64_serror)
/* The exception handler for a synchronous exception from a lower EL(AArch32). */
DEFINE_HANDLER(lower_el_aarch32_sync)
/* The exception handler for an IRQ exception from a lower EL (AArch32). */
DEFINE_HANDLER(lower_el_aarch32_irq)
/* The exception handler for an FIQ exception from a lower EL (AArch32). */
DEFINE_HANDLER(lower_el_aarch32_fiq)
/* The exception handler for a System Error exception from a lower EL(AArch32). */
DEFINE_HANDLER(lower_el_aarch32_serror)

INLINE bool interrupts_enabled()
{
    uint64_t daif;
    load_sysreg(daif, DAIF);
    return (daif & DAIF_I) == 0;
}

INLINE void set_interrupts_state(bool enabled)
{
    uint64_t daif;
    load_sysreg(daif, DAIF);
    if (enabled) {
        daif &= ~DAIF_I;
    } else {
        daif |= DAIF_I;
    }
    set_sysreg(DAIF, daif);
}

INLINE void enable_interrupts()
{
    uint64_t daif;
    load_sysreg(daif, DAIF);
    daif &= ~DAIF_I;
    set_sysreg(DAIF, daif);
}

INLINE void disable_interrupts()
{
    uint64_t daif;
    load_sysreg(daif, DAIF);
    daif |= DAIF_I;
    set_sysreg(DAIF, daif);
}

INLINE irq_state_t push_interrupts_state(bool enabled)
{
    bool old_enabled = interrupts_enabled();
    set_interrupts_state(enabled);
    return (irq_state_t) old_enabled;
}

INLINE irq_state_t push_interrupts_disabled()
{
    return push_interrupts_state(false);
}

INLINE void restore_interrupts_state(irq_state_t state)
{
    set_interrupts_state((bool) state);
}
