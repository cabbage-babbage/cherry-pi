#include <stacktrace.h>
#include <std.h>
#include <dbg.h>
#include <virtual_memory.h>
#include <proc.h>
#include <sysreg.h>
#include <syscontext.h>

// TODO: probably this functionality should go in virtual_memory.c/.h!
bool readable(uint64_t *virt_addr)
{
    asm volatile("at S1E1R, %[tmp]" : : [tmp] "r" (virt_addr));
    uint64_t par;
    load_sysreg(par, PAR_EL1);

    // PAR_EL1[bit 0] is the fail bit
    return (par & 1) == 0;
}

void print_instr_addr(uint64_t addr)
{
    if (addr == 0) {
        DEBUG_PRINTF("(noreturn) ");
        return;
    }
    /* Generally the link register points to the instruction we return to,
     * which is just after the 'bl' instruction that got us here. Print the
     * address of the call instruction instead. */
    DEBUG_PRINTF("%p ", addr - 4);
}

void _print_stacktrace(const char *debug_level, uint64_t fp, uint64_t lr)
{
    uint64_t *frame = (uint64_t *) fp;

    DEBUG_PRINTF_NL("[%s] stacktrace:\n", debug_level);
    proc_t *proc = current_process();
    if (proc) {
        DEBUG_PRINTF_NL("[%s] (note: current process is '%qs', context=%s, syscontext=%s)\n", debug_level,
                proc->name,
                (proc->context == CONTEXT_PROCESS ? "process" :
                proc->context == CONTEXT_SYSCALL ? "syscall" :
                proc->context == CONTEXT_EXITED ? "exited!" :
                "???"),
                (current_syscontext() == SYSCONTEXT_BOOT ? "boot" :
                current_syscontext() == SYSCONTEXT_PREEMPTIBLE ? "preemptible" :
                current_syscontext() == SYSCONTEXT_IRQ ? "irq" :
                "???"));
    }
    print_instr_addr(lr);
    while (frame) {
        /* Make sure we can actually read the frame, before trying to read it. */
        if (!readable(frame) || !readable(frame + 1)) {
            DEBUG_PRINTF("\n");
            DEBUG_PRINTF("[%s] could not read frame at 0x%p\n", debug_level, frame);
            return;
        }

        uint64_t *next_frame = (uint64_t *) frame[0];
        uint64_t return_addr = frame[1];
        print_instr_addr(return_addr);
        frame = next_frame;
    }
    DEBUG_PRINTF("\n");
}
