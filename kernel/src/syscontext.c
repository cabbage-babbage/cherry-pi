#include <syscontext.h>

syscontext_t syscontext;

syscontext_t current_syscontext()
{
    return syscontext;
}

void set_syscontext(syscontext_t context)
{
    syscontext = context;
}

bool in_context_boot()
{
    return syscontext == SYSCONTEXT_BOOT;
}

bool in_context_preemptible()
{
    return syscontext == SYSCONTEXT_PREEMPTIBLE;
}

bool in_context_irq()
{
    return syscontext == SYSCONTEXT_IRQ;
}
