#include <virtual_memory.h>
#include <std.h>
#include <sysreg.h>
#include <string.h>
#include <heap.h>
#include <stdops.h>
#include <mmio.h>
#include <dbg.h>

bool debug;

extern_symbol _kernel_data_start;

__attribute__((aligned(PAGE_SIZE))) pte_t ttbr0[512];
__attribute__((aligned(PAGE_SIZE))) pte_t ttbr1[512];

void initialize_virtual_memory()
{
    debug = false;

    /* Since initialize_basic_virtual_memory() already set up the TCR and SCTLR
     * for us, we just need to set up finer-grained page tables and load them
     * into TTBR0, TTBR1.
     *
     * Our virtual memory layout will be as follows:
     * - [0x0000_0000_0000_0000, 0x0000_FFFF_FFFF_FFFF):
     *   unmapped (kernel should only be using upper VA range)
     * - [0xFFFF_0000_0000_0000, virtual kernel data start):
     *   1:1 to physical memory, normal memory, R/X, privileged access only
     * - [virtual kernel data start, 0xFFFF_0000_0000_0000 + RAM size):
     *   1:1 to physical memory, normal memory, R/W, privileged access only
     *   (note RAM size is specified by the KERNEL_MEMORY_MB kernel parameter)
     * - for each range of (virtual) MMIO addresses that the kernel has to
     *   access (e.g. mini UART, timer):
     *   [virtual MMIO start, virtual MMIO end):
     *   1:1 to physical (mmio) memory, device memory, R/W, privileged access only
     *   (note we round down the start, and round up the end, to the nearest page boundary;
     *   also, we don't actually set it all up here, but instead each subsystem that uses
     *   MMIO must 'request access' by mapping the virtual MMIO range to the physical MMIO
     *   range -- that is, all subsystems except for UART and the timer, since it's so
     *   essential for the kernel to function that we should just set it up here)
     * - all other 0xFFFF_****_****_**** addresses are unmapped */

    memset(ttbr0, PAGE_SIZE, 0);
    memset(ttbr1, PAGE_SIZE, 0);

#define IGNORED 0

    /* Unmap all of TTBR0. */
    printf("  unmapping low virtual address range\n");
    int32_t res = map_virt_to_phys(ttbr0, ttbr1,
            0, VA_RANGE,
            UNMAPPED, IGNORED, IGNORED,
            MAP_CREATE);
    panic_if(res, "could not map; result = %i32\n", res);
    //printf("\n");

    /* Map kernel code 1:1 with RAM. */
    printf("  mapping kernel code\n");
    res = map_flat_kernel_virt_to_phys(ttbr1,
            TTBR1_START, (uint64_t) &_kernel_data_start - TTBR1_START,
            PTE_XN_EL0 | PTE_AF | PTE_INN_SH | PTE_RO | PTE_KERNEL,
            PTE_MEM,
            MAP_CREATE);
    panic_if(res, "could not map; result = %i32\n", res);
    //printf("\n");

    /* Map kernel data 1:1 with RAM. */
    printf("  mapping kernel data (KERNEL_MEMORY_MB=%u32)\n",
            (uint32_t) KERNEL_MEMORY_MB);
    uint64_t virtual_ram_end = TTBR1_START + KERNEL_MEMORY_MB * (uint64_t) 0x100000;
    res = map_flat_kernel_virt_to_phys(ttbr1,
            (uint64_t) &_kernel_data_start, virtual_ram_end - (uint64_t) &_kernel_data_start,
            PTE_XN_EL0 | PTE_XN_EL1 | PTE_AF | PTE_INN_SH | PTE_RW | PTE_KERNEL,
            PTE_MEM,
            MAP_CREATE);
    panic_if(res, "could not map; result = %i32\n", res);
    //printf("\n");

    printf("  mapping GPIO MMIO\n");
    res = map_flat_kernel_virt_to_phys(ttbr1,
            (uint64_t) (MMIO_BASE + 0x00200000), PAGE_SIZE,
            PTE_XN_EL0 | PTE_XN_EL1 | PTE_AF | PTE_OUT_SH | PTE_RW | PTE_KERNEL,
            PTE_MMIO,
            MAP_CREATE);
    panic_if(res, "could not map; result = %i32\n", res);

    printf("  mapping UART MMIO\n");
    res = map_flat_kernel_virt_to_phys(ttbr1,
            (uint64_t) (MMIO_BASE + 0x00215000), PAGE_SIZE,
            PTE_XN_EL0 | PTE_XN_EL1 | PTE_AF | PTE_OUT_SH | PTE_RW | PTE_KERNEL,
            PTE_MMIO,
            MAP_CREATE);
    panic_if(res, "could not map; result = %i32\n", res);

    printf("  mapping EMMC MMIO\n");
    res = map_flat_kernel_virt_to_phys(ttbr1,
            (uint64_t) (MMIO_BASE + 0x00300000), PAGE_SIZE,
            PTE_XN_EL0 | PTE_XN_EL1 | PTE_AF | PTE_OUT_SH | PTE_RW | PTE_KERNEL,
            PTE_MMIO,
            MAP_CREATE);
    panic_if(res, "could not map; result = %i32\n", res);

    printf("  mapping system timer MMIO\n");
    res = map_flat_kernel_virt_to_phys(ttbr1,
            TTBR1_START + 0x40000000, PAGE_SIZE,
            PTE_XN_EL0 | PTE_XN_EL1 | PTE_AF | PTE_OUT_SH | PTE_RW | PTE_KERNEL,
            PTE_MMIO,
            MAP_CREATE);
    panic_if(res, "could not map; result = %i32\n", res);

    printf("  mapping IRQ MMIO\n");
    res = map_flat_kernel_virt_to_phys(ttbr1,
            (uint64_t) (MMIO_BASE + 0x0000B000), PAGE_SIZE,
            PTE_XN_EL0 | PTE_XN_EL1 | PTE_AF | PTE_OUT_SH | PTE_RW | PTE_KERNEL,
            PTE_MMIO,
            MAP_CREATE);
    panic_if(res, "could not map; result = %i32\n", res);

    printf("  mapping USB MMIO\n");
    res = map_flat_kernel_virt_to_phys(ttbr1,
            (uint64_t) (MMIO_BASE + 0x00980000), PAGE_SIZE,
            PTE_XN_EL0 | PTE_XN_EL1 | PTE_AF | PTE_OUT_SH | PTE_RW | PTE_KERNEL,
            PTE_MMIO,
            MAP_CREATE);
    panic_if(res, "could not map; result = %i32\n", res);

    set_sysreg(TTBR0_EL1, kernel_virt_to_phys(ttbr0));
    set_sysreg(TTBR1_EL1, kernel_virt_to_phys(ttbr1));
    invalidate_tlb();
    asm volatile("isb; dsb ish");

    //debug = true;
}

/* Page tables at a particular level (0-3) contain PTEs of the same level.
 * The table_*** functions give alignments for the entire table at a given level,
 * while the pte_*** functions give alignments for the PTEs at a given level. */

USE_VALUE INLINE uint32_t table_alignment_bits_at_level(uint32_t level)
{
    panic_if_not(level <= 3, "level must be 0 to 3; got %u32\n", level);
    return 48 - 9 * level;
}

USE_VALUE INLINE uint64_t table_range_at_level(uint32_t level)
{
    panic_if_not(level <= 3, "level must be 0 to 3; got %u32\n", level);
    return (uint64_t) 1 << table_alignment_bits_at_level(level);
}

USE_VALUE INLINE uint32_t pte_alignment_bits_at_level(uint32_t level)
{
    panic_if_not(level <= 3, "level must be 0 to 3; got %u32\n", level);
    return 39 - 9 * level;
}

USE_VALUE INLINE uint64_t pte_range_at_level(uint32_t level)
{
    panic_if_not(level <= 3, "level must be 0 to 3; got %u32\n", level);
    return (uint64_t) 1 << pte_alignment_bits_at_level(level);
}

USE_VALUE INLINE uint32_t alignment_level(uint64_t val)
{
    uint32_t bits = alignment_bits(val);
    if (bits >= 39) {
        return 0;
    } else if (bits >= 30) {
        return 1;
    } else if (bits >= 21) {
        return 2;
    } else {
        return 3;
    }
}

USE_VALUE INLINE bool is_pte_aligned_at_level(uint64_t val, uint32_t level)
{
    return is_aligned_at_bits(val, pte_alignment_bits_at_level(level));
}

/* If the intervals don't intersect, results in an interval with
 * arbitrary start and 0 length. */
void intersect_intervals(
        /* inputs */
        uint64_t start1, uint64_t length1,
        uint64_t start2, uint64_t length2,
        /* output */
        uint64_t *start, uint64_t *length)
{
    uint128_t start1_128 = uint128_from_uint64(start1),
              length1_128 = uint128_from_uint64(length1);
    uint128_t start2_128 = uint128_from_uint64(start2),
              length2_128 = uint128_from_uint64(length2);
    /* These interval ends are exclusive. */
    uint128_t end1_128 = add_uint128(start1_128, length1_128),
              end2_128 = add_uint128(start2_128, length2_128);

    if (lte_uint128(end1_128, start2_128) ||
        lte_uint128(end2_128, start1_128)) {
        /* Intervals do not overlap. */
        *start = 0;
        *length = 0;
    } else {
        uint128_t int_start_128 = max_uint128(start1_128, start2_128);
        uint128_t int_end_128 = min_uint128(end1_128, end2_128);

        *start = uint64_from_uint128(int_start_128);
        *length = uint64_from_uint128(sub_uint128(int_end_128, int_start_128));
        //printf("intersect_intervals:\n"
        //       "      [0x%p, +0x%p)\n"
        //       "      [0x%p, +0x%p)\n"
        //       "   => [0x%p, +0x%p)\n",
        //       start1, length1, start2, length2, *start, *length);
    }
}

USE_VALUE const char *level_prefix(uint32_t level)
{
    switch (level) {
        case 0: return "";
        case 1: return "  ";
        case 2: return "    ";
        case 3: return "      ";
        case 4: return "        ";
        default: return "        >>";
    }
}

// TODO: remove the target_level parameter, and the alignment checks;
// the alignment checks at the target_level should be done before even
// getting here.
int32_t map_table_virt_to_phys(
        pte_t *page_table,
        uint64_t page_table_base,
        uint32_t page_table_level,
        uint32_t target_level,
        uint64_t virt_addr_start, uint64_t length,
        uint64_t phys_addr_start,
        uint64_t attribs, uint64_t mem_type,
        uint64_t mapping_flags)
{
    if (debug) {
        if (phys_addr_start != UNMAPPED) {
            printf("%smap: table(v=0x%p,lvl=%u32) tgtlvl(%u32) virt(0x%p, len=0x%u64{16}) phys(0x%p) flags(%s%s)\n",
                level_prefix(page_table_level),
                page_table_base, page_table_level,
                target_level,
                virt_addr_start, length,
                phys_addr_start,
                (mapping_flags & MAP_CREATE) ? "C" : "c", (mapping_flags & MAP_ALIGN) ? "A" : "a");
        } else {
            printf("%smap: table(v=0x%p,lvl=%u32) tgtlvl(%u32) virt(0x%p, len=0x%u64{16}) UNMAPPED flags(%s%s)\n",
                level_prefix(page_table_level),
                page_table_base, page_table_level,
                target_level,
                virt_addr_start, length,
                (mapping_flags & MAP_CREATE) ? "C" : "c", (mapping_flags & MAP_ALIGN) ? "A" : "a");
        }
    }

    panic_if_not(page_table,
            "page table must not be null\n");
    panic_if_not(page_table_level <= 3,
            "page tables only exist up to level 3; got %u32\n", page_table_level);
    if (phys_addr_start != UNMAPPED) {
        panic_if_not(1 <= target_level && target_level <= 3,
                "target level for mapping must be 1 to 3; got %u32\n", target_level);
    } else {
        panic_if_not(target_level <= 3,
                "target level for unmapping must be 0 to 3; got %u32\n", target_level);
    }
    panic_if_not(target_level >= page_table_level,
            "target level (%u32) must be at least the page table level (%u32)\n",
            target_level, page_table_level);
    panic_if_not(is_aligned_at_bits(page_table_base, table_alignment_bits_at_level(page_table_level)),
            "page table base virtual address (0x%p) must be aligned according to the table level (%u32)\n",
            page_table_base, page_table_level);

    uint64_t table_addr_range = table_range_at_level(page_table_level);
    bool addr_range_in_table =
        (virt_addr_start >= page_table_base) &&
        (virt_addr_start + length >= virt_addr_start) &&
        (virt_addr_start - page_table_base + length <= table_addr_range);
    panic_if_not(addr_range_in_table,
            "virtual address range (0x%p, length 0x%u64{16}) does not fit in "
            "page table address range (0x%p, length 0x%u64{16} at level %u32)\n",
            virt_addr_start, length, page_table_base, table_addr_range, page_table_level);

    if (mapping_flags & MAP_ALIGN) {
        panic("auto-alignment not implemented");
        /* Unset MAP_ALIGN, so recursive calls will not try to auto-align but
         * instead will report alignment issues back to us. Of course there
         * shouldn't be any alignment issues after we auto-align, so later we
         * should panic() if any recursive calls return MAP_RES_ALIGNMENT. */
        mapping_flags &= ~MAP_ALIGN;
    } else {
        /* Check alignment. */
        if (!is_pte_aligned_at_level(virt_addr_start, target_level) ||
            !is_pte_aligned_at_level(length, target_level)) {
            //if (!is_pte_aligned_at_level(virt_addr_start, target_level)) {
            //    printf("wrong alignment vstart=0x%p for target_level %u32\n",
            //            virt_addr_start, target_level);
            //}
            //if (!is_pte_aligned_at_level(length, target_level)) {
            //    printf("wrong alignment vlength=0x%p for target_level %u32\n",
            //            length, target_level);
            //}
            return MAP_RES_ALIGNMENT;
        }
        if (phys_addr_start != UNMAPPED &&
            !is_pte_aligned_at_level(phys_addr_start, target_level)) {
            //printf("wrong alignment pstart=0x%p for target_level %u32\n",
            //        phys_addr_start, target_level);
            return MAP_RES_ALIGNMENT;
        }
    }

    uint64_t pte_range = pte_range_at_level(page_table_level);
    /* Now we need to start overwriting PTEs in the current table.
     * There are 512 entries, each covering a pte_range number of bytes, starting at
     * the page_table_base virtual address. */
    uint64_t start_entry_index = (virt_addr_start - page_table_base) / pte_range;
    uint64_t num_entries = round_up_div(length, pte_range);

    // sanity check
    panic_if(start_entry_index > 512,
            "start_entry_index is %u64\n", start_entry_index);
    panic_if(start_entry_index + num_entries > 512,
            "start_entry_index (%u64) + num_entries (%u64) too large (%u64)\n",
            start_entry_index, num_entries, start_entry_index + num_entries);

    if (debug) {
        printf("%s+ modifying %u64 %s from index %u64\n",
            level_prefix(page_table_level),
            num_entries, num_entries == 1 ? "entry" : "entries", start_entry_index);
    }

    //uint64_t table_phys_base = align_down(phys_addr_start, table_range_at_level(page_table_level));

    for (uint64_t entry_index = start_entry_index;
            entry_index < start_entry_index + num_entries;
            entry_index++) {
        pte_t *entry = &page_table[entry_index];

        uint64_t entry_virt_base = page_table_base + entry_index * pte_range;
        uint64_t entry_range = pte_range_at_level(page_table_level);
        /* Determine how much of our virtual address range intersects with the entry's maximum range. */
        uint64_t entry_virt_start, entry_virt_length;
        intersect_intervals(
                virt_addr_start, length,
                entry_virt_base, entry_range,
                &entry_virt_start, &entry_virt_length);
        uint64_t entry_phys_start = phys_addr_start == UNMAPPED
            ? UNMAPPED
            : phys_addr_start + (entry_virt_start - virt_addr_start);

        /* The entry_target_level is the target level we will try to use for the current
         * entry. If the entry's virtual address range is entirely contained within the
         * virtual address range we're trying to map, we may be able to use a lower target
         * level and thus a larger block, thus reducing memory usage. */
        bool entry_range_in_virt_range =
            entry_virt_start == entry_virt_base && entry_virt_length == entry_range;
        uint32_t entry_target_level = entry_range_in_virt_range ? page_table_level : target_level;

        /* But we may have increase the target size if the entry_phys_start is not well-aligned.
         * For example, we cannot map an aligned 2 MiB virtual address block to a physical block
         * with a 4 KiB offset; that still requires level 3 PTEs, even though the virtual range
         * is contined in the range of a single level 2 PTE. */
        if (entry_phys_start != UNMAPPED) {
            entry_target_level = max_uint64(entry_target_level,
                    alignment_level(entry_phys_start));
        }

        /* If entry_target_level > page_table_level, we need to turn an unmapped or
         * block entry into a new table entry (we can leave a table entry as it was).
         * If we need a new table entry, try to allocate the new page table, and if
         * we converted from a block entry, we need to set the PTEs in the new table
         * to correspond to the original block entry. (no need to overwrite _all_ the
         * new PTEs, though, since we'll probably overwrite again some of them depending
         * on where our virtual address range intersects the new page table's address
         * range.)
         *
         * Otherwise our target and page table levels agree, so we should only end
         * up with an unmapped or block entry.
         * If the current entry is a table entry, we thus need to free it (recursively!)
         * and replace it with an unmapped/block entry;
         * Otherwise, just update the current unmapped or block entry. */

        if (debug) {
            printf("%s+ entry %u64: tlvl=%u32, virt base=0x%p, range=0x%p, phys_start=0x%p (overlap 0x%p, len 0x%p)\n",
                    level_prefix(page_table_level),
                    entry_index, entry_target_level, entry_virt_base, entry_range, entry_phys_start,
                    entry_virt_start, entry_virt_length);
        }

        if (entry_target_level > page_table_level) {
            /* entry_target_level is more than the current page_table_level, meaning
             * that we may need to create a new page table and then recurse. */
            if (!pte_is_table(*entry, page_table_level)) {
                /* We need to create a new table, and set the current entry to refer to it. */
                if (!pte_is_unmapped(*entry) && (mapping_flags & MAP_CREATE)) {
                    /* Don't overwrite a mapped entry if we're only supposed to CREATE. */
                    // TODO: we need to ensure that it's possible to free the table...
                    panic("not implemented");
                    return MAP_RES_OVERWRITE;
                }

                pte_t *new_table = pmalloc_page();
                if (!new_table) {
                    // TODO: we need to ensure that it's possible to free the table...
                    panic("not implemented");
                    return MAP_RES_OOM;
                }
                for (pte_t *new_entry = new_table; new_entry < new_table + 512; new_entry++) {
                    *new_entry = pte_encode_unmapped();
                }

                if (pte_is_block(*entry, page_table_level)) {
                    panic("need to copy from existing block entry into table entry\n");
                }

                *entry = pte_encode_table(kernel_virt_to_phys(new_table));
            }
            uint64_t next_level_table_phys = table_pte_phys_addr(*entry);
            pte_t *next_level_table = (pte_t *) phys_to_kernel_virt(next_level_table_phys);

            int32_t result = map_table_virt_to_phys(next_level_table,
                    entry_virt_base, page_table_level + 1, entry_target_level,
                    entry_virt_start, entry_virt_length,
                    entry_phys_start,
                    attribs, mem_type,
                    mapping_flags);
            panic_if(result == MAP_RES_ALIGNMENT,
                    "recursive mapping should not have reported unaligned addresses\n");
            if (result != MAP_RES_SUCCESS) {
                // TODO: we need to ensure that it's possible to free the table...
                panic("not implemented");
                return result;
            }
        } else {
            /* entry_target_level is the current page_table_level, meaning that we must
             * end up with an unmapped or block entry. */
            if (pte_is_table(*entry, page_table_level)) {
                if (mapping_flags & MAP_CREATE) {
                    // TODO: we need to ensure that it's possible to free the table...
                    panic("not implemented");
                    return MAP_RES_OVERWRITE;
                }
                panic("need to free the existing table entry (recursively)\n");
            } else if (pte_is_block(*entry, page_table_level)) {
                if (mapping_flags & MAP_CREATE) {
                    // TODO: check if we're actually changing the block entry?
                    // Not sure if it's worth the trouble.
                    // TODO: we need to ensure that it's possible to free the table...
                    panic("not implemented");
                    return MAP_RES_OVERWRITE;
                }
            }

            /* Now it's safe to perform the mapping. */
            if (phys_addr_start == UNMAPPED) {
                *entry = pte_encode_unmapped();
            } else {
                *entry = pte_encode_block(entry_target_level, attribs, mem_type, entry_phys_start);
            }
        }
    }

    return MAP_RES_SUCCESS;
}

int32_t map_level_virt_to_phys(
        pte_t *ttbr0, pte_t *ttbr1,
        uint32_t target_level,
        uint64_t virt_addr_start, uint64_t length,
        uint64_t phys_addr_start,
        uint64_t attribs, uint64_t mem_type,
        uint64_t mapping_flags)
{
    if (phys_addr_start != UNMAPPED) {
        panic_if_not(target_level >= 1 && target_level <= 3,
                "target_level for mapping must be in the range 1-3; got %u32\n",
                target_level);
    } else {
        panic_if_not(target_level <= 3,
                "target_level for unmapping must be in the range 0-3; got %u32\n",
                target_level);
    }

    /* Make sure that the virtual address range lies entirely within the TTBR0
     * or TTBR1 range, and then just map_table_virt_to_phys() with the correct
     * level 0 page table. */
    bool in_ttbr0 = false, in_ttbr1 = false;

    // TODO: we don't really need to check this, although it doesn't hurt.
    // Consider removing it.
    if (virt_addr_start <= TTBR0_END) {
        /* VA range starts in the TTBR0 range; does it end there too? */
        if (length <= VA_RANGE - virt_addr_start) {
            in_ttbr0 = true;
        } else {
            panic("virtual address range at 0x%p with length 0x%u64{16} starts, but does not entirely lie, within the TTBR0 range [0x0, 0x%p]\n",
                    virt_addr_start, length, TTBR0_END);
        }
    } else if (virt_addr_start >= TTBR1_START) {
        /* VA range starts in the TTBR1 range; does it end there too? */
        if (virt_addr_start + length >= virt_addr_start) {
            in_ttbr1 = true;
        } else {
            panic("virtual address range at 0x%p with length 0x%u64{16} starts, but does not entirely lie, within the TTBR1 range [0x%p, 0xFFFFFFFFFFFFFFFF]\n",
                    virt_addr_start, length, TTBR1_START);
        }
    }

    if (in_ttbr0) {
        if (!ttbr0) {
            uint64_t ttbr;
            load_sysreg(ttbr, TTBR0_EL1);
            ttbr0 = phys_to_kernel_virt(ttbr);
        }
        return map_table_virt_to_phys(
                ttbr0, 0, PTW_START_LEVEL,
                target_level,
                virt_addr_start, length,
                phys_addr_start,
                attribs, mem_type,
                mapping_flags);
    } else if (in_ttbr1) {
        if (!ttbr1) {
            uint64_t ttbr;
            load_sysreg(ttbr, TTBR1_EL1);
            ttbr1 = phys_to_kernel_virt(ttbr);
        }
        return map_table_virt_to_phys(
                ttbr1, TTBR1_START, PTW_START_LEVEL,
                target_level,
                virt_addr_start, length,
                phys_addr_start,
                attribs, mem_type,
                mapping_flags);
    } else {
        panic("should not have gotten here\n");
    }
}

/* Find the minimum target level whose PTE ranges contain the given virtual address range,
 * and so that no lower target level also has PTE ranges which can contain the given
 * range but with less "wasted space" on the sides. */
// TODO: explain this way better lol
// idea is that if the range is already aligned on (say) a 2MB boundary,
// we should return "level 2", but if the end doesn't lie on a 2MB boundary
// but instead on a 4KB boundary, we have to return "level 3".
uint32_t best_target_level(uint64_t virt_addr_start, uint64_t length)
{
    uint32_t start_level = alignment_level(virt_addr_start),
             length_level = alignment_level(length);
    return max_uint64(start_level, length_level);
}

// TODO: warn if virt_addr_start and phys_addr_start aren't similarly
// aligned (maybe with the same alignment_level() even), since it could
// mean that we have to go deeper in the PTEs to do the mapping
int32_t map_virt_to_phys(
        pte_t *ttbr0, pte_t *ttbr1,
        uint64_t virt_addr_start, uint64_t length,
        uint64_t phys_addr_start,
        uint64_t attribs, uint64_t mem_type,
        uint64_t mapping_flags)
{
    //printf("map_virt_to_phys: virt 0x%p, length 0x%p => phys 0x%p\n",
    //        virt_addr_start, length, phys_addr_start);
    uint32_t target_level = best_target_level(virt_addr_start, length);
    if (target_level == 0 && phys_addr_start != UNMAPPED) {
        /* Level 0 PTEs cannot be block entries. */
        target_level = 1;
    }
    target_level = max_uint64(target_level, PTW_START_LEVEL);
    return map_level_virt_to_phys(ttbr0, ttbr1,
            target_level,
            virt_addr_start, length,
            phys_addr_start,
            attribs, mem_type,
            mapping_flags);
}

// TODO: don't have this simply call map_virt_to_phys...
// directly call deeper in the call stack so that we can
// avoid passing in a ttbr0 table even though we don't need one
int32_t map_flat_kernel_virt_to_phys(
        pte_t *ttbr1,
        uint64_t virt_addr_start, uint64_t length,
        uint64_t attribs, uint64_t mem_type,
        uint64_t mapping_flags)
{
    return map_virt_to_phys(NULL, ttbr1,
            virt_addr_start, length,
            kernel_virt_to_phys((ptr_t) virt_addr_start),
            attribs, mem_type,
            mapping_flags);
}

void free_page_table(pte_t *page_table, uint32_t level, bool free_referenced_blocks)
{
    panic_if_not(level <= 3,
            "page tables only exist at levels 0-3; got level %u32\n",
            level);
    for (uint64_t entry_index = 0; entry_index < 512; entry_index++) {
        pte_t entry = page_table[entry_index];
        if (pte_is_table(entry, level)) {
            pte_t *next_level_table = phys_to_kernel_virt(table_pte_phys_addr(entry));
            free_page_table(next_level_table, level + 1, free_referenced_blocks);
        } else if (free_referenced_blocks && pte_is_block(entry, level)) {
            ptr_t virt_block = phys_to_kernel_virt(block_pte_phys_addr(entry, level));
            pfree_pages(virt_block);
        }
    }
    /* Free the table itself (one page). */
    pfree_pages(page_table);
}

INLINE void invalidate_tlb_entry(uint64_t virt_addr)
{
    asm volatile("tlbi VaaE1, %[va]" : [va] "=r" (virt_addr));
}

INLINE void invalidate_tlb()
{
    asm volatile("tlbi VMAllE1");
}

INLINE uint64_t kernel_virt_to_phys(ptr_t virt_addr)
{
    panic_if_not(is_kernel_virt_addr(virt_addr),
            "virt_addr must be in kernel virtual address range\n");
    return (uint64_t) virt_addr - TTBR1_START;
}

INLINE ptr_t phys_to_kernel_virt(uint64_t phys_addr)
{
    panic_if_not(phys_addr <= TTBR0_END,
            "phys_addr must be in the kernel physical address range\n");
    return (ptr_t) (phys_addr + TTBR1_START);
}

INLINE bool is_user_virt_addr(ptr_t virt_addr)
{
    return (uint64_t) virt_addr <= TTBR0_END;
}

INLINE bool is_kernel_virt_addr(ptr_t virt_addr)
{
    return (uint64_t) virt_addr >= TTBR1_START;
}

bool virt_addr_range_has_attribs(ptr_t virt_addr_start, uint64_t length,
        bool require_read, bool require_write,
        bool require_user_access, bool require_kernel_access,
        bool require_user_executable, bool require_kernel_executable)
{
    // TODO: actually check literally anything
    return true;
}

pte_t pte_encode_table(uint64_t phys_addr)
{
    panic_if_not(phys_addr % PAGE_SIZE == 0,
            "page table must be aligned to page size, i.e. %u64 bytes.\n",
            PAGE_SIZE);
    /* We implicitly set NSTable, APTable, XNTable, and PXNTable to 0. */
    return phys_addr | PTE_TABLE;
}

pte_t pte_encode_block(uint32_t level, uint64_t attribs, uint64_t mem_type, uint64_t phys_addr)
{
    panic_if(level == 0, "level 0 PTEs cannot be block entries.\n");
    panic_if(level > 3, "level must be at most 3.\n");
    panic_if_not(mem_type == PTE_MEM || mem_type == PTE_MMIO || mem_type == PTE_NOCACHE,
            "mem_type must be PTE_MEM, PTE_MMIO, or PTE_NOCACHE.\n");
    /* Check alignment of the phys_addr. See virtual_memory.txt for details. */
    uint32_t alignment_bits = 39 - 9 * level;
    uint64_t alignment_mask = ~((uint64_t) 0xFFFFFFFFFFFFFFFF << alignment_bits);
    panic_if_not((phys_addr & alignment_mask) == 0,
            "phys_addr must be aligned with bits %u32-0 zero, but is 0x%p.\n",
            alignment_bits - 1, phys_addr);
    /* Do a basic sanity-check that the attribs at least are in the correct bit ranges. */
    uint64_t attribs_mask = level < 3
        ? (uint64_t) 0xFFFC000000000FFC  /* allowed bits 63-50, 11-2 */
        : (uint64_t) 0xFFF8000000000FFC; /* allowed bits 63-51, 11-2 */
    panic_if_not((attribs & ~attribs_mask) == 0,
            "attribs has enabled bits outside the expected ranges: "
            "got value 0b%u64{2} for level %u32 PTE\n",
            attribs, level);

    uint64_t pte_type = level < 3 ? PTE_BLOCK : PTE_PAGE;
    return attribs | phys_addr | mem_type | pte_type;
}

pte_t pte_encode_unmapped()
{
    return PTE_UNMAPPED;
}

bool pte_is_table(pte_t entry, uint32_t level)
{
    return level <= 2 && (entry & 3) == PTE_TABLE;
}

bool pte_is_block(pte_t entry, uint32_t level)
{
    if (level == 0) {
        /* Level 0 does not have block entries. */
        return false;
    } else if (level < 3) {
        return (entry & 3) == PTE_BLOCK;
    } else if (level == 3) {
        return (entry & 3) == PTE_PAGE;
    } else {
        panic("level must be 0 to 3; got %u32\n", level);
    }
}

bool pte_is_unmapped(pte_t entry)
{
    return (entry & 3) == PTE_UNMAPPED;
}

uint64_t table_pte_phys_addr(pte_t entry)
{
    return entry & 0x0000FFFFFFFFF000;
}

uint64_t block_pte_phys_addr(pte_t entry, uint64_t level)
{
    panic_if(level == 0 || level > 3, "block PTEs only exist in levels 1-3.\n");
    if (level == 1) {
        return entry & 0x0000FFFFC0000000;
    } else if (level == 2) {
        return entry & 0x0000FFFFFFE00000;
    } else {
        return entry & 0x0000FFFFFFFFF000;
    }
}

uint64_t block_pte_attribs(pte_t entry, uint64_t level)
{
    panic_if(level == 0 || level > 3, "block PTEs only exist in levels 1-3.\n");
    uint64_t attribs_mask = level < 3
        ? (uint64_t) 0xFFFC000000000FFC  /* allowed bits 63-50, 11-2 */
        : (uint64_t) 0xFFF8000000000FFC; /* allowed bits 63-51, 11-2 */
    return entry & attribs_mask;
}

uint64_t block_pte_mem_type(pte_t entry)
{
    return entry & 0x1C;
}
