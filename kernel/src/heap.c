#include <heap.h>
#include <stdops.h>
#include <virtual_memory.h>
#include <string.h>
#include <dbg.h>

/* Currently we use quite a silly encoding scheme.
 * The start of the heap is reserved for encoding allocation information
 * about the rest of the heap, page-by-page. Each allocatable page (i.e.
 * not in the reserved region) is represented, in order, by two bits:
 *           bit 1                     bit 0
 *         | next page in same block | page is allocated |
 * The allocation bit specifies if the page is allocated or currently
 * free; the block indication bit specifies if the next page is part
 * of the same multi-page block allocation. */

uint64_t num_total_pages;
uint64_t num_allocatable_pages;
uint8_t *info_blocks;
uint8_t *allocatable_pages;

#define INFO_ALLOCATED  ((uint8_t) 0x1)
#define INFO_BLOCK      ((uint8_t) 0x2)

USE_VALUE INLINE uint8_t get_page_info(uint64_t page_nr)
{
    panic_if(page_nr >= num_allocatable_pages,
            "page_nr must be less than %u64; got %u64\n",
            num_allocatable_pages, page_nr);
    uint8_t info_block = info_blocks[page_nr / 4];
    uint64_t bit_offset_in_block = 2 * (page_nr % 4);
    return (info_block >> bit_offset_in_block) & 0x3;
}

INLINE void set_page_info(uint64_t page_nr, uint8_t new_info)
{
    panic_if(page_nr >= num_allocatable_pages,
            "page_nr must be less than %u64; got %u64\n",
            num_allocatable_pages, page_nr);
    panic_if((new_info & ~0x3) != 0,
            "new_info can only have bits 1-0 set; got 0b%u8\n",
            new_info);
    uint8_t *info_block = &info_blocks[page_nr / 4];
    uint64_t bit_offset_in_block = 2 * (page_nr % 4);
    *info_block &= ~(0x3 << bit_offset_in_block);
    *info_block |= new_info << bit_offset_in_block;
}

void test_heap()
{
#define expect(val) panic_if_not(val, "expected: " #val "\n")
#define expect_not(val) panic_if(val, "expected not: " #val "\n")
#define expect_page(val, page_nr) \
    panic_if((val) != allocatable_pages + (page_nr) * PAGE_SIZE, \
            "wrong page number\n")
#define free_pages(page_nr) \
    pfree_pages(allocatable_pages + (page_nr) * PAGE_SIZE)

    num_allocatable_pages = 10;
    expect_not(pmalloc_pages(11));
    expect_page(pmalloc_pages(5), 0);
    expect_not(pmalloc_pages(6));
    expect_page(pmalloc_pages(2), 5);
    expect_page(pmalloc_page(), 7);
    free_pages(0);
    expect_page(pmalloc_pages(3), 0);
    free_pages(7);
    expect_page(pmalloc_pages(3), 7);

    panic("tests were successful, but we probably shouldn't "
          "keep running after them\n");
}

void initialize_heap()
{
    panic_if_not(KERNEL_HEAP_START % PAGE_SIZE == 0,
            "KERNEL_HEAP_START must be page-aligned\n");
    panic_if_not(KERNEL_HEAP_START < ((uint64_t) KERNEL_MEMORY_MB << 20),
            "heap must be located before the end of kernel memory\n");
    // TODO: check that the heap is sized appropriately, i.e. to leave
    // some space for the stack.

    /* Since each allocatable page's page info only takes 2 bits, in a
     * single page we can store the info of PAGE_SIZE * (8 bits / 2 bits) pages,
     * i.e. we cover 64 MiB of allocatable pages with a single info page. */
    num_total_pages = (uint64_t) KERNEL_HEAP_MB * 256;
    uint64_t num_info_pages = round_up_div(num_total_pages, PAGE_SIZE * 4);
    panic_if(num_info_pages > num_total_pages,
            "the kernel heap must be made larger\n");
    num_allocatable_pages = num_total_pages - num_info_pages;

    printf("  heap total pages: %u64 (%u64 reserved, %u64 allocatable)\n",
            num_total_pages, num_info_pages, num_allocatable_pages);

    info_blocks = phys_to_kernel_virt(KERNEL_HEAP_START);
    allocatable_pages = info_blocks + num_info_pages * PAGE_SIZE;

    memset(info_blocks, num_info_pages * PAGE_SIZE, 0);
}

// TODO: make this all safe in process context!
// what if any of these functions are preempted?
// (probably just lock stuff)

ptr_t pmalloc_page()
{
    // TODO: this can probably be more efficient
    return pmalloc_pages(1);
}

ptr_t pmalloc_pages(uint64_t num_pages)
{
    panic_if(num_pages == 0,
            "cannot request no memory\n");
    //printf("pmalloc_pages(%u64):\n", num_pages);

    // TODO: do anything better than a linear search.
    uint64_t free_block_start_nr = 0;
    uint64_t free_pages_seen = 0;

    for (uint64_t page_nr = 0; page_nr < num_allocatable_pages; page_nr++) {
        if (!(get_page_info(page_nr) & INFO_ALLOCATED)) {
            /* Free page. Add to our current block, or start a new block. */
            if (free_pages_seen == 0) {
                free_block_start_nr = page_nr;
            }

            free_pages_seen++;
            //printf("  page %u64 free => start %u64, length %u64\n",
            //        page_nr, free_block_start_nr, free_pages_seen);
            if (free_pages_seen == num_pages) {
                break;
            }
        } else {
            /* Allocated page. Reset our free-block. */
            free_pages_seen = 0;
            //printf("  page %u64 already allocated\n");
        }
    }

    if (free_pages_seen == num_pages) {
        //printf("  allocation starts at page %u64\n", free_block_start_nr);
        //printf_nl("[kernel] allocated pages: start %u64, length %u64\n",
        //        free_block_start_nr, num_pages);
        /* We found a free block of pages. */
        for (uint64_t offset_nr = 0; offset_nr < num_pages; offset_nr++) {
            uint8_t page_info = INFO_ALLOCATED;
            if (offset_nr < num_pages - 1) {
                /* All but the last page indicate that the next page is
                 * part of the same block. */
                page_info |= INFO_BLOCK;
            }
            set_page_info(free_block_start_nr + offset_nr, page_info);
            panic_if_not(get_page_info(free_block_start_nr + offset_nr) == page_info,
                    "broken!\n");
        }
        return allocatable_pages + free_block_start_nr * PAGE_SIZE;
    } else {
        //printf("  not enough memory\n");
        return NULL;
    }
}

void pfree_pages(ptr_t start_page)
{
    panic_if_not((uint64_t) start_page % PAGE_SIZE == 0,
            "start_page must be page-aligned; got 0x%p\n",
            start_page);
    uint64_t start_page_nr = ((uint64_t) start_page - (uint64_t) allocatable_pages) / PAGE_SIZE;
    //printf("pfree_pages(0x%p == page %u64):\n", start_page, start_page_nr);
    //printf_nl("[kernel] freeing at page %u64, ", start_page_nr);
    uint64_t pages_freed = 0;
    for (uint64_t page_nr = start_page_nr; ; page_nr++) {
        uint8_t page_info = get_page_info(page_nr);
        panic_if_not(page_info & INFO_ALLOCATED,
                "attempting to free already-freed pages (starting at page %u64, now at %u64)\n",
                start_page_nr, page_nr);
        /* Deallocate the page. */
        set_page_info(page_nr, 0);
        pages_freed++;
        //printf("  freeing page %u64: ", page_nr);
        if (page_info & INFO_BLOCK) {
            //printf("block\n");
            continue;
        } else {
            //printf("end of block\n");
            break;
        }
    }
    //printf("length %u64\n", pages_freed);
}

ptr_t pmalloc(uint64_t bytes)
{
    // TODO: don't just allocate in pages
    return pmalloc_pages(round_up_div(bytes, PAGE_SIZE));
}

void pfree(ptr_t mem)
{
    // TODO: not just page-by-page
    pfree_pages(mem);
}
