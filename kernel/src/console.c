#include <console.h>

#include <device/uart.h>
#include <string.h>
#include <dbg.h>
#include <stdarg.h>
#include <uapi/errno.h>

/*
 * Stores whether or not a newline was the most recent
 * printed output.
 */
bool newline_most_recent;

void console_init()
{
    newline_most_recent = true;
}

void putc(char c)
{
    uart_blocking_send(c);
    newline_most_recent = c == '\n';
}

/* Replaces \n with \n\r. */
void puts(const char *s)
{
    while (*s) {
        uart_blocking_send(*s);
        if (*s == '\n') {
            uart_blocking_send('\r');
        }
        newline_most_recent = *s == '\n';
        s++;
    }
}

void puts_nl(const char *s)
{
    if (!newline_most_recent) {
        puts("\n");
    }
    puts(s);
}

USE_VALUE char numeric_char(uint8_t val)
{
    if (val < 10) {
        return '0' + val;
    } else if (val < 36) {
        return 'a' + (val - 10);
    } else {
        dbg_error("got val %u8; should be 0-35.\n", val);
        return '?';
    }
}

void put_int8(int8_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        dbg_error("got base %i32; should be 2-36.\n", base);
        return;
    }

    put_int64((int64_t) val, base);
}

void put_uint8(uint8_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        dbg_error("got base %i32; should be 2-36.\n", base);
        return;
    }

    put_uint64((uint64_t) val, base);
}

void put_int16(int16_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        dbg_error("got base %i32; should be 2-36.\n", base);
        return;
    }

    put_int64((int64_t) val, base);
}

void put_uint16(uint16_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        dbg_error("got base %i32; should be 2-36.\n", base);
        return;
    }

    put_uint64((uint64_t) val, base);
}

void put_int32(int32_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        dbg_error("got base %i32; should be 2-36.\n", base);
        return;
    }

    put_int64((int64_t) val, base);
}

void put_uint32(uint32_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        dbg_error("got base %i32; should be 2-36.\n", base);
        return;
    }

    put_uint64((uint64_t) val, base);
}

void put_uint64(uint64_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        dbg_error("got base %i32; should be 2-36.\n", base);
        return;
    }

    if (val == 0) {
        putc('0');
        return;
    }
    char buf[65];
    int digits = 0; /* May not be digits, but whatever. */
    while (val != 0) {
        buf[digits++] = numeric_char(val % base);
        val /= base;
    }
    for (int d = digits - 1; d >= 0; d--) {
        putc(buf[d]);
    }
}

void put_int64(int64_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        dbg_error("got base %i32; should be 2-36.\n", base);
        return;
    }

    if (val < 0) {
        putc('-');
        val = -val; /* TODO: what about MIN_INT64? */
    }
    put_uint64((uint64_t) val, base);
}

void put_ptr(ptr_t val)
{
    put_uint64((uint64_t) val, 16);
}

void putc_quoted(char c)
{
    uint8_t b = (uint8_t) c;
    switch (b) {
        case 0: return puts("\\0");
        case '\t': return puts("\\t");
        case '\r': return puts("\\r");
        case '\n': return puts("\\n");
        case '\\': return puts("\\\\");
        case '\'': return puts("\\'");
        case '"': return puts("\\\"");
    }
    /* Not a specially-escaped character. Either put it normally, or encode
     * in hex. */
    if (32 <= b && b <= 126) {
        return putc(c);
    } else {
        putc('\\');
        // TODO: fix this once we (finally!) have a zero-pad integer printing routine...
        if (b < 16) {
            putc('0');
        }
        put_uint8(b, 16);
    }
}

void puts_quoted(const char *s)
{
    while (*s) {
        putc_quoted(*s++);
    }
}

/*
 * Here we only use puts and puts_nl, instead of using printf,
 * in case a bug in printf is causing this to be called. (We
 * don't want to end up in an infinite loop of calling printf
 * to report an error with a broken printf!)
 * Therefore the `msg` must be a literal string.
 */
#define INVALID_PRINTF_FORMAT(fmt, msg) \
    do { \
        puts_nl("[PRINTF] invalid format string '"); \
        puts(fmt); \
        puts("': "); \
        puts(msg); \
        puts("\n"); \
        return; \
    } while (0)

/*
 * When fmt is just after a %iNN or %uNN specifier, this
 * checks for a {BB} base specifier. Return value is a pointer
 * to the format string _after_ such a base specifier (if it exists),
 * and this sets `base` to the parsed base (if valid), or -1 otherwise.
 * (If there is no base specifier, sets `base` to the default value 10.)
 */
USE_VALUE const char *read_base(const char *fmt, int32_t *base)
{
    // TODO: handle any base 2-36 that isn't just 2, 8, 10, or 16.
    if (startswith(fmt, "{2}")) {
        *base = 2;
        return fmt + 3;
    } else if (startswith(fmt, "{8}")) {
        *base = 8;
        return fmt + 3;
    } else if (startswith(fmt, "{10}")) {
        *base = 10;
        return fmt + 4;
    } else if (startswith(fmt, "{16}")) {
        *base = 16;
        return fmt + 4;
    } else if (*fmt == '{') {
        *base = -1;
        return fmt;
    } else {
        *base = 10;
        return fmt;
    }
}

void vprintf(const char *fmt, va_list args)
{
    const char *save_fmt = fmt;

    while (*fmt) {
        if (*fmt == '%') {
            fmt++;
            if (!*fmt) {
                INVALID_PRINTF_FORMAT(save_fmt, "must not end with '%'");
            }
            switch (*fmt) {
                case 'i': {
                    fmt++;
                    uint32_t size; // in bits
                    int32_t base;
                    if (startswith(fmt, "8")) {
                        fmt++;
                        size = 8;
                    } else if (startswith(fmt, "16")) {
                        fmt += 2;
                        size = 16;
                    } else if (startswith(fmt, "32")) {
                        fmt += 2;
                        size = 32;
                    } else if (startswith(fmt, "64")) {
                        fmt += 2;
                        size = 64;
                    } else {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid '%i' specifier");
                    }

                    fmt = read_base(fmt, &base);
                    if (base == -1) {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid base for '%i' specifier");
                    }

                    if (size == 8) {
                        put_int8((int8_t) va_arg(args, int), base);
                    } else if (size == 16) {
                        put_int16((int16_t) va_arg(args, int), base);
                    } else if (size == 32) {
                        put_int32(va_arg(args, int32_t), base);
                    } else if (size == 64) {
                        put_int64(va_arg(args, int64_t), base);
                    }

                    break;
                }
                case 'u': {
                    fmt++;
                    uint32_t size; // in bits
                    int32_t base;
                    if (startswith(fmt, "8")) {
                        fmt++;
                        size = 8;
                    } else if (startswith(fmt, "16")) {
                        fmt += 2;
                        size = 16;
                    } else if (startswith(fmt, "32")) {
                        fmt += 2;
                        size = 32;
                    } else if (startswith(fmt, "64")) {
                        fmt += 2;
                        size = 64;
                    } else {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid '%u' specifier");
                    }

                    fmt = read_base(fmt, &base);
                    if (base == -1) {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid base for '%u' specifier");
                    }

                    if (size == 8) {
                        put_uint8((uint8_t) va_arg(args, int), base);
                    } else if (size == 16) {
                        put_uint16((uint16_t) va_arg(args, int), base);
                    } else if (size == 32) {
                        put_uint32(va_arg(args, uint32_t), base);
                    } else if (size == 64) {
                        put_uint64(va_arg(args, uint64_t), base);
                    }

                    break;
                }
                case 'p':
                    fmt++;
                    ptr_t p = va_arg(args, ptr_t);
                    put_uint64((uint64_t) p, 16);
                    break;
                case 's':
                    fmt++;
                    puts(va_arg(args, const char*));
                    break;
                case 'c':
                    fmt++;
                    putc((char) va_arg(args, int));
                    break;
                case 'q':
                    fmt++;
                    if (*fmt == 'c') {
                        fmt++;
                        putc_quoted((char) va_arg(args, int));
                    } else if (*fmt == 's') {
                        fmt++;
                        puts_quoted(va_arg(args, const char*));
                    } else {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid '%q' specifier");
                    }
                    break;
                case 'b':
                    fmt++;
                    bool b = (bool) va_arg(args, int);
                    puts(b ? "true" : "false");
                    break;
                case 'e':
                    fmt++;
                    errno_t errno = va_arg(args, errno_t);
                    switch (errno) {
#define CASE_ERRNO(errno_name, errno_val) \
                        case errno_val: puts(errno_name); break;

                        FOREACH_ERRNO(CASE_ERRNO)
                        default:
                            puts("errno ");
                            put_uint32(errno, 10);
                            break;
                    }
                    break;
                case '%':
                    fmt++;
                    putc('%');
                    break;
                case '{':
                    fmt++;
                    putc('{');
                    break;
                case '}':
                    fmt++;
                    putc('}');
                    break;
                default:
                    INVALID_PRINTF_FORMAT(save_fmt, "nonexistent specifier");
            }
        } else {
            putc(*fmt);
            if (*fmt == '\n') {
                /* Convert \n into \n\r. */
                putc('\r');
                newline_most_recent = true;
            }
            fmt++;
        }
    }
}

/*
 * Like standard C printf. Allowable format specifiers: (N can be 8, 16, 32, or 64; B can be any decimal number 2-36)
 *     %iN     intN_t, in decimal
 *     %iN{B}  intN_t, in base B  [TODO]
 *     %uN     uintN_t, in decimal
 *     %uN{B}  uintN_t, in base B [TODO]
 *     %p      ptr_t, in hexadecimal
 *     %{q}s   char*, directly printed (or quoted if 'q' is specified)
 *     %{q}c   char, directly printed (or quoted if 'q' is specified)
 *     %b      bool, printed as 'true' or 'false'
 *     %e      uint32_t, printed as the name of the given errno
 *     %%      prints '%'
 *     %{      prints '{'
 *     %}      prints '}' (not really as necessary as %{)
 */
void printf(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}

void printf_nl(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    puts_nl("");
    vprintf(fmt, args);
    va_end(args);
}
