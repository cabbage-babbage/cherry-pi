#include <proc.h>
#include <sysreg.h>
#include <timer.h>
#include <irq.h>
#include <syscall.h>
#include <uapi/syscall_nr.h>
#include <heap.h>
#include <stdops.h>
#include <virtual_memory.h>
#include <string.h>
#include <elf.h>
#include <console.h>
#include <device/uart.h>
#include <exception_level.h>
#include <uapi/errno.h>
#include <device/tty.h>
#include <syscontext.h>
#include <uapi/signal.h>
#include <dbg.h>

#define MAX_PROCS 20
/* Basic quantum of processing time. */
#define CYCLE_MS 10

/* Actual storage for all the processes. They may end up in
 * various different lists, but this is really where they live. */
proc_t all_procs[MAX_PROCS];

uint32_t process_counter;
proc_t *current_proc;

irq_lock_t process_lists_lock;

proc_list_t processes_unallocated;
proc_list_t processes_ready;
proc_list_t processes_sleeping;

proc_t *idle_proc;

tty_config_t main_tty_cfg;
chardev_t main_tty;

void allocate_syscall_stack(proc_t *proc)
{
    /* Allocate memory for the kernel-space stack.
     * Note that this memory will never be freed, but that's ok -- this memory
     * lives with the process block, not with the process itself. */
    const uint64_t SYSCALL_STACK_PAGES = 5;
    ptr_t syscall_stack_mem = pmalloc_pages(SYSCALL_STACK_PAGES);
    panic_if_not(syscall_stack_mem,
            "out of memory for process syscall stacks\n");
    /* The initial syscall stack pointer points to the top of the stack region. */
    proc->syscall_stack = (uint8_t *) syscall_stack_mem + PAGE_SIZE * SYSCALL_STACK_PAGES;
}

uint32_t idle_proc_main(void *aux)
{
    /* This loop is very important to correct functioning of the kernel.
     * The idle process must never quit, but we also don't want it to
     * busy-loop. We instead busy-`wfe` so that interrupts and events
     * are detected as soon as possible and serviced by the kernel. */
    while (true) {
        asm volatile("wfe");
    }
}

uint32_t tty_proc_main(void *aux);

// TODO: don't just declare this here.. pull it from the right header
int64_t syscall_sleep(uint64_t ms);
/* 'root' process main function. */
uint32_t root_proc_main(void *aux)
{
    errno_t errno;

    errno = fork_kernel("idle", &idle_proc_main, NULL, &idle_proc);
    if (errno) {
        panic("[root] could not fork idle process: %e\n", errno);
    } else {
        printf("[root] created idle process, pid %u32\n",
                idle_proc->id);
    }

    // TODO: move this process setup to the tty code
    proc_t *tty_proc;
    errno = fork_kernel("tty", &tty_proc_main, &main_tty, &tty_proc);
    if (errno) {
        printf("[root] could not fork tty process: %e\n", errno);
    } else {
        printf("[root] created tty process, pid %u32\n",
                tty_proc->id);
    }

    /* Load the 'init' process from /bin/init, with cwd at the root. */
    open_file_t root;
    //printf("proc:root process opening /\n");
    errno = filesys_open("/", &root);
    if (errno) {
        printf("[root] could not open /: %e\n", errno);
    } else {
        proc_t *init_proc;
        int64_t errcode = fork_user("init", root, "bin/init",
                NULL, 0, &init_proc);
        if (errcode == 0) {
            printf("[root] created init process, pid %u64\n",
                    init_proc->id);
        } else {
            printf("[root] could not create init process: %e\n",
                    (errno_t) -errcode);
        }
    }

    /* The root process shouldn't die, but it doesn't need to do much,
     * either. Let's just have it sleep for long periods of time. */
    while (true) {
        syscall_sleep(1000 * 3600);
    }
}

void initialize_processes()
{
    process_counter = 0;

    /* process_lists_lock needs no initialization. */

    processes_unallocated = empty_process_list();
    processes_ready = empty_process_list();
    processes_sleeping = empty_process_list();

    for (proc_t *proc = all_procs; proc < all_procs + MAX_PROCS; proc++) {
        proc->prev = NULL;
        proc->next = NULL;
        proc->id = 0;
        allocate_syscall_stack(proc);

        /* The name is just for debugging, in case we have to print processes. */
        const char *name = "UNALLOC";
        memcpy(proc->name, name, strlen(name) + 1 /* null terminator */);

        append_process(&processes_unallocated, proc);
    }

    current_proc = NULL;

    main_tty_cfg = (tty_config_t) {
        .in_convert_cr_to_lf = true,
        .in_convert_ctrl_c_to_signal = true,
        .in_line_mode = true,
        .in_echo = true,
        .out_convert_lf_to_crlf = true,
        .out_stop = false,
    };
    wrap_tty(&main_tty, "tty", &uart_device, &main_tty_cfg);
    errno_t errno = device_register(&main_tty.dev);
    panic_if(errno, "could not register tty device: %e\n", errno);

    /* Add the tty device to /dev. */
    bool is_new;
    errno = filesys_create("/dev/tty", false,
            INODE_CHARDEV, INODE_PERM_R | INODE_PERM_W, main_tty.dev.id,
            &is_new, NULL);
    if (!errno && !is_new) {
        errno = EEXIST;
    }
    panic_if(errno, "could not create /dev/tty: %e\n", errno);

    idle_proc = NULL;
    /* Create the root process and ready it for execution. */
    proc_t *root_process = allocate_process();
    panic_if_not(root_process, "could not allocate root process\n");
    root_process->id = 1;
    root_process->gid = 1;
    process_counter++;
    panic_if_not(setup_kernel_process(root_process, "root", &root_proc_main,
                /*aux*/ NULL, /*parent*/NULL,
                /*make_ready*/false, /*deallocate_on_failure*/false) == 0,
            "could not set up root process\n");
    enqueue_process(&processes_ready, root_process);
}

void dump_proc(proc_t *proc, const char *prefix)
{
    printf("%sid=%u32, name='%s', is_kernel=%b:\n",
            prefix, proc->id, proc->name, proc->is_kernel);
    printf("%sx0=%u64{16}, x1=%u64{16}, x2=%u64{16}, x3=%u64{16},\n",
            prefix, proc->regs.x[0], proc->regs.x[1], proc->regs.x[2], proc->regs.x[3]);
    printf("%sx4=%u64{16}, x5=%u64{16}, x6=%u64{16}, x7=%u64{16},\n",
            prefix, proc->regs.x[4], proc->regs.x[5], proc->regs.x[6], proc->regs.x[7]);
    printf("%sx8=%u64{16}, x9=%u64{16}, x10=%u64{16}, x11=%u64{16},\n",
            prefix, proc->regs.x[8], proc->regs.x[9], proc->regs.x[10], proc->regs.x[11]);
    printf("%sx12=%u64{16}, x13=%u64{16}, x14=%u64{16}, x15=%u64{16},\n",
            prefix, proc->regs.x[12], proc->regs.x[13], proc->regs.x[14], proc->regs.x[15]);
    printf("%sx16=%u64{16}, x17=%u64{16}, x18=%u64{16}, x19=%u64{16},\n",
            prefix, proc->regs.x[16], proc->regs.x[17], proc->regs.x[18], proc->regs.x[19]);
    printf("%sx20=%u64{16}, x21=%u64{16}, x22=%u64{16}, x23=%u64{16},\n",
            prefix, proc->regs.x[20], proc->regs.x[21], proc->regs.x[22], proc->regs.x[23]);
    printf("%sx24=%u64{16}, x25=%u64{16}, x26=%u64{16}, x27=%u64{16},\n",
            prefix, proc->regs.x[24], proc->regs.x[25], proc->regs.x[26], proc->regs.x[27]);
    printf("%sx28=%u64{16}, FP=%u64{16}, LR=%u64{16}, SP=%u64{16},\n",
            prefix, proc->regs.x[28], proc->regs.x[29], proc->regs.x[30], proc->regs.x[31]);
    uint64_t pstate = proc->regs.pstate;
    printf("%spc=0x%u64{16}, pstate=0b%u64{2} (%c%c%c%c %s %s %c%c%c%c %s EL%u64 %c SPSel=%s)\n",
            prefix,
            proc->regs.pc, pstate,
            ((pstate >> 31) & 1) ? 'N' : 'n',
            ((pstate >> 30) & 1) ? 'Z' : 'z',
            ((pstate >> 29) & 1) ? 'C' : 'c',
            ((pstate >> 28) & 1) ? 'V' : 'v',
            ((pstate >> 21) & 1) ? "SS" : "ss",
            ((pstate >> 20) & 1) ? "IL" : "il",
            ((pstate >> 9) & 1) ? 'D' : 'd',
            ((pstate >> 8) & 1) ? 'A' : 'a',
            ((pstate >> 7) & 1) ? 'I' : 'i',
            ((pstate >> 6) & 1) ? 'F' : 'f',
            ((pstate >> 4) & 1) ? "not-aarch64" : "aarch64",
            (pstate >> 2) & 3,
            ((pstate >> 1) & 1) ? '1' : '0',
            (pstate & 1) ? "SP_EL1" : "SP_EL0");
}

/* Copy 32 8-byte registers from the src array to the dst array.
 * If the src and dst overlap, the result is undefined. */
void copy_regs(uint64_t *dst, const uint64_t *src)
{
    for (uint32_t i = 0; i < 32; i++) {
        dst[i] = src[i];
    }
}

/* Store proc->regs or proc->syscall_regs into the actual system state.
 * This includes ELR_EL1, SPSR_EL1, TTBR0_EL1, and x0...x31.
 * (If irq_saved_regs_base is NULL, don't copy x0...x31.) */
INLINE void store_process_regs(proc_t *proc, ptr_t irq_saved_regs_base)
{
    proc_regs_t *regs = proc->context == CONTEXT_SYSCALL
        ? &proc->syscall_regs
        : &proc->regs;

    if (irq_saved_regs_base) {
        /* The IRQ handler will push these into the actual general-purpose
         * registers on exception return. */
        copy_regs(irq_saved_regs_base, regs->x);
    }
    set_sysreg(ELR_EL1, regs->pc);
    set_sysreg(SPSR_EL1, regs->pstate);

    /* Kernel processes don't have low virtual addresses assigned,
     * so we don't need to update the TTBR0 register and invalidate
     * stale TLB entries. */
    if (!proc->is_kernel) {
        uint64_t current_ttbr0;
        load_sysreg(current_ttbr0, TTBR0_EL1);
        if (current_ttbr0 != regs->ttbr0) {
            set_sysreg(TTBR0_EL1, regs->ttbr0);
            invalidate_tlb();
            asm volatile("isb; dsb ish");
        }
    }
}

/* Load proc->regs or proc->syscall_regs from the system state. */
INLINE void load_process_regs(proc_t *proc, ptr_t irq_saved_regs_base)
{
    /*
     * The IRQ handler prefix already loaded all the general-purpose registers
     * into the irq_saved_regs_base.
     */
    proc_regs_t *regs = proc->context == CONTEXT_SYSCALL
        ? &proc->syscall_regs
        : &proc->regs;

    copy_regs(regs->x, irq_saved_regs_base);
    uint64_t pc;
    load_sysreg(pc, ELR_EL1);
    regs->pc = pc;

    uint64_t pstate;
    load_sysreg(pstate, SPSR_EL1);
    regs->pstate = pstate;

    uint64_t ttbr0;
    load_sysreg(ttbr0, TTBR0_EL1);
    regs->ttbr0 = ttbr0;
}

void start_processes()
{
    panic_if(is_empty(&processes_ready),
            "there are no ready processes: create_process() must be called before start_processes()\n");

    /* Select the first process to run. */
    proc_t *proc = processes_ready.head;
    current_proc = proc;

    set_sysreg(ELR_EL1, proc->regs.pc);
    set_sysreg(SPSR_EL1, proc->regs.pstate);
    set_sysreg(TTBR0_EL1, proc->regs.ttbr0);
    invalidate_tlb();
    asm volatile("isb; dsb ish");
    set_syscontext(SYSCONTEXT_PREEMPTIBLE);
    return_to_process(proc, NULL);
}

INLINE bool has_privileged_execution(proc_t *proc)
{
    switch (proc->context) {
        case CONTEXT_PROCESS:
            return proc->is_kernel;
        case CONTEXT_SYSCALL:
            return true;
        case CONTEXT_EXITED:
            /* We don't actually expect to ever run the process if it's in
             * this context, but some functions check the privilege level
             * for sanity checking, so we have to support that here. */
            return true;
        default:
            panic("unexpected context; probably one was forgotten (process '%qs', id %u32)\n",
                    proc->name, proc->id);
    }
}

INLINE void page_align_interval(
        uint64_t *start, uint64_t *length)
{
    uint64_t aligned_start = align_down(*start, PAGE_SIZE);
    uint64_t aligned_length = align_up(*length + *start - aligned_start, PAGE_SIZE);
    *start = aligned_start;
    *length = aligned_length;
}

proc_t *allocate_process()
{
    acquire_irq_lock(&process_lists_lock);
    if (is_empty(&processes_unallocated)) {
        release_irq_lock(&process_lists_lock);
        //printf("[kernel] process slots all taken\n");
        return NULL;
    }

    proc_t *proc = dequeue_process(&processes_unallocated);
    release_irq_lock(&process_lists_lock);

    return proc;
}

void assign_fresh_process_id(proc_t *proc)
{
    // TODO: do this with a semaphore-based lock instead?
    // this irq_lock_t is lower-overhead...
    irq_lock_t lock;
    acquire_irq_lock(&lock);
    process_counter++;
    proc->id = process_counter;
    release_irq_lock(&lock);
}

void free_process(proc_t *proc)
{
    acquire_lock(&proc->signals_lock);
    free_signals(&proc->signals);
    release_lock(&proc->signals_lock);

    if (proc->is_fork) {
        free_page_table(
                phys_to_kernel_virt(proc->regs.ttbr0),
                PTW_START_LEVEL, true /* free the referenced memory */);
    } else if (proc->is_kernel) {
        pfree_pages(proc->stack_memory);
    } else {
        /* User process. */
        free_page_table(phys_to_kernel_virt(proc->regs.ttbr0),
                PTW_START_LEVEL, false /* do not free the referenced memory */);
        /* Now we can free the referenced memory. */
        pfree_pages(proc->proc_memory);
        pfree_pages(proc->heap_memory);
        pfree_pages(proc->stack_memory);
        if (proc->aux_memory) {
            pfree_pages(proc->aux_memory);
        }
    }
}

/* Do the actual business of deallocating a process, but without any
 * synchronization. */
INLINE void deallocate_process_unsync(proc_t *proc)
{
    /* This is the minimal amount of modification necessary to move
     * the process from no list to the unallocated queue. */
    enqueue_process(&processes_unallocated, proc);
    proc->id = 0;
}

void deallocate_process(proc_t *proc)
{
    acquire_irq_lock(&process_lists_lock);
    deallocate_process_unsync(proc);
    release_irq_lock(&process_lists_lock);
}

INLINE bool process_is_allocated(proc_t *proc)
{
    return proc->id != 0;
}

void kill_ready_process(proc_t *proc)
{
    panic_if_not(in_context_preemptible(),
            "kill_ready_process() must be called from a preemptible context\n");

    if (!proc->is_kernel) {
        //printf("proc:closing cwd (inode %u64) of dying proc '%qs' (%u32)\n",
        //        proc->cwd.file->inode_nr, proc->name, proc->id);
        filesys_close(proc->cwd);
    }
    free_process(proc);

    acquire_irq_lock(&process_lists_lock);
    remove_process(&processes_ready, proc);

    if (proc->parent) {
        int64_t wait_id = proc->parent->wait_id;
        if (wait_id == 0 || wait_id == proc->id) {
            /* The parent process was waiting on us, or any child process, to exit,
             * so now it may run again. */
            enqueue_process(&processes_ready, proc->parent);
            /* Mark the parent as not waiting for any more children. */
            proc->parent->wait_id = -1;
        }
    }

    /* We can only fully deallocate the process if it has no parent; otherwise,
     * the parent might later wait() on the child to exit, at which point we
     * need to keep around enough of the child to be able to inform the parent
     * that the child exited. */
    if (!proc->parent) {
        deallocate_process_unsync(proc);
    }

    proc->context = CONTEXT_EXITED;

    release_irq_lock(&process_lists_lock);

    yield_process();
    panic("should not get here\n");
}

errno_t dup_file_desc(file_descriptor_t *desc)
{
    panic_if_not(desc->valid, "can only dup a valid descriptor\n");
    if (desc->type == FILE_PIPE) {
        bool is_reader = (desc->flags & FD_READABLE) != 0;
        pipe_t *pipe = desc->aux.pipe;
        pipe_dup_half(pipe, is_reader);
        return 0;
    } else {
        return filesys_dup(desc->file);
    }
}

void close_file_desc(file_descriptor_t *desc)
{
    panic_if_not(desc->valid, "can only close a valid descriptor\n");
    if (desc->type == FILE_PIPE) {
        bool is_reader = (desc->flags & FD_READABLE) != 0;
        pipe_t *pipe = desc->aux.pipe;
        pipe_close_half(pipe, is_reader);
    } else {
        filesys_close(desc->file);
    }
    desc->valid = false;
}

USE_VALUE errno_t load_file(open_file_t dir, const char *filepath,
        ptr_t *file_start, uint64_t *file_length)
{
    //printf("proc:loading file '%qs' from dir inode %u64\n",
    //        filepath, dir.file->inode_nr);
    open_file_t file;
    errno_t errno = filesys_open_at(open_file_locator(dir), filepath, &file);
    if (errno) {
        return errno;
    }

    inode_t inode;
    errno = filesys_get_inode(file, &inode);
    if (errno) {
        goto load_file_error_close_file;
    }
    if (inode.type != INODE_FILE) {
        errno = ENOTFILE;
        goto load_file_error_close_file;
    }
    if (!(inode.perm & INODE_PERM_X)) {
        errno = EPERM;
        goto load_file_error_close_file;
    }
    uint64_t size = inode.size;

    uint8_t *buf = pmalloc(size);
    if (!buf) {
        errno = ENOMEM;
        goto load_file_error_close_file;
    }

    uint64_t total_bytes_read = 0;
    uint64_t bytes_read;
    while (total_bytes_read < size) {
        errno = filesys_read_file(file, total_bytes_read,
                buf + total_bytes_read, size - total_bytes_read,
                &bytes_read);
        if (errno) {
            goto load_file_error_free_buf;
        }
        total_bytes_read += bytes_read;
        if (bytes_read == 0) {
            /* Unexpected EOF. */
            printf("bug: unexpected EOF at length %u64 instead of %u64\n",
                total_bytes_read, size);
            size = total_bytes_read;
        }
    }
    //printf("!!!!! DUMPING FILE %qs !!!!!\n", filepath);
    //hexdump(buf, size);

    filesys_close(file);
    *file_start = buf;
    *file_length = size;
    return 0;

load_file_error_free_buf:
    pfree(buf);
load_file_error_close_file:
    filesys_close(file);
    return errno;
}

int64_t setup_user_process(proc_t *proc, const char *name,
        open_file_t cwd, const char *filepath,
        ptr_t args, uint64_t args_length,
        proc_t *parent,
        file_descriptor_t *fds, uint32_t num_fd, bool close_fds_on_failure,
        bool make_ready, bool close_cwd_on_failure, bool deallocate_on_failure)
{
    errno_t errno;
    //printf_nl("[kernel] setup_user_process('%qs', file='%qs', %s):\n",
    //        name, filename, is_kernel ? "kernel" : "user");

    panic_if(num_fd > MAX_FDS,
            "only up to MAX_FDS (%u64) may be given; got %u64 instead\n",
            (uint64_t) MAX_FDS, num_fd);

    uint64_t name_len = strlen(name);
    if (name_len > 127) {
        dbg_error("process name must be at most 127 characters long; got '%qs'\n", name);
        errno = EINVAL;
        goto create_error_deallocate_proc;
    }

    /* To make things simple, we load the entire file into memory ahead-of-time. */
    // TODO: don't do this; have each functionality just read the part it needs.

    ptr_t file_start;
    uint64_t file_length;
    if ((errno = load_file(cwd, filepath, &file_start, &file_length))) {
        //dbg_error("could not load executable file '%qs': %e\n", filepath, errno);
        //errno = ENOENT;
        goto create_error_deallocate_proc;
    }

    //printf("got start 0x%p, length %u64\n", file_start, file_length);
    elf_header_t *e_header = check_elf(file_start, file_length);
    if (!e_header) {
        dbg_error("file '%qs' is not a valid ELF file\n", name);
        errno = EEXEC;
        goto create_error_free_file;
    }

    //printf("ELF headers:\n");
    //dump_elf_headers(file_start, "    ");

    /* Check that it's an executable ELF file that we can actually load. */
    if (e_header->e_type != ET_EXEC) {
        dbg_error("the only supported ELF e_type is ET_EXEC\n");
        errno = EEXEC;
        goto create_error_free_file;
    }
    if (e_header->e_machine != EM_AARCH64) {
        dbg_error("the only supported ELF e_machine is EM_AARCH64\n");
        errno = EEXEC;
        goto create_error_free_file;
    }
    if (e_header->e_phnum == 0) {
        dbg_error("there must be at least one ELF segment\n");
        errno = EEXEC;
        goto create_error_free_file;
    }

    // check the program segments:
    // - we can support (for now):
    //   - PT_LOAD (loadable segment)
    //   - PT_NULL (ignore it)
    //   - PT_NOTE (ignore it)
    //   disallow any other segment type
    //   (and make sure there's at least one loadable segment)
    // - do the flags make sense? (we only have R and RW, not W...)
    //   note, though, that the ELF spec allows more R/W/X than the segment requests,
    //   so we can always apply R, and then apply W/X as requested
    // - do they overlap?
    //   (no need to sort by p_vaddr! ELF spec says loadable (i.e. PT_LOAD) segments
    //   must be sorted in the program header table by p_vaddr, and check_elf() checks
    //   for this)
    //   (check_elf() also checks for overlap, but at byte granularity instead of page
    //   granularity; we should check here at page granularity)
    // - do the vaddr and paddr make sense? vaddr must be in the TTBR0 range,
    //   and we should probably require paddr == vaddr (for sanity checking;
    //   really we should just ignore paddr)
    // we check that the entrypoint is in a R/X segment, and is 4-byte aligned
    //    and is not in the first 4 KiB (since that's always unmapped)

    /* The number of virtual pages, starting at virtual address 0, that are guaranteed
     * to be unmapped. (Should be nonzero, so that null dereferences are trapped.) */
    const uint64_t UNMAPPED_NULL_PAGES = 1;
    const uint64_t UNMAPPED_NULL_BYTES = UNMAPPED_NULL_PAGES * PAGE_SIZE;

    uint64_t entrypoint = e_header->e_entry;
    if (entrypoint % 4 != 0) {
        dbg_error("program entrypoint must be 4-byte aligned\n");
        errno = EEXEC;
        goto create_error_free_file;
    }
    if (entrypoint < UNMAPPED_NULL_BYTES) {
        dbg_error("program entrypoint must not be in the first %u64 %s of virtual address space\n",
                UNMAPPED_NULL_PAGES,
                UNMAPPED_NULL_PAGES == 1 ? "page" : "pages");
        errno = EEXEC;
        goto create_error_free_file;
    }

    bool saw_loadable_segment = false;
    uint64_t last_aligned_p_vaddr = 0, last_aligned_p_memsz = 0;
    bool entrypoint_in_rx_loadable_segment = false;

    uint8_t *file_bytes = (uint8_t *) file_start;
    elf_ph_entry_t *p_header = (elf_ph_entry_t *) &file_bytes[e_header->e_phoff];
    for (uint16_t s = 0; s < e_header->e_phnum; s++) {
        elf_ph_entry_t *entry = &p_header[s];
        if (!(entry->p_type == PT_NULL || entry->p_type == PT_LOAD || entry->p_type == PT_NOTE)) {
            dbg_error("the only supported segment types are PT_NULL, PT_LOAD, and PT_NOTE\n");
            errno = EEXEC;
            goto create_error_free_file;
        }

        if (entry->p_type == PT_LOAD) {
            if (entry->p_vaddr <= entrypoint && entrypoint - entry->p_vaddr < entry->p_memsz) {
                uint32_t PF_XR = PF_X | PF_R;
                if ((entry->p_flags & PF_XR) == PF_XR) {
                    entrypoint_in_rx_loadable_segment = true;
                } else {
                    dbg_error("program entrypoint must be in a readable and executable loadable segment\n");
                    errno = EEXEC;
                    goto create_error_free_file;
                }
            }

            if (entry->p_paddr != entry->p_vaddr) {
                dbg_error("each segment must have equal p_paddr and p_vaddr\n");
                errno = EEXEC;
                goto create_error_free_file;
            }

            if (entry->p_vaddr > TTBR0_END) {
                dbg_error("each segment p_vaddr must be in the low virtual address space\n");
                errno = EEXEC;
                goto create_error_free_file;
            }
            if (VA_RANGE - entry->p_vaddr < entry->p_memsz) {
                dbg_error("each segment must lie entirely in the low virtual address space\n");
                errno = EEXEC;
                goto create_error_free_file;
            }

            /* Check that segments' pages do not overlap. */
            uint64_t aligned_p_vaddr = entry->p_vaddr,
                     aligned_p_memsz = entry->p_memsz;
            page_align_interval(&aligned_p_vaddr, &aligned_p_memsz);
            //printf("segment %u16: aligned to 0x%p, length 0x%p\n",
            //        s, aligned_p_vaddr, aligned_p_memsz);

            if (saw_loadable_segment) {
                /* Compare against the previous one. */
                panic_if_not(last_aligned_p_vaddr <= aligned_p_vaddr,
                        "byte alignment should imply page alignment; something is wrong\n");
                if (aligned_p_vaddr - last_aligned_p_vaddr < last_aligned_p_memsz) {
                    dbg_error("loadable segments must not overlap pages (in virtual addresses)\n");
                    errno = EEXEC;
                    goto create_error_free_file;
                }
            }

            saw_loadable_segment = true;
            last_aligned_p_vaddr = aligned_p_vaddr;
            last_aligned_p_memsz = aligned_p_memsz;
        }
    }

    // TODO: make sure (using last_aligned_p_vaddr, last_aligned_p_memsz maybe) that
    // the process is not requesting segments to be loaded at crazy-high virtual addresses;
    // we need to leave some room for the heap and stack.

    if (!saw_loadable_segment) {
        dbg_error("program ELF must have a loadable segment\n");
        errno = EEXEC;
        goto create_error_free_file;
    }
    if (!entrypoint_in_rx_loadable_segment) {
        dbg_error("program entrypoint must be in a readable and executable loadable segment\n");
        errno = EEXEC;
        goto create_error_free_file;
    }

    memcpy(proc->name, name, name_len + 1 /* null terminator */);
    proc->is_kernel = false;
    proc->context = CONTEXT_PROCESS;
    proc->is_stopped = false;
    proc->interrupt_fn = NULL;
    proc->is_interrupted = false;

    /* Program "call" arguments (just x0, x1) are set up later. Here, just set up
     * initial values for the other registers in a way that might be helpful for
     * debugging. */
    for (int i = 2; i < 29; i++) {
        proc->regs.x[i] = 0xab0ab0000 + i;
    }

    /* Set x29 (FP) to 0. According to the ARMv8-A ABI, this indicates the end of
     * the chain of frames, so our stacktrace functionality will not try to continue
     * reading frames past this point. */
    proc->regs.x[REG_FP] = 0;

    /* Set x30 (LR) to 0x0. The newly created process is required to make an exit
     * syscall to terminate its execution, and should not expect to make a clean
     * exit simply by returning from its initial entrypoint. This way, if the
     * process ends up returning, it will return to unmapped memory and cause
     * an instruction prefetch abort. */
    proc->regs.x[REG_LR] = 0;

    /* Set SP to the very top of the low virtual address range. We'll map some
     * pages at the top of the ttbr0 range for the process's stack. */
    proc->regs.x[REG_SP] = VA_RANGE;

    proc->regs.pc = entrypoint;

    proc->cwd = cwd;

    proc->is_fork = false;

    /* Set up the process's virtual memory and load the process image. */
    pte_t *ttbr0 = pmalloc_page();
    if (!ttbr0) {
        errno = ENOMEM;
        goto create_error_free_file;
    }
    //printf("ttbr0 is at 0x%p\n", ttbr0);
    memset(ttbr0, PAGE_SIZE, 0);

    uint64_t proc_pages = 0;
    for (uint16_t s = 0; s < e_header->e_phnum; s++) {
        elf_ph_entry_t *entry = &p_header[s];
        if (entry->p_type != PT_LOAD) {
            continue;
        }

        uint64_t aligned_p_vaddr = entry->p_vaddr,
                 aligned_p_memsz = entry->p_memsz;
        page_align_interval(&aligned_p_vaddr, &aligned_p_memsz);
        proc_pages += aligned_p_memsz / PAGE_SIZE;
    }
    //printf("proc_pages = %u64\n", proc_pages);
    uint8_t *proc_memory = pmalloc_pages(proc_pages); // kernel virtual address!
    if (!proc_memory) {
        errno = ENOMEM;
        goto create_error_free_ttbr0;
    }
    proc->proc_memory = proc_memory;

    uint8_t *segment_memory = proc_memory;
    for (uint16_t s = 0; s < e_header->e_phnum; s++) {
        elf_ph_entry_t *entry = &p_header[s];
        if (entry->p_type != PT_LOAD) {
            continue;
        }

        uint8_t *segment_start = &file_bytes[entry->p_offset];

        uint64_t aligned_p_vaddr = entry->p_vaddr,
                 aligned_p_memsz = entry->p_memsz;
        page_align_interval(&aligned_p_vaddr, &aligned_p_memsz);
        uint64_t alignment_offset = entry->p_vaddr - aligned_p_vaddr;

        /* Load the segment into the segment_memory. We're contractually
         * obligated (by the ELF specification) to pad with 0's if the
         * p_memsz is larger than the p_filesz. */
        //Here's what it looks like: (TODO: finish this!)
        // segment_memory                                  segment_memory
        //                                                 + aligned_p_memsz
        // | ... |    segment data   |                     |
        //       segment_memory      segment
        //       + alignment_offset
        memcpy(&segment_memory[alignment_offset],
                segment_start, entry->p_filesz);
        memset(&segment_memory[alignment_offset + entry->p_filesz],
                entry->p_memsz - entry->p_filesz, 0);
        // TODO: clear start and end of segment pages with zeros,
        // so program cannot read old data

        /* Memory map to the physical address of the segment. For the attributes,
         * we only allow writing and execution if the p_flags specifically requests
         * those permissions; reading is always allowed (i.e. we ignore the  PF_R
         * flag) since our PTEs do not support a readable/unreadable flag.*/
        uint64_t attribs = PTE_AF | PTE_INN_SH | PTE_USER;
        if ((entry->p_flags & PF_X) == 0) {
            /* Non-executable. */
            attribs |= PTE_XN_EL0 | PTE_XN_EL1;
        }
        if ((entry->p_flags & PF_W) != 0) {
            /* Writeable. */
            attribs |= PTE_RW;
        } else {
            /* Non-writeable. */
            attribs |= PTE_RO;
        }

        int32_t map_res = map_virt_to_phys(ttbr0, NULL,
                aligned_p_vaddr, aligned_p_memsz,
                kernel_virt_to_phys(segment_memory),
                attribs,
                PTE_MEM,
                MAP_CREATE);
        panic_if_not(map_res == MAP_RES_SUCCESS || map_res == MAP_RES_OOM,
                "could not map segment %u16: res=%i32\n", s, map_res);
        if (map_res == MAP_RES_OOM) {
            dbg_error("could not map segment %u16: res=%i32\n", s, map_res);
            errno = ENOMEM;
            goto create_error_free_proc_memory;
        }

        segment_memory += aligned_p_memsz;
    }

    /* Unmap the UNMAPPED_NULL_PAGES at the start of virtual memory, so that
     * null dereferences are trapped. */
    int32_t null_map_res = map_virt_to_phys(ttbr0, NULL,
            0, UNMAPPED_NULL_PAGES * PAGE_SIZE,
            UNMAPPED,
            0, 0, 0);
    panic_if_not(null_map_res == MAP_RES_SUCCESS || null_map_res == MAP_RES_OOM,
            "could not unmap null region: res=%i32\n", null_map_res);
    if (null_map_res == MAP_RES_OOM) {
        dbg_error("could not unmap null region: res=%i32\n", null_map_res);
        errno = ENOMEM;
        goto create_error_free_proc_memory;
    }

    /* We need to allocate and map the process's initial heap. */
    const uint64_t HEAP_PAGES = 10;
    ptr_t heap = pmalloc_pages(HEAP_PAGES);
    if (!heap) {
        errno = ENOMEM;
        goto create_error_free_proc_memory;
    }
    proc->heap_memory = heap;
    // TODO: find room for the heap... what if the ELF requested segments to be loaded
    // right here? (actually, TODO above will make sure that we have room for the heap
    // at a reasonable location)
    proc->heap_start = (ptr_t) (VA_RANGE - PAGE_SIZE * 512);
    proc->heap_pages = HEAP_PAGES;
    int32_t heap_map_res = map_virt_to_phys(ttbr0, NULL,
        (uint64_t) proc->heap_start, proc->heap_pages * PAGE_SIZE,
        kernel_virt_to_phys(heap),
        PTE_XN_EL0 | PTE_XN_EL1 | PTE_AF | PTE_INN_SH | PTE_RW | PTE_USER,
            PTE_MEM,
            MAP_CREATE);
    panic_if_not(heap_map_res == MAP_RES_SUCCESS || heap_map_res == MAP_RES_OOM,
            "could not map heap: res=%i32\n", heap_map_res);
    if (heap_map_res == MAP_RES_OOM) {
        dbg_error("could not map heap: res=%i32\n", heap_map_res);
        errno = ENOMEM;
        goto create_error_free_heap;
    }

    /* We need to allocate and map the process's stack. */
    // This just gives the process a STACK_PAGES-sized stack (and no stack protection...).
    const uint64_t STACK_PAGES = 5;
    ptr_t stack = pmalloc_pages(STACK_PAGES);
    if (!stack) {
        errno = ENOMEM;
        goto create_error_free_heap;
    }
    proc->stack_memory = stack;
    //printf("[kernel] mapping stack\n");
    int32_t stack_map_res = map_virt_to_phys(ttbr0, NULL,
        proc->regs.x[REG_SP] - STACK_PAGES * PAGE_SIZE, STACK_PAGES * PAGE_SIZE,
        kernel_virt_to_phys(stack),
        PTE_XN_EL0 | PTE_XN_EL1 | PTE_AF | PTE_INN_SH | PTE_RW | PTE_USER,
            PTE_MEM,
            MAP_CREATE);
    panic_if_not(stack_map_res == MAP_RES_SUCCESS || stack_map_res == MAP_RES_OOM,
            "could not map stack: res=%i32\n", stack_map_res);
    if (stack_map_res == MAP_RES_OOM) {
        dbg_error("could not map stack: res=%i32\n", stack_map_res);
        errno = ENOMEM;
        goto create_error_free_stack;
    }

    /* We set up the process as if the entrypoint is called as
     * entrypoint(args, args_length). */
    proc->regs.x[1] = args_length;

    /* Depending on the size of the argument buffer, we may need to allocate aux_memory
     * and place the buffer there instead of in the stack_memory. Set up x0 to point
     * to the buffer. */
    if (args_length > PAGE_SIZE) {
        /* Too big; place it in aux_memory. */
        panic("not implemented");
    } else {
        /* Push the buffer to the stack. We may as well 16-byte align the buffer,
         * and then just point the SP at the buffer's start. */
        proc->aux_memory = NULL;
        if (args_length == 0) {
            proc->regs.x[0] = 0;
        } else {
            uint64_t buf_space = align_up(args_length, 16);
            uint8_t *stack_buf = (uint8_t *) proc->stack_memory + STACK_PAGES * PAGE_SIZE - buf_space;

            memcpy(stack_buf, args, args_length);

            proc->regs.x[REG_SP] -= buf_space;
            proc->regs.x[0] = proc->regs.x[REG_SP];
        }
    }

    proc->regs.ttbr0 = kernel_virt_to_phys(ttbr0);

    /*
     * PSTATE format (i.e. via any CPSR_ELn):
     *  31  30  29  28   27-22   21   20   19-10   9   8   7   6   5   4    3-0
     * | N | Z | C | V | ----- | SS | IL | ----- | D | A | I | F | - | M | M[3:0] |
     * NZCV: ALU flags (negative, zero, carry out, overflow); doesn't matter for us (so make it 0)
     * SS: software step; we want this to be 0
     * IL: illegal execution; we want this to be 0
     * DAIF: exception masking; we want to mask DAF and unmask I (IRQs)
     * M: execution state that we `eret` to, with 0 = aarch64; we want 0
     * M[3:2]: exception level that we `eret` to; we set this to EL0
     * M[1]: not sure; should probably be 0 (TODO: find out)
     * M[0]: SPSel; doesn't matter for user processes (TODO: find out why this is undocumented)
     *
     * That is, we just need to set D, A, F, M[3:2], and M[0]
     */
    proc->regs.pstate = 0x340; /* 0b11_0100_0000 */
    proc->regs.pstate |= 0x0 /* 0b0000 */;

    proc->parent = parent;
    proc->wait_id = -1; /* Not waiting. */

    for (signal_nr_t i = 0; i < NUM_SIG; i++) {
        proc->signal_handler[i] = SIG_DFL;
    }
    create_lock(&proc->signals_lock);
    proc->signals = empty_signals_list();

    /* Copy the file descriptors given, and invalidate all others.
     * We don't dup_file_desc() the descriptors, since that's up to the caller. */
    for (uint64_t i = 0; i < num_fd; i++) {
        proc->fds[i] = fds[i];
    }
    for (uint64_t i = num_fd; i < MAX_FDS; i++) {
        proc->fds[i].valid = false;
    }

    /* Now the process is fully initialized and ready to be executed. */
    if (make_ready) {
        acquire_irq_lock(&process_lists_lock);
        enqueue_process(&processes_ready, proc);
        release_irq_lock(&process_lists_lock);
    }

    return 0;

create_error_free_stack:
    //printf("[kernel] create_error_free_stack\n");
    pfree_pages(stack);
create_error_free_heap:
    pfree_pages(heap);
create_error_free_proc_memory:
    //printf("[kernel] create_error_free_proc_memory\n");
    pfree_pages(proc_memory);
create_error_free_ttbr0:
    //printf("[kernel] create_error_free_ttbr0\n");
    /* Do not free the referenced blocks, just the page table itself. */
    free_page_table(ttbr0, PTW_START_LEVEL, false);
create_error_free_file:
    pfree(file_start);
create_error_deallocate_proc:
    proc->parent = NULL;
    //printf("[kernel] create_error_deallocate_proc\n");
    if (deallocate_on_failure) {
        deallocate_process(proc);
    }
    if (close_fds_on_failure) {
        for (uint32_t i = 0; i < num_fd; i++) {
            if (fds[i].valid) {
                close_file_desc(&fds[i]);
            }
        }
    }
    if (close_cwd_on_failure) {
        //printf("proc:closing cwd (inode %u64) due to error setting up user process\n",
        //        cwd.file->inode_nr);
        filesys_close(cwd);
    }
    //printf("[kernel] setup_user_process() failed\n");
    return -(int64_t) errno;
}

void kernel_process_stub(kernel_process_fn_t proc_main, void *aux)
{
    uint32_t exit_code = proc_main(aux);
    //printf("kernel process '%qs' (PID %u32) is exiting (code %u32)\n",
    //        current_process()->name, current_process()->id, exit_code);
    _syscall1(SYSCALL_EXIT, (uint64_t) exit_code);
}

USE_VALUE int64_t setup_kernel_process(proc_t *proc, const char *name, kernel_process_fn_t proc_main,
        void *aux, proc_t *parent,
        bool make_ready, bool deallocate_on_failure)
{
    errno_t errno;

    uint64_t name_len = strlen(name);
    if (name_len > 127) {
        dbg_error("process name must be at most 127 characters long; got '%qs'\n", name);
        errno = EINVAL;
        goto create_kernel_error_deallocate_proc;
    }

    memcpy(proc->name, name, name_len + 1 /* null terminator */);
    proc->is_kernel = true;
    proc->context = CONTEXT_PROCESS;
    proc->is_stopped = false;
    proc->interrupt_fn = NULL;
    proc->is_interrupted = false;

    /* Program "call" arguments (just x0, x1) are passed into kernel_process_stub.
     * We just set up initial values for the other registers in a way that might be
     * helpful for debugging. */
    proc->regs.x[0] = (uint64_t) proc_main;
    proc->regs.x[1] = (uint64_t) aux;
    for (int i = 2; i < 29; i++) {
        proc->regs.x[i] = 0xab0ab0000 + i;
    }

    /* Set x29 (FP) to 0. According to the ARMv8-A ABI, this indicates the end of
     * the chain of frames, so our stacktrace functionality will not try to continue
     * reading frames past this point. */
    proc->regs.x[REG_FP] = 0;

    /* Set x30 (LR) to 0x0. The newly created process is required to make an exit
     * syscall to terminate its execution, and should not expect to make a clean
     * exit simply by returning from its initial entrypoint (the kernel_process_stub).
     * This way, if the process ends up returning, it will return to unmapped memory
     * and cause an instruction prefetch abort. */
    proc->regs.x[REG_LR] = 0;

    proc->regs.pc = (uint64_t) &kernel_process_stub;

    proc->cwd = (open_file_t) { .mount = NULL, .file = NULL };

    proc->is_fork = false;
    proc->proc_memory = NULL;
    proc->heap_memory = NULL;
    proc->aux_memory = NULL;
    proc->heap_start = NULL;
    proc->heap_pages = 0;

    /* We need to allocate and set up the process's stack. */
    // This just gives the process a STACK_PAGES-sized stack (and no stack protection...).
    const uint64_t STACK_PAGES = 5;
    ptr_t stack = pmalloc_pages(STACK_PAGES);
    if (!stack) {
        errno = ENOMEM;
        goto create_kernel_error_deallocate_proc;
    }
    proc->stack_memory = stack;
    /* The kernel process uses only the kernel address mapping, so we point its SP directly
     * to (the top of) the allocated stack. */
    proc->regs.x[REG_SP] = (uint64_t) stack + STACK_PAGES * PAGE_SIZE;

    proc->regs.ttbr0 = 0;

    /*
     * PSTATE format (i.e. via any CPSR_ELn):
     *  31  30  29  28   27-22   21   20   19-10   9   8   7   6   5   4    3-0
     * | N | Z | C | V | ----- | SS | IL | ----- | D | A | I | F | - | M | M[3:0] |
     * NZCV: ALU flags (negative, zero, carry out, overflow); doesn't matter for us (so make it 0)
     * SS: software step; we want this to be 0
     * IL: illegal execution; we want this to be 0
     * DAIF: exception masking; we want to mask DAF and unmask I (IRQs)
     * M: execution state that we `eret` to, with 0 = aarch64; we want 0
     * M[3:2]: exception level that we `eret` to; we set this according to EL1
     * M[1]: not sure; should probably be 0 (TODO: find out)
     * M[0]: SPSel; we set this to 1 (TODO: find out why this is undocumented)
     *
     * That is, we just need to set D, A, F, M[3:2], and M[0]
     */
    proc->regs.pstate = 0x340; /* 0b11_0100_0000 */
    proc->regs.pstate |= 0x5 /* 0b0101 */;

    proc->parent = parent;
    proc->wait_id = -1; /* Not waiting. */

    for (signal_nr_t i = 0; i < NUM_SIG; i++) {
        proc->signal_handler[i] = SIG_DFL;
    }
    create_lock(&proc->signals_lock);
    proc->signals = empty_signals_list();
    proc->usermode_signals_masked = false;
    proc->signal_processing_pending = false;

    /* Invalidate all file descriptors. */
    for (uint64_t i = 0; i < MAX_FDS; i++) {
        proc->fds[i].valid = false;
    }

    /* Now the process is fully initialized and ready to be executed. */
    if (make_ready) {
        acquire_irq_lock(&process_lists_lock);
        enqueue_process(&processes_ready, proc);
        release_irq_lock(&process_lists_lock);
    }

    return 0;

create_kernel_error_deallocate_proc:
    proc->parent = NULL;
    //printf("[kernel] create_error_deallocate_proc\n");
    if (deallocate_on_failure) {
        deallocate_process(proc);
    }
    //printf("[kernel] setup_user_process() failed\n");
    return -errno;
}

int64_t replace_process(proc_t *original, const char *name, const char *filepath,
        ptr_t args, uint64_t args_length)
{
    //printf("[kernel] replace_process('%qs' PID %u32, name '%qs', filename '%qs')\n",
    //        original->name, original->id, name, filename);

    /* Use stack space for a temporary process. This may become an issue if control blocks become
     * much larger, but right now they're less than 2 KiB, so it should be fine.
     * We need to use a temporary process (and ideally not allocate it from the main process list)
     * so that we can avoid replacing the original process in-situ; this way, we can set up a new
     * process control block and then, only on success, overwrite the original process with the
     * freshly set up control block. That way, on failure the original process continues executing
     * without any changes */
    STATIC_ASSERT(sizeof(proc_t) <= 2048, "consider not allocating the temporary process on the stack");
    proc_t proc;

    /* Do not have setup_user_process() set up the file descriptors; we'll handle it ourselves. */
    int64_t setup_errno = setup_user_process(
            &proc, name,
            original->cwd, filepath,
            args, args_length, original->parent,
            NULL, 0, false,
            false, false, false);
    if (setup_errno) {
        /* Nothing to free; setup_user_process() releases all resources owned by
         * the newly allocated process. */
        return setup_errno;
    }

    /* We have to be careful about how exactly we kill the original process
     * and start the new process, since we might actually be running as the
     * original process right now. As long as the process_lists_lock is
     * acquired, the currently executing process cannot get preempted, so
     * we just have to deallocate the original process and then make ready
     * the new process while holding the lock. Afterwards, we can explicitly
     * preempt the current process, in case the current and original processes
     * are in fact the same. */
    free_process(original);

    acquire_irq_lock(&process_lists_lock);
    remove_process(&processes_ready, original);

    /* Now neither the original nor the new process is in any list, so we can
     * freely modify their control blocks to do the final replacement. */

    /* Maintain the original id. */
    /* Maintain the original gid. */
    memcpy(original->name, proc.name, sizeof(original->name));
    original->is_kernel = proc.is_kernel;
    /* Maintain the original context. */
    original->is_stopped = proc.is_stopped;
    original->interrupt_fn = proc.interrupt_fn;
    original->is_interrupted = proc.is_interrupted;
    original->regs = proc.regs;
    /* Do not reset syscall_regs; its value is replaced before use. */
    /* Maintain the original cwd. */
    original->is_fork = proc.is_fork;
    original->proc_memory = proc.proc_memory;
    original->heap_memory = proc.heap_memory;
    original->stack_memory = proc.stack_memory;
    original->aux_memory = proc.aux_memory;
    original->heap_start = proc.heap_start;
    original->heap_pages = proc.heap_pages;
    /* Do not reset syscall_stack; its value is replaced before use. */
    original->wakeup_time = proc.wakeup_time;
    /* Maintain the original parent. */
    original->exit_code = proc.exit_code;
    original->wait_id = proc.wait_id;
    memcpy(original->signal_handler, proc.signal_handler, sizeof(signal_handler_t) * NUM_SIG);
    original->signals_lock = proc.signals_lock;
    original->signals = proc.signals;
    original->usermode_signals_masked = proc.usermode_signals_masked;
    original->signal_processing_pending = proc.signal_processing_pending;

    /* Close any open file descriptors marked as CLOSE_ON_EXEC. Maintain
     * any others. */
    for (uint64_t i = 0; i < MAX_FDS; i++) {
        file_descriptor_t *fd = &original->fds[i];
        if (fd->valid && fd->flags & FD_CLOSE_ON_EXEC) {
            close_file_desc(fd);
        }
    }

    enqueue_process(&processes_ready, original);
    release_irq_lock(&process_lists_lock);

    return 0;
}

// TODO: this should probably live in virtual_memory.h
/* Returns new table, or NULL if out of memory.
 * Does not leak memory on failure. */
USE_VALUE pte_t *copy_page_table(pte_t *table, uint32_t level)
{
    panic_if_not(level <= 3,
            "page tables only exist at levels 0-3; got level %u32", level);

    pte_t *new_table = pmalloc_page();
    if (!new_table) {
        return NULL;
    }

    for (uint32_t i = 0; i < 512; i++) {
        //printf("%s%s%sentry %u32: ",
        //        level > 0 ? "  " : "",
        //        level > 1 ? "  " : "",
        //        level > 2 ? "  " : "",
        //        i);
        pte_t entry = table[i];
        pte_t new_entry;

        if (pte_is_table(entry, level)) {
            uint64_t phys_addr = table_pte_phys_addr(entry);
            //printf("table(phys=0x%p)\n", phys_addr);
            pte_t *next_table = phys_to_kernel_virt(phys_addr);
            pte_t *new_next_table = copy_page_table(next_table, level + 1);
            // TODO: free allocated memory before quitting
            panic_if_not(new_next_table, "not implemented");
            new_entry = pte_encode_table(kernel_virt_to_phys(new_next_table));
        } else if (pte_is_block(entry, level)) {
            uint64_t phys_addr = block_pte_phys_addr(entry, level);
            uint64_t attribs = block_pte_attribs(entry, level);
            uint64_t mem_type = block_pte_mem_type(entry);
            //printf("block(phys=0x%p)\n", phys_addr);
            ptr_t block = phys_to_kernel_virt(phys_addr);
            uint64_t num_pages = (uint64_t) 1 << (9 * (3 - level));

            /* Copy the entire block. */
            ptr_t new_block = pmalloc_pages(num_pages);
            // TODO: free allocated memory before quitting
            panic_if_not(new_block, "not implemented");
            memcpy(new_block, block, num_pages * PAGE_SIZE);
            new_entry = pte_encode_block(
                    level, attribs, mem_type,
                    kernel_virt_to_phys(new_block));
        } else if (pte_is_unmapped(entry)) {
            //printf("unmapped\n");
            new_entry = pte_encode_unmapped();
        } else {
            panic("should not get here\n");
        }

        new_table[i] = new_entry;
    }

    return new_table;
}

// TODO: this should probably live in virtual_memory.h
/* Returns new ttbr0, or 0 if out of enough memory.
 * Does not leak memory on failure. */
USE_VALUE uint64_t copy_virtual_memory(uint64_t ttbr0)
{
    ptr_t new_base_table = copy_page_table(phys_to_kernel_virt(ttbr0), PTW_START_LEVEL);
    if (new_base_table) {
        return kernel_virt_to_phys(new_base_table);
    } else {
        // TODO: free any allocated resources
        panic("not implemented");
        return 0;
    }
}

int64_t fork_process(proc_t *original, proc_t **forked)
{
    /* We only set *forked on success. */
    *forked = NULL;

    proc_t *proc = allocate_process();
    if (!proc) {
        return -EFULL;
    }

    errno_t errno;

    /* We always set up the forked process in the process context
     * instead of in the syscall context, even though we're likely
     * currently executing in the FORK syscall context of the original
     * process. */

    memcpy(proc->name, original->name, sizeof(proc->name));
    proc->is_kernel = original->is_kernel;
    proc->context = CONTEXT_PROCESS;
    proc->is_stopped = false;
    proc->interrupt_fn = NULL;
    proc->is_interrupted = false;
    proc->regs = original->regs;
    proc->regs.x[0] = 0;
    proc->cwd = original->cwd;
    proc->is_fork = true;
    proc->proc_memory = NULL;
    proc->heap_memory = NULL;
    proc->stack_memory = NULL;
    proc->aux_memory = NULL;
    proc->heap_start = original->heap_start;
    proc->heap_pages = original->heap_pages;
    proc->wakeup_time = 0;
    proc->parent = original;
    proc->exit_code = 0;
    proc->wait_id = -1;
    memcpy(proc->signal_handler, original->signal_handler, sizeof(signal_handler_t) * NUM_SIG);
    create_lock(&proc->signals_lock);
    proc->signals = empty_signals_list();
    proc->usermode_signals_masked = false;
    proc->signal_processing_pending = false;
    /* First reset all descriptors, and then start copying them from the
     * original. We have to reset first so that on error, we don't get
     * confused about which ones are actually initialized or not. */
    for (uint32_t i = 0; i < MAX_FDS; i++) {
        proc->fds[i].valid = false;
    }
    for (uint32_t i = 0; i < MAX_FDS; i++) {
        if (original->fds[i].valid) {
            errno = dup_file_desc(&original->fds[i]);
            if (errno) {
                goto fork_error_close_descriptors;
            }
        }
        proc->fds[i] = original->fds[i];
    }

    /* Forking kernel processes is not exactly possible, since we cannot
     * assign the fork and the original the same virtual address space for
     * their stacks. */
    panic_if(original->is_kernel, "cannot fork kernel process\n");

    /* Now we can copy the image of the original process, if required. */
    // TODO: do copy-on-write instead of just copying everything.
    //printf("[kernel] forking virtual memory\n");
    uint64_t new_ttbr0 = copy_virtual_memory(original->regs.ttbr0);
    if (!new_ttbr0) {
        errno = ENOMEM;
        goto fork_error_close_descriptors;
    }
    proc->regs.ttbr0 = new_ttbr0;

    /* Before we return, mark the cwd as duplicated. */
    //printf("proc:dup cwd (inode %u64) due to forking '%qs' (%u32)\n",
    //        original->cwd.file->inode_nr, original->name, original->id);
    errno = filesys_dup(original->cwd);
    if (errno) {
        goto fork_error_close_descriptors;
    }

    assign_fresh_process_id(proc);
    proc->gid = original->gid;

    /* Now the process is fully initialized and ready to be executed. */
    acquire_irq_lock(&process_lists_lock);
    enqueue_process(&processes_ready, proc);
    release_irq_lock(&process_lists_lock);

    *forked = proc;
    return 0;

fork_error_close_descriptors:
    /* Close all file descriptors we successfully copied. */
    for (uint32_t i = 0; i < MAX_FDS; i++) {
        if (proc->fds[i].valid) {
            close_file_desc(&proc->fds[i]);
        }
    }
//fork_error_deallocate_proc:
    proc->parent = NULL;
    deallocate_process(proc);
    return -errno;
}

int64_t fork_user(const char *name, open_file_t cwd, const char *filepath,
        ptr_t args, uint64_t args_length,
        proc_t **child)
{
    proc_t *proc = current_process();

    *child = allocate_process();
    if (!*child) {
        return -EFULL;
    }

    file_descriptor_t child_fds[MAX_FDS];
    for (uint64_t i = 0; i < MAX_FDS; i++) {
        file_descriptor_t *fd = &proc->fds[i];
        if (!fd->valid || (fd->flags & FD_CLOSE_ON_EXEC)) {
            child_fds[i].valid = false;
        } else {
            errno_t errno = dup_file_desc(fd);
            if (errno) {
                /* We have to close all the interfaces we already
                 * duplicated. */
                for (uint64_t j = 0; j < i; j++) {
                    if (child_fds[j].valid) {
                        close_file_desc(&child_fds[j]);
                    }
                }
                deallocate_process(*child);
                *child = NULL;
                return -(int64_t) errno;
            }
            child_fds[i] = *fd;
        }
    }

    int64_t setup_errcode = setup_user_process(*child, name,
            cwd, filepath,
            args, args_length,
            proc,
            child_fds, MAX_FDS, true,
            false, true, true);

    if (setup_errcode) {
        *child = NULL;
        return setup_errcode;
    }

    assign_fresh_process_id(*child);
    (*child)->gid = proc->gid;

    acquire_irq_lock(&process_lists_lock);
    enqueue_process(&processes_ready, *child);
    release_irq_lock(&process_lists_lock);

    return 0;
}

int64_t fork_kernel(const char *name, kernel_process_fn_t proc_main,
        void *aux,
        proc_t **child)
{
    proc_t *proc = current_process();

    *child = allocate_process();
    if (!*child) {
        return -EFULL;
    }

    int64_t setup_errcode = setup_kernel_process(*child, name, proc_main, aux,
            proc, false, true);

    if (setup_errcode) {
        *child = NULL;
        return setup_errcode;
    }

    assign_fresh_process_id(*child);
    (*child)->gid = proc->gid;

    acquire_irq_lock(&process_lists_lock);
    enqueue_process(&processes_ready, *child);
    release_irq_lock(&process_lists_lock);

    return 0;
}

INLINE proc_t *current_process()
{
    return current_proc;
}

INLINE void foreach_process(process_action_t action, void *aux)
{
    for (proc_t *proc = &all_procs[0]; proc < all_procs + MAX_PROCS; proc++) {
        if (!action(proc, aux)) {
            return;
        }
    }
}

INLINE void foreach_allocated_process(process_action_t action, void *aux)
{
    for (proc_t *proc = &all_procs[0]; proc < all_procs + MAX_PROCS; proc++) {
        if (process_is_allocated(proc) && !action(proc, aux)) {
            return;
        }
    }
}

typedef struct {
    uint32_t gid;
    process_action_t action;
    void *aux;
} foreach_in_group_state_t;
INLINE bool foreach_in_group_action(proc_t *proc, void *aux)
{
    foreach_in_group_state_t *state = (foreach_in_group_state_t *) aux;
    if (proc->gid == state->gid) {
        return state->action(proc, state->aux);
    } else {
        return true; /* continue */
    }
}
INLINE void foreach_process_in_group(uint32_t gid, process_action_t action, void *aux)
{
    foreach_in_group_state_t state = {
        .gid = gid,
        .action = action,
        .aux = aux
    };
    foreach_allocated_process(foreach_in_group_action, &state);
}

typedef struct {
    uint32_t search_id;
    proc_t *found;
} process_with_id_state_t;
INLINE bool process_with_id_action(proc_t *proc, void *aux)
{
    process_with_id_state_t *state = aux;
    if (proc->id == state->search_id) {
        state->found = proc;
        return false; /* break */
    } else {
        return true; /* continue */
    }
}
proc_t *process_with_id(uint32_t id)
{
    // TODO: better way than linear search?
    process_with_id_state_t state = { id, NULL };
    foreach_allocated_process(process_with_id_action, &state);
    // sanity check
    panic_if_not(!state.found || state.found->id == id,
            "sanity check failed\n");
    return state.found;
}

void yield_process()
{
    // TODO: this is such a hack
    timer_interrupt_absolute(0);
    asm volatile("nop"); // not sure if this is even required...
}

void prepare_irq_wait()
{
    proc_t *proc = current_process();
    proc->interrupt_fn = &wakeup_irq;
}

void irq_wait()
{
    proc_t *proc = current_process();

    if (proc->is_interrupted) {
        /* Short-circuit the sleep if we're already interrupted (for any reason). */
        return;
    }

    acquire_irq_lock(&process_lists_lock);
    remove_process(&processes_ready, proc);
    release_irq_lock(&process_lists_lock);
    yield_process();
}

void wakeup_irq(proc_t *proc)
{
    //TODO: have each proc store which list it's in, so this is O(1) instead of O(processes ready).
    bool already_ready = false;
    forall_processes(ready, processes_ready) {
        if (ready == proc) {
            already_ready = true;
            break;
        }
    }
    if (!already_ready) {
        enqueue_process(&processes_ready, proc);
    }
    proc->interrupt_fn = NULL;
}

void run_syscall();
void return_to_process(proc_t *proc, ptr_t regs)
{
    panic_if_not(in_context_preemptible(),
            "return_to_process() must be called from a preemptible context\n");
    panic_if(interrupts_enabled(),
            "return_to_process() must be called with interrupts disabled\n");

    // TODO: reduce duplication with handle_process_syscall().
    /* Check for the process indicating that it needs to return to the kernel for
     * some signal processing before it next runs in usermode. */
    if (proc->signal_processing_pending && proc->context == CONTEXT_PROCESS) {
        /* Cause the process to continue running as if it had made a 'kern' syscall. */
        copy_regs(proc->syscall_regs.x, proc->regs.x);
        proc->syscall_regs.x[0] = SYSCALL_KERN;
        proc->syscall_regs.x[REG_SP] = (uint64_t) proc->syscall_stack;
        proc->syscall_regs.pc = (uint64_t) &run_syscall;
        proc->syscall_regs.pstate = proc->regs.pstate;
        proc->syscall_regs.pstate |= 0x5; /* 0b0101: execute in EL1 */
        proc->syscall_regs.ttbr0 = proc->regs.ttbr0;
        proc->context = CONTEXT_SYSCALL;
        regs = proc->syscall_regs.x;
        /* Store process registers into the system registers (particularly ELR_EL1
         * and SPSR_EL1). */
        store_process_regs(proc, NULL);
    }

    if (!regs) {
        regs = proc->context == CONTEXT_SYSCALL ? proc->syscall_regs.x : proc->regs.x;
    }

    if (has_privileged_execution(proc)) {
        return_to_process_el1(regs);
    } else {
        return_to_process_el0(regs);
    }
}

/* Must only be called from an IRQ context.
 * This function decides on the next process to execute, then stores its process state
 * into system registers and the given 'process_regs' memory block. */
void schedule_next_process(ptr_t process_regs)
{
    panic_if_not(in_context_irq(),
            "schedule_next_process() must only be called from an IRQ context\n");

    /* Make sure to wake up any threads past their due date. */
    uint64_t time = timer_counter();
    while (!is_empty(&processes_sleeping) && time >= processes_sleeping.head->wakeup_time) {
        proc_t *awoken_proc = remove_head_process(&processes_sleeping);
        awoken_proc->interrupt_fn = NULL;
        enqueue_process(&processes_ready, awoken_proc);
    }

    /* If a process is waiting on the timer, we may as well request an
     * interrupt for its wakeup time. Otherwise, we at least need to
     * make sure that the timer won't continuously interrupt us, by
     * requesting an interrupt in the far-off future. */
    if (is_empty(&processes_sleeping)) {
        timer_interrupt_absolute(UINT64_MAX);
    } else {
        timer_interrupt_absolute(processes_sleeping.head->wakeup_time);
    }

    /* Check that (at least) the idle process is fully ready to run, i.e. that
     * it's in the ready queue. */
    // TODO: once processes store the list they're in, this is O(1).
    if (idle_proc) {
        bool idle_ready = false;
        forall_processes(proc, processes_ready) {
            if (proc == idle_proc) {
                idle_ready = true;
                break;
            }
        }
        panic_if_not(idle_ready,
                "the idle process should always be ready to run\n");
    }

    /* Select the next process to run. Currently just do round-robin scheduling
     * between all ready processes. */
    proc_t *next_proc = dequeue_process(&processes_ready);
    enqueue_process(&processes_ready, next_proc);
    /* Skip the idle proc if there's any other proc to run. */
    // TODO: longer term solution: multiple priority levels
    if (next_proc == idle_proc && processes_ready.head != idle_proc) {
        next_proc = dequeue_process(&processes_ready);
        enqueue_process(&processes_ready, next_proc);
    }

    panic_if(next_proc->context == CONTEXT_EXITED,
            "scheduler chose an exited process to run next\n");

    //if (next_proc != current_proc) {
    //    //printf("(%s=>%s)", current_proc->name, next_proc->name);
    //    printf("[%u32=>%u32]", current_proc->id, next_proc->id);
    //}
    current_proc = next_proc;

    store_process_regs(next_proc, process_regs);

    timer_interrupt_relative(ms_to_ticks(CYCLE_MS));
}

void interrupt_process(proc_t *proc)
{
    irq_lock_t irq_lock;
    acquire_irq_lock(&irq_lock);
    proc->is_interrupted = true;
    if (proc->interrupt_fn) {
        proc->interrupt_fn(proc);
    }
    release_irq_lock(&irq_lock);
}

bool was_interrupted()
{
    proc_t *proc = current_process();

    bool interrupted;
    irq_lock_t irq_lock;

    acquire_irq_lock(&irq_lock);
    interrupted = proc->is_interrupted;
    proc->is_interrupted = false;
    release_irq_lock(&irq_lock);

    return interrupted;
}

void deliver_signal(proc_t *proc, signal_t *signal)
{
    acquire_lock(&proc->signals_lock);
    enqueue_signal(&proc->signals, signal);
    proc->signal_processing_pending = true;
    release_lock(&proc->signals_lock);

    interrupt_process(proc);
}

INLINE bool deliver_group_signal_action(proc_t *proc, void *aux)
{
    signal_nr_t nr = *((signal_nr_t *) aux);
    signal_t *signal = alloc_signal(nr, 0, NULL);
    if (signal) {
        deliver_signal(proc, signal);
    } else {
        printf("[could not allocate signal]\n");
    }
    return true; /* continue */
}
void deliver_group_signal(uint32_t gid, signal_nr_t nr)
{
    foreach_process_in_group(gid, &deliver_group_signal_action, &nr);
}

// TODO: move some of syscall.c into here, so we don't have to define the prototype
// here...
void syscall_exit(uint32_t exit_code);
__attribute__((noreturn)) void exit_current_process()
{
    syscall_exit(255);
    __builtin_unreachable();
}

bool is_usermode_signal(proc_t *proc, signal_t *signal)
{
    panic_if(signal->nr >= NUM_SIG,
            "invalid signal number "signal_nr_spec"\n",
            signal->nr);
    signal_handler_t handler = proc->signal_handler[signal->nr];
    return !(handler == SIG_DFL || handler == SIG_IGN);
}

void interrupt_stopped_process(proc_t *proc)
{
    enqueue_process(&processes_ready, proc);
    proc->is_interrupted = false;
    proc->interrupt_fn = NULL;
}

// TODO: this should be in virtual_memory.h!
bool is_user_writeable(ptr_t virt_addr_start, uint64_t length);

errno_t process_signal(signal_t *signal)
{
    proc_t *proc = current_process();

    signal_handler_t handler = proc->signal_handler[signal->nr];

    if (handler == SIG_DFL) {
        switch (signal->nr) {
            case SIGKILL:
                printf("[%qs (%u32) killed by SIGKILL]\n", proc->name, proc->id);
                exit_current_process();
                break;
            case SIGINT:
                printf("[%qs (%u32) killed by SIGINT]\n", proc->name, proc->id);
                exit_current_process();
                break;
            case SIGTERM:
                printf("[%qs (%u32) killed by SIGTERM]\n", proc->name, proc->id);
                exit_current_process();
                break;
            case SIGSTOP:
            case SIGTTIN:
            case SIGTTOU:
                if (proc->is_stopped) {
                    printf("[%s: %qs (%u32) already stopped]\n",
                            signal->nr == SIGSTOP ? "SIGSTOP" :
                            signal->nr == SIGTTIN ? "SIGTTIN" :
                            signal->nr == SIGTTOU ? "SIGTTOU" :
                            "SIG????",
                            proc->name, proc->id);
                } else {
                    proc->is_stopped = true;
                    printf("[%qs (%u32) stopped by %s]\n", proc->name, proc->id,
                            signal->nr == SIGSTOP ? "SIGSTOP" :
                            signal->nr == SIGTTIN ? "SIGTTIN" :
                            signal->nr == SIGTTOU ? "SIGTTOU" :
                            "SIG????");
                }
                return 0;
            case SIGCONT:
                if (!proc->is_stopped) {
                    printf("[SIGCONT: %qs (%u32) was not stopped]\n", proc->name, proc->id);
                } else {
                    panic_if(proc->interrupt_fn,
                            "interrupt_stopped_process() should have been called!\n");
                    proc->is_stopped = false;
                    printf("[%qs (%u32) continuing]\n", proc->name, proc->id);
                }
                return 0;
            case SIGCHLD:
                /* Ignore it. */
                return 0;
            default:
                panic("unexpected signal number "signal_nr_spec"\n",
                        signal->nr);
                break;
        }
    } else if (handler == SIG_IGN) {
        panic_if(signal->nr == SIGKILL || signal->nr == SIGSTOP || signal->nr == SIGCONT,
                "SIGKILL, SIGSTOP, and SIGCONT should only have default handlers\n");
        return 0;
    } else {
        panic_if(signal->nr == SIGKILL || signal->nr == SIGSTOP || signal->nr == SIGCONT,
                "SIGKILL, SIGSTOP, and SIGCONT should only have default handlers\n");

        /* Set up the stack and registers as follows (stack growing down):
         * +--------------------+
         * | program call stack |
         * +--------------------+
         * | saved registers:   |
         * |   pc               |
         * |   x31 (sp)         |
         * |   x30 (lr)         |
         * |   x29 (fp)      <----- new fp
         * |   x28              |
         * |   ...              |
         * |   x0            <----- new x3
         * +--------------------+
         * | signal buffer   <----- new x2
         * | (aligned)          |       ||
         * +--------------------+       ||
         *                   <----- new sp
         * new lr points to 0 (the signal handler should not return except
         * by sigreturn(...))
         * new pc is the signal handler
         *
         * handler args are passed via registers:
         *    x0 = signal number
         *    x1 = buffer length
         *    x2 = buffer pointer
         *    x3 = pointer to saved registers */
        const uint64_t STACK_ALIGN = 16; /* Defined by aarch64. */
        uint64_t aligned_buf_size;
        uint64_t stack_size = 33 * sizeof(uint64_t);
        uint64_t new_sp = proc->regs.x[REG_SP];
        new_sp -= stack_size;
        /* Calculate aligned_buf_size to be the minimum value at least signal->buf_len so
         * that new_sp - aligned_buf_size is STACK_ALIGN aligned. */
        new_sp -= signal->buf_len;
        uint64_t alignment_padding = new_sp % STACK_ALIGN;
        new_sp -= alignment_padding;
        aligned_buf_size = alignment_padding + signal->buf_len;
        panic_if_not(new_sp + aligned_buf_size + 33 * sizeof(uint64_t) == proc->regs.x[REG_SP],
                "stack was not set up correctly\n");

        if (!is_user_writeable((ptr_t) new_sp, stack_size)) {
            return EFAULT;
        }

        uint8_t *stack = (uint8_t *) new_sp;
        memcpy(stack, signal->buf, signal->buf_len);
        stack += aligned_buf_size;
        for (uint32_t i = 0; i < 32; i++) {
            *((uint64_t *) stack) = proc->regs.x[i];
            stack += sizeof(uint64_t);
        }
        *((uint64_t *) stack) = proc->regs.pc;

        proc->regs.x[0] = signal->nr;
        proc->regs.x[1] = signal->buf_len;
        proc->regs.x[2] = new_sp;
        proc->regs.x[3] = new_sp + aligned_buf_size;
        proc->regs.x[REG_FP] = new_sp + aligned_buf_size + REG_FP * sizeof(uint64_t);
        proc->regs.x[REG_LR] = 0;
        proc->regs.x[REG_SP] = new_sp;
        proc->regs.pc = (uint64_t) handler;

        proc->usermode_signals_masked = true;
        return 0;
    }
}

void process_signals()
{
    proc_t *proc = current_process();

    /* We got here! No longer pending. */
    proc->signal_processing_pending = false;

    while (true) {
        signal_t *pending = NULL;

        /* Atomically: dequeue into 'pending', or leave as NULL if there
         * are no (unmasked) signals pending. */
        acquire_lock(&proc->signals_lock);
        signal_t *head = peek_signal(&proc->signals);
        if (head) {
            /* Masked due to it being a signal with a usermode handler, but
             * we're already running a usermode handler. */
            bool usermode_masked = proc->usermode_signals_masked
                                && is_usermode_signal(proc, head);
            /* Masked due to the process being stopped (due to a previous SIGSTOP),
             * and the signal is not a high-priority signal. */
            bool stopped_masked = proc->is_stopped
                             && !(head->nr == SIGKILL ||
                                  head->nr == SIGSTOP ||
                                  head->nr == SIGCONT);
            if (!(usermode_masked || stopped_masked)) {
                pending = dequeue_signal(&proc->signals);
            }
        }
        release_lock(&proc->signals_lock);

        bool had_pending = pending != NULL;
        if (had_pending) {
            errno_t errno = process_signal(pending);
            panic_if(errno, "could not process signal: %e\n", errno);
            free_signal(pending);
        }

        if (proc->is_stopped) {
            //printf("[%qs (%u32) is currently stopped]\n", proc->name, proc->id);
            irq_lock_t irq_lock;
            acquire_irq_lock(&irq_lock);

            remove_process(&processes_ready, proc);
            proc->interrupt_fn = &interrupt_stopped_process;

            release_irq_lock(&irq_lock);

            yield_process();
            /* We only resume execution once interrupted (by delivery
             * of a signal). We continue in the loop, to ensure that
             * we _only_ process signals and do nothing else. */
            continue;
        } else if (!had_pending) {
            /* We're not stopped, but also didn't find a signal that
             * we could process; we're done for now. */
            break;
        } else {
            /* We're not stopped, but we were able to process a signal.
             * Continue processing more signals until we're stopped or
             * there's nothing to process. */
            continue;
        }
    }
}

void run_syscall(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1, uint64_t arg2, uint64_t arg3)
{
    panic_if_not(in_context_preemptible(), "should be running in a preemptible context\n");
    panic_if_not(current_exception_level() == 1, "should be executing in EL1\n");

    proc_t *proc = current_process();

    // TODO: automatically restart syscalls *on sigreturn()* if possible.

    /* Check for pending signals before and after the syscall, to reduce latency. */
    if (proc->signal_processing_pending) {
        process_signals();
    }

    bool has_return_value;
    uint64_t value = syscall_base(syscall_nr, arg0, arg1, arg2, arg3, &has_return_value);
    if (has_return_value) {
        proc->regs.x[0] = value;
    }

    if (proc->signal_processing_pending) {
        process_signals();
    }

    /* After finishing the syscall, we need to continue executing the process
     * as normal. We must disable interrupts before setting the context back
     * to CONTEXT_PROCESS, as otherwise we might get preempted in between setting
     * the context and actually returning to normal processing, which could end up
     * overwriting the normal-context process registers with our syscall-context
     * registers. Interrupts will automatically get reenabled on exception return
     * (i.e. at the end of return_to_process().) */
    disable_interrupts();
    proc->context = CONTEXT_PROCESS;

    store_process_regs(proc, NULL);
    return_to_process(proc, NULL);
}

void handle_process_syscall(ptr_t irq_saved_regs_base)
{
    panic_if(in_context_irq(),
            "handle_process_syscall() should be called from an IRQ context\n");

    proc_t *proc = current_process();
    panic_if(proc->context == CONTEXT_SYSCALL,
            "reentrant syscalls are not supported\n");

    load_process_regs(proc, irq_saved_regs_base);

    /* We need to set up the syscall execution context. For this, we:
     * - copy all general purpose registers
     * - set the PC to point to our process-context syscall handler
     * - set the SP to use the process's syscall_stack region
     * - set the pstate to execute in EL1
     * - copy the ttbr0 */
    copy_regs(proc->syscall_regs.x, proc->regs.x);

    proc->syscall_regs.x[REG_SP] = (uint64_t) proc->syscall_stack;

    proc->syscall_regs.pc = (uint64_t) &run_syscall;

    proc->syscall_regs.pstate = proc->regs.pstate;
    proc->syscall_regs.pstate |= 0x5; /* 0b0101: execute in EL1 */

    proc->syscall_regs.ttbr0 = proc->regs.ttbr0;

    /* Now we're ready to continue executing the process, but in
     * the syscall context. */
    proc->context = CONTEXT_SYSCALL;
    store_process_regs(proc, irq_saved_regs_base);
    panic_if_not(has_privileged_execution(proc),
            "sanity check failed :(\n");
    return_to_process(proc, irq_saved_regs_base);
}

void handle_timer_irq(ptr_t irq_saved_regs_base, bool from_el1)
{
    /* Interrupts are currently disabled, since we're in an IRQ handler. */
    panic_if_not(in_context_irq(),
            "handle_timer_irq() should be called from an IRQ context\n");

    proc_t *cur_proc = current_process();

    bool expect_el1 = has_privileged_execution(cur_proc);
    if (expect_el1 != from_el1) {
        panic_if(expect_el1 && !from_el1,
            "kernel process %qs (PID %u32) was unexpectedly running in EL0\n",
            cur_proc->name, cur_proc->id);
        panic_if(!expect_el1 && from_el1,
            "user process %qs (PID %u32) was unexpectedly running in EL1\n",
            cur_proc->name, cur_proc->id);
    }

    load_process_regs(cur_proc, irq_saved_regs_base);

    schedule_next_process(irq_saved_regs_base);
}
