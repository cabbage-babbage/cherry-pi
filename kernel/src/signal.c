#include <signal.h>
#include <heap.h>
#include <dbg.h>

signal_t *alloc_signal(signal_nr_t nr, uint8_t buf_len, uint8_t *buf)
{
    panic_if(buf_len != 0 && buf == NULL,
            "buf may only be NULL if buf_len is zero\n");
    signal_t *signal = pmalloc(sizeof(signal_t));
    if (!signal) {
        return NULL;
    }
    signal->prev = NULL;
    signal->next = NULL;
    signal->nr = nr;
    signal->buf_len = buf_len;
    signal->buf = buf;
    return signal;
}

void free_signal(signal_t *signal)
{
    if (signal->buf) {
        pfree(signal->buf);
    }
    pfree(signal);
}

signal_list_t empty_signals_list()
{
    return (signal_list_t) {
        .head = NULL,
        .tail = NULL,
        .closed = false
    };
}

void append_signal(signal_list_t *list, signal_t *signal)
{
    panic_if(signal->prev || signal->next,
            "the signal should not be in a list already\n");
    
    signal->prev = list->tail;
    if (list->tail) {
        list->tail->next = signal;
    }
    list->tail = signal;
    if (!list->head) {
        list->head = signal;
    }
}

/* The 'place' must be in the list already (or be NULL). If the place is non-NULL,
 * this inserts the signal just before the given place; if the place is NULL, this
 * inserts the signal at the end of the list. */
void insert_signal_before(signal_list_t *list, signal_t *signal, signal_t *place)
{
    panic_if(signal->prev || signal->next,
            "the signal should not be in a list already\n");

    if (!place) {
        append_signal(list, signal);
        return;
    }

    if (place == list->head) {
        list->head = signal;
    } else {
        place->prev->next = signal;
    }
    signal->next = place;
    signal->prev = place->prev;
    place->prev = signal;
}

void enqueue_signal(signal_list_t *list, signal_t *signal)
{
    panic_if(signal->prev || signal->next,
            "the signal should not be in a list already\n");

    if (list->closed) {
        free_signal(signal);
        return;
    }

    /* Do basic priority handling. The list must always have signals in the
     * following order:
     * - SIGKILL;
     * - SIGSTOP, SIGCONT, SIGTTIN, SIGTTOU;
     * - any other signal. */

    if (signal->nr == SIGKILL) {
        /* Special case. Clear all other signals, since the SIGKILL is necessarily
         * the final signal processed. We can even close the list, for the same
         * reason. */
        signal_t *cur = list->head;
        while (cur) {
            signal_t *next = cur->next;
            free_signal(cur);
            cur = next;
        }

        list->head = signal;
        list->tail = signal;
        list->closed = true;
    } else if (signal->nr == SIGSTOP || signal->nr == SIGCONT ||
               signal->nr == SIGTTIN || signal->nr == SIGTTOU) {
        /* Insert the signal just after any existing SIGKILL, SIGTOP, SIGCONT,
         * SIGTTIN, or SIGTTOU. In other words, insert it just _before_ the first
         * signal that's _not_ one of those types. */
        signal_t *place = list->head;
        while (place && (place->nr == SIGKILL
                    || place->nr == SIGSTOP || place->nr == SIGCONT
                    || place->nr == SIGTTIN || place->nr == SIGTTOU)) {
            place = place->next;
        }
        insert_signal_before(list, signal, place);
    } else {
        append_signal(list, signal);
    }
}

signal_t *peek_signal(signal_list_t *list)
{
    return list->head;
}

signal_t *dequeue_signal(signal_list_t *list)
{
    panic_if_not(list->head, "signal list is empty\n");

    signal_t *head = list->head;
    if (head->next) {
        head->next->prev = NULL;
    }
    list->head = head->next;
    if (head->next) {
        head->next = NULL;
    } else {
        list->tail = NULL;
    }
    return head;
}

void free_signals(signal_list_t *list)
{
    signal_t *cur = list->head;
    while (cur) {
        signal_t *next = cur->next;
        free_signal(cur);
        cur = next;
    }

    list->head = NULL;
    list->tail = NULL;
    list->closed = true;
}
