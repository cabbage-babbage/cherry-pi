#include <syscall.h>
#include <uapi/syscall_nr.h>
#include <uapi/errno.h>
#include <proc.h>
#include <synch/lock.h>
#include <virtual_memory.h>
#include <timer.h>
#include <fs/filesys.h>
#include <heap.h>
#include <stdops.h>
#include <string.h>
#include <device/chardev.h>
#include <device/blockdev.h>
#include <dbg.h>

bool is_user_readable(ptr_t virt_addr_start, uint64_t length)
{
    return virt_addr_range_has_attribs(virt_addr_start, length,
            /* read */ true, /* write */ false,
            /* user access */ true, /* kernel access */ false,
            /* user executable */ false, /* kernel executable */ false);
}

bool is_user_writeable(ptr_t virt_addr_start, uint64_t length)
{
    return virt_addr_range_has_attribs(virt_addr_start, length,
            /* read */ false, /* write */ true,
            /* user access */ true, /* kernel access */ false,
            /* user executable */ false, /* kernel executable */ false);
}

bool is_user_rw_accessible(ptr_t virt_addr_start, uint64_t length)
{
    return virt_addr_range_has_attribs(virt_addr_start, length,
            /* read */ true, /* write */ true,
            /* user access */ true, /* kernel access */ false,
            /* user executable */ false, /* kernel executable */ false);
}

bool is_user_string_readable(ptr_t virt_addr_start)
{
    // TODO
    return true;
}

bool exit_process_action(proc_t *proc, void *aux)
{
    proc_t *parent = aux;

    if (proc->parent == parent) {
        //printf("[exit action] orphaning %u32\n", proc->id);
        // TODO: assign parent to parent's parent, instead of orphaning?
        proc->parent = NULL;
        if (proc->context == CONTEXT_EXITED) {
            //printf("[exit action] deallocating %u32\n", proc->id);
            deallocate_process_unsync(proc);
        }
    }

    return true; /* continue */
}
void syscall_exit(uint32_t exit_code)
{
    proc_t *proc = current_process();

    /* Set the parent of each of our children to NULL, and if they were
     * already in CONTEXT_EXITED, go ahead and deallocate them too. */
    acquire_irq_lock(&process_lists_lock);
    foreach_allocated_process(&exit_process_action, proc);
    release_irq_lock(&process_lists_lock);

    /* Make sure to close all the open file descriptors. */
    for (uint32_t fd = 0; fd < MAX_FDS; fd++) {
        if (proc->fds[fd].valid) {
            close_file_desc(&proc->fds[fd]);
        }
    }

    proc->exit_code = exit_code;
    kill_ready_process(proc);

    panic("process continued running after exiting\n");
}

bool find_exited_child_process_action(proc_t *maybe_child, void *aux)
{
    if (maybe_child->parent == current_process() &&
            maybe_child->context == CONTEXT_EXITED) {
        proc_t **child = (proc_t **) aux;
        *child = maybe_child;
        return false; /* break */
    } else {
        return true; /* continue */
    }
}
/* Returns NULL if there is no child in CONTEXT_EXITED. */
proc_t *find_exited_child_unsync()
{
    proc_t *child = NULL;
    foreach_allocated_process(&find_exited_child_process_action, &child);
    return child;
}

#define WAIT_ANY ((uint64_t) 1 << 0)
#define WAIT_NONBLOCKING ((uint64_t) 1 << 1)
int64_t syscall_wait(uint32_t *child_id, uint64_t flags)
{
    if (!is_user_rw_accessible(child_id, sizeof(*child_id))) {
        return -EFAULT;
    }

    bool wait_any = (flags & WAIT_ANY) != 0;
    bool blocking = !(flags & WAIT_NONBLOCKING);

    proc_t *proc = current_process();

    proc_t *child;
    if (wait_any) {
        /* Wait for any child to exit. If we already have an exited child, we can
         * skip the waiting! */
        acquire_irq_lock(&process_lists_lock);
        child = find_exited_child_unsync();
        if (!child) {
            if (blocking) {
                /* We actually have to wait. */
                remove_process(&processes_ready, proc);
                proc->wait_id = 0; /* WAIT_ANY */
            } else {
                /* We can immediately return. */
                release_irq_lock(&process_lists_lock);
                *child_id = 0;
                return 0;
            }
        }
        release_irq_lock(&process_lists_lock);

        /* We will not be woken up again until the some child exits and places us
         * back on the ready-queue. */
        yield_process();

        /* It's safe to do this without locking, since the exited child will not be
         * modified by anyone until we deallocate it. */
        child = find_exited_child_unsync();
        panic_if_not(child,
                "some child should have exited\n");
    } else {
        /* First let's find the child process and make sure that we're actually
         * its parent. It is safe to do this without locking; we aren't modifying
         * anything yet. */
        child = process_with_id(*child_id);
        if (!child || child->parent != current_process()) {
            return -ENOENT;
        }

        if (child->context != CONTEXT_EXITED) {
            if (blocking) {
                /* We need to wait for the child to exit. It will wake us up when it
                 * exits. */
                acquire_irq_lock(&process_lists_lock);
                remove_process(&processes_ready, proc);
                proc->wait_id = *child_id;
                release_irq_lock(&process_lists_lock);

                /* We will not be woken up again until the child exits and places us
                 * back on the ready-queue. */
                yield_process();

                panic_if_not(child->context == CONTEXT_EXITED,
                        "child should have exited\n");
            } else {
                /* We can return immediately. */
                *child_id = 0;
                return 0;
            }
        }
    }

    *child_id = child->id;
    uint32_t child_exit_code = child->exit_code;
    deallocate_process(child);
    return (int64_t) child_exit_code;
}

void interrupt_sleep(proc_t *proc)
{
    remove_process(&processes_sleeping, proc);
    enqueue_process(&processes_ready, proc);
    proc->interrupt_fn = NULL;
}
errno_t sleep_until(uint64_t ticks)
{
    proc_t *proc = current_process();

    if (was_interrupted()) {
        return EINTR;
    }

    /* It's ok to do this before updating the process lists, since wakeup_time is only
     * used when the process is in the processes_sleeping list. */
    proc->wakeup_time = ticks;

    acquire_irq_lock(&process_lists_lock);
    /* Remove the current process from processes_ready, and insert
     * it into processes_sleeping at the correct location to ensure
     * that the waiting list is sorted by wakeup_time. */
    remove_process(&processes_ready, proc);
    if (is_empty(&processes_sleeping) || (proc->wakeup_time < processes_sleeping.head->wakeup_time)) {
        prepend_process(&processes_sleeping, proc);
    } else {
        /* Find the first place in the list whose wakeup time is after
         * our process's, and insert our process just before it.
         * Of course there may not be such a place, meaning our process
         * has a wakeup time after all other waiting processes. Just put
         * it on the end of the list in that case. */
        proc_t *place = processes_sleeping.head;
        while (place && !(place->wakeup_time > proc->wakeup_time)) {
            place = place->next;
        }
        if (place) {
            insert_process_before(&processes_sleeping, proc, place);
        } else {
            append_process(&processes_sleeping, proc);
        }
    }
    proc->interrupt_fn = &interrupt_sleep;
    release_irq_lock(&process_lists_lock);

    yield_process();

    return was_interrupted() ? EINTR : 0;
}

int64_t syscall_sleep(uint64_t ms)
{
    return -(int64_t) sleep_until(timer_counter() + ms_to_ticks(ms));
}

#define CFG_READABLE        ((uint32_t) 1 << 0)
#define CFG_WRITEABLE       ((uint32_t) 1 << 1)
#define CFG_APPEND          ((uint32_t) 1 << 2)
#define CFG_CLOSE_ON_EXEC   ((uint32_t) 1 << 3)
#define CFG_NONBLOCKING     ((uint32_t) 1 << 4)
#define CFG_IS_TTY          ((uint32_t) 1 << 5)

uint32_t get_fd_cfg(file_descriptor_t *desc)
{
    uint32_t flags = 0;
    if (desc->flags & FD_READABLE) {
        flags |= CFG_READABLE;
    }
    if (desc->flags & FD_WRITEABLE) {
        flags |= CFG_WRITEABLE;
    }
    if (desc->flags & FD_APPEND) {
        flags |= CFG_APPEND;
    }
    if (desc->flags & FD_CLOSE_ON_EXEC) {
        flags |= CFG_CLOSE_ON_EXEC;
    }
    if (desc->flags & FD_NONBLOCKING) {
        flags |= CFG_NONBLOCKING;
    }
    if (desc->flags & FD_IS_TTY) {
        flags |= CFG_IS_TTY;
    }
    return flags;
}

int64_t syscall_get_desc_cfg(uint32_t fd)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *desc = &proc->fds[fd];
    if (!desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }

    return (int64_t) get_fd_cfg(desc);
}

int64_t syscall_set_desc_cfg(uint32_t fd, uint32_t cfg)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *desc = &proc->fds[fd];
    if (!desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }

    /* Check that only modifiable flags are actually changed. */
    const uint32_t modifiable_flags = CFG_CLOSE_ON_EXEC | CFG_NONBLOCKING;
    const uint32_t fixed_flags = ~modifiable_flags;
    uint32_t current_cfg = get_fd_cfg(desc);
    if ((cfg & fixed_flags) != (current_cfg & fixed_flags)) {
        return -EINVAL;
    }

    if (cfg & CFG_CLOSE_ON_EXEC) {
        desc->flags |= FD_CLOSE_ON_EXEC;
    } else {
        desc->flags &= ~FD_CLOSE_ON_EXEC;
    }

    if (cfg & CFG_NONBLOCKING) {
        desc->flags |= FD_NONBLOCKING;
    } else {
        desc->flags &= ~FD_NONBLOCKING;
    }

    return 0;
}

#define O_READ          ((uint32_t) 1 << 0)
#define O_WRITE         ((uint32_t) 1 << 1)
#define O_APPEND        ((uint32_t) 1 << 2)
#define O_DIR           ((uint32_t) 1 << 3)
#define O_NONBLOCK      ((uint32_t) 1 << 4)
#define O_CLOSE_EXEC    ((uint32_t) 1 << 5)
#define O_CREATE        ((uint32_t) 1 << 6)
#define O_NEW           ((uint32_t) 1 << 7)
#define O_CLOSE         ((uint32_t) 1 << 8)

int64_t open_base(open_file_t dir, const char *path, uint32_t flags)
{
    proc_t *proc = current_process();
    errno_t errno;

    /* First check the flags, before making any changes.
     * O_DIR only may be combined with certain other flags.
     * O_CLOSE requires O_CREATE or O_NEW. */
    if (flags & O_DIR) {
        uint32_t allowed_flags = O_DIR | O_CLOSE_EXEC | O_CREATE | O_NEW | O_CLOSE;
        if ((flags & allowed_flags) != flags) {
            return -EINVAL;
        }
    }
    if (flags & O_CLOSE) {
        if (!(flags & (O_CREATE | O_NEW))) {
            return -EINVAL;
        }
    }

    /* We only allow a terminal / if opening a directory. */
    uint64_t path_len = strlen(path);
    if (path_len > 0 && path[path_len - 1] == '/' && !(flags & O_DIR)) {
        return -EINVAL;
    }

    bool open_desc = !(flags & O_CLOSE);

    /* Find an available descriptor to use. */
    uint32_t fd = 0;
    if (open_desc) {
        for (; fd < MAX_FDS; fd++) {
            if (!proc->fds[fd].valid) {
                break;
            }
        }
        if (fd == MAX_FDS) {
            return -EFULL;
        }
    }

    open_file_t file;
    if (flags & (O_CREATE | O_NEW)) {
        bool is_new;
        /* We do keep the file open, even if we're not opening a descriptor,
         * since we will need to load the inode and see what kind of file it
         * is (i.e. directory or not). */
        // TODO: get permissions somehow...
        inode_type_t type = (flags & O_DIR) ? INODE_DIR : INODE_FILE;
        inode_perm_t perm = INODE_PERM_R | INODE_PERM_W;
        errno = filesys_create_at(open_file_locator(dir), path, /*keep_open*/ true,
                type, perm, 0,
                &is_new, &file);
        if (errno) {
            //goto open_base_close_file;
            return -(int64_t) errno;
        }

        if ((flags & O_NEW) && !is_new) {
            errno = EEXIST;
            goto open_base_close_file;
        }
    } else {
        /* Sanity check. */
        panic_if_not(open_desc, "got O_CLOSE, but also no O_CREATE or O_DIR\n");

        errno = filesys_open_at(open_file_locator(dir), path, &file);
        if (errno) {
            //goto open_base_close_file;
            return -(int64_t) errno;
        }
    }

    inode_type_t type;
    device_t *dev = NULL; /* Only valid if the type indicates a device file. */
    bool is_tty;

    {
        inode_t inode;
        errno = filesys_get_inode(file, &inode);
        if (errno) {
            goto open_base_close_file;
        }
        type = inode.type;

        // TODO: this should probably just be checked by filesys_open_at,
        // i.e. pass in required permissions to filesys_open_at and have
        // it return EPERM before ever returning the file.
        bool require_read = (flags & O_READ) != 0;
        bool require_write = (flags & (O_WRITE | O_APPEND)) != 0;
        if ((require_read && !(inode.perm & INODE_PERM_R)) ||
            (require_write && !(inode.perm & INODE_PERM_W))) {
            errno = EPERM;
            goto open_base_close_file;
        }

        if (type == INODE_CHARDEV || type == INODE_BLOCKDEV) {
            dev = find_device_by_id(inode.content.device_id);
            if (!dev) {
                printf("[bug] inode has invalid device id "device_id_spec"\n",
                        inode.content.device_id);
                errno = EIO;
                goto open_base_close_file;
            }

            is_tty = dev->type == DEVICE_TTY;
        } else {
            is_tty = false;
        }
    }

    /* Check that it's the right type. */
    panic_if(type == INODE_NONE || type == INODE_SYMLINK,
            "filesys_open_at() returned an open inode of type none or symlink\n");
    if (flags & O_DIR) {
        if (type != INODE_DIR) {
            errno = ENOTDIR;
            goto open_base_close_file;
        }
    } else {
        if (type == INODE_DIR) {
            errno = EISDIR;
            goto open_base_close_file;
        }
    }

    if (open_desc) {
        uint32_t fd_flags = 0;
        if (flags & O_READ) {
            fd_flags |= FD_READABLE;
        }
        if (flags & O_WRITE) {
            fd_flags |= FD_WRITEABLE;
        }
        if (flags & O_APPEND) {
            fd_flags |= FD_APPEND;
        }
        if (flags & O_CLOSE_EXEC) {
            fd_flags |= FD_CLOSE_ON_EXEC;
        }
        if (flags & O_NONBLOCK) {
            fd_flags |= FD_NONBLOCKING;
        }
        if (is_tty) {
            fd_flags |= FD_IS_TTY;
        }

        /* Sanity check. */
        panic_if(proc->fds[fd].valid, "we should have obtained an unused descriptor\n");

        proc->fds[fd] = (file_descriptor_t) {
            .valid = true,
            .type = type,
            .file = file,
            .offset = 0,
            .flags = fd_flags
        };
        /* Set aux separately. */
        if (type == INODE_DIR) {
            proc->fds[fd].aux.submount = file.mount->submounts_head;
        } else if (type == INODE_CHARDEV || type == INODE_BLOCKDEV) {
            proc->fds[fd].aux.dev = dev;
        }

        return fd;
    } else {
        filesys_close(file);
        return 0;
    }

open_base_close_file:
    filesys_close(file);
    return -(int64_t) errno;
}

int64_t syscall_open_at(uint32_t dirfd, const char *path, uint32_t flags)
{
    if (!is_user_string_readable((ptr_t) path)) {
        return -EFAULT;
    }

    proc_t *proc = current_process();

    if (dirfd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *dir_desc = &proc->fds[dirfd];
    if (!dir_desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }
    if (dir_desc->type != INODE_DIR) {
        return -ENOTDIR;
    }

    return open_base(dir_desc->file, path, flags);
}

int64_t syscall_open(const char *path, uint32_t flags)
{
    if (!is_user_string_readable((ptr_t) path)) {
        return -EFAULT;
    }

    return open_base(current_process()->cwd, path, flags);
}

int64_t syscall_close(uint32_t fd)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *desc = &proc->fds[fd];
    if (!desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }

    close_file_desc(desc);
    return 0;
}

int64_t dup_base(uint32_t fd, bool find_available, uint32_t new_fd)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS ||
        new_fd >= MAX_FDS) {
        return -EINVAL;
    }

    if (!proc->fds[fd].valid) {
        return -ENOENT;
    }

    /* Try to find an available descriptor, if requested. */
    if (find_available) {
        for (new_fd = 0; new_fd < MAX_FDS; new_fd++) {
            if (!proc->fds[new_fd].valid) {
                break;
            }
        }
        if (new_fd == MAX_FDS) {
            return -EFULL;
        }
    }

    if (new_fd == fd) {
        return new_fd;
    }

    if (proc->fds[new_fd].valid) {
        close_file_desc(&proc->fds[new_fd]);
    }

    errno_t errno = dup_file_desc(&proc->fds[fd]);
    if (errno) {
        return -(int64_t) errno;
    }
    proc->fds[new_fd] = proc->fds[fd];

    return new_fd;
}

int64_t syscall_dup(uint32_t fd)
{
    return dup_base(fd, true, 0);
}

int64_t syscall_dup_into(uint32_t fd, uint32_t new_fd)
{
    return dup_base(fd, false, new_fd);
}

#define MAX_READ_BYTES  ((uint64_t) 1 << 63)
#define MAX_WRITE_BYTES ((uint64_t) 1 << 63)

int64_t syscall_read(uint32_t fd, uint8_t *buf, uint64_t bytes)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *desc = &proc->fds[fd];

    if (!desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }

    if (!(desc->flags & FD_READABLE)) {
        return -EPERM;
    }

    if (!is_user_writeable((ptr_t) buf, bytes)) {
        return -EFAULT;
    }

    /* Internally limit the number of bytes read. */
    if (bytes > MAX_READ_BYTES) {
        bytes = MAX_READ_BYTES;
    }

    bool blocking = !(desc->flags & FD_NONBLOCKING);

    uint64_t bytes_read = 0;
    errno_t errno;

    if (desc->type == INODE_CHARDEV) {
        uint32_t io_flags = 0;
        if (blocking) {
            io_flags |= IO_BLOCKING;
        }
        chardev_t *dev = (chardev_t *) desc->aux.dev;
        errno = dev->read(dev, buf, bytes, io_flags, &bytes_read);
    } else if (desc->type == INODE_BLOCKDEV) {
        uint32_t io_flags = 0;
        if (blocking) {
            io_flags |= IO_BLOCKING;
        }
        blockdev_t *dev = (blockdev_t *) desc->aux.dev;
        errno = dev->read(dev, desc->offset, buf, bytes, io_flags, &bytes_read);
        desc->offset += bytes_read;
    } else if (desc->type == FILE_PIPE) {
        errno = pipe_read(desc->aux.pipe, buf, bytes, blocking, &bytes_read);
    } else {
        /* Regular file read. */
        if (!blocking) {
            return ENOTSUP;
        }
        errno = filesys_read_file(desc->file, desc->offset,
                buf, bytes, &bytes_read);
        desc->offset += bytes_read;
    }

    panic_if(bytes_read > bytes,
            "more bytes were read (%u64) than requested (%u64)",
            bytes_read, bytes);

    if (errno == 0) {
        return (int64_t) bytes_read;
    } else {
        return -(int64_t) errno;
    }
}

int64_t syscall_write(uint32_t fd, const uint8_t *buf, uint64_t bytes)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *desc = &proc->fds[fd];

    if (!desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }

    if (!(desc->flags & FD_WRITEABLE)) {
        return -EPERM;
    }

    if (!is_user_readable((ptr_t) buf, bytes)) {
        return -EFAULT;
    }

    /* Internally limit the number of bytes read. */
    if (bytes > MAX_WRITE_BYTES) {
        bytes = MAX_WRITE_BYTES;
    }

    bool blocking = !(desc->flags & FD_NONBLOCKING);

    uint64_t bytes_written = 0;
    errno_t errno;

    if (desc->type == INODE_CHARDEV) {
        /* It doesn't matter whether or not FD_APPEND is set, since
         * character devices are always append-only. */
        uint32_t io_flags = 0;
        if (blocking) {
            io_flags |= IO_BLOCKING;
        }
        chardev_t *dev = (chardev_t *) desc->aux.dev;
        errno = dev->write(dev, buf, bytes, io_flags, &bytes_written);
    } else if (desc->type == INODE_BLOCKDEV) {
        if (desc->flags & FD_APPEND) {
            return ENOTSUP;
        }
        uint32_t io_flags = 0;
        if (blocking) {
            io_flags |= IO_BLOCKING;
        }
        blockdev_t *dev = (blockdev_t *) desc->aux.dev;
        errno = dev->write(dev, desc->offset, buf, bytes, io_flags, &bytes_written);
        desc->offset += bytes_written;
    } else if (desc->type == FILE_PIPE) {
        errno = pipe_write(desc->aux.pipe, buf, bytes, blocking, &bytes_written);
    } else {
        /* Regular file write. */
        if (!blocking) {
            return ENOTSUP;
        }
        bool append = (desc->flags & FD_APPEND) != 0;
        errno = filesys_write_file(desc->file, desc->offset, append,
                buf, bytes, &bytes_written);
        desc->offset += bytes_written;
    }

    panic_if(bytes_written > bytes,
            "more bytes were written (%u64) than requested (%u64)",
            bytes_written, bytes);

    if (errno == 0) {
        return (int64_t) bytes_written;
    } else {
        return -(int64_t) errno;
    }
}

int64_t syscall_fork()
{
    proc_t *proc = current_process();

    proc_t *forked;
    int64_t fork_errcode = fork_process(proc, &forked);

    /* The forked process always gets a return value of 0;
     * this is actually performed by fork_process.
     * The parent (originating) process gets a return value
     * of +pid on success, and -errno on failure. */
    if (fork_errcode) {
        return fork_errcode;
    } else {
        return (int64_t) forked->id;
    }
}

int64_t syscall_exec(const char *name, const char *filename, ptr_t args, uint64_t args_length,
        bool *has_return_value)
{
    *has_return_value = true;

    if (!is_user_string_readable((ptr_t) name) ||
        !is_user_string_readable((ptr_t) filename) ||
        !is_user_readable(args, args_length)) {
        printf("[%s called exec with an inaccessible argument]\n",
                current_process()->name);
        return -EFAULT;
    }

    proc_t *proc = current_process();

    int64_t replace_errcode = replace_process(proc, name, filename, args, args_length);

    /* On success, we're executing in the syscall context of the new process, which
     * is kind of weird since it hasn't even begun executing yet (except as this
     * execution right now); it certainly hasn't executed a syscall yet. As the result
     * of the "syscall", therefore just return the current x0 value, so we syscall-return
     * directly into the process with no modification to its x0 register.
     * On failure, the current (i.e. original) process gets a return value of -errno from
     * the syscall. */
    if (replace_errcode) {
        return replace_errcode;
    } else {
        /* Success! */
        *has_return_value = false;
        return 0; /* This value doesn't matter. */
    }
}

int64_t syscall_get_heap(ptr_t *heap_start, uint64_t *heap_size)
{
    if ((heap_start && !is_user_writeable((ptr_t) heap_start, sizeof(*heap_start))) ||
        (heap_size && !is_user_writeable((ptr_t) heap_size, sizeof(*heap_size)))) {
        printf("[%s called get_heap with an inaccessible argument]\n",
                current_process()->name);
        return -EFAULT;
    }

    proc_t *proc = current_process();

    if (heap_start) {
        *heap_start = proc->heap_start;
    }
    if (heap_size) {
        *heap_size = proc->heap_pages * PAGE_SIZE;
    }

    //printf("[%u32 heap: 0x%p, %u64 pages]\n",
    //        proc->id, proc->heap_start, proc->heap_pages);

    return 0;
}

int64_t syscall_set_heap(uint64_t new_size)
{
    panic("syscall_set_heap() not implemented\n");
}

int64_t syscall_get_pid()
{
    return current_process()->id;
}

int64_t syscall_chdir(const char *path)
{
    if (!is_user_string_readable((ptr_t) path)) {
        return -EFAULT;
    }

    proc_t *proc = current_process();

    open_file_t newdir;
    errno_t errno = filesys_open_at(open_file_locator(proc->cwd), path, &newdir);
    if (errno) {
        return -(int64_t) errno;
    }

    /* Make sure it's a directory! */
    inode_t newdir_inode;
    errno = filesys_get_inode(newdir, &newdir_inode);
    if (errno) {
        filesys_close(newdir);
        return -(int64_t) errno;
    }

    if (newdir_inode.type != INODE_DIR) {
        filesys_close(newdir);
        return -ENOTDIR;
    }

    filesys_close(proc->cwd);
    proc->cwd = newdir;
    return 0;
}

int64_t syscall_ioctl(uint32_t fd, uint64_t ioctl_nr, uint8_t *buf, uint64_t buf_len)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *desc = &proc->fds[fd];

    if (!desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }

    if (!is_user_rw_accessible((ptr_t) buf, buf_len)) {
        return -EFAULT;
    }

    if (!(desc->type == INODE_CHARDEV || desc->type == INODE_BLOCKDEV)) {
        return -ENOTDEV;
    }

    errno_t errno = device_ioctl(desc->aux.dev, ioctl_nr, buf, buf_len);
    return -(int64_t) errno;
}

int64_t syscall_readdir(uint32_t fd, dir_entry_t *buf, uint64_t buf_entries,
        uint64_t *entries_read)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *desc = &proc->fds[fd];

    if (!desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }
    if (desc->type != INODE_DIR) {
        return -ENOTDIR;
    }

    if (buf_entries > UINT64_MAX / sizeof(dir_entry_t)) {
        return -EFAULT;
    }
    uint64_t buf_len = buf_entries * sizeof(dir_entry_t);
    if (!is_user_writeable((ptr_t) buf, buf_len)) {
        return -EFAULT;
    }

    if (!is_user_writeable((ptr_t) entries_read, sizeof(uint64_t))) {
        return -EFAULT;
    }

    *entries_read = 0;

    open_file_t dir = desc->file;

    /* Read through the submounts! */
    while (buf_entries > 0 && desc->aux.submount) {
        submount_t *submount = desc->aux.submount;
        if (submount->point.dir == dir.file->inode_nr) {
            uint64_t mount_name_len = strlen(submount->point.name);
            panic_if(mount_name_len > FILE_NAME_MAX_LEN,
                    "got mount point with too-long name '%s'\n",
                    submount->point.name);

            buf->inode_nr = 0;
            buf->name_len = mount_name_len;
            memcpy(buf->name, submount->point.name, mount_name_len);

            buf++;
            buf_entries--;
            (*entries_read)++;
        }
        desc->aux.submount = submount->next;
    }

    panic_if_not(desc->offset % sizeof(dir_entry_t) == 0,
            "expected to have (so far) read a whole number of directory entries\n");

    // TODO: skip unused entries and also entries which have mounts sitting on top

    uint64_t total_bytes_read = 0;
    while (total_bytes_read < buf_len) {
        uint64_t bytes_read;
        errno_t errno = filesys_read_file(dir, desc->offset,
                (uint8_t *) buf + total_bytes_read, buf_len - total_bytes_read,
                &bytes_read);
        desc->offset += bytes_read;
        total_bytes_read += bytes_read;
        if (errno) {
            /* Rounding down is expected. */
            *entries_read = total_bytes_read / sizeof(dir_entry_t);
            /* Skip to the next directory entry, just in case userspace tries to readdir using
             * the same descriptor even after an error. */
            desc->offset = align_up(desc->offset, sizeof(dir_entry_t));
            return -(int64_t) errno;
        }
        if (bytes_read == 0) {
            break;
        }
    }
    panic_if_not(total_bytes_read % sizeof(dir_entry_t) == 0,
            "expected to read a whole number of directory entries\n");
    *entries_read += total_bytes_read / sizeof(dir_entry_t);
    return 0;
}

typedef struct {
    uint64_t filesys_nr;
    inode_nr_t inode_nr;
    uint64_t refs;
    uint64_t size;
    inode_type_t type;
    inode_perm_t perm;
    device_id_t device_id;
    uint64_t time_created;
    uint64_t time_modified;
} stat_t;

errno_t stat_base(open_file_t file, stat_t *buf)
{
    inode_t inode;
    errno_t errno = filesys_get_inode(file, &inode);
    if (errno) {
        return errno;
    }

    if (file.file->inode_nr != inode.nr) {
        printf("[bug] mismatched inode numbers: open_file_t "inode_nr_spec" vs. loaded inode "inode_nr_spec"\n",
                file.file->inode_nr, inode.nr);
    }

    uint64_t size;
    if (inode.type == INODE_BLOCKDEV) {
        device_t *dev = find_device_by_id(inode.content.device_id);
        if (!dev) {
            printf("[bug] inode has invalid device id "device_id_spec"\n",
                    inode.content.device_id);
            return EIO;
        }
        if (dev->type != DEVICE_BLOCKDEV) {
            printf("[bug] blockdev inode refers to non-blockdev '%qs'\n",
                    dev->name);
            return EIO;
        }

        blockdev_t *bdev = (blockdev_t *) dev;
        size = blockdev_size(bdev);
    } else {
        size = inode.size;
    }

    *buf = (stat_t) {
        .filesys_nr = file.mount->filesys->id,
        .inode_nr = file.file->inode_nr,
        .refs = inode.refs,
        .size = size,
        .type = inode.type,
        .perm = inode.perm,
        .device_id = (inode.type == INODE_CHARDEV || inode.type == INODE_BLOCKDEV)
                     ? inode.content.device_id
                     : 0,
        .time_created = inode.time_created,
        .time_modified = inode.time_modified
    };

    return 0;
}

int64_t syscall_stat(const char *path, stat_t *buf)
{
    proc_t *proc = current_process();

    if (!is_user_string_readable((ptr_t) path) ||
        !is_user_readable((ptr_t) buf, sizeof(stat_t))) {
        return -EFAULT;
    }

    open_file_t file;
    errno_t errno = filesys_open_at(open_file_locator(proc->cwd), path, &file);
    if (errno) {
        return -(int64_t) errno;
    }

    errno = stat_base(file, buf);
    filesys_close(file);
    return -(int64_t) errno;
}

int64_t syscall_stat_at(uint32_t fd, const char *path, stat_t *buf)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *desc = &proc->fds[fd];
    if (!desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }
    if (desc->type == FILE_PIPE) {
        return -EISPIPE;
    }

    if (!is_user_string_readable((ptr_t) path) ||
        !is_user_readable((ptr_t) buf, sizeof(stat_t))) {
        return -EFAULT;
    }

    open_file_t file;
    errno_t errno = filesys_open_at(open_file_locator(desc->file), path, &file);
    if (errno) {
        return -(int64_t) errno;
    }

    errno = stat_base(file, buf);
    filesys_close(file);
    return -(int64_t) errno;
}

int64_t syscall_mkpipe(uint32_t *rfd, uint32_t *wfd, uint32_t flags)
{
    proc_t *proc = current_process();

    if (!is_user_writeable((ptr_t) rfd, sizeof(uint32_t)) ||
        !is_user_writeable((ptr_t) wfd, sizeof(uint32_t))) {
        return -EFAULT;
    }

    const uint32_t allowed_flags = O_NONBLOCK | O_CLOSE_EXEC;
    if (flags & ~allowed_flags) {
        return -EINVAL;
    }

    uint32_t fd_read, fd_write;
    /* Find two available descriptors. */
    uint32_t fd;
    for (fd = 0; fd < MAX_FDS; fd++) {
        if (!proc->fds[fd].valid) {
            fd_read = fd;
            fd++;
            break;
        }
    }
    for (; fd < MAX_FDS; fd++) {
        if (!proc->fds[fd].valid) {
            fd_write = fd;
            break;
        }
    }
    if (fd == MAX_FDS) {
        return -EFULL;
    }

    pipe_t *pipe = alloc_pipe();
    if (!pipe) {
        return -ENOMEM;
    }

    pipe->open_readers = 1;
    pipe->open_writers = 1;

    uint32_t fd_flags = 0;
    if (flags & O_NONBLOCK) {
        fd_flags |= FD_NONBLOCKING;
    }
    if (flags & O_CLOSE_EXEC) {
        fd_flags |= FD_CLOSE_ON_EXEC;
    }

    proc->fds[fd_read] = (file_descriptor_t) {
        .valid = true,
        .type = FILE_PIPE,
        .file = {0},
        .offset = 0,
        .flags = fd_flags | FD_READABLE,
        .aux.pipe = pipe
    };
    proc->fds[fd_write] = (file_descriptor_t) {
        .valid = true,
        .type = FILE_PIPE,
        .file = {0},
        .offset = 0,
        .flags = fd_flags | FD_WRITEABLE,
        .aux.pipe = pipe
    };

    *rfd = fd_read;
    *wfd = fd_write;
    return 0;
}

int64_t syscall_sigaction(signal_nr_t signal_nr, signal_handler_t handler)
{
    proc_t *proc = current_process();

    if (signal_nr >= NUM_SIG) {
        return -EINVAL;
    }

    /* Particular signals are fixed to use the default handler always. */
    if (signal_nr == SIGKILL || signal_nr == SIGSTOP || signal_nr == SIGCONT) {
        return -EINVAL;
    }

    proc->signal_handler[signal_nr] = handler;
    return 0;
}

int64_t syscall_signal(uint32_t pid, signal_nr_t signal_nr, uint8_t buf_len, const uint8_t *buf)
{
    //proc_t *proc = current_process();

    if (signal_nr >= NUM_SIG) {
        return -EINVAL;
    }
    if (!is_user_readable((ptr_t) buf, buf_len)) {
        return -EFAULT;
    }

    // TODO: we need some kind of refcounting so that the target doesn't get deallocated
    // between finding it and delivering the signal
    proc_t *target = process_with_id(pid);
    if (!target) {
        return -ENOENT;
    }

    uint8_t *buf_copy;
    if (buf_len == 0) {
        buf_copy = NULL;
    } else {
        buf_copy = pmalloc(buf_len);
        if (!buf_copy) {
            return -ENOMEM;
        }
    }

    signal_t *signal = alloc_signal(signal_nr, buf_len, buf_copy);
    if (!signal) {
        pfree(buf_copy);
        return -ENOMEM;
    }

    deliver_signal(target, signal);
    return 0;
}

int64_t syscall_sigreturn(void *context, bool *has_return_value)
{
    *has_return_value = true;

    proc_t *proc = current_process();

    /* Read process_signal() to see how the signal stack is set up;
     * the structure is not commented in duplicate here. */

    if (!is_user_readable(context, 33 * sizeof(uint64_t))) {
        return -EFAULT;
    }

    uint64_t *saved_regs = context;
    for (uint32_t i = 0; i < 32; i++) {
        proc->regs.x[i] = *saved_regs++;
    }
    proc->regs.pc = *saved_regs;

    acquire_lock(&proc->signals_lock);
    proc->usermode_signals_masked = false;
    release_lock(&proc->signals_lock);

    /* Get some signal processing on the return path from this
     * syscall; now that usermode signals are unmasked, there
     * could very well be another pending usermode signal waiting
     * for processing. */
    proc->signal_processing_pending = true;

    *has_return_value = false;
    return 0; /* Value doesn't matter. */
}

int64_t syscall_kern(bool *has_return_value)
{
    *has_return_value = false;
    return 0; /* Value doesn't matter. */
}

int64_t syscall_get_pgid()
{
    return current_process()->gid;
}

int64_t syscall_set_pgid(uint32_t gid)
{
    proc_t *proc = current_process();

    if (gid == 0) {
        proc->gid = proc->id;
    } else {
        // TODO: should probably have more checks...
        proc->gid = gid;
    }

    return 0;
}

uint64_t syscall_get_time()
{
    return timer_counter() * (uint64_t) 1000 / timer_frequency();
}

#define SEEK_SET ((uint8_t) 0)
#define SEEK_CUR ((uint8_t) 1)
#define SEEK_END ((uint8_t) 2)
int64_t syscall_seek(uint32_t fd, int64_t offset, uint8_t whence)
{
    proc_t *proc = current_process();

    if (fd >= MAX_FDS) {
        return -EINVAL;
    }

    file_descriptor_t *desc = &proc->fds[fd];

    if (!desc->valid) {
        /* File descriptor is not open. */
        return -ENOENT;
    }

    if (!(whence == SEEK_SET || whence == SEEK_CUR || whence == SEEK_END)) {
        return -EINVAL;
    }

    /* Depending on the descriptor type, either produce an error or get the
     * file size (and an indicator of whether or not it's ok to seek past the
     * end of the file). */
    uint64_t fd_size;
    bool past_end_ok;
    if (desc->type == INODE_CHARDEV) {
        return -ENOTSUP;
    } else if (desc->type == INODE_BLOCKDEV) {
        blockdev_t *dev = (blockdev_t *) desc->aux.dev;
        fd_size = blockdev_size(dev);
        /* Allow seeks past the end only if the block device size is unknown (reported as 0). */
        past_end_ok = fd_size == 0;
    } else if (desc->type == FILE_PIPE) {
        return -ENOTSUP;
    } else {
        /* Regular file seek. */
        inode_t inode;
        errno_t errno = filesys_get_inode(desc->file, &inode);
        if (errno) {
            return errno;
        }
        fd_size = inode.size;
        /* Sparse reads/writes make this ok. */
        past_end_ok = true;
    }

    /* Now do the seek. */
    int64_t new_offset;
    switch (whence) {
        case SEEK_SET: new_offset = offset; break;
        case SEEK_CUR: new_offset = desc->offset + offset; break;
        case SEEK_END: new_offset = fd_size + offset; break;
        default: panic("we already verified that whence was valid!\n");
    }
    if (new_offset < 0 || (!past_end_ok && new_offset > fd_size)) {
        return -ERANGE;
    }
    desc->offset = offset;
    return 0;
}

uint64_t syscall_base(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1, uint64_t arg2,
        uint64_t arg3, bool *has_return_value)
{
    /* For most syscalls this is the default behavior; any syscalls which have special
     * return-value behavior just get has_return_value passed as another argument. */
    *has_return_value = true;

    switch (syscall_nr) {
        case SYSCALL_EXIT:
            syscall_exit((uint32_t) arg0);
            panic("process continued running after exiting\n");
        case SYSCALL_WAIT:
            return (uint64_t) syscall_wait((uint32_t *) arg0, arg1);
        case SYSCALL_SLEEP:
            return (uint64_t) syscall_sleep(arg0);
        case SYSCALL_GET_DESC_CFG:
            return (uint64_t) syscall_get_desc_cfg((uint32_t) arg0);
        case SYSCALL_SET_DESC_CFG:
            return (uint64_t) syscall_set_desc_cfg((uint32_t) arg0, (uint32_t) arg1);
        case SYSCALL_OPEN:
            return (uint64_t) syscall_open((const char *) arg0, (uint32_t) arg1);
        case SYSCALL_OPEN_AT:
            return (uint64_t) syscall_open_at((uint32_t) arg0, (const char *) arg1,
                    (uint32_t) arg2);
        case SYSCALL_CLOSE:
            return (uint64_t) syscall_close((uint32_t) arg0);
        case SYSCALL_DUP:
            return (uint64_t) syscall_dup((uint32_t) arg0);
        case SYSCALL_DUP_INTO:
            return (uint64_t) syscall_dup_into((uint32_t) arg0, (uint32_t) arg1);
        case SYSCALL_READ:
            return (uint64_t) syscall_read((uint32_t) arg0, (uint8_t *) arg1, arg2);
        case SYSCALL_WRITE:
            return (uint64_t) syscall_write((uint32_t) arg0, (uint8_t *) arg1, arg2);
        case SYSCALL_FORK:
            return (uint64_t) syscall_fork();
        case SYSCALL_EXEC:
            return (uint64_t) syscall_exec((const char *) arg0, (const char *) arg1,
                    (ptr_t) arg2, arg3, has_return_value);
        case SYSCALL_GET_HEAP:
            return (uint64_t) syscall_get_heap((ptr_t *) arg0, (uint64_t *) arg1);
        case SYSCALL_SET_HEAP:
            return (uint64_t) syscall_set_heap(arg0);
        case SYSCALL_GET_PID:
            return (uint64_t) syscall_get_pid();
        case SYSCALL_CHDIR:
            return (uint64_t) syscall_chdir((const char *) arg0);
        case SYSCALL_IOCTL:
            return (uint64_t) syscall_ioctl((uint32_t) arg0, arg1, (uint8_t *) arg2, arg3);
        case SYSCALL_READDIR:
            return (uint64_t) syscall_readdir((uint32_t) arg0, (dir_entry_t *) arg1, arg2,
                    (uint64_t *) arg3);
        case SYSCALL_STAT:
            return (uint64_t) syscall_stat((const char *) arg0, (stat_t *) arg1);
        case SYSCALL_STAT_AT:
            return (uint64_t) syscall_stat_at((uint32_t) arg0, (const char *) arg1, (stat_t *) arg2);
        case SYSCALL_MKPIPE:
            return (uint64_t) syscall_mkpipe((uint32_t *) arg0, (uint32_t *) arg1, (uint32_t) arg2);
        case SYSCALL_SIGACTION:
            return (uint64_t) syscall_sigaction((signal_nr_t) arg0, (signal_handler_t) arg1);
        case SYSCALL_SIGNAL:
            return (uint64_t) syscall_signal((uint32_t) arg0, (signal_nr_t) arg1, (uint8_t) arg2,
                    (const uint8_t *) arg3);
        case SYSCALL_SIGRETURN:
            return (uint64_t) syscall_sigreturn((void *) arg0, has_return_value);
        case SYSCALL_GET_PGID:
            return (uint64_t) syscall_get_pgid();
        case SYSCALL_SET_PGID:
            return (uint64_t) syscall_set_pgid((uint32_t) arg0);
        case SYSCALL_GET_TIME:
            return syscall_get_time();
        case SYSCALL_SEEK:
            return (uint64_t) syscall_seek((uint32_t) arg0, (int64_t) arg1, (uint8_t) arg2);
        case SYSCALL_KERN:
            return (uint64_t) syscall_kern(has_return_value);
        case SYSCALL_PANIC:
            // very much debug only!
            panic("process '%s' (pid %u32) panicked: %s",
                    current_process()->name,
                    current_process()->id,
                    (const char *) arg0);
        default:
            // TODO: just do nothing for now. Maybe this should just kill the process
            // or something.
            printf_nl("[kernel] unknown syscall_nr %u32\n", syscall_nr);
            return (uint64_t) -ENOCALL;
    }
}

/* This implements the _syscallN functions. */
asm(
    ".global _syscall0, _syscall1, _syscall2, _syscall3\n"
    ".global _syscall4, _syscall5, _syscall6, _syscall7\n"
    "_syscall0:\n"
    "_syscall1:\n"
    "_syscall2:\n"
    "_syscall3:\n"
    "_syscall4:\n"
    "_syscall5:\n"
    "_syscall6:\n"
    "_syscall7:\n"
    "    svc #1\n"
    "    ret"
);
