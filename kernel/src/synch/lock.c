#include <synch/lock.h>
#include <proc.h>
#include <dbg.h>

void create_lock(lock_t *lock)
{
    create_sema(&lock->sema, 1);
    lock->owner = NULL;
}

void acquire_lock(lock_t *lock)
{
    sema_down(&lock->sema);
    panic_if(lock->owner, "lock already has owner '%qs'\n", lock->owner->name);
    lock->owner = current_process();
}

bool try_acquire_lock(lock_t *lock)
{
    if (!sema_try_down(&lock->sema)) {
        return false;
    }
    panic_if(lock->owner, "lock already has owner '%qs'\n", lock->owner->name);
    lock->owner = current_process();
    return true;
}

bool acquire_lock_interruptible(lock_t *lock)
{
    if (!sema_down_interruptible(&lock->sema)) {
        return false;
    }
    panic_if(lock->owner, "lock already has owner '%qs'\n", lock->owner->name);
    lock->owner = current_process();
    return true;
}

void release_lock(lock_t *lock)
{
    if (lock->owner == current_process()) {
        lock->owner = NULL;
        sema_up(&lock->sema);
    } else {
        panic("process '%qs' attempted to unlock a lock owned by process '%qs'\n",
                current_process()->name, lock->owner->name);
    }
}

void release_lock_noirq(lock_t *lock)
{
    if (lock->owner == current_process()) {
        lock->owner = NULL;
        sema_up_noirq(&lock->sema);
    } else {
        panic("process '%qs' attempted to unlock a lock owned by process '%qs'\n",
                current_process()->name, lock->owner->name);
    }
}

bool lock_is_acquired(lock_t *lock)
{
    return lock->sema.state == 0;
}

void acquire_irq_lock(irq_lock_t *lock)
{
    lock->pushed_state = push_interrupts_disabled();
}

void release_irq_lock(irq_lock_t *lock)
{
    restore_interrupts_state(lock->pushed_state);
}

void create_rw_lock(rw_lock_t *lock, bool prefer_writers)
{
    lock->prefer_writers = prefer_writers;
    create_lock(&lock->counts_lock);
    lock->readers = 0;
    lock->writers_waiting = 0;
    create_sema(&lock->writers_allowed, 1);
    lock->writer = NULL;
}

void acquire_read(rw_lock_t *lock)
{
    if (lock->prefer_writers) {
        // TODO: wait until writers_waiting == 0
    }
    acquire_lock(&lock->counts_lock);
    lock->readers++;
    if (lock->readers == 1) {
        /* We're the first reader, so block future writers. */
        sema_down(&lock->writers_allowed);
    }
    release_lock(&lock->counts_lock);
}

void release_read(rw_lock_t *lock)
{
    if (lock->prefer_writers) {
        // TODO: wait until writers_waiting == 0
    }
    acquire_lock(&lock->counts_lock);
    panic_if(lock->readers == 0,
            "mismatched acquire_read() and release_read()\n");
    lock->readers--;
    if (lock->readers == 0) {
        sema_up(&lock->writers_allowed);
    }
    release_lock(&lock->counts_lock);
}

bool acquire_read_interruptible(rw_lock_t *lock)
{
    if (lock->prefer_writers) {
        // TODO: (interruptibly) wait until writers_waiting == 0
    }
    acquire_lock(&lock->counts_lock);
    lock->readers++;
    if (lock->readers == 1) {
        /* We're the first reader, so block future writers.
         * Clean up if we were interrupted.*/
        if (!sema_down_interruptible(&lock->writers_allowed)) {
            lock->readers--;
            release_lock(&lock->counts_lock);
            return false;
        }
    }
    release_lock(&lock->counts_lock);
    return true;
}

void acquire_write(rw_lock_t *lock)
{
    acquire_lock(&lock->counts_lock);
    lock->writers_waiting++;
    release_lock(&lock->counts_lock);

    sema_down(&lock->writers_allowed);
    lock->writer = current_process();
}

void release_write(rw_lock_t *lock)
{
    panic_if(lock->writer != current_process(),
            "found mismatch between acquire_write() and release_write()\n");

    lock->writer = NULL;

    acquire_lock(&lock->counts_lock);
    lock->writers_waiting--;
    release_lock(&lock->counts_lock);

    sema_up(&lock->writers_allowed);
}

bool acquire_write_interruptible(rw_lock_t *lock)
{
    acquire_lock(&lock->counts_lock);
    lock->writers_waiting++;
    release_lock(&lock->counts_lock);

    if (!sema_down_interruptible(&lock->writers_allowed)) {
        acquire_lock(&lock->counts_lock);
        lock->writers_waiting--;
        release_lock(&lock->counts_lock);
        return false;
    }
    lock->writer = current_process();
    return true;
}
