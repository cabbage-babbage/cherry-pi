#include <synch/latch.h>

void create_latch(latch_t *latch)
{
    create_sema(&latch->complete, 0);
}

void latch_set(latch_t *latch)
{
    sema_limit_up(&latch->complete, 1);
}

void latch_wait(latch_t *latch)
{
    sema_down(&latch->complete);
    sema_limit_up(&latch->complete, 1);
}

bool latch_try_wait(latch_t *latch)
{
    if (!sema_try_down(&latch->complete)) {
        return false;
    }
    sema_limit_up(&latch->complete, 1);
    return true;
}

bool latch_wait_interruptible(latch_t *latch)
{
    if (!sema_down_interruptible(&latch->complete)) {
        return false;
    }
    sema_limit_up(&latch->complete, 1);
    return true;
}
