#include <synch/monitor.h>
#include <irq.h>
#include <syscontext.h>
#include <proc.h>
#include <dbg.h>

void create_cond_var(cond_var_t *var, lock_t *monitor_lock)
{
    var->monitor_lock = monitor_lock;
    var->processes_waiting = empty_process_list();
}

void cond_var_wait(cond_var_t *var)
{
    panic_if_not(in_context_preemptible(),
            "condition variables may only be used from a preemptible context\n");
    /* Sanity check as well. */
    panic_if_not(interrupts_enabled(), "interrupts should be enabled\n");

    proc_t *proc = current_process();
    disable_interrupts();

    release_lock_noirq(var->monitor_lock);
    remove_process(&processes_ready, proc);
    enqueue_process(&var->processes_waiting, proc);

    enable_interrupts();
    yield_process();
    /* yield_process() won't return until we're placed back on processes_ready
     * by a call to cond_var_notify() or cond_var_broadcast(). */

    acquire_lock(var->monitor_lock);
}

static void interrupt_wait_cond_var(proc_t *proc)
{
    panic_if(interrupts_enabled(), "irqs must be off\n");

    cond_var_t *var = proc->interrupt_aux.cond_var_waiting.var;
    /* Make sure that the process is off the condition variable's waiting
     * list, and is back in the ready queue. We may not have to do anything;
     * it could be the case that the process was waiting on the condition
     * variable, then was notified (and thus placed on the ready queue), but
     * then was interrupted by another process before it next is scheduled. */
    bool is_waiting = false;
    // TODO: have each process store which list it's in; then we can just
    // check whether the process's owning list is var->processes_waiting
    // or not.
    forall_processes(waiting, var->processes_waiting) {
        if (proc == waiting) {
            is_waiting = true;
            break;
        }
    }
    if (is_waiting) {
        remove_process(&var->processes_waiting, proc);
        enqueue_process(&processes_ready, proc);
    }

    proc->interrupt_fn = NULL;
}

bool cond_var_wait_interruptible(cond_var_t *var)
{
    panic_if_not(in_context_preemptible(),
            "condition variables may only be used from a preemptible context\n");
    /* Sanity check as well. */
    panic_if_not(interrupts_enabled(), "interrupts should be enabled\n");

    proc_t *proc = current_process();
    disable_interrupts();

    proc->interrupt_fn = &interrupt_wait_cond_var;
    // TODO: barrier
    proc->interrupt_aux.cond_var_waiting.var = var;

    release_lock_noirq(var->monitor_lock);
    remove_process(&processes_ready, proc);
    enqueue_process(&var->processes_waiting, proc);

    enable_interrupts();
    yield_process();
    /* We won't continue until we've been notified (i.e. on var), or interrupted
     * and thus placed back on the ready queue by interrupt_wait_cond_var. */

    disable_interrupts();
    proc->interrupt_fn = NULL;
    bool interrupted = proc->is_interrupted;
    proc->is_interrupted = false;
    enable_interrupts();

    if (interrupted) {
        return false;
    } else {
        return acquire_lock_interruptible(var->monitor_lock);
    }
}

void cond_var_notify(cond_var_t *var)
{
    panic_if_not(in_context_preemptible(),
            "condition variables may only be used from a preemptible context\n");
    /* Sanity check as well. */
    panic_if_not(interrupts_enabled(), "interrupts should be enabled\n");

    disable_interrupts();
    if (!is_empty(&var->processes_waiting)) {
        proc_t *proc = dequeue_process(&var->processes_waiting);
        enqueue_process(&processes_ready, proc);
    }
    enable_interrupts();
}

void cond_var_broadcast(cond_var_t *var)
{
    panic_if_not(in_context_preemptible(),
            "condition variables may only be used from a preemptible context\n");
    /* Sanity check as well. */
    panic_if_not(interrupts_enabled(), "interrupts should be enabled\n");

    disable_interrupts();
    while (!is_empty(&var->processes_waiting)) {
        proc_t *proc = dequeue_process(&var->processes_waiting);
        enqueue_process(&processes_ready, proc);
    }
    enable_interrupts();
}
