#include <synch/semaphore.h>
#include <irq.h>
#include <proc.h>
#include <syscontext.h>
#include <dbg.h>

void create_sema(semaphore_t *sema, uint64_t state)
{
    sema->state = state;
    sema->processes_waiting = empty_process_list();
}

void sema_down(semaphore_t *sema)
{
    if (in_context_boot()) {
        panic_if(sema->state == 0, "cannot wait on semaphore while in boot context\n");
        sema->state--;
        return;
    }

    panic_if_not(in_context_preemptible(),
            "semaphores may only be used from a preemptible context\n");
    /* Sanity check as well. */
    panic_if_not(interrupts_enabled(), "interrupts should be enabled\n");

    proc_t *proc = current_process();
    disable_interrupts();

    while (sema->state == 0) {
        remove_process(&processes_ready, proc);
        enqueue_process(&sema->processes_waiting, proc);
        enable_interrupts();
        yield_process();
        disable_interrupts();
    }

    sema->state--;

    enable_interrupts();
}

bool sema_try_down(semaphore_t *sema)
{
    if (in_context_boot()) {
        if (sema->state > 0) {
            sema->state--;
            return true;
        } else {
            return false;
        }
    }

    panic_if_not(in_context_preemptible(),
            "semaphores may only be used from a preemptible context\n");
    /* Sanity check as well. */
    panic_if_not(interrupts_enabled(), "interrupts should be enabled\n");

    bool success;
    disable_interrupts();
    if (sema->state > 0) {
        sema->state--;
        success = true;
    } else {
        success = false;
    }
    enable_interrupts();

    return success;
}

static void interrupt_wait_sema_down(proc_t *proc)
{
    panic_if(interrupts_enabled(), "irqs must be off\n");

    semaphore_t *sema = proc->interrupt_aux.sema_waiting.sema;
    /* We must ensure that the process is off the semaphore's waiting list
     * and is back in the ready queue. We may not have to do anything; the
     * process may have been taken off the waiting list by another process
     * sema_up()ping this sema just before we got here. */
    bool is_waiting = false;
    // TODO: have each process store which list it's in; then we can just
    // check whether the process's owning list is sema->processes_waiting
    // or not.
    forall_processes(waiting, sema->processes_waiting) {
        if (proc == waiting) {
            is_waiting = true;
            break;
        }
    }
    if (is_waiting) {
        remove_process(&sema->processes_waiting, proc);
        enqueue_process(&processes_ready, proc);
    }
    proc->interrupt_fn = NULL;
}

bool sema_down_interruptible(semaphore_t *sema)
{
    if (in_context_boot()) {
        panic_if(sema->state == 0, "cannot wait on semaphore while in boot context\n");
        sema->state--;
        return true;
    }

    panic_if_not(in_context_preemptible(),
            "semaphores may only be used from a preemptible context\n");
    /* Sanity check as well. */
    panic_if_not(interrupts_enabled(), "interrupts should be enabled\n");

    proc_t *proc = current_process();

    disable_interrupts();

    proc->interrupt_fn = &interrupt_wait_sema_down;
    // TODO: barrier
    proc->interrupt_aux.sema_waiting.sema = sema;

    while (sema->state == 0 && !proc->is_interrupted) {
        remove_process(&processes_ready, proc);
        enqueue_process(&sema->processes_waiting, proc);
        enable_interrupts();
        yield_process();
        disable_interrupts();
    }

    proc->interrupt_fn = NULL;

    bool do_down = !proc->is_interrupted;
    proc->is_interrupted = false;

    if (do_down) {
        sema->state--;
    }

    enable_interrupts();
    return do_down;
}

void sema_up(semaphore_t *sema)
{
    sema_limit_up(sema, UINT64_MAX);
}

void sema_up_noirq(semaphore_t *sema)
{
    panic_if(interrupts_enabled(), "interrupts should be disabled\n");

    sema->state++;
    /* Re-enable one of the waiting processes. Eventually the
     * scheduler will run it, and it can then decrement the state. */
    if (!is_empty(&sema->processes_waiting)) {
        proc_t *proc = dequeue_process(&sema->processes_waiting);
        enqueue_process(&processes_ready, proc);
    }
}

INLINE void sema_limit_up(semaphore_t *sema, uint64_t max)
{
    if (in_context_boot()) {
        if (sema->state < max) {
            sema->state++;
        }
        return;
    }

    panic_if_not(in_context_preemptible(),
            "semaphores may only be used from a preemptible context\n");
    /* Sanity check as well. */
    panic_if_not(interrupts_enabled(), "interrupts should be enabled\n");

    disable_interrupts();
    if (sema->state < max) {
        sema->state++;
        /* Re-enable one of the waiting processes. Eventually the
         * scheduler will run it, and it can then decrement the state. */
        if (!is_empty(&sema->processes_waiting)) {
            proc_t *proc = dequeue_process(&sema->processes_waiting);
            enqueue_process(&processes_ready, proc);
        }
    }
    enable_interrupts();
}
