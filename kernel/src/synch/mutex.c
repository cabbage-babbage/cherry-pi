#include <synch/mutex.h>

void create_mutex(mutex_t *mutex)
{
    create_sema(&mutex->sema, 0);
}

void acquire_mutex(mutex_t *mutex)
{
    sema_down(&mutex->sema);
}

void release_mutex(mutex_t *mutex)
{
    sema_limit_up(&mutex->sema, 1);
}

bool try_acquire_mutex(mutex_t *mutex)
{
    return sema_try_down(&mutex->sema);
}

bool acquire_mutex_interruptible(mutex_t *mutex)
{
    return sema_down_interruptible(&mutex->sema);
}
