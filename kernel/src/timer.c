#include <timer.h>
#include <sysreg.h>
#include <virtual_memory.h>

INLINE uint64_t timer_frequency()
{
    uint64_t cntfrq;
    load_sysreg(cntfrq, CNTFRQ_EL0);
    return cntfrq;
}

INLINE uint64_t timer_counter()
{
    uint64_t counter;
    load_sysreg(counter, CNTPCT_EL0);
    return counter;
    //uint32_t timer_lo, timer_hi;

    //while (true) {
    //    timer_hi = *TIMER_CHI;
    //    timer_lo = *TIMER_CLO;
    //    if (*TIMER_CHI == timer_hi) {
    //        break;
    //    }
    //}

    //uint64_t counter;
    //counter |= (uint64_t) timer_lo;
    //counter |= ((uint64_t) timer_hi) << 32;
    //return counter;
}

void initialize_timer()
{
    // TODO: lock down timer access in EL0. CNTKCTL_ELn?
}

void enable_timer_interrupts()
{
    /* Set ENABLE = 1 (0b-1), IMASK = 0 (0b0-). */
    set_sysreg(CNTP_CTL_EL0, 1 /* 0b01 */);
    /* TODO: describe this */
    *((volatile uint8_t *) phys_to_kernel_virt(0x40000040)) = 0x0F;
}

void disable_timer_interrupts()
{
    /* Set ENABLE = 1 (0b-1), IMASK = 1 (0b1-). */
    set_sysreg(CNTP_CTL_EL0, 2 /* 0b11 */);
}

void timer_interrupt_absolute(uint64_t ticks)
{
    set_sysreg(CNTP_CVAL_EL0, ticks);
}

void timer_interrupt_relative(uint64_t ticks)
{
    set_sysreg(CNTP_CVAL_EL0, timer_counter() + ticks);
}

bool timer_irq_firing()
{
    uint64_t cntp_ctl;
    load_sysreg(cntp_ctl, CNTP_CTL_EL0);
    /* Check ISTATUS (bit 2) == 1, IMASK (bit 1) == 0,
     * and ENABLE (bit 0) == 1. */
    return (cntp_ctl & 0x7) == 0x5;
}

uint64_t ticks_to_ns(uint64_t ticks)
{
    // TODO: check for overflow
    return ticks * BILLION / timer_frequency();
}

uint64_t ticks_to_ms(uint64_t ticks)
{
    // TODO: check for overflow
    return ticks * 1000 / timer_frequency();
}

uint64_t ns_to_ticks(uint64_t ns)
{
    // TODO: check for overflow
    return timer_frequency() * ns / BILLION;
}

uint64_t ms_to_ticks(uint64_t ms)
{
    // TODO: check for overflow
    return timer_frequency() * ms / 1000;
}
