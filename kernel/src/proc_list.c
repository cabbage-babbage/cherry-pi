#include <proc_list.h>
#include <proc.h>
#include <dbg.h>

proc_list_t empty_process_list()
{
    proc_list_t list = {
        .len = 0,
        .head = NULL,
        .tail = NULL
    };
    return list;
}

INLINE bool is_empty(proc_list_t *list)
{
    return list->len == 0;
}

/* NOTE: this may give false negatives, i.e. say that
 * the process is not in any list even if it is. This
 * can happen if the process is the only entry in some
 * list. However, all positives are true, so it's safe
 * to us this function in order to (e.g.) panic if the
 * process is already in a list; we may not catch all
 * cases, but it'll still help with debugging. */
USE_VALUE INLINE bool is_in_any_list(proc_t *proc)
{
    return proc->prev || proc->next;
}

void prepend_process(proc_list_t *list, proc_t *proc)
{
    panic_if(is_in_any_list(proc),
            "cannot prepend a process that's already in a list\n");
    if (list->len == 0) {
        list->head = proc;
        list->tail = proc;
    } else {
        proc->next = list->head;
        list->head->prev = proc;
        list->head = proc;
    }

    list->len++;
}

void append_process(proc_list_t *list, proc_t *proc)
{
    panic_if(is_in_any_list(proc),
            "cannot append a process that's already in a list\n");
    //printf_nl("append '%qs' to '%qs'\n", proc->name, list->name);
    if (list->len == 0) {
        list->head = proc;
        list->tail = proc;
    } else {
        proc->prev = list->tail;
        list->tail->next = proc;
        list->tail = proc;
    }

    list->len++;
}

void insert_process_before(proc_list_t *list, proc_t *proc, proc_t *place)
{
    panic_if(is_in_any_list(proc),
            "cannot insert a process that's already in a list\n");

    if (place == list->head) {
        list->head = proc;
    } else {
        place->prev->next = proc;
    }
    proc->next = place;
    proc->prev = place->prev;
    place->prev = proc;

    list->len++;
}

void remove_process(proc_list_t *list, proc_t *proc)
{
    panic_if(is_empty(list), "list is empty\n");
    //printf_nl("remove '%qs' from '%qs'\n", proc->name, list->name);
    if (proc->prev) {
        proc->prev->next = proc->next;
    }
    if (proc->next) {
        proc->next->prev = proc->prev;
    }

    if (list->head == proc) {
        list->head = proc->next;
    }
    if (list->tail == proc) {
        list->tail = proc->prev;
    }

    proc->prev = NULL;
    proc->next = NULL;

    list->len--;
}

proc_t *remove_head_process(proc_list_t *list)
{
    panic_if(is_empty(list),
            "cannot remove the head of an empty list\n");
    proc_t *head = list->head;
    remove_process(list, head);
    return head;
}

proc_t *remove_tail_process(proc_list_t *list)
{
    panic_if(is_empty(list),
            "cannot remove the tail of an empty list\n");
    proc_t *tail = list->tail;
    remove_process(list, tail);
    return tail;
}
