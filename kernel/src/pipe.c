#include <pipe.h>
#include <heap.h>
#include <stdops.h>
#include <string.h>
#include <dbg.h>

pipe_t *alloc_pipe()
{
    pipe_t *pipe = pmalloc(sizeof(pipe_t));
    if (!pipe) {
        return NULL;
    }

    const uint64_t PIPE_PAGES = 1;
    uint8_t *buf = pmalloc_pages(PIPE_PAGES);
    if (!buf) {
        pfree(pipe);
        return NULL;
    }

    create_rw_lock(&pipe->refs_lock, true);
    pipe->open_readers = 0;
    pipe->open_writers = 0;
    create_lock(&pipe->read_lock);
    create_lock(&pipe->write_lock);
    create_sema(&pipe->read_avail, 0);
    create_sema(&pipe->write_avail, 1);
    pipe->size = PIPE_PAGES * PAGE_SIZE;
    pipe->head = 0;
    pipe->tail = 0;
    pipe->buf = buf;

    return pipe;
}

void pipe_dup_half(pipe_t *pipe, bool read_half)
{
    acquire_write(&pipe->refs_lock);
    if (read_half) {
        pipe->open_readers++;
    } else {
        pipe->open_writers++;
    }
    release_write(&pipe->refs_lock);
}

void pipe_close_half(pipe_t *pipe, bool read_half)
{
    acquire_write(&pipe->refs_lock);
    if (read_half) {
        panic_if(!pipe->open_readers,
                "tried to close a pipe read-half with no open readers\n");
        pipe->open_readers--;
        if (!pipe->open_readers) {
            sema_limit_up(&pipe->write_avail, 1);
        }
    } else {
        panic_if(!pipe->open_writers,
                "tried to close a pipe write-half with no open writers\n");
        pipe->open_writers--;
        if (!pipe->open_writers) {
            sema_limit_up(&pipe->read_avail, 1);
        }
    }
    release_write(&pipe->refs_lock);

    if (!pipe->open_readers && !pipe->open_writers) {
        /* Free the pipe and its buf. */
        pfree_pages(pipe->buf);
        pfree(pipe);
    }
}

/* Returns the number of bytes filled in the pipe. Unsynchronized!
 * However, if there is no active reader (pipe->head is not being modified)
 * then this is guaranteed to return at most the current fill level,
 * and if there is no active writer (pipe->tail is not being modified) then
 * this is guaranteed to return at least the current fill level.
 * However, if there is both an active reader and an active writer, the return
 * value is undefined (without synchronization).
 *
 * In other words, if there is no active reader, but potentially an active writer,
 * this function can return a fill level anywhere between the real fill level on
 * entry to this function, or the real fill level on exit from this function (which
 * may be larger, due to the writer).
 * Likewise, if there is no active writer, but potentially an active reader,
 * this function can return a fill level anywhere between the real fill level on
 * entry, or the fill level on exit (which may be smaller, due to the reader). */
INLINE uint64_t pipe_fill(pipe_t *pipe)
{
    uint64_t head = pipe->head, tail = pipe->tail;
    if (tail < head) {
        return (tail + pipe->size) - head;
    } else {
        return tail - head;
    }
}

/* Just like pipe_fill(), except this returns the amount of space free in the pipe
 * buffer for writing. If there is no active reader, this gives a safe lower bound;
 * if there is no active writer, this returns a safe upper bound. */
INLINE uint64_t pipe_space(pipe_t *pipe)
{
    uint64_t fill = pipe_fill(pipe);
    panic_if(fill >= pipe->size, "pipe was somehow fully filled\n");
    return pipe->size - 1 - fill;
}

/* Unsynchronized! You must hold the read_lock. */
INLINE void pipe_advance_head(pipe_t *pipe, uint64_t count)
{
    pipe->head = (pipe->head + count) % pipe->size;
}

/* Unsynchronized! You must hold the write_lock. */
INLINE void pipe_advance_tail(pipe_t *pipe, uint64_t count)
{
    pipe->tail = (pipe->tail + count) % pipe->size;
}

/* This function requires that the pipe actually has at least 'count' bytes available for reading.
 * Unsynchronized! You must hold the read_lock. */
void pipe_read_buf(pipe_t *pipe, uint8_t *buf, uint64_t count)
{
    panic_if(count > pipe_fill(pipe), "pipe does not have enough bytes available to read\n");

    /* Read from the pipe buf[head...size], and then buf[0...tail]. */

    uint64_t read = min_uint64(pipe->size - pipe->head, count);
    memcpy(buf, &pipe->buf[pipe->head], read);
    pipe_advance_head(pipe, read);
    buf += read;
    count -= read;
    if (count == 0) {
        return;
    }

    memcpy(buf, &pipe->buf[pipe->head], count);
    pipe_advance_head(pipe, count);
}

/* This function requires that the pipe actually has at least 'count' bytes of space available for writing.
 * Unsychronized! You must hold the write_lock. */
void pipe_write_buf(pipe_t *pipe, const uint8_t *buf, uint64_t count)
{
    panic_if(count > pipe_space(pipe),
            "pipe does not have enough space available to write\n");

    /* Write to pipe buf[tail...size], and then buf[0...head]. */

    uint64_t write = min_uint64(pipe->size - pipe->tail, count);
    memcpy(&pipe->buf[pipe->tail], buf, write);
    pipe_advance_tail(pipe, write);
    buf += write;
    count -= write;
    if (count == 0) {
        return;
    }

    memcpy(&pipe->buf[pipe->tail], buf, count);
    pipe_advance_tail(pipe, count);
}

// avoid nonblocking reads when there are any blocking readers, as it may cause livelock
errno_t pipe_read(pipe_t *pipe, uint8_t *buf, uint64_t buf_size,
        bool blocking, uint64_t *bytes_read)
{
    *bytes_read = 0;
    if (blocking) {
        if (!acquire_lock_interruptible(&pipe->read_lock)) {
            return EINTR;
        }
        while (buf_size > 0) {
            if (!pipe->open_writers && !pipe_fill(pipe)) {
                /* No writers and no data currently available; this indicates that
                 * no more data will ever come through the pipe. */
                break;
            }
            if (!sema_down_interruptible(&pipe->read_avail)) {
                release_lock(&pipe->read_lock);
                return EINTR;
            }
            if (!pipe->open_writers && !pipe_fill(pipe)) {
                /* No writers and no data currently available; this indicates that
                 * no more data will ever come through the pipe. */
                break;
            }

            uint64_t fill = pipe_fill(pipe);
            panic_if(fill == 0, "no pipe fill even after waiting on read_avail\n");
            uint64_t read = min_uint64(buf_size, fill);
            pipe_read_buf(pipe, buf, read);
            buf += read;
            buf_size -= read;
            *bytes_read += read;

            if (pipe_fill(pipe)) {
                sema_limit_up(&pipe->read_avail, 1);
            }

            /* We don't wait until all the data is read before doing this,
             * to avoid potentially deadlocking the pipe. */
            sema_limit_up(&pipe->write_avail, 1);
        }
        release_lock(&pipe->read_lock);

        return 0;
    } else {
        if (try_acquire_lock(&pipe->read_lock)) {
            uint64_t read = min_uint64(buf_size, pipe_fill(pipe));
            pipe_read_buf(pipe, buf, read);
            if (read > 0) {
                sema_limit_up(&pipe->write_avail, 1);
            }
            release_lock(&pipe->read_lock);
            *bytes_read = read;
            return read ? 0 : EAGAIN;
        } else {
            /* Could not read; try again later. */
            return EAGAIN;
        }
    }
}

// avoid nonblocking writes when there are any blocking writers, as it may cause livelock
errno_t pipe_write(pipe_t *pipe, const uint8_t *buf, uint64_t buf_size,
        bool blocking, uint64_t *bytes_written)
{
    if (!pipe->open_readers) {
        *bytes_written = 0;
        return EPIPE;
    }

    *bytes_written = 0;
    if (blocking) {
        if (!acquire_lock_interruptible(&pipe->write_lock)) {
            return EINTR;
        }
        if (buf_size == 0) {
            sema_limit_up(&pipe->read_avail, 1);
        }
        while (buf_size > 0 && pipe->open_readers) {
            if (!sema_down_interruptible(&pipe->write_avail)) {
                release_lock(&pipe->write_lock);
                return EINTR;
            }
            if (!pipe->open_readers) {
                break;
            }

            uint64_t space = pipe_space(pipe);
            panic_if(space == 0, "no pipe space even after waiting on write_avail\n");
            uint64_t written = min_uint64(buf_size, space);
            pipe_write_buf(pipe, buf, written);
            buf += written;
            buf_size -= written;
            *bytes_written += written;

            if (pipe_space(pipe)) {
                sema_limit_up(&pipe->write_avail, 1);
            }

            /* We don't wait until all the data is written before doing this,
             * to avoid potentially deadlocking the pipe. */
            sema_limit_up(&pipe->read_avail, 1);
        }
        release_lock(&pipe->write_lock);

        if (!pipe->open_readers) {
            return EPIPE;
        }
        return 0;
    } else {
        if (!pipe->open_readers) {
            return EPIPE;
        }
        if (try_acquire_lock(&pipe->write_lock)) {
            uint64_t written = min_uint64(buf_size, pipe_space(pipe));
            pipe_write_buf(pipe, buf, written);
            if (written > 0) {
                sema_limit_up(&pipe->read_avail, 1);
            }
            release_lock(&pipe->write_lock);
            *bytes_written = written;
            return written ? 0 : EAGAIN;
        } else {
            /* Could not write; try again later. */
            return EAGAIN;
        }
    }
}
