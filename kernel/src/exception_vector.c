#include <exception_vector.h>
#include <std.h>
#include <console.h>
#include <sysreg.h>
#include <dbg.h>

extern_symbol exception_vector;
extern_symbol exception_vector_entry_0;
extern_symbol exception_vector_entry_1;
extern_symbol exception_vector_entry_2;
extern_symbol exception_vector_entry_3;
extern_symbol exception_vector_entry_4;
extern_symbol exception_vector_entry_5;
extern_symbol exception_vector_entry_6;
extern_symbol exception_vector_entry_7;
extern_symbol exception_vector_entry_8;
extern_symbol exception_vector_entry_9;
extern_symbol exception_vector_entry_10;
extern_symbol exception_vector_entry_11;
extern_symbol exception_vector_entry_12;
extern_symbol exception_vector_entry_13;
extern_symbol exception_vector_entry_14;
extern_symbol exception_vector_entry_15;

void initialize_exception_vectors()
{
    printf("initializing exception vectors:\n");

    // Sanity check that the exception vector entries are at the correct
    // locations, i.e. starting at the exception_vector label and then
    // at every 0x80 bytes.
#define expected_entry_location(n) (&exception_vector + (n) * 0x80)
#define check_entry(n) \
    do { \
        printf("    entry " #n ": expected 0x%p, actual 0x%p\n", \
                expected_entry_location(n), &exception_vector_entry_##n); \
        if (&exception_vector_entry_##n != expected_entry_location(n)) { \
            panic("invalid entry\n"); \
        } \
    } while (0)

    printf("  checking exception vector entry offsets:\n");
    check_entry(0);
    check_entry(1);
    check_entry(2);
    check_entry(3);
    check_entry(4);
    check_entry(5);
    check_entry(6);
    check_entry(7);
    check_entry(8);
    check_entry(9);
    check_entry(10);
    check_entry(11);
    check_entry(12);
    check_entry(13);
    check_entry(14);
    check_entry(15);

    ptr_t vbar_el1;
    load_sysreg(vbar_el1, VBAR_EL1);

    printf("  initial: vbar_el1 = 0x%p\n",
            vbar_el1);

    ptr_t exception_vector_base = &exception_vector;
    printf("  exception vector is at 0x%p\n", exception_vector_base);

    set_sysreg(VBAR_EL1, exception_vector_base);

    load_sysreg(vbar_el1, VBAR_EL1);

    printf("  new vbar_el1 = 0x%p\n",
            vbar_el1);
}
