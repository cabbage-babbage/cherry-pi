#include <device/uart.h>
#include <console.h>
#include <dbg.h>
#include <exception_vector.h>
#include <exception_level.h>
#define PRINT_SYSREG
#include <sysreg.h>
#include <irq.h>
#include <timer.h>
#include <proc.h>
#include <synch/semaphore.h>
#include <synch/lock.h>
#include <virtual_memory.h>
#include <heap.h>
#include <fs/fs.h>
#include <syscontext.h>
#include <device/device.h>
#include <device/emmc.h>

extern_symbol _bss_start, _bss_end;
extern_symbol _kernel_load_low_start, _kernel_load_low_end;
extern_symbol _kernel_load_high_start, _kernel_load_high_end;
extern_symbol _kernel_data_start;

/* Zero bss. */
static void zero_bss()
{
    for (uint8_t *bss = &_bss_start; bss < &_bss_end; bss++) {
        *bss = 0;
    }
}

//lock_t printf_lock;
//
//void busy_wait()
//{
//    for (uint64_t i = 0; i < 250000000; i++) {
//        asm volatile("nop");
//    }
//}
//
//void user_process()
//{
//    printf_nl("[user] testing syscalls:\n");
//    printf_nl("[user] syscall_plus_one(15) = %u32:\n",
//            syscall_plus_one(15));
//
//    uint64_t res = syscall_puts("yay, kernel again printed my string!\n");
//    printf("[user] result of syscall: 0x%u64{16}\n", res);
//
//    printf_nl("[user] testing sleep\n");
//    syscall_sleep_ms(1000);
//    printf_nl("[user] should be 1 second later\n");
//
//    printf_nl("[user] exiting\n");
//    // should work either way: making the syscall, or returning into
//    // the syscall (i.e. the stub_process_exit)
//    //syscall_exit();
//    //panic("shouldn't have gotten here\n");
//}
//
//void other_process()
//{
//    printf("[other] sleeping for a few seconds (and should wake up before root)\n");
//    syscall_sleep_until(503503500 - 1);
//
//    for (uint32_t i = 0; i < 5; i++) {
//        putc('a');
//        busy_wait();
//    }
//
//    acquire_lock(&printf_lock);
//
//    for (uint32_t i = 0; i < 5; i++) {
//        putc('?');
//        busy_wait();
//    }
//
//    release_lock(&printf_lock);
//}
//
//void root_process()
//{
//    create_lock(&printf_lock);
//
//    printf_nl("[root] waiting 3 seconds before making 'other'\n");
//    syscall_sleep_ms(3000);
//    printf_nl("[root] is back!\n");
//
//    panic_if_not(
//            create_process("other", &other_process, true, NULL, SP_UNDEFINED, NULL),
//            "could not create other process\n");
//
//    //panic_if_not(
//    //        create_process("user", &user_process, false, NULL, SP_UNDEFINED, NULL),
//    //        "could not create user process\n");
//
//    printf("[root] sleeping for a few seconds\n");
//    syscall_sleep_until(503503500);
//
//    for (uint32_t i = 0; i < 3; i++) {
//        putc('-');
//        busy_wait();
//    }
//
//    printf_nl("[root] acquiring the lock\n");
//    acquire_lock(&printf_lock);
//    for (uint32_t i = 0; i < 10; i++) {
//        putc('0' + i);
//        busy_wait();
//    }
//    printf_nl("[root] releasing the lock\n");
//    release_lock(&printf_lock);
//
//    printf_nl("[root] nothing to do; looping\n");
//    //while (true) {
//    //    busy_wait();
//    //    putc('.');
//    //}
//}

/*
 * Main kernel function. Not quite the initial entrypoint to the kernel;
 * that's in entry.s. Instead, by the time we get here, the entrypoint has
 * already set up several things for us:
 * - virtual memory is set up so that both low and high virtual address
 *   ranges map 1:1 to physical memory;
 * - we enter here at EL1 instead of EL2, and with all interrupts disabled.
 * This function, and all code that it calls, is located at high virtual
 * address ranges, leaving the low range to be modified per-process.
 */
void kernel_main()
{
    /* We must initialize EL0 and EL1 execution first, since we're
     * already in EL1. This (among other things) ensures that NEON
     * is enabled, so printf, which is compiled to use advanced
     * SIMD operations, can execute without causing an exception. */
    initialize_el0_and_el1();

    set_syscontext(SYSCONTEXT_BOOT);

    initialize_devices();

    uart_init();
    puts("\n"
         "uart is initialized\n");

    zero_bss();
    console_init();

    printf("console is initialized\n");
    //printf("enter any input to continue...\n");
    //uart_getc();

    uint64_t endianness_check = 0;
    *(uint8_t*) &endianness_check = 1;
    panic_if_not(endianness_check == 1,
            "expected little-endian operation\n");

    printf("initializing heap\n");
    initialize_heap();

    printf("initializing virtual memory\n");
    initialize_virtual_memory();

    printf("kernel entry spans 0x%p to 0x%p (%u64 KB)\n",
            &_kernel_load_low_start, &_kernel_load_low_end,
            (uint64_t) (&_kernel_load_low_end - &_kernel_load_low_start) >> 10);
    printf("kernel main spans 0x%p to 0x%p (%u64 KB), with data at 0x%p\n",
            &_kernel_load_high_start, &_kernel_load_high_end,
            (uint64_t) (&_kernel_load_high_end - &_kernel_load_high_start) >> 10,
            &_kernel_data_start);

    if (current_exception_level() != 1) {
        panic("expected to be running at EL1!\n");
    }

    initialize_exception_vectors();

    panic_if(interrupts_enabled(), "interrupts should be disabled\n");

    printf("current_el: %u64\n", current_exception_level());

    //printf("making a 'syscall' EL1 -> EL1 with value 0x1234:\n");
    //asm volatile("svc #0x1234");

    //printf("making syscall to add one to a number:\n");
    //printf("  input 15 -> output %u32\n",
    //        syscall_plus_one(15));

    //printf("making syscall to print a string:\n");
    //uint64_t res = syscall_puts("yay, kernel printed my string!\n");
    //printf("   result of syscall: 0x%u64{16}\n", res);

    printf("initializing standard devices\n");
    initialize_standard_devices();

    printf("initializing EMMC\n");
    errno_t errno = initialize_emmc();
    if (errno) {
        printf("could not initialize EMMC: %e\n", errno);
    }

    printf("initializing file system\n");
    initialize_fs();

    printf("adding registered devices to /dev\n");
    add_devices("/dev");

    printf("initializing processes\n");
    initialize_processes();

    /* We need to initialize the timer and set up an interrupt in
     * the future before starting processes, to begin the multi-
     * processing cycle. */

    printf("initializing timer\n");
    initialize_timer();

    printf("timer frequency: %u64 Hz\n", timer_frequency());

    printf("enabling timer interrupts\n");
    // TODO: set up a guaranteed interrupt as soon as we unmask interrupts,
    // or still leave this interrupt in the future?
    timer_interrupt_relative(ms_to_ticks(500));
    panic_if(interrupts_enabled(), "interrupts should be disabled\n");
    enable_timer_interrupts();

    printf("enabling uart interrupts\n");
    uart_enable_interrupts();

    printf("starting processes\n");
    /* Note that this will enable interrupts automatically, so
     * we don't need to have already enabled interrupts. */
    start_processes();

    panic("should not get here\n");

    /*
     * Each of these blocks fails in EL0 (i.e. is trapped to the kernel),
     * which is good -- user code cannot read or write timer or interrupt
     * settings.
     */
    //printf("getting timer frequency: ");
    //timer_frequency();
    //printf("ok");

    //printf("getting timer counter: ");
    //timer_counter();
    //printf("ok");

    //printf("disabling timer interrupts: ");
    //disable_timer_interrupts();
    //printf("ok");

    //printf("enabling timer interrupts: ");
    //enable_timer_interrupts();
    //printf("ok");

    //printf("setting timer comparator value: ");
    //timer_interrupt_absolute(2342342342);
    //printf("ok");

    //printf("reading DAIF: ");
    //interrupts_enabled();
    //printf("ok");

    //printf("disabing interrupts: ");
    //disable_interrupts();
    //printf("ok");

    /* Loop forever, just in case we somehow get here;
     * We're must not return. */
    while (true) ;
}
