#include <fs/fs.h>
#include <fs/filesys.h>
#include <fs/file_ops.h>
#include <fs/inode_ops.h>
#include <fs/cpfs/inode_ops.h>
#include <fs/tmpfs/inode_ops.h>
#include <fs/block_ops.h>
#include <fs/initrd.h>
#include <fs/cache.h>
#include <device/chardev.h>
#include <device/uart.h>
#include <dbg.h>

/* Root mount: initrd cpfs. */

initrd_block_ops_aux_t initrd_block_ops_aux;
block_ops_t block_ops;

cpfs_inode_ops_aux_t cpfs_inode_ops_aux;
inode_ops_t inode_ops;

default_file_ops_aux_t file_ops_aux;
file_ops_t file_ops;

filesys_t root_fs;

/* /dev mount: a tmpfs. */

inode_ops_t devfs_inode_ops;

default_file_ops_aux_t devfs_file_ops_aux;
file_ops_t devfs_file_ops;

inode_t devfs_root_inode;

filesys_t devfs;

/* /tmp mount: a tmpfs. */

inode_ops_t tmpfs_inode_ops;

default_file_ops_aux_t tmpfs_file_ops_aux;
file_ops_t tmpfs_file_ops;

inode_t tmpfs_root_inode;

filesys_t tmpfs;

/* Helper function for mounting. */
// TODO: move into filesys.c?
USE_VALUE errno_t filesys_mount_at(const char *dirpath, const char *name, filesys_t *filesys)
{
    open_file_t dir;
    errno_t errno = filesys_open(dirpath, &dir);
    if (errno) {
        return errno;
    }

    mount_point_t mp = {
        .dir = dir.file->inode_nr,
        .name = name
    };
    errno = filesys_mount(dir.mount, mp, filesys);

    filesys_close(dir);
    return errno;
}

void initialize_fs()
{
    initialize_fs_cache();
    initialize_filesys();

    /* Make the root mount. */

    get_initrd_block_ops(&block_ops, &initrd_block_ops_aux);

    cpfs_inode_ops_aux.block_ops = &block_ops;
    get_cpfs_inode_ops(&inode_ops, &cpfs_inode_ops_aux);

    file_ops_aux.inode_ops = &inode_ops;
    get_default_file_ops(&file_ops, &file_ops_aux);

    root_fs = (filesys_t) {
        .name = "cpfs",
        .id = 0,
        .ops = file_ops,
        .mount_count = 0
    };

    errno_t errno = filesys_mount_root(&root_fs);
    panic_if(errno, "could not mount root fs: %e\n", errno);

    /* Make the devfs. */

    get_tmpfs_inode_ops(&devfs_inode_ops);

    devfs_file_ops_aux.inode_ops = &devfs_inode_ops;
    get_default_file_ops(&devfs_file_ops, &devfs_file_ops_aux);

    devfs = (filesys_t) {
        .name = "devfs",
        .id = 1,
        .ops = devfs_file_ops,
        .mount_count = 0
    };

    errno = filesys_mount_at("/", "dev", &devfs);
    panic_if(errno, "could not mount devfs: %e\n", errno);

    /* Make the tmpfs. */

    get_tmpfs_inode_ops(&tmpfs_inode_ops);

    tmpfs_file_ops_aux.inode_ops = &tmpfs_inode_ops;
    get_default_file_ops(&tmpfs_file_ops, &tmpfs_file_ops_aux);

    tmpfs = (filesys_t) {
        .name = "tmpfs",
        .id = 2,
        .ops = tmpfs_file_ops,
        .mount_count = 0
    };

    errno = filesys_mount_at("/", "tmp", &tmpfs);
    panic_if(errno, "could not mount tmpfs: %e\n", errno);
}
