#include <fs/tmpfs/inode_ops.h>
#include <std.h>
#include <string.h>
#include <stdops.h>
#include <heap.h>
#include <virtual_memory.h>
#include <timer.h>
#include <dbg.h>

/* tmpfs inode and block numbers are just physical addresses of the
 * associated allocated kernel memory; also as with cpfs, inode numbers
 * are just block numbers. */

INLINE uint8_t *tmpfs_block_addr(block_nr_t block_nr)
{
    return phys_to_kernel_virt(block_nr);
}

void tmpfs_read_block(block_nr_t block_nr, uint8_t *dst)
{
    panic_if(block_nr == 0, "got block_nr = 0\n");
    memcpy(dst, tmpfs_block_addr(block_nr), BLOCK_SIZE);
}

void tmpfs_write_block(block_nr_t block_nr, const uint8_t *src)
{
    panic_if(block_nr == 0, "got block_nr = 0\n");
    memcpy(tmpfs_block_addr(block_nr), src, BLOCK_SIZE);
}

errno_t tmpfs_alloc_inode(inode_ops_t *ops, inode_nr_t *inode_nr,
        inode_type_t type, inode_perm_t perm, device_id_t device_id)
{
    inode_t *inode = pmalloc(sizeof(inode_t));
    if (!inode) {
        return EFULL;
    }

    uint64_t time = timer_counter();

    memset(inode, sizeof(inode_t), 0);

    inode->nr = kernel_virt_to_phys(inode);
    inode->refs = 1;
    inode->size = 0;
    inode->type = type;
    inode->perm = perm;
    inode->time_created = time;
    inode->time_modified = time;

    if (type == INODE_CHARDEV || type == INODE_BLOCKDEV) {
        inode->content.device_id = device_id;
    }

    *inode_nr = inode->nr;

    return 0;
}

errno_t tmpfs_free_inode(inode_ops_t *ops, inode_nr_t inode_nr)
{
    panic("tmpfs_free_inode() not implemented\n");

    // TODO: free the referenced blocks
    pfree(phys_to_kernel_virt(inode_nr));
    return 0;
}

errno_t tmpfs_read_inode(inode_ops_t *ops, inode_nr_t inode_nr, inode_t *dst)
{
    memcpy(dst, tmpfs_block_addr((block_nr_t) inode_nr), sizeof(inode_t));
    return 0;
}

errno_t tmpfs_write_inode(inode_ops_t *ops, inode_nr_t inode_nr, const inode_t *src)
{
    memcpy(tmpfs_block_addr((block_nr_t) inode_nr), src, sizeof(inode_t));
    return 0;
}

// TODO: does this actually need an errno_t return value...
/* Requires that block_offset <= *offset < block_offset + BLOCK_SIZE.
 * Returns an error code, and also the number of bytes read by this function alone.
 * Also updates offset, buf, and buf_size. On error, the caller may assume that
 * no bytes were read into the buf.
 * If block_nr is 0, it indicates that zeros should be loaded into the buf instead
 * of actual data from a block. */
errno_t tmpfs_read_file_block(
        block_nr_t block_nr, uint64_t block_offset,
        uint64_t *offset, uint8_t **buf, uint64_t *buf_size,
        uint64_t *read)
{
    panic_if_not(block_offset <= *offset && *offset < block_offset + BLOCK_SIZE,
            "no overlap in tmpfs_read_file_block()\n");
    *read = min_uint64((block_offset + BLOCK_SIZE) - *offset, *buf_size);
    //printf("  tmpfs_read_file_block(block_nr="block_nr_spec", block offset=%u64, offset=%u64, buf_size=%u64): read %u64 bytes\n",
    //        block_nr, block_offset, *offset, *buf_size, *read);

    if (block_nr) {
        uint8_t *block = tmpfs_block_addr(block_nr);
        memcpy(*buf, block + (*offset - block_offset), *read);
    } else {
        memset(*buf, *read, 0);
    }

    *offset += *read;
    *buf += *read;
    panic_if(*read > *buf_size, "error\n");
    *buf_size -= *read;
    return 0;
}

/* array_stride should be BLOCK_NRS_PER_BLOCK ** indirection * BLOCK_SIZE,
 * but this is not checked.
 * The bytes_read returned is just the number of bytes read by this function,
 * i.e. it starts at 0. */
errno_t tmpfs_read_file_blocks(
        uint8_t indirection,
        uint64_t array_start_offset, uint64_t array_stride,
        block_nr_t *array, uint64_t array_size,
        uint64_t *offset, uint8_t **buf, uint64_t *buf_size,
        uint64_t *bytes_read)
{
    /* Get local copies; we'll update everything at the end. */
    uint64_t l_offset = *offset;
    uint64_t l_buf_size = *buf_size;

    uint64_t array_end_offset = array_start_offset + array_stride * array_size;

    /* Make sure it's not a pointless function call. */
    panic_if_not(array_start_offset <= l_offset && l_offset < array_end_offset,
            "no overlap in tmpfs_read_file_blocks()\n");

    uint64_t limit = min_uint64(array_end_offset - l_offset, l_buf_size);

    *bytes_read = 0;

    uint64_t start = (l_offset - array_start_offset) / array_stride;
    uint64_t end = round_up_div(l_offset + limit - array_start_offset, array_stride);
    //printf("tmpfs_read_file_blocks(indirection=%u8,\n"
    //        "    array_start_offset=%u64, array_stride=%u64, array_size=%u64,\n"
    //        "    offset=%u64, buf_size=%u64): start=%u64, end=%u64\n",
    //        indirection, array_start_offset, array_stride, array_size,
    //        l_offset, l_buf_size,
    //        start, end);

    panic_if(end > array_size,
            "something is wrong\n"
            "array_start_offset=%u64, array_stride=%u64, array_size=%u64, array_end_offset=%u64,\n"
            "offset=%u64, buf_size=%u64, limit=%u64,\n"
            "start=%u64, end=%u64",
            array_start_offset, array_stride, array_size, array_end_offset,
            l_offset, l_buf_size, limit,
            start, end);
    if (indirection == 0) {
        /* The array entries are direct pointers to blocks containing file content. */
        for (uint64_t i = start; i < end; i++) {
            uint64_t block_offset = array_start_offset + array_stride * i;
            uint64_t read;
            // TODO: just merge tmpfs_read_file_block into here, instead of separate call.
            errno_t errno = tmpfs_read_file_block(
                array[i], block_offset,
                offset, buf, buf_size,
                &read);
            //printf("  read %u64 from offset %u64 in direct_blocks[%u64]\n", read, offset - block_offset, direct_index);
            if (errno) {
                return errno;
            }
            *bytes_read += read;
        }
    } else {
        //printf("  indirection %u8: start=%u64, end=%u64\n", indirection, start, end);
        for (uint64_t i = start; i < end; i++) {
            uint64_t entry_offset = array_start_offset + array_stride * i;
            //printf("  indirection %u8: array[%u64] = "block_nr_spec"\n",
            //        indirection, i, array[i]);
            if (array[i]) {
                block_nr_t pointers[BLOCK_NRS_PER_BLOCK];
                tmpfs_read_block(array[i], (uint8_t *) pointers);

                uint64_t read;
                errno_t errno = tmpfs_read_file_blocks(
                        indirection - 1,
                        entry_offset, array_stride / BLOCK_NRS_PER_BLOCK,
                        pointers, BLOCK_NRS_PER_BLOCK,
                        offset, buf, buf_size,
                        &read);
                //printf("  read %u64 from offset %u64 in direct_blocks[%u64]\n", read, offset - block_offset, direct_index);
                if (errno) {
                    return errno;
                }
                *bytes_read += read;
            } else {
                panic("didn't expect sparse file\n"); // TODO: remove this (keep for now, for confidence)
                //printf("  read %u64 from offset %u64 in direct_blocks[%u64]\n", read, offset - block_offset, direct_index);
                memset(buf, limit, 0);
                *offset += limit;
                *buf += limit;
                panic_if(limit > *buf_size, "error\n");
                *buf_size -= limit;
                *bytes_read += limit;
            }
        }
    }

    return 0;
}

INLINE errno_t tmpfs_read_regular(inode_ops_t *ops, inode_t *inode, uint64_t offset,
        uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_read)
{
    errno_t errno;

    if (offset >= inode->size) {
        *bytes_read = 0;
        return 0;
    }
    if (buf_size > inode->size - offset) {
        buf_size = inode->size - offset;
    }

    const uint64_t END_EMBEDDED_CONTENT = INODE_EMBEDDED_CONTENT_SIZE;
    const uint64_t END_DIRECT = END_EMBEDDED_CONTENT
        + INODE_NUM_DIRECT * BLOCK_SIZE;
    const uint64_t END_INDIRECT = END_DIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;
    const uint64_t END_DOUBLE_INDIRECT = END_INDIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;
    const uint64_t END_TRIPLE_INDIRECT = END_DOUBLE_INDIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;

    *bytes_read = 0;

    if (buf_size == 0) {
        return 0;
    }
    if (buf_size > 0 && offset < END_EMBEDDED_CONTENT) {
        uint64_t read = min_uint64(END_EMBEDDED_CONTENT - offset, buf_size);
        memcpy(buf, inode->content.embedded + offset, read);
        offset += read;
        buf += read;
        panic_if(read > buf_size, "error\n");
        buf_size -= read;
        *bytes_read += read;
    }

    // TODO: do all of these with some kind of:
    //    tmpfs_read_file_blocks(
    //       indirection level,
    //       block_nr array start offset,
    //       block_nr array stride [to avoid computing based on indirection level each time],
    //       the actual array,
    //       &offset, &buf, &buf_size)
    // this would do the recursion as well.
    if (buf_size == 0) {
        return 0;
    }
    if (offset < END_DIRECT) {
        uint64_t read;
        errno = tmpfs_read_file_blocks(
                /*indirection*/ 0,
                END_EMBEDDED_CONTENT, BLOCK_SIZE,
                inode->blocks_direct, INODE_NUM_DIRECT,
                &offset, &buf, &buf_size, &read);
        *bytes_read += read;
        if (errno) {
            return errno;
        }
    }

    if (buf_size == 0) {
        return 0;
    }
    if (offset < END_INDIRECT) {
        if (inode->block_indirect) {
            block_nr_t pointers[BLOCK_NRS_PER_BLOCK];
            tmpfs_read_block(inode->block_indirect, (uint8_t *) pointers);

            uint64_t read;
            errno = tmpfs_read_file_blocks(
                    /*indirection*/ 0,
                    END_DIRECT, BLOCK_SIZE,
                    pointers, BLOCK_NRS_PER_BLOCK,
                    &offset, &buf, &buf_size, &read);
            *bytes_read += read;
            if (errno) {
                return errno;
            }
        } else {
            uint64_t read = min_uint64(END_INDIRECT - offset, buf_size);
            memset(buf, read, 0);
            offset += read;
            buf += read;
            panic_if(read > buf_size, "error\n");
            buf_size -= read;
            *bytes_read += read;
        }
    }

    if (buf_size == 0) {
        return 0;
    }
    if (offset < END_DOUBLE_INDIRECT) {
        if (inode->block_double_indirect) {
            block_nr_t pointers[BLOCK_NRS_PER_BLOCK];
            tmpfs_read_block(inode->block_double_indirect, (uint8_t *) pointers);
            if (errno) {
                return errno;
            }
            uint64_t read;
            errno = tmpfs_read_file_blocks(
                    /*indirection*/ 1,
                    END_INDIRECT, BLOCK_NRS_PER_BLOCK * BLOCK_SIZE,
                    pointers, BLOCK_NRS_PER_BLOCK,
                    &offset, &buf, &buf_size, &read);
            *bytes_read += read;
            if (errno) {
                return errno;
            }
        } else {
            uint64_t read = min_uint64(END_DOUBLE_INDIRECT - offset, buf_size);
            memset(buf, read, 0);
            offset += read;
            buf += read;
            panic_if(read > buf_size, "error\n");
            buf_size -= read;
            *bytes_read += read;
        }
    }

    if (buf_size == 0) {
        return 0;
    }
    if (offset < END_TRIPLE_INDIRECT) {
        panic("read from block_triple_indirect not implemented\n"
                "(buf_size remaining at %u64)\n", buf_size);
    }

    panic_if(buf_size > 0,
        "something is wrong; we should be done reading by now\n");
    return 0;
}

errno_t tmpfs_read_file(inode_ops_t *ops, inode_nr_t inode_nr, uint64_t offset,
        uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_read)
{
    inode_t inode;
    errno_t errno = ops->read_inode ? ops->read_inode(ops, inode_nr, &inode) : ENOTSUP;
    if (errno) {
        return errno;
    }

    switch (inode.type) {
        case INODE_NONE:
        case INODE_CHARDEV:
        case INODE_BLOCKDEV:
            panic("cannot read regular file from inode "inode_nr_spec"; type is %s\n",
                    inode_nr,
                    inode.type == INODE_NONE ? "none" :
                    inode.type == INODE_CHARDEV ? "chardev" :
                    "blockdev");
        case INODE_FILE:
        case INODE_DIR:
        case INODE_SYMLINK:
            return tmpfs_read_regular(ops, &inode, offset,
                    buf, buf_size,
                    bytes_read);
        default:
            panic("unexpected inode type %u64\n", (uint64_t) inode.type);
    }
}

/* Requires that block_offset <= *offset < block_offset + BLOCK_SIZE.
 * Returns the number of bytes written by this function alone.
 * Also updates offset, buf, and buf_size. */
void tmpfs_write_file_block(
        block_nr_t block_nr, uint64_t block_offset,
        uint64_t *offset, const uint8_t **buf, uint64_t *buf_size,
        uint64_t *written)
{
    panic_if_not(block_nr, "block_nr must be nonzero\n");
    panic_if_not(block_offset <= *offset && *offset < block_offset + BLOCK_SIZE,
            "no overlap in tmpfs_read_file_block()\n");
    *written = min_uint64((block_offset + BLOCK_SIZE) - *offset, *buf_size);

    uint8_t *block = tmpfs_block_addr(block_nr);
    memcpy(block + (*offset - block_offset), *buf, *written);

    *offset += *written;
    *buf += *written;
    panic_if(*written > *buf_size, "error\n");
    *buf_size -= *written;
}

/* array_stride should be BLOCK_NRS_PER_BLOCK ** indirection * BLOCK_SIZE,
 * but this is not checked.
 * The bytes_read returned is just the number of bytes read by this function,
 * i.e. it starts at 0. */
USE_VALUE errno_t tmpfs_write_file_blocks(
        uint8_t indirection,
        uint64_t array_start_offset, uint64_t array_stride,
        block_nr_t *array, uint64_t array_size,
        uint64_t *offset, const uint8_t **buf, uint64_t *buf_size,
        uint64_t file_size,
        uint64_t *bytes_written, bool *updated_array)
{
    /* Get local copies; we'll update everything at the end. */
    uint64_t l_offset = *offset;
    uint64_t l_buf_size = *buf_size;

    uint64_t array_end_offset = array_start_offset + array_stride * array_size;

    panic_if_not(array_start_offset <= l_offset,
            "tmps_write_file_blocks() called with offset too low\n");

    if (l_offset >= array_end_offset) {
        /* Sparse write: set all previously uninitialized array entries to 0,
         * which is the sparse-block marker. Depending on the current file size
         * in relation to the array start offset and array stride, early array
         * entries are already assigned and valid, while later entries must be
         * initialized. */
        uint64_t num_valid_entries;
        if (file_size < array_start_offset) {
            *updated_array = true;
            num_valid_entries = 0;
        } else if (file_size < array_end_offset) {
            *updated_array = true;
            num_valid_entries = (file_size - array_start_offset) / array_stride;
        } else {
            *updated_array = false;
            num_valid_entries = array_size;
        }
        for (uint64_t i = num_valid_entries; i < array_size; i++) {
            array[i] = 0;
        }
        /* No change to offset, buf, or buf_size; we haven't written anything. */
        *bytes_written = 0;
        return 0;
    }

    uint64_t limit = min_uint64(array_end_offset - l_offset, l_buf_size);

    *bytes_written = 0;
    *updated_array = false;

    uint64_t start = (l_offset - array_start_offset) / array_stride;
    uint64_t end = round_up_div(l_offset + limit - array_start_offset, array_stride);
    //printf("tmpfs_write_file_blocks(indirection=%u8,\n"
    //        "    array_start_offset=%u64, array_stride=%u64, array_size=%u64,\n"
    //        "    offset=%u64, buf_size=%u64): start=%u64, end=%u64\n",
    //        indirection, array_start_offset, array_stride, array_size,
    //        l_offset, l_buf_size,
    //        start, end);

    panic_if(end > array_size,
            "something is wrong\n"
            "array_start_offset=%u64, array_stride=%u64, array_size=%u64, array_end_offset=%u64,\n"
            "offset=%u64, buf_size=%u64, limit=%u64,\n"
            "start=%u64, end=%u64",
            array_start_offset, array_stride, array_size, array_end_offset,
            l_offset, l_buf_size, limit,
            start, end);
    if (indirection == 0) {
        /* The array entries are direct pointers to blocks containing file content. */
        for (uint64_t i = start; i < end; i++) {
            if (!array[i]) {
                /* Allocate a data block! */
                uint8_t *block = pmalloc(BLOCK_SIZE);
                if (!block) {
                    // TODO: undo changes?
                    return EFULL;
                }
                memset(block, BLOCK_SIZE, 0);
                array[i] = (block_nr_t) kernel_virt_to_phys(block);
                *updated_array = true;
            }

            uint64_t block_offset = array_start_offset + array_stride * i;
            uint64_t written;
            // TODO: just merge tmpfs_write_file_block into here, instead of separate call.
            tmpfs_write_file_block(
                array[i], block_offset,
                offset, buf, buf_size,
                &written);
            //printf("  wrote %u64 from offset %u64 in direct_blocks[%u64]\n", written, offset - block_offset, direct_index);
            *bytes_written += written;
        }
    } else {
        //printf("  indirection %u8: start=%u64, end=%u64\n", indirection, start, end);
        for (uint64_t i = start; i < end; i++) {
            uint64_t entry_offset = array_start_offset + array_stride * i;
            //printf("  indirection %u8: array[%u64] = "block_nr_spec"\n",
            //        indirection, i, array[i]);
            if (!array[i]) {
                /* Allocate an indirect block. */
                uint8_t *block = pmalloc(BLOCK_SIZE);
                if (!block) {
                    // TODO: undo changes?
                    return EFULL;
                }
                memset(block, BLOCK_SIZE, 0);
                array[i] = (block_nr_t) kernel_virt_to_phys(block);
                *updated_array = true;
            }

            block_nr_t pointers[BLOCK_NRS_PER_BLOCK];
            tmpfs_read_block(array[i], (uint8_t *) pointers);

            uint64_t written;
            bool pointers_updated;
            errno_t errno = tmpfs_write_file_blocks(
                    indirection - 1,
                    entry_offset, array_stride / BLOCK_NRS_PER_BLOCK,
                    pointers, BLOCK_NRS_PER_BLOCK,
                    offset, buf, buf_size, file_size,
                    &written, &pointers_updated);
            if (errno) {
                // TODO: undo changes?
                return errno;
            }
            //printf("  wrote %u64 from offset %u64 in direct_blocks[%u64]\n", written, offset - block_offset, direct_index);
            *bytes_written += written;
            if (pointers_updated) {
                tmpfs_write_block(array[i], (uint8_t *) pointers);
            }
        }
    }

    return 0;
}

INLINE errno_t tmpfs_write_regular(inode_ops_t *ops, inode_nr_t inode_nr, inode_t *inode,
        uint64_t offset, bool append, const uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_written)
{
    errno_t errno;

    if (append) {
        offset = inode->size;
    }
    panic_if(offset + buf_size < offset,
            "ain't no way the file and buffer are big enough to wrap around 64-bit memory\n");

    uint64_t time = timer_counter();
    bool updated_inode = false;

    const uint64_t END_EMBEDDED_CONTENT = INODE_EMBEDDED_CONTENT_SIZE;
    const uint64_t END_DIRECT = END_EMBEDDED_CONTENT
        + INODE_NUM_DIRECT * BLOCK_SIZE;
    const uint64_t END_INDIRECT = END_DIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;
    const uint64_t END_DOUBLE_INDIRECT = END_INDIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;
    const uint64_t END_TRIPLE_INDIRECT = END_DOUBLE_INDIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;

    *bytes_written = 0;

    if (offset + buf_size > END_TRIPLE_INDIRECT) {
        return ERANGE;
    }

    if (buf_size == 0) {
        errno = 0;
        goto tmpfs_write_file_done;
    }
    if (buf_size > 0 && offset < END_EMBEDDED_CONTENT) {
        uint64_t write = min_uint64(END_EMBEDDED_CONTENT - offset, buf_size);
        //printf("  wrote %u64 from offset %u64 in embedded content\n", write, offset);
        memcpy(inode->content.embedded + offset, buf, write);
        offset += write;
        buf += write;
        panic_if(write > buf_size, "error\n");
        buf_size -= write;
        *bytes_written += write;
        updated_inode = true;
    }
    if (offset > inode->size) {
        /* Ratchet up the inode size to what we now know has been written. */
        inode->size = min_uint64(offset, END_EMBEDDED_CONTENT);
        updated_inode = true;
    }

    if (buf_size == 0) {
        errno = 0;
        goto tmpfs_write_file_done;
    }
    /* direct blocks */
    {
        uint64_t write;
        bool updated_array;
        errno = tmpfs_write_file_blocks(
                /*indirection*/ 0,
                END_EMBEDDED_CONTENT, BLOCK_SIZE,
                inode->blocks_direct, INODE_NUM_DIRECT,
                &offset, &buf, &buf_size, inode->size,
                &write, &updated_array);
        *bytes_written += write;
        if (updated_array) {
            updated_inode = true;
        }
        if (errno) {
            goto tmpfs_write_file_done;
        }
    }
    if (offset > inode->size) {
        /* Ratchet up the inode size to what we now know has been written. */
        inode->size = min_uint64(offset, END_DIRECT);
        updated_inode = true;
    }

    if (buf_size == 0) {
        errno = 0;
        goto tmpfs_write_file_done;
    }
    /* indirect block */
    {
        uint64_t write;
        bool updated_array;
        errno = tmpfs_write_file_blocks(
                /*indirection*/ 1,
                END_DIRECT, BLOCK_SIZE * BLOCK_NRS_PER_BLOCK,
                &inode->block_indirect, 1,
                &offset, &buf, &buf_size, inode->size,
                &write, &updated_array);
        *bytes_written += write;
        if (updated_array) {
            updated_inode = true;
        }
        if (errno) {
            goto tmpfs_write_file_done;
        }
    }
    if (offset > inode->size) {
        /* Ratchet up the inode size to what we now know has been written. */
        inode->size = min_uint64(offset, END_INDIRECT);
        updated_inode = true;
    }

    if (buf_size == 0) {
        errno = 0;
        goto tmpfs_write_file_done;
    }
    /* double-indirect block */
    {
        uint64_t write;
        bool updated_array;
        errno = tmpfs_write_file_blocks(
                /*indirection*/ 2,
                END_INDIRECT, BLOCK_SIZE * BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK,
                &inode->block_double_indirect, 1,
                &offset, &buf, &buf_size, inode->size,
                &write, &updated_array);
        *bytes_written += write;
        if (updated_array) {
            updated_inode = true;
        }
        if (errno) {
            goto tmpfs_write_file_done;
        }
    }
    if (offset > inode->size) {
        /* Ratchet up the inode size to what we now know has been written. */
        inode->size = min_uint64(offset, END_DOUBLE_INDIRECT);
        updated_inode = true;
    }

    if (buf_size == 0) {
        errno = 0;
        goto tmpfs_write_file_done;
    }
    /* triple-indirect block */
    {
        uint64_t write;
        bool updated_array;
        errno = tmpfs_write_file_blocks(
                /*indirection*/ 3,
                END_INDIRECT, BLOCK_SIZE * BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK,
                &inode->block_triple_indirect, 1,
                &offset, &buf, &buf_size, inode->size,
                &write, &updated_array);
        *bytes_written += write;
        if (updated_array) {
            updated_inode = true;
        }
        if (errno) {
            goto tmpfs_write_file_done;
        }
    }
    if (offset > inode->size) {
        /* Ratchet up the inode size to what we now know has been written. */
        inode->size = min_uint64(offset, END_TRIPLE_INDIRECT);
        updated_inode = true;
    }

    panic_if(buf_size > 0,
        "something is wrong; we should be done reading by now\n");
    errno = 0;
    goto tmpfs_write_file_done;

tmpfs_write_file_done:
    if (updated_inode) {
        inode->time_modified = time;
        errno = ops->write_inode(ops, inode_nr, inode);
    }
    return errno;
}

errno_t tmpfs_write_file(inode_ops_t *ops, inode_nr_t inode_nr, uint64_t offset,
        bool append,
        const uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_written)
{
    /* Make sure we can read/write inodes. */
    if (!ops->read_inode || !ops->write_inode) {
        return ENOTSUP;
    }

    inode_t inode;
    errno_t errno = ops->read_inode(ops, inode_nr, &inode);
    if (errno) {
        return errno;
    }

    switch (inode.type) {
        case INODE_NONE:
        case INODE_CHARDEV:
        case INODE_BLOCKDEV:
            panic("cannot write regular file at inode "inode_nr_spec"; type is %s\n",
                    inode_nr,
                    inode.type == INODE_NONE ? "none" :
                    inode.type == INODE_CHARDEV ? "chardev" :
                    "blockdev");
        case INODE_FILE:
        case INODE_DIR:
        case INODE_SYMLINK:
            return tmpfs_write_regular(ops, inode_nr, &inode,
                    offset, append, buf, buf_size,
                    bytes_written);
        default:
            panic("unexpected inode type %u64\n", (uint64_t) inode.type);
    }
}

void get_tmpfs_inode_ops(inode_ops_t *dst)
{
    *dst = (inode_ops_t)  {
        .alloc_inode = &tmpfs_alloc_inode,
        .free_inode = &tmpfs_free_inode,
        .read_inode = &tmpfs_read_inode,
        .write_inode = &tmpfs_write_inode,
        .read_file = &tmpfs_read_file,
        .write_file = &tmpfs_write_file,
        .aux = NULL
    };

    /* Create /. */
    inode_nr_t root_nr;
    errno_t errno = tmpfs_alloc_inode(dst, &root_nr,
        INODE_DIR, INODE_PERM_R | INODE_PERM_W | INODE_PERM_X, 0);
    panic_if(errno, "could not make tmpfs root inode: %e\n", errno);
    dst->root_nr = root_nr;

    /* Create and add the . entry. */
    dir_entry_t entry;
    memset(&entry, sizeof(dir_entry_t), 0);
    entry.inode_nr = root_nr;
    entry.name_len = 1;
    entry.name[0] = '.';

    uint64_t bytes_written;
    errno = tmpfs_write_file(dst, root_nr, 0, /*append*/ false,
            (uint8_t *) &entry, sizeof(dir_entry_t),
            &bytes_written);
    panic_if(errno, "could not make tmpfs /.: %e\n", errno);
    panic_if(bytes_written != sizeof(dir_entry_t),
            "did not write entirety of tmpfs /. entry\n");

    /* Create and add the .. entry. */
    entry.name_len = 2;
    entry.name[1] = '.';

    errno = tmpfs_write_file(dst, root_nr, sizeof(dir_entry_t),
            /*append*/ false,
            (uint8_t *) &entry, sizeof(dir_entry_t),
            &bytes_written);
    panic_if(errno, "could not make tmpfs /..: %e\n", errno);
    panic_if(bytes_written != sizeof(dir_entry_t),
            "did not write entirety of tmpfs /.. entry\n");
}
