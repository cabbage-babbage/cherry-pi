#include <fs/initrd.h>
#include <std.h>
#include <string.h>
#include <stdops.h>
#include <virtual_memory.h>
#include <dbg.h>

errno_t initrd_read_block(struct block_ops *ops, block_nr_t block_nr, uint8_t *dst)
{
    initrd_block_ops_aux_t *aux = (initrd_block_ops_aux_t *) ops->aux;

    panic_if(block_nr >= aux->num_blocks,
            "block number "block_nr_spec" out of bounds\n");
    memcpy(dst, (uint8_t *) aux->initrd_start + block_nr * BLOCK_SIZE, BLOCK_SIZE);
    return 0;
}
errno_t initrd_write_block(struct block_ops *ops, block_nr_t block_nr, const uint8_t *src)
{
    initrd_block_ops_aux_t *aux = (initrd_block_ops_aux_t *) ops->aux;

    panic_if(block_nr >= aux->num_blocks,
            "block number "block_nr_spec" out of bounds\n");
    memcpy((uint8_t *) aux->initrd_start + block_nr * BLOCK_SIZE, src, BLOCK_SIZE);
    return 0;
}

extern_symbol _kernel_load_high_end;

void get_initrd_block_ops(block_ops_t *dst, initrd_block_ops_aux_t *aux)
{
    aux->initrd_start = (ptr_t) align_up((uint64_t) &_kernel_load_high_end, PAGE_SIZE);
    // TODO: this assumes that the initrd encodes a cpfs image; make it so we don't
    // have to assume this!
    aux->num_blocks = *((uint64_t *) aux->initrd_start);
    printf("initrd begins at 0x%p\n", aux->initrd_start);

    *dst = (block_ops_t) {
        .read_block = &initrd_read_block,
        .write_block = &initrd_write_block,
        .aux = aux
    };
}
