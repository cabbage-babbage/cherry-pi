#include <fs/cache.h>
#include <synch/lock.h>

///* This lock protects each entry's 'valid', 'can_evict', 'device_nr', and 'block' entries. */
//lock_t fs_cache_lock;
//fs_cache_entry_t fs_cache[FS_CACHE_ENTRIES];

void initialize_fs_cache()
{
    // TODO
    //create_lock(&fs_cache_lock);
    //for (uint32_t i = 0; i < FS_CACHE_ENTRIES; i++) {
    //    fs_cache[i].valid = false;
    //    fs_cache[i].pinned = false;
    //    create_rw_lock(&fs_cache[i].lock);
    //}
}

#define FS_CACHE_ENTRIES 10

typedef struct {
    /* Specifies whether or not the entry currently has contents associated to the
     * stored device and block numbers. */
    bool valid;

    fs_device_nr_t device_nr;
    block_nr_t block;

    /* This specifically protects 'contents' and 'modified', and must be held while accessing
     * the entry contents. This lock may only be acquired if the entry is pinned and ready. */
    rw_lock_t lock;
    uint8_t contents[BLOCK_SIZE];
    bool modified;
} fs_cache_entry_t;

///* The fs_cache_lock must be held during this function call, and this does not
// * block.
// * On success (that is, if the given device's block is in the cache), we
// * return the cache entry, pinned. On failure, NULL. */
//fs_cache_entry_t *fs_find_in_cache(fs_device_nr_t device_nr, block_nr_t block)
//{
//    // TODO: there must a better way
//    for (uint32_t i = 0; i < FS_CACHE_ENTRIES; i++) {
//        if (!fs_cache[i].valid) {
//            continue;
//        }
//        if (fs_cache[i].device_nr == device_nr && fs_cache[i].block == block) {
//            fs_cache[i].pinned = true;
//            return &fs_cache[i];
//        }
//    }
//    return NULL;
//}
//
///* The fs_cache_lock must be held during this function call, and this does not
// * block.
// * Find an available (invalid) entry in the cache. If the cache is full, evict
// * an existing entry. (If it is impossible to evict an entry, for example if
// * every single entry is pinned, this produces the errno EAGAIN.)
// * Returns 0 on success, and writes the fresh entry to 'cached', with the entry
// * marked valid and pinned. On failure, returns -errno and sets 'cached'
// * to NULL. */
//int64_t fs_cache_allocate(fs_cache_entry_t **cached)
//{
//    for (uint32_t i = 0; i < FS_CACHE_ENTRIES; i++) {
//        if (!fs_cache[i].valid) {
//            fs_cache[i].valid = true;
//            fs_cache[i].pinned = true;
//            *cached = &fs_cache[i];
//            return 0;
//        }
//    }
//
//    /* We must evict an entry. Prefer evicting non-modified entries, since we
//     * don't have to write back. */
//
//    bool all_pinned = true;
//    for (uint32_t i = 0; i < FS_CACHE_ENTRIES; i++) {
//        if (!fs_cache[i].pinned) {
//            all_pinned = false;
//        }
//    }
//
//    if (all_pinned) {
//        return -EAGAIN;
//    }
//
//    // TODO: pick a non-pinned entry to evict
//    panic("fs cache eviction not implemented\n");
//}
//
///* Cached load (blocking). On success, returns 0 and writes the cached block structure
// * to 'cached', with the entry pinned. On failure, returns -errno and sets
// * 'cached' to NULL. */
//int64_t fs_load_block(fs_device_t *dev, block_nr_t block_nr, fs_cache_entry_t **cached)
//{
//    acquire_lock(&fs_cache_lock);
//
//    fs_cache_entry_t *entry = fs_find_in_cache(dev->device_nr, block_nr);
//    if (entry) {
//        release_lock(&fs_cache_lock);
//        *cached = entry;
//        return 0;
//    }
//
//    int64_t errcode = fs_cache_allocate(&entry);
//
//    if (errcode) {
//        release_lock(&fs_cache_lock);
//        *cached = NULL;
//        return errcode;
//    }
//
//    entry->valid = true;
//    entry->pinned = true;
//    entry->device_nr = dev->device_nr;
//    entry->block = block_nr;
//    entry->modified = false;
//
//    acquire_write(&entry->lock);
//    release_lock(&fs_cache_lock);
//
//    errcode = dev->read(block_nr, entry->contents);
//    if (errcode) {
//        entry->pinned = false;
//        *cached = NULL;
//        return errcode;
//    }
//
//    *cached = entry;
//    return 0;
//}

/* Blocking read. dst must be a least BLOCK_SIZE byes. */
int64_t fs_cache_read(fs_device_t *dev, block_nr_t block_nr, uint8_t *dst)
{
    if (dev->cacheable) {
        // TODO: actually cache
        return dev->read(dev, block_nr, dst);
    } else {
        return dev->read(dev, block_nr, dst);
    }
}

/* Blocking write. src must be a least BLOCK_SIZE byes. */
int64_t fs_cache_write(fs_device_t *dev, block_nr_t block_nr, const uint8_t *src)
{
    if (dev->cacheable) {
        // TODO: actually cache
        return dev->write(dev, block_nr, src);
    } else {
        return dev->write(dev, block_nr, src);
    }
}
