#include <fs/filesys.h>
#include <synch/lock.h>
#include <heap.h>
#include <string.h>
#include <fs/inode.h>
#include <uapi/errno.h>
#include <dbg.h>

/* Global lock for working with filesystems and mounts. Parallelism isn't really
 * necessary for such operations, since they don't occur very often.
 * Most operations will acquire the lock as a reader, e.g. for walking the mounts
 * to find an inode. Only those operations that actually modify the mounts, like
 * filesys_mount() and filesys_unmount(), need to acquire this as a writer.
 * This lock prefers writers, so mount operations cannot starve even if the
 * filesystem is busy. */
rw_lock_t filesys_lock;

mount_t root_mount;

void initialize_filesys()
{
    create_rw_lock(&filesys_lock, /*prefer_writers*/ true);

    root_mount = (mount_t) {
        .parent = NULL,
        .filesys = NULL,
        .submounts_head = NULL,
        .depth = 0
    };
}

errno_t filesys_mount_root(filesys_t *filesys)
{
    if (root_mount.filesys) {
        printf("BUG: filesys_mount_root() was called more than once\n");
        return -EINVAL;
    }

    root_mount.filesys = filesys;
    filesys->mount_count++;
    return 0;
}

errno_t filesys_mount(mount_t *mount, mount_point_t point, filesys_t *filesys)
{
    //if (mount->depth >= MAX_MOUNT_DEPTH) {
    //    return -EMDEPTH;
    //}

    submount_t *submount = pmalloc(sizeof(submount_t));
    if (!submount) {
        return ENOMEM;
    }
    *submount = (submount_t) {
        .next = NULL,
        .point = point,
        .mount = (mount_t) {
            .parent = mount,
            .point = point,
            .filesys = filesys,
            .submounts_head = NULL,
            .depth = mount->depth + 1
        }
    };

    acquire_write(&filesys_lock);
    submount->next = mount->submounts_head;
    mount->submounts_head = submount;
    filesys->mount_count++;
    release_write(&filesys_lock);

    return 0;
}

errno_t filesys_unmount(mount_t *mount, bool sync)
{
    // TODO: support this
    return ENOTSUP;
}

INLINE fs_locator_t open_file_locator(open_file_t file)
{
    return (fs_locator_t) {
        .mount = file.mount,
        .inode_nr = file.file->inode_nr
    };
}

/* Splits the given path into the first path component and the rest of the path.
 * The first path component (written into the 'part' buffer, with length written to
 * 'part_len') is defined to be the longest prefix of the path ending with a '/' or
 * the end of the string. The updated 'path' points to the remaining components, and
 * never begins with '/'.
 * Returns true on success, and false if the first component is too long (i.e. longer
 * than FILE_NAME_MAX_LEN), in which case all out parameters are unmodified.
 *
 * Examples:
 *    split_path("a/b/c") => component "a", remaining path "b/c"
 *    split_path("/a/b/c") => component "", remaining path "a/b/c"
 *    split_path("a///b/c") => component "a", remaining path "b/c"
 *    split_path("") => component "", remaining path ""
 *    split_path("aa.....aa/b/c") => failure, name too long
 * */
bool split_path(const char **path, char *part, uint64_t *part_len)
{
    const char *local_path = *path;
    uint64_t local_part_len = 0;
    while (*local_path && *local_path != '/') {
        if (local_part_len == FILE_NAME_MAX_LEN) {
            return false;
        }
        part[local_part_len++] = *local_path++;
    }
    while (*local_path == '/') {
        local_path++;
    }
    *path = local_path;
    *part_len = local_part_len;
    return true;
}

/* If the mount has a submount at the specified directory and filename, returns
 * the submount via 'submount'. Otherwise, returns false and sets submount to NULL.
 * The filesys_lock should be held (as a reader) during this call. */
bool find_submount(mount_t *mount, inode_nr_t dir, const char *name,
        mount_t **submount)
{
    // TODO: find a more efficient way. (though usually there won't be many mounts,
    // so maybe it's fine...)
    for (submount_t *cur = mount->submounts_head; cur; cur = cur->next) {
        if (cur->point.dir == dir && streq(cur->point.name, name)) {
            *submount = &cur->mount;
            return true;
        }
    }
    return false;
}

/* Opens the locator and puts the result in file. On failure, returns the errno,
 * and file is undefined (but not opened, of course); file should not be closed. */
INLINE errno_t open_locator(fs_locator_t loc, open_file_t *file)
{
    //printf("filesys:opening inode "inode_nr_spec"\n", loc.inode_nr);
    file->mount = loc.mount;
    return loc.mount->filesys->ops.open_file
        ? loc.mount->filesys->ops.open_file(&loc.mount->filesys->ops,
                loc.inode_nr, &file->file)
        : ENOTSUP;
}

// TODO: only close directory once we've opened the entry? or is it ok to close
// the directory and then open the entry, since we know the entry inode_nr?
// maybe synchronization issue with the entry getting deleted in the middle...
errno_t filesys_open_at(fs_locator_t dir, const char *path, open_file_t *file)
{
    char part[FILE_NAME_MAX_LEN + 1];
    uint64_t part_len;

    //fs_locator_t mount_stack[MAX_MOUNT_DEPTH];

    //printf("filesys:open_at(dir.inode_nr="inode_nr_spec", path=%qs):\n",
    //        dir.inode_nr, path);

    if (*path == '/') {
        dir = (fs_locator_t) {
            .mount = &root_mount,
            .inode_nr = root_mount.filesys->ops.root_nr
        };
        while (*path == '/') {
            path++;
        }
    }

    acquire_read(&filesys_lock);

    open_file_t cur;
    uint32_t errno = open_locator(dir, &cur);
    if (errno) {
        release_read(&filesys_lock);
        return errno;
    }

    while (*path) {
        if (!split_path(&path, part, &part_len)) {
            release_read(&filesys_lock);
            return EFNAME;
        }

        /* Null-terminate. */
        part[part_len] = 0;

        /* Check if we're entering a submount. */
        mount_t *submount;
        if (find_submount(cur.mount, cur.file->inode_nr, part, &submount)) {
            if (submount->depth != cur.mount->depth + 1) {
                printf("bug: submount depth (%u32) is not mount depth (%u32) + 1\n",
                        submount->depth, cur.mount->depth);
            }

            //mount_stack[cur.mount->depth] = cur;
            filesys_close(cur);
            fs_locator_t next_loc = {
                .mount = submount,
                .inode_nr = submount->filesys->ops.root_nr
            };
            if ((errno = open_locator(next_loc, &cur))) {
                release_read(&filesys_lock);
                return errno;
            }
        }
        /* Check if we're leaving a submount. */
        else if (cur.file->inode_nr == cur.mount->filesys->ops.root_nr
                 && streq(part, "..") && cur.mount->depth > 0) {
            //cur = mount_stack[cur.mount->depth - 1];
            fs_locator_t next_loc = {
                .mount = cur.mount->parent,
                .inode_nr = cur.mount->point.dir
            };
            filesys_close(cur);
            if ((errno = open_locator(next_loc, &cur))) {
                release_read(&filesys_lock);
                return errno;
            }
        }
        /* Otherwise, standard behavior. */
        else {
            fs_locator_t next_loc;
            next_loc.mount = cur.mount;
            errno = cur.mount->filesys->ops.find_entry
                ? cur.mount->filesys->ops.find_entry(&cur.mount->filesys->ops,
                        cur.file, part, &next_loc.inode_nr)
                : ENOTSUP;
            filesys_close(cur);
            if (errno) {
                release_read(&filesys_lock);
                return errno;
            }
            if ((errno = open_locator(next_loc, &cur))) {
                release_read(&filesys_lock);
                return errno;
            }
        }
    }

    release_read(&filesys_lock);

    *file = cur;
    //printf("filesys:open_at => inode %u64, open_count %u64\n",
    //        cur.file->inode_nr, cur.file->open_count);
    return 0;
}

USE_VALUE errno_t filesys_open(const char *path, open_file_t *file)
{
    fs_locator_t root = {
        .mount = &root_mount,
        .inode_nr = root_mount.filesys->ops.root_nr
    };
    return filesys_open_at(root, path, file);
}

USE_VALUE errno_t filesys_walk_at(fs_locator_t dir, const char *path, fs_locator_t *dst)
{
    open_file_t file;
    errno_t errno = filesys_open_at(dir, path, &file);
    if (errno) {
        return errno;
    }

    dst->mount = file.mount;
    dst->inode_nr = file.file->inode_nr;
    filesys_close(file);

    return 0;
}

USE_VALUE errno_t filesys_walk(const char *path, fs_locator_t *dst)
{
    fs_locator_t root = {
        .mount = &root_mount,
        .inode_nr = root_mount.filesys->ops.root_nr
    };
    return filesys_walk_at(root, path, dst);
}

INLINE errno_t filesys_dup(open_file_t file)
{
    //printf("filesys:duplicating open inode "inode_nr_spec"\n",
    //        file.file->inode_nr);
    file_ops_t *ops = &file.mount->filesys->ops;
    if (ops->dup_file) {
        return ops->dup_file(ops, file.file);
    } else {
        return ENOTSUP;
    }
}

INLINE void filesys_close(open_file_t file)
{
    //printf("filesys:closing open inode "inode_nr_spec"\n",
    //        file.file->inode_nr);
    file_ops_t *ops = &file.mount->filesys->ops;
    errno_t errno;
    if (ops->close_file) {
        errno = ops->close_file(ops, file.file);
    } else {
        errno = ENOTSUP;
    }

    if (errno) {
        printf("bug: could not close an open file: %e\n", errno);
    }
}

INLINE errno_t filesys_find_entry(open_file_t dir, const char *name, inode_nr_t *entry_nr)
{
    file_ops_t *ops = &dir.mount->filesys->ops;
    if (ops->find_entry) {
        return ops->find_entry(ops, dir.file, name, entry_nr);
    } else {
        return ENOTSUP;
    }
}

INLINE errno_t filesys_create_entry(open_file_t dir, const char *name,
            inode_type_t type, inode_perm_t perm, device_id_t device_id,
            bool *is_new, inode_nr_t *entry_nr)
{
    file_ops_t *ops = &dir.mount->filesys->ops;
    if (ops->create_entry) {
        return ops->create_entry(ops, dir.file, name, type, perm, device_id, is_new, entry_nr);
    } else {
        return ENOTSUP;
    }
}

// TODO: only close directory once we've opened the entry? or is it ok to close
// the directory and then open the entry, since we know the entry inode_nr?
// maybe synchronization issue with the entry getting deleted in the middle...
errno_t filesys_create_at(
        fs_locator_t dir, const char *path, bool keep_open,
        inode_type_t type, inode_perm_t perm, device_id_t device_id,
        bool *is_new, open_file_t *file)
{
    //printf("filesys_create_at(%u64:"inode_nr_spec", %qs, keep_open=%b)\n",
    //        dir.mount->filesys->id, dir.inode_nr, path, keep_open);

    char part[FILE_NAME_MAX_LEN + 1];
    uint64_t part_len;

    if (*path == '/') {
        dir = (fs_locator_t) {
            .mount = &root_mount,
            .inode_nr = root_mount.filesys->ops.root_nr
        };
        while (*path == '/') {
            path++;
        }
        //printf("  starting at /\n");
    }

    acquire_read(&filesys_lock);

    open_file_t cur;
    uint32_t errno = open_locator(dir, &cur);
    if (errno) {
        release_read(&filesys_lock);
        return errno;
    }

    bool got_basename = false;
    bool already_exists = false;

    while (*path) {
        if (!split_path(&path, part, &part_len)) {
            release_read(&filesys_lock);
            return EFNAME;
        }

        /* Null-terminate. */
        part[part_len] = 0;

        bool is_basename = *path == 0;
        //printf("  current part: %qs (is basename: %b)\n", part, is_basename);

        if (is_basename) {
            got_basename = true;

            if (streq(part, "") || streq(part, ".") || streq(part, "..")) {
                release_read(&filesys_lock);
                return EINVAL;
            }
        }

        /* Check if we're entering a submount. */
        mount_t *submount;
        if (find_submount(cur.mount, cur.file->inode_nr, part, &submount)) {
            if (submount->depth != cur.mount->depth + 1) {
                printf("bug: submount depth (%u32) is not mount depth (%u32) + 1\n",
                        submount->depth, cur.mount->depth);
            }

            filesys_close(cur);
            fs_locator_t next_loc = {
                .mount = submount,
                .inode_nr = submount->filesys->ops.root_nr
            };
            if ((errno = open_locator(next_loc, &cur))) {
                release_read(&filesys_lock);
                return errno;
            }
            //printf("    entered submount, now at %u64:"inode_nr_spec"\n",
            //        cur.mount->filesys->id, cur.file->inode_nr);

            if (is_basename) {
                //printf("    path already exists\n");
                already_exists = true;
            }
        }
        /* Check if we're leaving a submount. */
        else if (cur.file->inode_nr == cur.mount->filesys->ops.root_nr
                 && streq(part, "..") && cur.mount->depth > 0) {
            fs_locator_t next_loc = {
                .mount = cur.mount->parent,
                .inode_nr = cur.mount->point.dir
            };
            filesys_close(cur);
            if ((errno = open_locator(next_loc, &cur))) {
                release_read(&filesys_lock);
                return errno;
            }

            panic_if(is_basename, "shouldn't have gotten here: basename cannot be '..'\n");
        }
        /* Otherwise, standard behavior. */
        else {
            fs_locator_t next_loc;
            next_loc.mount = cur.mount;
            errno = cur.mount->filesys->ops.find_entry
                ? cur.mount->filesys->ops.find_entry(&cur.mount->filesys->ops,
                        cur.file, part, &next_loc.inode_nr)
                : ENOTSUP;
            //printf("    looking up in current dir: %e\n", errno);
            /* It's fine if the basename does not exist. */
            if (errno == ENOENT && is_basename) {
                //printf("    basename does not exist; will create it\n");
                /* Do not close cur; we need it open to add the new entry later. */
                already_exists = false;
                break;
            }

            filesys_close(cur);
            if (errno) {
                release_read(&filesys_lock);
                return errno;
            }
            if ((errno = open_locator(next_loc, &cur))) {
                release_read(&filesys_lock);
                return errno;
            }

            if (is_basename) {
                //printf("    path already exists\n");
                already_exists = true;
            }
        }
    }

    release_read(&filesys_lock);

    /* If already_exists, then cur is referring to the existing entry.
     * Otherwise, cur refers to the directory in which we should make a new entry. */

    if (!got_basename) {
        filesys_close(cur);
        return EINVAL;
    }

    if (!already_exists) {
        //printf("  creating new entry\n");
        open_file_t dir = cur;
        const char *basename = part;
        inode_nr_t entry_nr;

        file_ops_t *ops = &dir.mount->filesys->ops;
        if (ops->create_entry) {
            errno = ops->create_entry(ops, dir.file, basename, type, perm, device_id,
                    is_new, &entry_nr);
        } else {
            errno = ENOTSUP;
        }

        //printf("  creation => %e\n", errno);

        if (errno) {
            filesys_close(cur);
            return errno;
        }

        // TODO: atomically create and open the new entry. Currently there is a race condition:
        // we could get preempted between creation and opening by another process which deletes
        // the new entry.
        filesys_close(dir);
        fs_locator_t loc = { .mount = dir.mount, .inode_nr = entry_nr };
        errno = open_locator(loc, &cur);
        //printf("  opening new entry: %e\n", errno);
        if (errno) {
            filesys_close(cur);
            return errno;
        }
    }

    /* Now cur refers to the pre-existing or freshly created entry. */

    if (keep_open) {
        *file = cur;
    } else {
        filesys_close(cur);
    }
    *is_new = !already_exists;
    return 0;
}

INLINE errno_t filesys_create(const char *path, bool keep_open,
        inode_type_t type, inode_perm_t perm, device_id_t device_id,
        bool *is_new, open_file_t *file)
{
    fs_locator_t root = {
        .mount = &root_mount,
        .inode_nr = root_mount.filesys->ops.root_nr
    };
    return filesys_create_at(root, path, keep_open,
            type, perm, device_id,
            is_new, file);
}

INLINE errno_t filesys_delete_entry(open_file_t dir, inode_nr_t entry_nr)
{
    file_ops_t *ops = &dir.mount->filesys->ops;
    if (ops->delete_entry) {
        return ops->delete_entry(ops, dir.file, entry_nr);
    } else {
        return ENOTSUP;
    }
}

INLINE errno_t filesys_get_inode(open_file_t file, inode_t *inode)
{
    file_ops_t *ops = &file.mount->filesys->ops;
    if (ops->get_inode) {
        return ops->get_inode(ops, file.file, inode);
    } else {
        return ENOTSUP;
    }
}

INLINE errno_t filesys_read_file(open_file_t file, uint64_t offset,
        uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_read)
{
    file_ops_t *ops = &file.mount->filesys->ops;
    if (ops->read_file) {
        return ops->read_file(ops, file.file, offset, buf, buf_size,
                bytes_read);
    } else {
        return ENOTSUP;
    }
}

INLINE errno_t filesys_write_file(open_file_t file, uint64_t offset,
        bool append,
        const uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_written)
{
    file_ops_t *ops = &file.mount->filesys->ops;
    if (ops->write_file) {
        return ops->write_file(ops, file.file, offset, append,
                buf, buf_size, bytes_written);
    } else {
        return ENOTSUP;
    }
}
