#include <fs/cpfs/inode_ops.h>
#include <string.h>
#include <stdops.h>
#include <timer.h>
#include <dbg.h>

typedef struct {
    block_nr_t prev;
    block_nr_t next;
} __attribute__((packed)) cpfs_free_block_t;

errno_t cpfs_writeback_header(block_ops_t *ops, cpfs_header_t *header)
{
    if (!ops->write_block) {
        return ENOTSUP;
    }

    uint8_t block[BLOCK_SIZE];
    memcpy(block, header, sizeof(cpfs_header_t));
    memset(block + sizeof(cpfs_header_t), BLOCK_SIZE - sizeof(cpfs_header_t), 0);
    return ops->write_block(ops, 0, block);
}

/* Allocate a data block and write its block number to block_nr (on success). This
 * does not zero out the fresh block. */
errno_t cpfs_alloc_data(inode_ops_t *ops, block_nr_t *block_nr)
{
    cpfs_inode_ops_aux_t *aux = ops->aux;
    block_ops_t *block_ops = aux->block_ops;
    errno_t errno;

    if (!block_ops->read_block || !block_ops->write_block) {
        return ENOTSUP;
    }

    acquire_write(&aux->cpfs_metadata_lock);

    if (!aux->header.free_data_head) {
        release_write(&aux->cpfs_metadata_lock);
        return EFULL;
    }

    block_nr_t new_block_nr = aux->header.free_data_head;
    uint8_t block[BLOCK_SIZE];
    errno = block_ops->read_block(block_ops, new_block_nr, block);
    if (errno) {
        release_write(&aux->cpfs_metadata_lock);
        printf("[could not read block "block_nr_spec"]\n", new_block_nr);
        return errno;
    }

    cpfs_free_block_t *free_block = (cpfs_free_block_t *) block;

    panic_if(free_block->prev, "free-blocks head block should not have a prev block\n");
    aux->header.free_data_head = free_block->next;
    errno = cpfs_writeback_header(block_ops, &aux->header);
    if (errno) {
        release_write(&aux->cpfs_metadata_lock);
        printf("[could not writeback header]\n");
        return errno;
    }

    if (free_block->next) {
        uint8_t next_block[BLOCK_SIZE];
        errno = block_ops->read_block(block_ops, free_block->next, next_block);
        if (errno) {
            release_write(&aux->cpfs_metadata_lock);
            printf("[could not read next block "block_nr_spec"]\n", free_block->next);
            return errno;
        }
        cpfs_free_block_t *next_free = (cpfs_free_block_t *) next_block;
        next_free->prev = 0;
        errno = block_ops->write_block(block_ops, free_block->next, next_block);
        if (errno) {
            release_write(&aux->cpfs_metadata_lock);
            printf("[could not write next block "block_nr_spec"]\n", free_block->next);
            return errno;
        }
    }

    /* Links have been fixed up, so metadata can be released. */
    release_write(&aux->cpfs_metadata_lock);

    /* The block itself does not require any changes. */
    *block_nr = new_block_nr;
    return 0;
}

errno_t cpfs_alloc_inode(inode_ops_t *ops, inode_nr_t *inode_nr,
        inode_type_t type, inode_perm_t perm, device_id_t device_id)
{
    cpfs_inode_ops_aux_t *aux = ops->aux;
    block_ops_t *block_ops = aux->block_ops;
    errno_t errno;

    if (!ops->read_inode || !ops->write_inode || !block_ops->write_block) {
        return ENOTSUP;
    }

    acquire_write(&aux->cpfs_metadata_lock);

    if (!aux->header.free_inodes_head) {
        release_write(&aux->cpfs_metadata_lock);
        return EFULL;
    }

    inode_nr_t new_inode_nr = aux->header.free_inodes_head;
    inode_t inode;
    errno = ops->read_inode(ops, new_inode_nr, &inode);
    if (errno) {
        release_write(&aux->cpfs_metadata_lock);
        printf("[could not read inode "inode_nr_spec"]\n", new_inode_nr);
        return errno;
    }

    panic_if(inode.prev, "free-inodes head inode should not have a prev inode\n");
    aux->header.free_inodes_head = inode.next;
    errno = cpfs_writeback_header(block_ops, &aux->header);
    if (errno) {
        release_write(&aux->cpfs_metadata_lock);
        printf("[could not writeback header]\n");
        return errno;
    }

    if (inode.next) {
        inode_t next_inode;
        errno = ops->read_inode(ops, inode.next, &next_inode);
        if (errno) {
            release_write(&aux->cpfs_metadata_lock);
            printf("[could not read next inode "inode_nr_spec"]\n", inode.next);
            return errno;
        }
        next_inode.prev = 0;
        errno = ops->write_inode(ops, inode.next, &next_inode);
        if (errno) {
            release_write(&aux->cpfs_metadata_lock);
            printf("[could not write next inode "inode_nr_spec"]\n", inode.next);
            return errno;
        }
    }

    /* Links have been fixed up, so metadata can be released. */
    release_write(&aux->cpfs_metadata_lock);

    /* Now we can set up our inode. */
    uint64_t time = timer_counter();
    if (inode.nr != new_inode_nr) {
        printf("[warning: unexpected inode number at inode "inode_nr_spec"]\n",
                new_inode_nr);
    }
    inode.nr = new_inode_nr;
    inode.refs = 1;
    inode.next = 0;
    inode.size = 0;
    inode.type = type;
    inode.perm = perm;
    inode.time_created = time;
    inode.time_modified = time;

    memset(inode.blocks_direct, INODE_NUM_DIRECT * sizeof(block_nr_t), 0);
    inode.block_indirect = 0;
    inode.block_double_indirect = 0;
    inode.block_triple_indirect = 0;

    if (type == INODE_CHARDEV || type == INODE_BLOCKDEV) {
        inode.content.device_id = device_id;
    }

    errno = ops->write_inode(ops, inode.nr, &inode);
    if (errno) {
        printf("[could not write new inode "inode_nr_spec"]\n", inode.nr);
        return errno;
    }

    *inode_nr = inode.nr;
    return 0;
}

errno_t cpfs_free_inode(inode_ops_t *ops, inode_nr_t inode_nr)
{
    //cpfs_inode_ops_aux_t *aux = ops->aux;
    //block_ops_t *block_ops = aux->block_ops;

    printf("not implemented: cpfs_free_inode("inode_nr_spec")\n", inode_nr);
    return ENOTSUP;
}


STATIC_ASSERT(sizeof(inode_t) == BLOCK_SIZE,
        "buffer overflow possible in cpfs_read_inode() and cpfs_write_inode()!\n");

errno_t cpfs_read_inode(inode_ops_t *ops, inode_nr_t inode_nr, inode_t *dst)
{
    cpfs_inode_ops_aux_t *aux = ops->aux;
    block_ops_t *block_ops = aux->block_ops;

    panic_if(inode_nr < 1 || inode_nr > aux->header.num_inodes,
            "inode nr "inode_nr_spec" is out of range", inode_nr);
    return block_ops->read_block
        ? block_ops->read_block(block_ops, (block_nr_t) inode_nr, (uint8_t *) dst)
        : ENOTSUP;
}

errno_t cpfs_write_inode(inode_ops_t *ops, inode_nr_t inode_nr, const inode_t *src)
{
    cpfs_inode_ops_aux_t *aux = ops->aux;
    block_ops_t *block_ops = aux->block_ops;

    panic_if(inode_nr < 1 || inode_nr > aux->header.num_inodes,
            "inode nr "inode_nr_spec" is out of range", inode_nr);
    return block_ops->write_block
        ? block_ops->write_block(block_ops, (block_nr_t) inode_nr, (const uint8_t *) src)
        : ENOTSUP;
}

/* Requires that block_offset <= *offset < block_offset + BLOCK_SIZE.
 * Returns an error code, and also the number of bytes read by this function alone.
 * Also updates offset, buf, and buf_size. On error, the caller may assume that
 * no bytes were read into the buf.
 * If block_nr is 0, it indicates that zeros should be loaded into the buf instead
 * of actual data from a block. */
errno_t cpfs_read_file_block(block_ops_t *ops,
        block_nr_t block_nr, uint64_t block_offset,
        uint64_t *offset, uint8_t **buf, uint64_t *buf_size,
        uint64_t *read)
{
    panic_if_not(block_offset <= *offset && *offset < block_offset + BLOCK_SIZE,
            "no overlap in cpfs_read_file_block()\n");
    *read = min_uint64((block_offset + BLOCK_SIZE) - *offset, *buf_size);
    //printf("  cpfs_read_file_block(block_nr="block_nr_spec", block offset=%u64, offset=%u64, buf_size=%u64): read %u64 bytes\n",
    //        block_nr, block_offset, *offset, *buf_size, *read);

    if (block_nr) {
        uint8_t block[BLOCK_SIZE];
        errno_t errno = ops->read_block(ops, block_nr, block);
        if (errno) {
            *read = 0;
            return errno;
        }
        memcpy(*buf, block + (*offset - block_offset), *read);
    } else {
        memset(*buf, *read, 0);
    }

    *offset += *read;
    *buf += *read;
    panic_if(*read > *buf_size, "error\n");
    *buf_size -= *read;
    return 0;
}

/* array_stride should be BLOCK_NRS_PER_BLOCK ** indirection * BLOCK_SIZE,
 * but this is not checked.
 * The bytes_read returned is just the number of bytes read by this function,
 * i.e. it starts at 0. */
errno_t cpfs_read_file_blocks(block_ops_t *ops,
        uint8_t indirection,
        uint64_t array_start_offset, uint64_t array_stride,
        block_nr_t *array, uint64_t array_size,
        uint64_t *offset, uint8_t **buf, uint64_t *buf_size,
        uint64_t *bytes_read)
{
    /* Get local copies; we'll update everything at the end. */
    uint64_t l_offset = *offset;
    uint64_t l_buf_size = *buf_size;

    uint64_t array_end_offset = array_start_offset + array_stride * array_size;

    /* Make sure it's not a pointless function call. */
    panic_if_not(array_start_offset <= l_offset && l_offset < array_end_offset,
            "no overlap in cpfs_read_file_blocks()\n");

    uint64_t limit = min_uint64(array_end_offset - l_offset, l_buf_size);

    *bytes_read = 0;

    uint64_t start = (l_offset - array_start_offset) / array_stride;
    uint64_t end = round_up_div(l_offset + limit - array_start_offset, array_stride);
    //printf("cpfs_read_file_blocks(indirection=%u8,\n"
    //        "    array_start_offset=%u64, array_stride=%u64, array_size=%u64,\n"
    //        "    offset=%u64, buf_size=%u64): start=%u64, end=%u64\n",
    //        indirection, array_start_offset, array_stride, array_size,
    //        l_offset, l_buf_size,
    //        start, end);

    panic_if(end > array_size,
            "something is wrong\n"
            "array_start_offset=%u64, array_stride=%u64, array_size=%u64, array_end_offset=%u64,\n"
            "offset=%u64, buf_size=%u64, limit=%u64,\n"
            "start=%u64, end=%u64",
            array_start_offset, array_stride, array_size, array_end_offset,
            l_offset, l_buf_size, limit,
            start, end);
    if (indirection == 0) {
        /* The array entries are direct pointers to blocks containing file content. */
        for (uint64_t i = start; i < end; i++) {
            uint64_t block_offset = array_start_offset + array_stride * i;
            uint64_t read;
            // TODO: just merge cpfs_read_file_block into here, instead of separate call.
            errno_t errno = cpfs_read_file_block(ops,
                array[i], block_offset,
                offset, buf, buf_size,
                &read);
            //printf("  read %u64 from offset %u64 in direct_blocks[%u64]\n", read, offset - block_offset, direct_index);
            if (errno) {
                return errno;
            }
            *bytes_read += read;
        }
    } else {
        //printf("  indirection %u8: start=%u64, end=%u64\n", indirection, start, end);
        for (uint64_t i = start; i < end; i++) {
            uint64_t entry_offset = array_start_offset + array_stride * i;
            //printf("  indirection %u8: array[%u64] = "block_nr_spec"\n",
            //        indirection, i, array[i]);
            if (array[i]) {
                block_nr_t pointers[BLOCK_NRS_PER_BLOCK];
                errno_t errno = ops->read_block(ops, array[i], (uint8_t *) pointers);
                if (errno) {
                    return errno;
                }
                uint64_t read;
                errno = cpfs_read_file_blocks(ops,
                        indirection - 1,
                        entry_offset, array_stride / BLOCK_NRS_PER_BLOCK,
                        pointers, BLOCK_NRS_PER_BLOCK,
                        offset, buf, buf_size,
                        &read);
                //printf("  read %u64 from offset %u64 in direct_blocks[%u64]\n", read, offset - block_offset, direct_index);
                if (errno) {
                    return errno;
                }
                *bytes_read += read;
            } else {
                panic("didn't expect sparse file\n");
                //printf("  read %u64 from offset %u64 in direct_blocks[%u64]\n", read, offset - block_offset, direct_index);
                memset(buf, limit, 0);
                *offset += limit;
                *buf += limit;
                panic_if(limit > *buf_size, "error\n");
                *buf_size -= limit;
                *bytes_read += limit;
            }
        }
    }

    return 0;
}

errno_t cpfs_read_file(inode_ops_t *ops, inode_nr_t inode_nr, uint64_t offset,
        uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_read)
{
    cpfs_inode_ops_aux_t *aux = ops->aux;
    block_ops_t *block_ops = aux->block_ops;
    errno_t errno;

    /* We definitely need the ability to read blocks. */
    if (!block_ops->read_block) {
        return ENOTSUP;
    }

    inode_t inode;
    errno = ops->read_inode ? ops->read_inode(ops, inode_nr, &inode) : ENOTSUP;
    if (errno) {
        return errno;
    }

    panic_if(inode.type == INODE_NONE,
            "cannot read file from inode "inode_nr_spec"; inode is unallocated\n",
            inode_nr);

    //printf("cpfs_read_file(inode="inode_nr_spec", offset=%u64, buf_size=%u64): file size=%u64\n",
    //        inode_nr, offset, buf_size, inode.size);

    if (offset >= inode.size) {
        *bytes_read = 0;
        return 0;
    }
    if (buf_size > inode.size - offset) {
        buf_size = inode.size - offset;
    }

    const uint64_t END_EMBEDDED_CONTENT = INODE_EMBEDDED_CONTENT_SIZE;
    const uint64_t END_DIRECT = END_EMBEDDED_CONTENT
        + INODE_NUM_DIRECT * BLOCK_SIZE;
    const uint64_t END_INDIRECT = END_DIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;
    const uint64_t END_DOUBLE_INDIRECT = END_INDIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;
    const uint64_t END_TRIPLE_INDIRECT = END_DOUBLE_INDIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;

    *bytes_read = 0;

    if (buf_size == 0) {
        return 0;
    }
    if (buf_size > 0 && offset < END_EMBEDDED_CONTENT) {
        uint64_t read = min_uint64(END_EMBEDDED_CONTENT - offset, buf_size);
        //printf("  read %u64 from offset %u64 in embedded content\n", read, offset);
        memcpy(buf, inode.content.embedded + offset, read);
        offset += read;
        buf += read;
        panic_if(read > buf_size, "error\n");
        buf_size -= read;
        *bytes_read += read;
    }

    // TODO: do all of these with some kind of:
    //    cpfs_read_file_blocks(block_ops,
    //       indirection level,
    //       block_nr array start offset,
    //       block_nr array stride [to avoid computing based on indirection level each time],
    //       the actual array,
    //       &offset, &buf, &buf_size)
    // this would do the recursion as well.
    if (buf_size == 0) {
        return 0;
    }
    if (offset < END_DIRECT) {
        uint64_t read;
        //printf("  DIRECT READING\n");
        errno = cpfs_read_file_blocks(block_ops,
                /*indirection*/ 0,
                END_EMBEDDED_CONTENT, BLOCK_SIZE,
                inode.blocks_direct, INODE_NUM_DIRECT,
                &offset, &buf, &buf_size, &read);
        *bytes_read += read;
        if (errno) {
            return errno;
        }
    }

    if (buf_size == 0) {
        return 0;
    }
    if (offset < END_INDIRECT) {
        //printf("  INDIRECT READING\n");
        if (inode.block_indirect) {
            block_nr_t pointers[BLOCK_NRS_PER_BLOCK];
            errno = block_ops->read_block(block_ops, inode.block_indirect, (uint8_t *) pointers);
            if (errno) {
                return errno;
            }
            uint64_t read;
            errno = cpfs_read_file_blocks(block_ops,
                    /*indirection*/ 0,
                    END_DIRECT, BLOCK_SIZE,
                    pointers, BLOCK_NRS_PER_BLOCK,
                    &offset, &buf, &buf_size, &read);
            *bytes_read += read;
            if (errno) {
                return errno;
            }
        } else {
            uint64_t read = min_uint64(END_INDIRECT - offset, buf_size);
            //printf("  read %u64 from offset %u64 in block_indirect (sparse)\n", read, offset - END_DIRECT);
            memset(buf, read, 0);
            offset += read;
            buf += read;
            panic_if(read > buf_size, "error\n");
            buf_size -= read;
            *bytes_read += read;
        }
    }

    if (buf_size == 0) {
        return 0;
    }
    if (offset < END_DOUBLE_INDIRECT) {
        if (inode.block_double_indirect) {
            block_nr_t pointers[BLOCK_NRS_PER_BLOCK];
            errno = block_ops->read_block(block_ops, inode.block_double_indirect, (uint8_t *) pointers);
            if (errno) {
                return errno;
            }
            uint64_t read;
            errno = cpfs_read_file_blocks(block_ops,
                    /*indirection*/ 1,
                    END_INDIRECT, BLOCK_NRS_PER_BLOCK * BLOCK_SIZE,
                    pointers, BLOCK_NRS_PER_BLOCK,
                    &offset, &buf, &buf_size, &read);
            *bytes_read += read;
            if (errno) {
                return errno;
            }
        } else {
            uint64_t read = min_uint64(END_DOUBLE_INDIRECT - offset, buf_size);
            //printf("  read %u64 from offset %u64 in block_indirect (sparse)\n", read, offset - END_DIRECT);
            memset(buf, read, 0);
            offset += read;
            buf += read;
            panic_if(read > buf_size, "error\n");
            buf_size -= read;
            *bytes_read += read;
        }
    }

    if (buf_size == 0) {
        return 0;
    }
    if (offset < END_TRIPLE_INDIRECT) {
        panic("read from block_triple_indirect not implemented\n"
                "(buf_size remaining at %u64)\n", buf_size);
    }

    panic_if(buf_size > 0,
        "something is wrong; we should be done reading by now\n");
    return 0;
}

/* This assumes (unlike cpfs_write_file()!) as a convenient fiction that the buf begins
 * at the start of the file, instead of the start of the freshly written content; that
 * is, this function writes buf[offset, +length) into a file's block, which covers
 * [block_offset, +BLOCK_SIZE) in the file.
 * This updates offset and length on success; on failure, they are unmodified.
 * If 'zero' is set, this zeros all data in the block outside the specified write.
 * Requires that block_offset <= *offset < block_offset + BLOCK_SIZE; also, the block_nr
 * must be nonzero (it must refer to an allocated block). */
errno_t cpfs_write_file_block(block_ops_t *ops,
        block_nr_t block_nr, uint64_t block_offset,
        const uint8_t *buf, uint64_t *offset, uint64_t *length,
        bool zero)
{
    panic_if_not(block_offset <= *offset && *offset < block_offset + BLOCK_SIZE,
            "no overlap in cpfs_write_file_block()\n");
    panic_if_not(block_nr, "cpfs_write_file_block() expects allocated block\n");

    if (!ops->read_block || !ops->write_block) {
        return ENOTSUP;
    }

    errno_t errno;
    uint64_t write = min_uint64(block_offset + BLOCK_SIZE, *offset + *length) - *offset;

    uint8_t block[BLOCK_SIZE];
    if (zero) {
        memset(block, BLOCK_SIZE, 0);
    } else {
        errno = ops->read_block(ops, block_nr, block);
        if (errno) {
            return errno;
        }
    }

    memcpy(block + (*offset - block_offset), buf + *offset, write);

    errno = ops->write_block(ops, block_nr, block);
    if (errno) {
        return errno;
    }

    panic_if(write > *length, "logic error\n");
    *offset += write;
    *length -= write;
    return 0;
}

/* array_stride should be BLOCK_NRS_PER_BLOCK ** indirection * BLOCK_SIZE,
 * but this is not checked.
 * Like cpfs_write_file_block(), this function assumes we'd like to write
 * buf[*offset, +*length) into the file at [array_start_offset, +array_stride*array_size).
 * This updates offset and length, and sets updated_array if the array was dirtied
 * due to block allocations. (Otherwise, this does not modify updated_array.) */
errno_t cpfs_write_file_blocks(inode_ops_t *ops,
        uint8_t indirection,
        uint64_t array_start_offset, uint64_t array_stride,
        block_nr_t *array, uint64_t array_size,
        const uint8_t *buf, uint64_t *offset, uint64_t *length,
        bool *updated_array)
{
    cpfs_inode_ops_aux_t *aux = ops->aux;
    block_ops_t *block_ops = aux->block_ops;
    errno_t errno;

    uint64_t array_end_offset = array_start_offset + array_stride * array_size;

    /* Make sure it's not a pointless function call. */
    panic_if_not(array_start_offset <= *offset && *offset < array_end_offset,
            "no overlap in cpfs_write_file_blocks()\n");

    uint64_t limit = min_uint64(array_end_offset - *offset, *length);

    uint64_t start = (*offset - array_start_offset) / array_stride;
    uint64_t end = round_up_div(*offset + limit - array_start_offset, array_stride);
    //printf("cpfs_write_file_blocks(indirection=%u8,\n"
    //        "    array_start_offset=%u64, array_stride=%u64, array_size=%u64,\n"
    //        "    offset=%u64, buf_size=%u64): start=%u64, end=%u64\n",
    //        indirection, array_start_offset, array_stride, array_size,
    //        l_offset, l_buf_size,
    //        start, end);

    panic_if(end > array_size,
            "something is wrong\n"
            "array_start_offset=%u64, array_stride=%u64, array_size=%u64, array_end_offset=%u64,\n"
            "offset=%u64, length=%u64, limit=%u64,\n"
            "start=%u64, end=%u64",
            array_start_offset, array_stride, array_size, array_end_offset,
            *offset, *length, limit,
            start, end);
    if (indirection == 0) {
        /* The array entries are direct pointers to blocks containing file content. */
        for (uint64_t i = start; i < end; i++) {
            bool zero_block = false;
            if (!array[i]) {
                errno = cpfs_alloc_data(ops, &array[i]);
                if (errno) {
                    // TODO: undo changes?
                    return errno;
                }
                *updated_array = true;
                zero_block = true;
            }
            uint64_t block_offset = array_start_offset + array_stride * i;
            errno = cpfs_write_file_block(block_ops,
                array[i], block_offset,
                buf, offset, length,
                zero_block);
            //printf("  read %u64 from offset %u64 in direct_blocks[%u64]\n", read, offset - block_offset, direct_index);
            if (errno) {
                // TODO: undo changes?
                return errno;
            }
        }
    } else {
        //printf("  indirection %u8: start=%u64, end=%u64\n", indirection, start, end);
        for (uint64_t i = start; i < end; i++) {
            uint64_t entry_offset = array_start_offset + array_stride * i;
            //printf("  indirection %u8: array[%u64] = "block_nr_spec"\n",
            //        indirection, i, array[i]);
            block_nr_t pointers[BLOCK_NRS_PER_BLOCK];
            bool pointers_dirty;
            if (array[i]) {
                errno = block_ops->read_block(block_ops, array[i], (uint8_t *) pointers);
                if (errno) {
                    // TODO: undo changes?
                    return errno;
                }
                pointers_dirty = false;
            } else {
                errno = cpfs_alloc_data(ops, &array[i]);
                if (errno) {
                    // TODO: undo changes?
                    return errno;
                }
                *updated_array = true;
                memset(pointers, BLOCK_NRS_PER_BLOCK * sizeof(block_nr_t), 0);
                pointers_dirty = true;
            }

            bool pointers_updated;
            errno = cpfs_write_file_blocks(ops,
                    indirection - 1,
                    entry_offset, array_stride / BLOCK_NRS_PER_BLOCK,
                    pointers, BLOCK_NRS_PER_BLOCK,
                    buf, offset, length,
                    &pointers_updated);
            if (errno) {
                // TODO: undo changes?
                return errno;
            }

            if (pointers_updated) {
                pointers_dirty = true;
            }

            if (pointers_dirty) {
                errno = block_ops->write_block(block_ops, array[i], (const uint8_t *) pointers);
                if (errno) {
                    // TODO: undo changes?
                    return errno;
                }
            }
        }
    }

    return 0;
}

errno_t cpfs_write_file(inode_ops_t *ops, inode_nr_t inode_nr, uint64_t offset,
        bool append,
        const uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_written)
{
    if (buf_size == 0) {
        *bytes_written = 0;
        return 0;
    }

    cpfs_inode_ops_aux_t *aux = ops->aux;
    block_ops_t *block_ops = aux->block_ops;
    errno_t errno;

    if (!ops->read_inode || !ops->write_inode ||
        !block_ops->read_block || !block_ops->write_block) {
        return ENOTSUP;
    }

    inode_t inode;
    errno = ops->read_inode(ops, inode_nr, &inode);
    if (errno) {
        return errno;
    }

    panic_if(inode.type == INODE_NONE,
            "cannot write file at inode "inode_nr_spec"; inode is unallocated\n",
            inode_nr);

    if (append) {
        offset = inode.size;
    }
    panic_if(offset + buf_size < offset,
            "ain't no way the file and buffer are big enough to wrap around 64-bit memory\n");

    const uint64_t END_EMBEDDED_CONTENT = INODE_EMBEDDED_CONTENT_SIZE;
    const uint64_t END_DIRECT = END_EMBEDDED_CONTENT
        + INODE_NUM_DIRECT * BLOCK_SIZE;
    const uint64_t END_INDIRECT = END_DIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;
    const uint64_t END_DOUBLE_INDIRECT = END_INDIRECT
        + BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;
    //const uint64_t END_TRIPLE_INDIRECT = END_DOUBLE_INDIRECT
    //    + BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE;

    *bytes_written = 0;

    uint64_t time = timer_counter();
    bool updated_inode = false;

    if (offset < END_EMBEDDED_CONTENT) {
        uint64_t written = min_uint64(END_EMBEDDED_CONTENT - offset, buf_size);
        memcpy(inode.content.embedded + offset, buf, written);
        updated_inode = true;
        offset += written;
        buf += written;
        buf_size -= written;
        *bytes_written += written;
    }
    if (buf_size == 0) {
        errno = 0;
        goto cpfs_write_file_done;
    }

    /* Now we write buf[offset, +buf_size) instead of buf[0, +buf_size); just some
     * reindexing. */
    buf -= offset;

    uint64_t old_offset = offset;
    errno = cpfs_write_file_blocks(ops,
        /*indirection*/0,
        END_EMBEDDED_CONTENT, BLOCK_SIZE,
        inode.blocks_direct, INODE_NUM_DIRECT,
        buf, &offset, &buf_size,
        &updated_inode);
    if (errno || buf_size == 0) {
        *bytes_written += offset - old_offset;
        goto cpfs_write_file_done;
    }

    old_offset = offset;
    errno = cpfs_write_file_blocks(ops,
        /*indirection*/1,
        END_DIRECT, BLOCK_NRS_PER_BLOCK * BLOCK_SIZE,
        &inode.block_indirect, 1,
        buf, &offset, &buf_size,
        &updated_inode);
    if (errno || buf_size == 0) {
        *bytes_written += offset - old_offset;
        goto cpfs_write_file_done;
    }

    old_offset = offset;
    errno = cpfs_write_file_blocks(ops,
        /*indirection*/2,
        END_INDIRECT, BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE,
        &inode.block_double_indirect, 1,
        buf, &offset, &buf_size,
        &updated_inode);
    if (errno || buf_size == 0) {
        *bytes_written += offset - old_offset;
        goto cpfs_write_file_done;
    }

    old_offset = offset;
    errno = cpfs_write_file_blocks(ops,
        /*indirection*/3,
        END_DOUBLE_INDIRECT, BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_NRS_PER_BLOCK * BLOCK_SIZE,
        &inode.block_triple_indirect, 1,
        buf, &offset, &buf_size,
        &updated_inode);
    if (errno || buf_size == 0) {
        *bytes_written += offset - old_offset;
        goto cpfs_write_file_done;
    }

    panic("should be done with writing by now...\n");

cpfs_write_file_done:
    if (!errno && inode.size < offset + buf_size) {
        inode.size = offset + buf_size;
        updated_inode = true;
    }

    if (updated_inode) {
        inode.time_modified = time;
        /* Try writing the dirty inode back. If it fails, and we aren't already reporting
         * an error, set the returned errno to this failure code. */
        errno_t errno2 = ops->write_inode(ops, inode.nr, &inode);
        if (errno2) {
            printf("[could not writeback inode "inode_nr_spec": %e]\n", inode.nr, errno2);
            if (!errno) {
                errno = errno2;
            }
        }
    }
    return errno;
}

void get_cpfs_inode_ops(inode_ops_t *dst, cpfs_inode_ops_aux_t *aux)
{
    create_rw_lock(&aux->cpfs_metadata_lock, /*prefer_writers*/true);

    block_ops_t *ops = aux->block_ops;
    uint8_t block0[BLOCK_SIZE];
    errno_t errno = ops->read_block(ops, 0, block0);
    panic_if(errno, "could not read metadata block: %e\n", errno);
    aux->header = *(cpfs_header_t *) block0;

    printf("cpfs metadata:\n");
    printf("  num blocks: %u64\n", aux->header.num_blocks);
    printf("  num inodes: %u64\n", aux->header.num_inodes);
    printf("  free-inodes head: "inode_nr_spec"\n", aux->header.free_inodes_head);
    printf("  free-data head: "block_nr_spec"\n", aux->header.free_data_head);

    *dst = (inode_ops_t)  {
        .root_nr = 1,
        .alloc_inode = &cpfs_alloc_inode,
        .free_inode = &cpfs_free_inode,
        .read_inode = &cpfs_read_inode,
        .write_inode = &cpfs_write_inode,
        .read_file = &cpfs_read_file,
        .write_file = &cpfs_write_file,
        .aux = aux
    };
}
