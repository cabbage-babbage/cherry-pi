#include <fs/file_ops.h>
#include <string.h>
#include <heap.h>
#include <dbg.h>

/* streq, but the second "argument" is a string that has a length instead of
 * null termination. */
bool streql(const char *s, uint64_t length, const char *buf)
{
    if (strlen(s) != length) {
        return false;
    }
    return memeq(s, buf, length);
}

typedef struct {
    bool valid;

    open_inode_t file;
} open_files_entry_t;

typedef struct {
    lock_t lock;
    uint64_t num_entries;
    open_files_entry_t *entries;
} open_files_t;

void dump_open_files(open_files_t *open_files)
{
    acquire_lock(&open_files->lock);

    printf("file_ops:all open files:\n");

    bool first = true;
    for (uint64_t i = 0; i < open_files->num_entries; i++) {
        open_files_entry_t *entry = &open_files->entries[i];
        if (entry->valid) {
            printf("%s%u64 (%u64 ref%s)",
                first ? "  " : ", ",
                entry->file.inode_nr,
                entry->file.open_count,
                entry->file.open_count == 1 ? "" : "s");
            first = false;
        }
    }
    printf("\n");

    release_lock(&open_files->lock);
}

errno_t file_ops_open_file(file_ops_t *ops, inode_nr_t inode_nr, open_inode_t **file)
{
    // TODO: verify that the inode actually exists?

    //printf("file_ops:open_file(inode="inode_nr_spec"): ", inode_nr);
    open_files_t *open_files = ((default_file_ops_aux_t *) ops->aux)->open_files;

    acquire_lock(&open_files->lock);

    open_files_entry_t *available_entry = NULL;
    for (uint64_t i = 0; i < open_files->num_entries; i++) {
        open_files_entry_t *entry = &open_files->entries[i];
        if (entry->valid && entry->file.inode_nr == inode_nr) {
            acquire_write(&entry->file.lock);
            entry->file.open_count++;
            release_write(&entry->file.lock);

            release_lock(&open_files->lock);
            //printf("new open_count %u64\n", entry->file.open_count);
            *file = &entry->file;
            //dump_open_files(open_files);
            return 0;
        } else if (!available_entry && !entry->valid) {
            available_entry = entry;
        }
    }

    /* No existing entry; we need to make one. */
    if (!available_entry) {
        printf("could not open file: file_ops open files list already full\n");
        return EFULL;
    }

    available_entry->valid = true;
    create_rw_lock(&available_entry->file.lock, /*prefer_writers*/ true);
    available_entry->file.inode_nr = inode_nr;
    available_entry->file.open_count = 1;
    //printf("new open_count %u64\n", available_entry->file.open_count);

    release_lock(&open_files->lock);

    *file = &available_entry->file;
    //dump_open_files(open_files);
    return 0;
}

errno_t file_ops_dup_file(file_ops_t *ops, open_inode_t *file)
{
    //printf("file_ops:dup_file(inode="inode_nr_spec"): open_count %u64 => %u64\n",
    //        file->inode_nr, file->open_count, file->open_count + 1);
    acquire_write(&file->lock);
    file->open_count++;
    release_write(&file->lock);
    //open_files_t *open_files = ((default_file_ops_aux_t *) ops->aux)->open_files;
    //dump_open_files(open_files);
    return 0;
}

errno_t file_ops_close_file(file_ops_t *ops, open_inode_t *file)
{
    open_files_t *open_files = ((default_file_ops_aux_t *) ops->aux)->open_files;

    //printf("file_ops:close_file(inode="inode_nr_spec"): open_count %u64 => ",
    //        file->inode_nr, file->open_count);

    acquire_lock(&open_files->lock);
    acquire_write(&file->lock);
    if (file->open_count == 0) {
        //printf("%u64\n", file->open_count);
        printf("bug: file_ops_close_file() called on file with open_count == 0\n");
        print_stacktrace("BUG");
    } else {
        file->open_count--;
        //printf("%u64\n", file->open_count);
    }

    if (file->open_count == 0) {
        // TODO: if the inode is marked for deletion, go and delete it

        // TODO: can this deadlock with file_ops_open_file()?
        // //also: what happens if we're preempted right here by a call to file_ops_open_file(),
        // // and it still sees that the entry is valid... this is definitely not synchronized
        // // correctly
        // TODO: see if we can improve parallelism. currently only a single file can be
        // closed at a time, though maybe that isn't too bad...

        /* We can figure out which entry it was from, since it should simply be
         * the 'file' entry of one of the open_files entries. */
        open_files_entry_t *entry = (open_files_entry_t *) (
                (uint8_t *) file - OFFSET_OF(open_files_entry_t, file));

        entry->valid = false;
        //printf("  invalidating open inode\n");
    }
    release_write(&file->lock);
    release_lock(&open_files->lock);

    //dump_open_files(open_files);
    return 0;
}

errno_t file_ops_find_entry(file_ops_t *ops, open_inode_t *dir, const char *name, inode_nr_t *entry_nr)
{
    inode_ops_t *inode_ops = ((default_file_ops_aux_t *) ops->aux)->inode_ops;
    errno_t errno;

    acquire_read(&dir->lock);

    inode_t dir_inode;
    errno = inode_ops->read_inode
        ? inode_ops->read_inode(inode_ops, dir->inode_nr, &dir_inode)
        : ENOTSUP;
    if (errno) {
        release_read(&dir->lock);
        *entry_nr = 0;
        return errno;
    }

    if (dir_inode.type != INODE_DIR) {
        release_read(&dir->lock);
        *entry_nr = 0;
        return ENOTDIR;
    }

    // TODO: read the dir entries in groups, instead of 1-by-1
    dir_entry_t entry;
    uint64_t bytes_read;
    uint64_t offset = 0;
    while (!(errno = inode_ops->read_file(
                    inode_ops, dir->inode_nr, offset,
                    (uint8_t *) &entry, sizeof(entry), &bytes_read))
           && bytes_read == sizeof(entry)) {
        if (streql(name, entry.name_len, entry.name)) {
            release_read(&dir->lock);
            *entry_nr = entry.inode_nr;
            return 0;
        }
        offset += sizeof(entry);
    }

    release_read(&dir->lock);

    if (errno) {
        *entry_nr = 0;
        return errno;
    } else if (bytes_read == 0) {
        *entry_nr = 0;
        return ENOENT;
    } else {
        /* We should never read just a part of a directory entry. */
        printf("bug: directory corruption in inode %u64\n", dir->inode_nr);
        *entry_nr = 0;
        return ENOENT;
    }
}

errno_t file_ops_create_entry(file_ops_t *ops, open_inode_t *dir, const char *name,
        inode_type_t type, inode_perm_t perm, uint64_t device_index,
        bool *is_new, inode_nr_t *entry_nr)
{
    inode_ops_t *inode_ops = ((default_file_ops_aux_t *) ops->aux)->inode_ops;

    // TODO: we could ENOTSUP creating device files (like linux nodev fs option),
    // so that device files can only exist in temporary file systems

    if (!inode_ops->read_inode || !inode_ops->write_inode ||
        !inode_ops->read_file || !inode_ops->write_file ||
        !inode_ops->alloc_inode) {
        return ENOTSUP;
    }

    uint64_t name_len = strlen(name);
    if (name_len > FILE_NAME_MAX_LEN) {
        return EFNAME;
    }

    acquire_write(&dir->lock);

    inode_t dir_inode;
    errno_t errno = inode_ops->read_inode(inode_ops, dir->inode_nr, &dir_inode);
    if (errno) {
        release_write(&dir->lock);
        *entry_nr = 0;
        return errno;
    }

    if (dir_inode.type != INODE_DIR) {
        release_write(&dir->lock);
        *entry_nr = 0;
        return ENOTDIR;
    }

    /* Read through the dir entries to see if we can just use an unused slot.
     * Otherwise we can append to the end. We also need to make sure the entry
     * does not already exist. */

    bool got_write_offset = false;
    uint64_t write_offset;

    // TODO: read the dir entries in groups, instead of 1-by-1
    dir_entry_t entry;
    uint64_t bytes_read;
    uint64_t offset = 0;
    while (!(errno = inode_ops->read_file(
                    inode_ops, dir->inode_nr, offset,
                    (uint8_t *) &entry, sizeof(entry), &bytes_read))
           && bytes_read == sizeof(entry)) {
        if (streql(name, entry.name_len, entry.name)) {
            release_write(&dir->lock);
            *entry_nr = entry.inode_nr;
            *is_new = false;
            return 0;
        } else if (!got_write_offset && entry.name_len == 0) {
            got_write_offset = true;
            write_offset = offset;
        }
        offset += sizeof(entry);
    }

    if (errno) {
        release_write(&dir->lock);
        *entry_nr = 0;
        return errno;
    } else if (bytes_read != 0) {
        release_write(&dir->lock);
        /* We should never read just a part of a directory entry. */
        printf("bug: directory corruption in inode %u64\n", dir->inode_nr);
        *entry_nr = 0;
        return ENOENT;
    }

    if (!got_write_offset) {
        write_offset = dir_inode.size;
    }

    panic_if(write_offset % sizeof(dir_entry_t) != 0,
            "should have gotten write offset at a multiple of dir entry size\n");

    /* Allocate a fresh inode for the new entry. */
    errno = inode_ops->alloc_inode(inode_ops, entry_nr, type, perm, device_index);
    if (errno) {
        release_write(&dir->lock);
        *entry_nr = 0;
        return errno;
    }

    if (type == INODE_DIR) {
        /* Make sure to add the . and .. entries. */
        memset(&entry, sizeof(dir_entry_t), 0);
        entry.inode_nr = *entry_nr;
        entry.name_len = 1;
        entry.name[0] = '.';

        uint64_t written;
        errno = inode_ops->write_file(
                inode_ops, *entry_nr, 0, /*append*/ false,
                (const uint8_t *) &entry, sizeof(dir_entry_t),
                &written);
        if (errno) {
            // TODO: any more cleanup?
            release_write(&dir->lock);
            return errno;
        }
        panic_if(written != sizeof(dir_entry_t),
                "did not write entirety of . entry\n");

        entry.inode_nr = dir->inode_nr;
        entry.name_len = 2;
        entry.name[1] = '.';

        errno = inode_ops->write_file(
                inode_ops, *entry_nr, sizeof(dir_entry_t), /*append*/ false,
                (const uint8_t *) &entry, sizeof(dir_entry_t),
                &written);
        if (errno) {
            // TODO: any more cleanup?
            release_write(&dir->lock);
            return errno;
        }
        panic_if(written != sizeof(dir_entry_t),
                "did not write entirety of .. entry\n");
    }

    /* Set up our entry. */
    memset(&entry, sizeof(dir_entry_t), 0);
    entry.inode_nr = *entry_nr;
    entry.name_len = name_len;
    memcpy(entry.name, name, name_len);

    /* Write the entry. */
    uint64_t written;
    errno = inode_ops->write_file(
            inode_ops, dir->inode_nr, write_offset, /*append*/ false,
            (const uint8_t *) &entry, sizeof(entry),
            &written);
    if (errno) {
        // TODO: any more cleanup? probably want to free the freshly allocated inode...
        release_write(&dir->lock);
        return errno;
    }
    panic_if(written < sizeof(entry),
            "did not write entirety of the new entry\n");

    release_write(&dir->lock);
    *is_new = true;
    return 0;
}

errno_t file_ops_delete_entry(file_ops_t *ops, open_inode_t *dir, inode_nr_t entry_nr)
{
    //inode_ops_t *inode_ops = ((default_file_ops_aux_t *) ops->aux)->inode_ops;

    panic("file_ops_delete_entry() not implemented\n");
}

errno_t file_ops_get_inode(file_ops_t *ops, open_inode_t *file, inode_t *inode)
{
    inode_ops_t *inode_ops = ((default_file_ops_aux_t *) ops->aux)->inode_ops;

    if (!inode_ops->read_inode) {
        return ENOTSUP;
    }

    acquire_read(&file->lock);
    errno_t errno = inode_ops->read_inode(inode_ops, file->inode_nr, inode);
    release_read(&file->lock);
    return errno;
}

errno_t file_ops_read_file(file_ops_t *ops, open_inode_t *file, uint64_t offset,
        uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_read)
{
    inode_ops_t *inode_ops = ((default_file_ops_aux_t *) ops->aux)->inode_ops;

    if (!inode_ops->read_file) {
        return ENOTSUP;
    }

    acquire_read(&file->lock);
    errno_t errno = inode_ops->read_file(inode_ops, file->inode_nr, offset,
            buf, buf_size, bytes_read);
    release_read(&file->lock);
    return errno;
}

errno_t file_ops_write_file(file_ops_t *ops, open_inode_t *file, uint64_t offset,
        bool append,
        const uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_written)
{
    inode_ops_t *inode_ops = ((default_file_ops_aux_t *) ops->aux)->inode_ops;

    if (!inode_ops->write_file) {
        return ENOTSUP;
    }

    acquire_write(&file->lock);
    errno_t errno = inode_ops->write_file(inode_ops, file->inode_nr, offset,
            append,
            buf, buf_size, bytes_written);
    release_write(&file->lock);
    return errno;
}

INLINE void get_default_file_ops(file_ops_t *dst, default_file_ops_aux_t *aux)
{
    open_files_t *open_files = pmalloc(sizeof(open_files_t));
    panic_if_not(open_files, "could not malloc open_files\n");
    aux->open_files = open_files;

    create_lock(&open_files->lock);
    open_files->num_entries = 50;
    open_files->entries = pmalloc(open_files->num_entries * sizeof(open_files_entry_t));
    panic_if_not(open_files->entries, "could not malloc open_files->entries\n");
    for (uint64_t i = 0; i < open_files->num_entries; i++) {
        open_files->entries[i].valid = false;
    }

    *dst = (file_ops_t) {
        .root_nr = aux->inode_ops->root_nr,
        .open_file = file_ops_open_file,
        .dup_file = file_ops_dup_file,
        .close_file = file_ops_close_file,
        .find_entry = file_ops_find_entry,
        .create_entry = file_ops_create_entry,
        .delete_entry = file_ops_delete_entry,
        .get_inode = file_ops_get_inode,
        .read_file = file_ops_read_file,
        .write_file = file_ops_write_file,
        .aux = aux
    };
}
