#include <std.h>
#include <virtual_memory.h>
#include <sysreg.h>

/* Note that any code or data located in this file will have low virtual addresses.
 *
 * There are some helper functions in this file which are very similar to the ones
 * in virtual_memory.c, but which are instead prefixed with 'start_', indicating
 * that their code is in low virtual addresses instead of the high virtual addresses
 * that virtual_memory.c code uses. We cannot call (from this file) into any code
 * with high virtual addresses, as this code itself is setting up the virtual
 * address space. */

STATIC_ASSERT(PTW_START_LEVEL <= 1,
        "PTW_START_LEVEL must be at most 1, since the high virtual address range "
        "must cover more than 1 GiB. Try increasing KERNEL_VA_BITS.");
#if PTW_START_LEVEL <= 0
__attribute__((aligned(PAGE_SIZE)))
    pte_t page_table_level_0[512];
#endif
#if PTW_START_LEVEL <= 1
__attribute__((aligned(PAGE_SIZE)))
    pte_t page_table_level_1[512];
#endif
#if PTW_START_LEVEL <= 2
__attribute__((aligned(PAGE_SIZE)))
    pte_t page_table_level_2[512];
#endif

pte_t start_pte_encode_table(pte_t *next_level_table)
{
    uint64_t phys_addr = (uint64_t) next_level_table;
    /* We implicitly set NSTable, APTable, XNTable, and PXNTable to 0. */
    return phys_addr | PTE_TABLE;
}

pte_t start_pte_encode_block(uint32_t level, uint64_t attribs, uint64_t mem_type, uint64_t phys_addr)
{
    uint64_t pte_type = level < 3 ? PTE_BLOCK : PTE_PAGE;
    return attribs | phys_addr | mem_type | pte_type;
}

pte_t start_pte_encode_unmapped()
{
    return PTE_UNMAPPED;
}

void initialize_basic_virtual_memory()
{
    // point both low and high virtual addresses to same physical space for now,
    // with ASID = 0 (upper 16 bits, though maybe top 8 of those must be 0...)
    uint64_t init_page_table_phys;
#if PTW_START_LEVEL == 0
    init_page_table_phys = (uint64_t) page_table_level_0;
#elif PTW_START_LEVEL == 1
    init_page_table_phys = (uint64_t) page_table_level_1;
#else
#error "PTW_START_LEVEL must be at most 1."
#endif
    set_sysreg(TTBR0_EL1, init_page_table_phys | TTBR_CnP);
    set_sysreg(TTBR1_EL1, init_page_table_phys | TTBR_CnP);

    // TODO: verify these
    uint64_t mair_entry_0 = 0xFF; /* normal memory */
    uint64_t mair_entry_1 = 0x04; /* device memory (MMIO) */
    uint64_t mair_entry_2 = 0x44; /* non-cacheable memory */
    set_sysreg(MAIR_EL1,
            (mair_entry_0 << 0)
            | (mair_entry_1 << 8)
            | (mair_entry_2 << 16));

    // set up level 0 page table: mark everything invalid except for entry 0,
    // which covers virtual addresses 0x**** 0000 0000 0000
    //                   to less than 0x**** 0080 0000 0000
    // then set up level 1 page table: mark everything invalid except for entry 0,
    // which covers virtual addresses 0x**** 0000 0000 0000
    //                   to less than 0x**** 0000 4000 0000 (i.e. 1GB)
    // then finally set up level 2 page table: everything before 0x3F000000 is marked
    // as normal (kernel) memory, while everything including and after that address
    // is designated for MMIO
    //
    //   60   56   52   48   44   40   36   32   28   24   20   16   12    8    4    0
    // **** **** **** **** 0000 0000 1000 0000 0000 0000 0000 0000 0000 0000 0000 0000
    // <   TTBR0/TTBR1   > < level0  >< level1  ><     offset in 1GB block           >
    // <   TTBR0/TTBR1   > < level0  >< level1  >< level2  ><  offset in 2MB block   >
    // <   TTBR0/TTBR1   > < level0  >< level1  >< level2  >< level3  > <offset 4KB b>

#if PTW_START_LEVEL <= 0
    page_table_level_0[0] = start_pte_encode_table(page_table_level_1);
    for (uint32_t i = 1; i < 512; i++) {
        page_table_level_0[i] = start_pte_encode_unmapped();
    }
#endif

#if PTW_START_LEVEL <= 1
    page_table_level_1[0] = start_pte_encode_table(page_table_level_2);
    for (uint32_t i = 1; i < 512; i++) {
        page_table_level_1[i] = start_pte_encode_unmapped();
    }
#endif

#if PTW_START_LEVEL <= 2
    for (uint32_t i = 0; i < 512; i++) {
        uint64_t phys_addr = (uint64_t) i * 0x200000;
        uint64_t attribs, mem_type;
        if (phys_addr < 0x3F000000) {
            /* kernel memory (for now) */
            attribs = PTE_AF
                | PTE_INN_SH
                | PTE_RW
                | PTE_KERNEL;
            mem_type = PTE_MEM;
        } else {
            /* just make it device memory for now */
            attribs = PTE_AF
                | PTE_OUT_SH
                | PTE_RW
                | PTE_USER;
            mem_type = PTE_MMIO;
        }
        /* 2MB block entry */
        page_table_level_2[i] = start_pte_encode_block(2, attribs, mem_type, phys_addr);
    }
#endif

    uint64_t id_aa64mmfr0;
    load_sysreg(id_aa64mmfr0, ID_AA64MMFR0_EL1);
    uint64_t pa_range = id_aa64mmfr0 & 7;

    uint64_t tcr
        = TCR_T0SZ(64 - PTW_VA_BITS /* 5 bits */) // PTW_VA_BITS for addressing in TTBR0
        /* EPD0 = 0: MMU will walk the page table to fill TLB miss for table from TTBR0 */
        | TCR_IRGN0(1) /* inner write-back read&write-allocate cacheable */
        | TCR_ORGN0(1) /* inner write-back read&write-allocate cacheable */
        | TCR_SH0(3) /* inner shareable */
        | TCR_TG0(0) /* 4KB granule for TTBR0 */
        | TCR_T1SZ(64 - PTW_VA_BITS /* 5 bits */) // PTW_VA_BITS for addressing in TTBR1
        /* TCR_A1 = 0: ASID pulled from TTBR0 instead of TTBR1 */
        /* EPD1 = 0: MMU will walk the page table to fill TLB miss for table from TTBR1 */
        | TCR_IRGN1(1) /* inner write-back read&write-allocate cacheable */
        | TCR_ORGN1(1) /* inner write-back read&write-allocate cacheable */
        | TCR_SH1(3) /* inner shareable */
        | TCR_TG1(2) /* 4KB granule for TTBR1 */ // !!! different encoding than for TG0 !!!
        | TCR_IPS(pa_range)
        | TCR_AS /* Linux uses this, so we probably should too... */ // TODO: verify it's correct!
        /* TCR_TBI0 = 0: use top byte of VA for page table matching for TTBR0 */
        /* TCR_TBI1 = 0: use top byte of VA for page table matching for TTBR1 */
        /* TCR_HA = 0 */ // due to -FEAT_HAFDBS
        /* TCR_HD = 0 */ // due to -FEAT_HAFDBS
        /* TCR_HPD0 = 0 */ // due to -FEAT_HAFDBS
        /* TCR_HPD1 = 0 */ // due to -FEAT_HPDS
        /* TCR_HPD1 = 0 */ // due to -FEAT_HPDS
        /* TCR_HWU*** = 0 */ // hardware cannot use the specified bits in PTEs from TTBR0/TTBR1
        /* TCR_TBID0 = 0: apply TBI0 to instruction and data access */
        /* TCR_TBID1 = 0: apply TBI1 to instruction and data access */
        /* TCR_TBID1 = 0: apply TBI1 to instruction and data access */
        /* TCR_NFD0 = 0: MMU will walk page table to fill for non-fault unprivileged access in TTBR0 */
        /* TCR_NFD1 = 0: MMU will walk page table to fill for non-fault unprivileged access in TTBR1 */
        /* TCR_E0PD0 = 0: unprivileged access in TTBR0 does not fault */
        /* TCR_E0PD1 = 0: unprivileged access in TTBR1 does not fault */
        /* TCR_TMCA0 = 0: do not generate unchecked accesses for VA[59:55] = 0b00000 */ // TODO what is unchecked access
        /* TCR_TMCA1 = 0: do not generate unchecked accesses for VA[59:55] = 0b11111 */ // TODO what is unchecked access
        ;
    set_sysreg(TCR_EL1, tcr);
    /* Make sure the TCR change is seen before we set up the MMU. */
    asm volatile("isb");

    // TODO: figure out if ASID is 8 or 16 bits (probably 8 bits?)
    // TODO: FEAT_PAuth?
    // processor:
    //   -FEAT_CSV3
    //   -FEAT_DIT
    //   -FEAT_AMUv1,v1p1
    //   -FEAT_MPAM
    //   -FEAT_SEL2
    //   -FEAT_SVE
    //   -FEAT_RAS
    //   -FEAT_GIC
    //   +FEAT_AdvSIMD (but without support for half-precision FP)
    //   +FEAT_FP (but without support for half-precision FP)
    //   +FEAT_EL0-3 (in 32 or 64 bit)
    //   -FEAT_MTE
    //   -FEAT_SSBS
    //   -FEAT_SSBS2
    //   -FEAT_BTI
    // memory:
    //   -FEAT_ECV
    //   -FEAT_FGT
    //   -FEAT_ExS
    //   4KB, 16KB, 64KB granule sizes supported in stage 1, stage 2
    //   -BigEndEL0 (no mixed-endian support at EL0)
    //   +SNSMem (supports distinct secure/non-secure memory)
    //   +BigEnd (mixed-endian support)
    //   ASIDBits: 16 bits
    //   PARange: 40 bit PAs (1TB)
    //   -FEAT_ETS
    //   -FEAT_TWED
    //   -FEAT_XNX
    //   -SpecSEI (PE does not generate SError due to external abort on speculative read)
    //   -FEAT_PAN,-FEAT_PAN2
    //   -FEAT_LOR
    //   -HPDS (disabling of hierarchical controls in translation tables is _un_supported)
    //   -FEAT_VHE
    //   VMIDBits: 8 bits (i.e. -FEAT_VMID16)
    //   -FEAT_HAFDBS (hardware does not update access/dirty flags in translation tables)

    // SCTLR_EL1 options (and RES1):
    // * EnIA (bit 31) [FEAT_PAuth?] TODO
    // * EnIB (bit 30) [FEAT_PAuth?] TODO
    // * LSMAOE (bit 29) [FEAT_LSMAOC?] (-FEAT_LSMAOC => RES1) TODO (RES1 according to Linux)
    // * nTLSMD (bit 28) [FEAT_LSMAOC?] (-FEAT_LSMAOC => RES1) TODO (RES1 according to Linux)
    // * EnDA (bit 27) [FEAT_PAuth?] TODO
    // * UCI (bit 26): use 0, so EL0 cache maintenance instrs are trapped
    // * EE (bit 25): TODO
    // * E0E (bit 24): TODO
    // * SPAN (bit 23): RES1 (due to -FEAT_PAN)
    // * EIS (bit 22): RES1 (due to -FEAT_ExS)
    // * IESB (bit 21): use 1, since Linux does. TODO: figure out what this really does.
    // * TSCXT (bit 20): TODO [FEAT_CSV2?] <- look this up... probably already have this info
    //                   (this is RES1 according to Linux)
    // * WXN (bit 19): use 0, so we can separate write and execute access in page tables
    // * nTWE (bit 18): use 0, so EL0 `wfe` is trapped (if it would cause low-power state)
    // * nTWI (bit 16): use 0, so EL0 `wfi` is trapped (if it would cause low-power state)
    // * UCT (bit 15): use 0, so CTR_EL0 access from EL0 is trapped
    // * DZE (bit 14): use 0, so EL0 `dc dza` is trapped
    // * EnDB (bit 13) [FEAT_PAuth?] TODO (probably 0)
    // * I (bit 12): TODO
    // * EOS (bit 11): RES1 (due to -FEAT_ExS)
    // * EnRCTX (bit 10) [FEAT_SPECRES?]: use 0 (or RES0, either way)
    // * UMA (bit 9): use 0, so EL0 DAIF access is trapped
    // * SED (bit 8): use 0, so EL0 `setend` is trapped
    // * ITD (bit 7): use 1 (seems to trap some EL0 aarch32 stuff. don't care about aarch32...)
    // * nAA (bit 6) [FEAT_LSE2?]: use 0, to generate alignment faults for a bunch of instructions
    // * CP15BEN (bit 5): use 1 (enables some aarch32 EL0 stuff, again don't care)
    // * SA0 (bit 4): use 1, so SP must be 16-byte aligned in EL0
    // * SA (bit 3): use 1, so SP must be 16-byte aligned in EL1
    // * C (bit 2): TODO (probably 0 though)
    // * A (bit 1): use 0, so loads/stores are not checked for alignment in EL0, EL1
    // * M (bit 0): use 1, to enable MMU
    uint64_t sctlr;
    load_sysreg(sctlr, SCTLR_EL1);
    sctlr &= BIT_OFF(26)
        & BIT_OFF(19)
        & BIT_OFF(18)
        & BIT_OFF(17)
        & BIT_OFF(16)
        & BIT_OFF(15)
        & BIT_OFF(14)
        & BIT_OFF(10)
        & BIT_OFF(9)
        & BIT_OFF(8)
        & BIT_OFF(6)
        & BIT_OFF(1);
    sctlr |= BIT_ON(29)
        | BIT_ON(28)
        | BIT_ON(23)
        | BIT_ON(22)
        | BIT_ON(21)
        | BIT_ON(20)
        | BIT_ON(20)
        | BIT_ON(11)
        | BIT_ON(7)
        | BIT_ON(5)
        | BIT_ON(4)
        | BIT_ON(3)
        | BIT_ON(0);

    asm volatile("tlbi VMAllE1");

    set_sysreg(SCTLR_EL1, sctlr);
    asm volatile("isb; ic iallu; dsb nsh; isb");
}
