#include <exception_level.h>
#include <console.h>
#include <sysreg.h>

void initialize_el0_and_el1()
{
    /* We must use puts instead of printf, since printf is compiled to use NEON operations
     * but we have not yet enabled those for EL1 (which is our execution level). */
    puts("initializing EL0 and EL1 execution states:\n");
    // Enable FP and NEON ops for EL0 and EL1, since the code generated for printf uses NEON registers.
    // (Recall NEON is advanced SIMD.)
    puts("  enabling FP and NEON for EL0 and EL1\n");
    uint64_t cpacr_el1; // Even though it's an EL1 register, it applies settings for EL0 and EL1.
    load_sysreg(cpacr_el1, CPACR_EL1);
    cpacr_el1 |= 0x3 << 20; // Set [21:20] to 0b11: do not trap FP/NEON ops.
    set_sysreg(CPACR_EL1, cpacr_el1);
    // Instruction sequence barrier; required after changing CPACR_EL1,
    // so that following NEON operations in EL1 are guaranteed to see the updated
    // trap settings, i.e. do not trap.
    asm volatile("isb");
}

uint32_t current_exception_level()
{
    uint64_t current_el;
    load_sysreg(current_el, CurrentEL);
    return (current_el >> 2) & 3;
}
