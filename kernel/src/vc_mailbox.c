#include <vc_mailbox.h>
#include <mmio.h>
#include <virtual_memory.h>
#include <string.h>
#include <dbg.h>

#define MAILBOX_BASE         0x0000B880

#define MAILBOX1_RW          mmio(MAILBOX_BASE + 0x00)
#define MAILBOX1_POLL        mmio(MAILBOX_BASE + 0x10)
#define MAILBOX1_SENDER      mmio(MAILBOX_BASE + 0x14)
#define MAILBOX1_STATUS      mmio(MAILBOX_BASE + 0x18)
#define MAILBOX1_CONFIG      mmio(MAILBOX_BASE + 0x1C)
#define MAILBOX2_RW          mmio(MAILBOX_BASE + 0x20)
#define MAILBOX2_POLL        mmio(MAILBOX_BASE + 0x30)
#define MAILBOX2_SENDER      mmio(MAILBOX_BASE + 0x34)
#define MAILBOX2_STATUS      mmio(MAILBOX_BASE + 0x38)
#define MAILBOX2_CONFIG      mmio(MAILBOX_BASE + 0x3C)

#define MAILBOX_FULL       0x80000000
#define MAILBOX_EMPTY      0x40000000

uint32_t mailbox_read(uint8_t channel)
{
    panic_if(channel & ~0x0F, "channel must be 0 to 15; got %u8\n", channel);
    while (true) {
        do { asm volatile("nop"); } while (*MAILBOX1_STATUS & MAILBOX_EMPTY);
        uint32_t read = *MAILBOX1_RW;
        //printf("[mailbox] read 0x%u32{16} from channel %u8\n", read & ~0x0F, channel);
        if ((read & 0x0F) == channel) {
            return read & ~0x0F;
        }
    }
}

void mailbox_write(uint8_t channel, uint32_t data)
{
    panic_if(channel & ~0x0F, "channel must be 0 to 15; got %u8\n", channel);
    panic_if(data & 0x0F, "data must have low 4 bits zero; got %u32\n", data);
    do { asm volatile("nop"); } while (*MAILBOX1_STATUS & MAILBOX_FULL);
    *MAILBOX2_RW = data | (uint32_t) channel;
    //printf("[mailbox] wrote 0x%u32{16} to channel %u8\n", data, channel);
}


#define MAILBOX_RESPONSE   0x80000000

bool mailbox_call(uint8_t channel, uint32_t *buffer)
{
    panic_if(channel & ~0x0F, "channel must be 0 to 15; got %u8\n", channel);
    uint64_t buffer_phys = kernel_virt_to_phys(buffer);
    panic_if(buffer_phys > UINT32_MAX,
            "buffer address must be a kernel virtual address flat-mapped to physical memory; "
            "got 0x%p\n",
            buffer);
    panic_if(buffer_phys & 0x0F, "buffer must be 16-byte aligned; got 0x%p\n", buffer);
    mailbox_write(channel, (uint32_t) buffer_phys);
    while (mailbox_read(channel) != buffer_phys) {
        /* Do nothing; just wait for the VC to respond to our message. */
    }
    return buffer[1] == MAILBOX_REQUEST_SUCCESSFUL;
}

bool mailbox_call_property_tag(uint32_t *buf, uint32_t buf_len,
        uint32_t tag_id, uint32_t *tag_buf, uint32_t tag_buf_len,
        uint32_t tag_buf_input_len,
        uint32_t *response_length)
{
    panic_if_not(buf_len >= 6 + tag_buf_len,
            "buf_len is not large enough (see the function documentation)\n");
    panic_if_not(tag_buf_input_len <= tag_buf_len,
            "tag_buf_input_len must be at most tag_buf_len\n");

    /* message header */
    buf[0] = (6 + tag_buf_len) * sizeof(uint32_t);
    buf[1] = MAILBOX_REQUEST;
    /* tag starts here */
    buf[2] = tag_id;
    buf[3] = tag_buf_len * sizeof(uint32_t);
    buf[4] = 0; /* Request code. VC will fill this in with the response code. */
    if (tag_buf) {
        memcpy(buf + 5, tag_buf, tag_buf_input_len * sizeof(uint32_t)); /* Input tag data. */
    }
    memset(buf + 5 + tag_buf_input_len, (tag_buf_len - tag_buf_input_len) * sizeof(uint32_t), 0); /* Zero-fill the rest. */
    /* tag ends here */
    /* end-of-tags indicator */
    buf[5 + tag_buf_len] = 0;

    if (!mailbox_call(CHANNEL_ARM_TO_VC, buf)) {
        dbg_abort_with(false, "mailbox call failed\n");
    }

    /* Sanity check that the VC responded to our tag. */
    if (buf[2] != tag_id) {
        dbg_abort_with(false,
                "VC responded to tag %u32 instead of the expected tag %u32\n",
                buf[2], tag_id);
    }
    if ((buf[4] & TAG_CODE_RESPONSE) == 0) {
        dbg_abort_with(false,
                "VC did not respond to the tag\n");
    }

    uint32_t full_length = buf[4] & TAG_CODE_LENGTH;
    if (tag_buf) {
        uint32_t max_length = tag_buf_len * sizeof(uint32_t);
        uint32_t actual_length = max_length < full_length ? max_length : full_length;
        memcpy(tag_buf, buf + 5, actual_length);
    }
    *response_length = full_length;

    return true;
}
