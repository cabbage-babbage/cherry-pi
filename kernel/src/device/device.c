#include <device/device.h>
#include <synch/lock.h>
#include <fs/filesys.h>
#include <device/null.h>
#include <device/zero.h>
#include <device/full.h>
#include <dbg.h>

INLINE errno_t device_ioctl(device_t *dev, uint64_t ioctl_nr,
        uint8_t *buf, uint64_t buf_len)
{
    return dev->ioctl(dev, ioctl_nr, buf, buf_len);
}

INLINE void device_close(device_t *dev)
{
    errno_t errno = dev->close(dev);
    if (errno) {
        printf("[ERROR] could not close device "device_id_spec" (%s): %e\n",
                dev->id, dev->name, errno);
    }
}

INLINE errno_t device_dup(device_t *dev)
{
    return dev->dup(dev);
}

rw_lock_t registry_lock;
#define MAX_DEVICES 10
device_t *device_registry[MAX_DEVICES];

void initialize_devices()
{
    create_rw_lock(&registry_lock, /*prefer_writers*/ true);
    for (device_id_t i = 0; i < MAX_DEVICES; i++) {
        device_registry[i] = NULL;
    }
}

void initialize_standard_devices()
{
    initialize_null_device();
    initialize_zero_device();
    initialize_full_device();
}

void add_devices(const char *dirname)
{
    errno_t errno;
    fs_locator_t dir;
    errno = filesys_walk(dirname, &dir);
    panic_if(errno, "could not open %s: %e\n", dirname, errno);

    acquire_read(&registry_lock);

    for (device_id_t i = 0; i < MAX_DEVICES; i++) {
        device_t *device = device_registry[i];
        if (!device) {
            continue;
        }

        errno_t errno;
        bool is_new;
        errno = filesys_create_at(dir, device->name, /*keep_open*/false,
                device->type == DEVICE_BLOCKDEV ? INODE_BLOCKDEV : INODE_CHARDEV,
                INODE_PERM_R | INODE_PERM_W, device->id,
                &is_new, NULL);
        if (!errno && !is_new) {
            errno = EEXIST;
        }
        panic_if(errno, "could not add device %s/%s: %e\n",
                dirname, device->name, errno);
    }

    release_read(&registry_lock);
}

errno_t device_register(device_t *dev)
{
    panic_if(dev->id,
            "expected device to be currently unregistered\n");

    bool registered = false;

    acquire_write(&registry_lock);
    /* Always leave id 0 unassigned, to help catch bugs. */
    for (device_id_t id = 1; id < MAX_DEVICES; id++) {
        if (!device_registry[id]) {
            device_registry[id] = dev;
            dev->id = id;
            registered = true;
            break;
        }
    }
    release_write(&registry_lock);

    return registered ? 0 : EFULL;
}

device_t *find_device_by_id(device_id_t id)
{
    device_t *dev = NULL;

    acquire_read(&registry_lock);
    if (id < MAX_DEVICES) {
        dev = device_registry[id];
    }
    release_read(&registry_lock);

    return dev;
}
