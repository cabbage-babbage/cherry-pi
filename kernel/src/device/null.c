#include <device/chardev.h>
#include <dbg.h>

chardev_t null_device;

errno_t null_ioctl(device_t *dev, uint64_t ioctl_nr,
        uint8_t *buf, uint64_t buf_len)
{
    return ENOIOCTL;
}

errno_t null_dup(device_t *dev)
{
    /* Nothing required; this device is always open. */
    return 0;
}

errno_t null_close(device_t *dev)
{
    /* Nothing required; this device is always open. */
    return 0;
}

errno_t null_read(chardev_t *dev, uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_read)
{
    *bytes_read = 0;
    return 0;
}

errno_t null_write(chardev_t *dev, const uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_written)
{
    *bytes_written = buf_size;
    return 0;
}

void initialize_null_device()
{
    null_device = (chardev_t) {
        .dev.id = 0,
        .dev.type = DEVICE_CHARDEV,
        .dev.name = "null",
        .dev.ioctl = &null_ioctl,
        .dev.dup = &null_dup,
        .dev.close = &null_close,
        .dev.aux = NULL,
        .read = &null_read,
        .write = &null_write
    };

    errno_t errno = device_register(&null_device.dev);
    panic_if(errno, "could not register null device: %e\n", errno);
}
