#include <device/uart.h>
#include <proc.h>
#include <irq.h>
#include <uapi/errno.h>
#include <dbg.h>

#define RX_BUF_SIZE 128
#define TX_BUF_SIZE 128

// TODO: make these circular buffers instead of arrays-as-queues
char rx_buffer[RX_BUF_SIZE];
uint32_t rx_buffer_fill;

char tx_buffer[TX_BUF_SIZE];
uint32_t tx_buffer_fill;

/* The I/O lock is used to synchronize multiple processes attempting
 * to read/write UART at the same time. It also must be held to update
 * the proc_waiting_for_uart_rx. */
lock_t uart_io_lock;
proc_t *proc_waiting_for_uart_rx;

chardev_t uart_device;

void handle_uart_rx(ptr_t irq_saved_regs_base);
void handle_uart_tx_available(ptr_t irq_saved_regs_base);

errno_t uart_read(chardev_t *dev, uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_read);
errno_t uart_write(chardev_t *dev, const uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_written);
errno_t uart_ioctl(device_t *dev, uint64_t ioctl_nr, uint8_t *buf,
        uint64_t buf_len);
errno_t uart_dup(device_t *dev);
errno_t uart_close(device_t *dev);

/**
 * Set baud rate and characteristics (115200 8N1) and map to GPIO
 */
void uart_init()
{
    rx_buffer_fill = 0;
    tx_buffer_fill = 0;

    register unsigned int r;

    /* initialize UART */
    *AUX_ENABLE |=1;       // enable UART1, AUX mini uart
    *AUX_MU_IER = 0;
    *AUX_MU_CNTL = 0;
    *AUX_MU_LCR = 3;       // 8 bits
    *AUX_MU_MCR = 0;
    *AUX_MU_IER = 0;
    *AUX_MU_IIR = 0xc6;    // disable interrupts
    *AUX_MU_BAUD = 270;    // 115200 baud
    /* map UART1 to GPIO pins */
    r=*GPFSEL1;
    r&=~((7<<12)|(7<<15)); // gpio14, gpio15
    r|=(2<<12)|(2<<15);    // alt5
    *GPFSEL1 = r;
    *GPPUD = 0;            // enable pins 14 and 15
    r=150; while(r--) { asm volatile("nop"); }
    *GPPUDCLK0 = (1<<14)|(1<<15);
    r=150; while(r--) { asm volatile("nop"); }
    *GPPUDCLK0 = 0;        // flush GPIO setup
    *AUX_MU_CNTL = 3;      // enable Tx, Rx

    uart_device = (chardev_t) {
        .dev.id = 0,
        .dev.type = DEVICE_CHARDEV,
        .dev.name = "uart",
        .dev.ioctl = &uart_ioctl,
        .dev.dup = &uart_dup,
        .dev.close = &uart_close,
        .dev.aux = NULL,
        .read = &uart_read,
        .write = &uart_write
    };
    errno_t errno = device_register(&uart_device.dev);
    panic_if(errno, "could not register uart device: %e\n", errno);

    create_lock(&uart_io_lock);
    proc_waiting_for_uart_rx = NULL;
}

const uint32_t ENABLE_RX_IRQ = 1;
const uint32_t ENABLE_TX_IRQ = 2;

void uart_enable_interrupts()
{
    /* The bcm2835 peripherals specification describes bits 7:2
     * as reserved 0, but the errata specify that bits 3:2 must
     * be set to receive interrupts. */
    const uint32_t MAGIC = 0xC;
    *AUX_MU_IER |= ENABLE_RX_IRQ | MAGIC;

    // enable IRQ 29, auxiliary (uart1/mini uart, spi1, spi2)
    *IRQ_ENABLE_1 |= (uint32_t) 1 << 29;
}

INLINE bool uart_rx_empty()
{
    return rx_buffer_fill == 0;
}

void uart_rx_enqueue(char c)
{
    if (rx_buffer_fill == RX_BUF_SIZE) {
        // TODO: don't panic, but still record this somehow
        panic("rx buffer full\n");
        return;
    } else {
        rx_buffer[rx_buffer_fill] = c;
        rx_buffer_fill++;
    }
}

char uart_rx_dequeue()
{
    panic_if_not(rx_buffer_fill, "rx buffer is empty\n");
    char c = rx_buffer[0];
    rx_buffer_fill--;
    for (uint32_t i = 0; i < rx_buffer_fill; i++) {
        rx_buffer[i] = rx_buffer[i + 1];
    }
    return c;
}

INLINE bool uart_tx_full()
{
    return tx_buffer_fill == TX_BUF_SIZE;
}

void uart_tx_enqueue(char c)
{
    if (tx_buffer_fill == TX_BUF_SIZE) {
        // TODO: don't panic, but still record this somehow
        panic("tx buffer is full\n");
        return;
    } else {
        tx_buffer[tx_buffer_fill] = c;
        tx_buffer_fill++;
    }
}

char uart_tx_dequeue()
{
    panic_if_not(tx_buffer_fill, "tx buffer is empty\n");
    char c = tx_buffer[0];
    tx_buffer_fill--;
    for (uint32_t i = 0; i < tx_buffer_fill; i++) {
        tx_buffer[i] = tx_buffer[i + 1];
    }
    return c;
}

INLINE bool uart_irq_firing()
{
    return !(*AUX_MU_IIR & 0x1);
}

void uart_handle_irq(ptr_t saved_regs_base)
{
    uint32_t aux_mu_ier = *AUX_MU_IER;
    uint32_t aux_mu_iir = *AUX_MU_IIR;
    if ((aux_mu_ier & ENABLE_TX_IRQ) && (aux_mu_iir & 0x2)) {
        putc('.');
        /* Transmit buffer is empty. */
        while (tx_buffer_fill && (*AUX_MU_LSR & 0x10)) {
            char tx = uart_tx_dequeue();
            *AUX_MU_IO = tx;
        }
        if (tx_buffer_fill < TX_BUF_SIZE) {
            handle_uart_tx_available(saved_regs_base);
        }
        // TODO: turn off transmit-empty interrupts until
        // we have something to transmit again!
    }
    if ((aux_mu_ier & ENABLE_RX_IRQ) && (aux_mu_iir & 0x4)) {
        /* Receive buffer is nonempty. */
        while (*AUX_MU_LSR & 0x1) {
            char rx = (char) *AUX_MU_IO;
            uart_rx_enqueue(rx);
            handle_uart_rx(saved_regs_base);
        }
    }
    /* Reset the interrupt-pending flag. */
    // TODO: not sure if this is required
    *AUX_MU_IIR |= 0x1;
}

/**
 * Send a character
 */
void uart_blocking_send(char c) {
    /* wait until we can send */
    do{asm volatile("nop");}while(!(*AUX_MU_LSR&0x20));
    /* write the character to the buffer */
    *AUX_MU_IO=c;
}

/**
 * Receive a character
 */
char uart_blocking_getc() {
    /* wait until something is in the buffer */
    do{asm volatile("nop");}while(!(*AUX_MU_LSR&0x01));
    /* read it and return */
    return (char)(*AUX_MU_IO);
}

void handle_uart_rx(ptr_t irq_saved_regs_base)
{
    panic_if(interrupts_enabled(),
            "handle_uart_rx() should only be called in an IRQ context\n");

    if (proc_waiting_for_uart_rx) {
        wakeup_irq(proc_waiting_for_uart_rx);
    }
}

void handle_uart_tx_available(ptr_t irq_saved_regs_base)
{
    panic_if(interrupts_enabled(),
            "handle_uart_tx_available() should only be called in an IRQ context\n");

    panic("handle_uart_tx_available() not implemented\n");
    // TODO: something similar to handle_uart_rx
}

/* Main device interface read function. */
errno_t uart_read(chardev_t *dev, uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_read)
{
    panic_if(dev != &uart_device,
            "uart device function was passed a device other than uart_device\n");

    proc_t *proc = current_process();

    /* The device lock is used to synchronize between the IRQ handler
     * updating the buffers and processes accessing the buffers. */
    irq_lock_t uart_dev_lock;

    /* Whether we're blocking or not, we should at least check the RX buffer first. */
    *bytes_read = 0;
    if (!acquire_lock_interruptible(&uart_io_lock)) {
        return EINTR;
    }
    while (*bytes_read < bytes && !uart_rx_empty()) {
        acquire_irq_lock(&uart_dev_lock);
        buf[(*bytes_read)++] = uart_rx_dequeue();
        release_irq_lock(&uart_dev_lock);
    }
    release_lock(&uart_io_lock);

    if (flags & IO_BLOCKING) {
        if (!acquire_lock_interruptible(&uart_io_lock)) {
            return EINTR;
        }

        while (*bytes_read < bytes && !proc->is_interrupted) {
            /* Wait for handle_uart_rx() to wake us up. */
            prepare_irq_wait();
            proc_waiting_for_uart_rx = current_process();
            irq_wait();

            while (*bytes_read < bytes && !uart_rx_empty()) {
                acquire_irq_lock(&uart_dev_lock);
                buf[(*bytes_read)++] = uart_rx_dequeue();
                release_irq_lock(&uart_dev_lock);
            }
        }

        proc_waiting_for_uart_rx = NULL;

        release_lock(&uart_io_lock);

        if (was_interrupted()) {
            return EINTR;
        }
    } else {
        /* EAGAIN if the read is nonblocking but there wasn't any data to read
         * without blocking. */
        if (*bytes_read == 0) {
            return EAGAIN;
        }
    }

    return 0;
}

/* Main device interface write function. */
errno_t uart_write(chardev_t *dev, const uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_written)
{
    panic_if(dev != &uart_device,
            "uart device function was passed a device other than uart_device\n");

    panic_if_not(flags & IO_BLOCKING, "nonblocking UART write not implemented\n");

    // TODO: nonblocking writes, and also blocking writes but without busy waiting
    for (uint64_t i = 0; i < bytes; i++) {
        uart_blocking_send(buf[i]);
    }
    *bytes_written = bytes;
    return 0;
}

errno_t uart_ioctl(device_t *dev, uint64_t ioctl_nr, uint8_t *buf,
        uint64_t buf_len)
{
    panic_if(dev != &uart_device.dev,
            "uart device function was passed a device other than uart_device\n");

    return ENOIOCTL;
}

errno_t uart_dup(device_t *dev)
{
    panic_if(dev != &uart_device.dev,
            "uart device function was passed a device other than uart_device\n");

    /* Nothing required; the UART device is always open. */
    return 0;
}

errno_t uart_close(device_t *dev)
{
    panic_if(dev != &uart_device.dev,
            "uart device function was passed a device other than uart_device\n");

    /* Also nothing required; the UART device is always open. */
    return 0;
}
