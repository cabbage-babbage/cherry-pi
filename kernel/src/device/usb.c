#include <device/usb.h>
#include <irq.h>

usb_irq_handler_t _usb_irq_handler;
void *_usb_irq_aux;

void usb_set_irq_handler(usb_irq_handler_t handler, void *aux)
{
    _usb_irq_handler = handler;
    _usb_irq_aux = aux;
}

bool usb_irq_firing()
{
    return ((*GPU_PENDING_1 >> 9) & 0x1) != 0;
}

void usb_handle_irq()
{
    /* Just drop the IRQ if we don't have a handler registered. */
    if (_usb_irq_handler) {
        _usb_irq_handler(_usb_irq_aux);
    }
}
