#include <device/tty.h>
#include <uapi/errno.h>
#include <synch/lock.h>
#include <stdops.h>
#include <heap.h>
#include <string.h>
#include <proc.h>
#include <dbg.h>

// TODO: make this a circular buffer, and allow parallel writes/reads.
/* A buffer of input bytes from a tty's underlying device. The implementation
 * assumes that only a single process is writing into the buffer, but there may be
 * many processes reading from the buffer. */
typedef struct {
    /* The number of allocated bytes. This cannot change after initialization. */
    uint64_t size;

    /* The actual buffer and its fill level. Protected by the buf_lock. */
    uint8_t *buf;
    uint64_t fill;

    lock_t buf_lock;

    /* 0-1 semaphore which indicates if there is any data available to read. */
    semaphore_t read_avail;
} tty_buf_t;

void tty_buf_read_buf(tty_buf_t *tty_buf, uint8_t *buf, uint64_t count)
{
    panic_if(count > tty_buf->fill, "tty_buf does not have enough bytes available to read\n");
    memcpy(buf, tty_buf->buf, count);
    tty_buf->fill -= count;
    // TODO: update this once the tty_buf is a circular buffer
    for (uint64_t i = 0; i < tty_buf->fill; i++) {
        tty_buf->buf[i] = tty_buf->buf[i + count];
    }
}

/* Multiple processes can call this in parallel. */
errno_t tty_buf_read(tty_buf_t *tty_buf, uint8_t *buf, uint64_t buf_size,
        bool blocking, uint64_t *bytes_read)
{
    if (blocking) {
        *bytes_read = 0;
        while (buf_size > 0) {
            if (!sema_down_interruptible(&tty_buf->read_avail)) {
                return EINTR;
            }

            if (!acquire_lock_interruptible(&tty_buf->buf_lock)) {
                return EINTR;
            }

            /* Even if read_avail was set, we may not have any data to read;
             * such is the case when there was a 0-length write, due to (for
             * example) the user entering ^D on an empty line. In this case,
             * the read is complete. */

            uint64_t read = min_uint64(tty_buf->fill, buf_size);
            tty_buf_read_buf(tty_buf, buf, read);
            buf += read;
            buf_size -= read;
            *bytes_read += read;

            if (tty_buf->fill > 0) {
                sema_limit_up(&tty_buf->read_avail, 1);
            }

            release_lock(&tty_buf->buf_lock);

            if (read == 0) {
                break;
            }
        }
        return 0;
    } else {
        *bytes_read = 0;
        if (!try_acquire_lock(&tty_buf->buf_lock)) {
            return EAGAIN;
        }

        uint64_t read = min_uint64(tty_buf->fill, buf_size);
        tty_buf_read_buf(tty_buf, buf, read);
        *bytes_read += read;

        release_lock(&tty_buf->buf_lock);

        return read == 0 ? EAGAIN : 0;
    }
}

/* Assumes only one process has write access. */
void tty_buf_write(tty_buf_t *tty_buf, const uint8_t *buf, uint64_t buf_size)
{
    /* This simply drops all data that doesn't fit into the tty_buf. */

    acquire_lock(&tty_buf->buf_lock);

    uint64_t write = min_uint64(tty_buf->size - tty_buf->fill, buf_size);
    memcpy(tty_buf->buf + tty_buf->fill, buf, write);
    tty_buf->fill += write;

    /* Even if we didn't actually write any data, wake up any readers. */
    sema_limit_up(&tty_buf->read_avail, 1);

    release_lock(&tty_buf->buf_lock);
}

#define LINE_MAX_LEN 128

typedef struct {
    chardev_t *base_dev;

    /* The lock protects config. */
    lock_t tty_lock;
    tty_config_t config;

    char line[LINE_MAX_LEN]; /* Not null-terminated. */
    uint64_t line_len;

    tty_buf_t tty_buf;

    uint32_t fg_gid;
} tty_state_t;

USE_VALUE errno_t read_one(chardev_t *dev, uint8_t *in, bool *eof)
{
    uint64_t bytes_read;
    errno_t errno = chardev_read(dev, in, 1, IO_BLOCKING, &bytes_read);
    if (errno) {
        return errno;
    }
    *eof = bytes_read == 0;
    return 0;
}

void echo_one(chardev_t *dev, uint8_t out)
{
    uint64_t bytes_written;
    errno_t errno = chardev_write(dev, &out, 1, IO_BLOCKING, &bytes_written);
    if (errno) {
        printf("[tty: could not echo]\n");
    }
}

void send_signal(proc_t *proc, signal_nr_t nr)
{
    signal_t *signal = alloc_signal(nr, 0, NULL);
    if (signal) {
        deliver_signal(proc, signal);
    } else {
        printf("[tty: could not allocate signal]\n");
    }
}

/* Performs any signaling- and echoing-related processing
 * on the given input, and sets has_input to false if the input
 * was consumed. */
void tty_process_one(tty_state_t *state, bool *has_input, uint8_t in, uint8_t *out)
{
    chardev_t *base_dev = state->base_dev;
    tty_config_t *cfg = &state->config;

    /* Signaling. */
    const uint8_t CTRL_C = 3, CTRL_D = 4, CTRL_E = 5, CTRL_F = 6, CTRL_S = 19;
    if (cfg->in_convert_ctrl_c_to_signal && (in == CTRL_C || in == CTRL_E)) {
        if (state->fg_gid) {
            deliver_group_signal(state->fg_gid, SIGINT);
        }
        *has_input = false;
        return;
    } else if (in == CTRL_F) {
        if (state->fg_gid) {
            deliver_group_signal(state->fg_gid, SIGKILL);
        }
        *has_input = false;
        return;
    } else if (in == CTRL_S) {
        if (state->fg_gid) {
            deliver_group_signal(state->fg_gid, SIGSTOP);
        }
        *has_input = false;
        return;
    }

    /* Local echoing. */
    if (in == '\r' && cfg->in_convert_cr_to_lf) {
        *out = '\n';
    } else {
        *out = in;
    }

    if (cfg->in_echo && in != CTRL_D) {
        if (*out == '\n' && cfg->out_convert_lf_to_crlf) {
            echo_one(base_dev, '\r');
        }
        echo_one(base_dev, *out);
    }
}

uint32_t tty_process_input(tty_state_t *state)
{
    chardev_t *base_dev = state->base_dev;
    errno_t errno;

    volatile tty_config_t *cfg = &state->config;

    bool has_input = false;
    uint8_t in;

    while (true) {
        while (cfg->in_line_mode) {
            if (!has_input) {
                bool dev_eof;
                errno = read_one(base_dev, &in, &dev_eof);
                if (errno) {
                    printf("[tty: error reading base device: %e]\n", errno);
                    return 1;
                }

                if (dev_eof) {
                    goto eof;
                }

                has_input = true;
            }

            if (!cfg->in_line_mode) {
                break;
            }

            /* Consume the input. */
            uint8_t out;
            tty_process_one(state, &has_input, in, &out);
            if (!has_input) {
                continue;
            }

            has_input = false;

            const uint8_t CTRL_D = 4;
            bool line_complete = in == CTRL_D || in == '\r';
            bool append_to_line = in != CTRL_D && (in == '\r' || (state->line_len < LINE_MAX_LEN - 1));

            if (append_to_line) {
                state->line[state->line_len++] = out;
            }

            if (line_complete) {
                tty_buf_write(&state->tty_buf, (const uint8_t *) state->line, state->line_len);
                state->line_len = 0;
            }
        }

        if (state->line_len != 0) {
            tty_buf_write(&state->tty_buf, (const uint8_t *) state->line, state->line_len);
            state->line_len = 0;
        }

        if (!has_input) {
            bool dev_eof;
            errno = read_one(base_dev, &in, &dev_eof);
            if (errno) {
                printf("[tty: error reading base device: %e]\n", errno);
                return 1;
            }

            if (dev_eof) {
                goto eof;
            }

            has_input = true;
        }

        if (cfg->in_line_mode) {
            continue;
        }

        /* Consume the input. */
        uint8_t out;
        tty_process_one(state, &has_input, in, &out);
        if (!has_input) {
            continue;
        }

        has_input = false;
        tty_buf_write(&state->tty_buf, &out, 1);
    }

eof:
    printf("[tty: EOF]\n");
    return 0;
}

uint32_t tty_proc_main(void *aux)
{
    chardev_t *tty_dev = aux;
    tty_state_t *state = tty_dev->dev.aux;

    uint32_t exit_code = tty_process_input(state);

    return exit_code;
}

errno_t tty_read(chardev_t *tty_dev, uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_read)
{
    tty_state_t *state = tty_dev->dev.aux;

    uint32_t fg_gid = state->fg_gid;
    if (state->fg_gid && current_process()->gid != fg_gid) {
        send_signal(current_process(), SIGTTIN);
        *bytes_read = 0;
        return EINTR;
    }

    bool blocking = (flags & IO_BLOCKING) != 0;
    errno_t errno = tty_buf_read(&state->tty_buf, buf, bytes, blocking, bytes_read);
    return errno;
}

errno_t tty_write(chardev_t *tty_dev, const uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_written)
{
    tty_state_t *state = tty_dev->dev.aux;
    chardev_t *base_dev = state->base_dev;
    tty_config_t *cfg = &state->config;

    *bytes_written = 0;

    // TODO: enforce RAW flag when writing via base_dev?

    while (bytes > 0) {
        if (cfg->out_stop) {
            uint32_t fg_gid = state->fg_gid;
            if (state->fg_gid && current_process()->gid != fg_gid) {
                send_signal(current_process(), SIGTTOU);
                return EINTR;
            }
        }

        if (cfg->out_convert_lf_to_crlf && *buf == '\n') {
            /* Insert \r. */
            const uint8_t cr_buf[1] = { '\r' };
            uint64_t count;
            errno_t errno = chardev_write(base_dev, cr_buf, 1, flags, &count);

            /* We don't add the count to bytes_written, since bytes_written
             * counts how many bytes we've written _from the buffer_, instead
             * of counting how many bytes we've written _to some destination_. */

            /* Convert EAGAIN to success, if we've already written some data. */
            if (errno == EAGAIN && *bytes_written > 0) {
                // TODO: what if we try to convert \n to \r\n, but get an EAGAIN
                // while writing \r...
                return 0;
            }

            if (errno || !count) {
                return errno;
            }
        }

        uint64_t count;
        errno_t errno = chardev_write(base_dev, buf, 1, flags, &count);

        *bytes_written += count;

        /* Convert EAGAIN to success, if we've already written some data. */
        if (errno == EAGAIN && *bytes_written > 0) {
            return 0;
        }

        if (errno || !count) {
            return errno;
        }
        buf += count;
        bytes -= count;
    }

    return 0;
}

errno_t tty_ioctl(device_t *tty_dev, uint64_t ioctl_nr, uint8_t *buf, uint64_t buf_len)
{
    tty_state_t *state = tty_dev->aux;

    if (ioctl_nr == TTY_IOCTL_GET_CONFIG) {
        if (buf_len != sizeof(tty_config_t)) {
            return EINVAL;
        }
        acquire_lock(&state->tty_lock);
        *((tty_config_t *) buf) = state->config;
        release_lock(&state->tty_lock);
        return 0;
    } else if (ioctl_nr == TTY_IOCTL_SET_CONFIG) {
        if (buf_len != sizeof(tty_config_t)) {
            return EINVAL;
        }
        acquire_lock(&state->tty_lock);
        tty_config_t old_config = state->config;
        tty_config_t new_config = *((tty_config_t *) buf);
        state->config = new_config;
        *((tty_config_t *) buf) = old_config;
        release_lock(&state->tty_lock);
        return 0;
    } else if (ioctl_nr == TTY_IOCTL_SET_FG) {
        if (buf_len != sizeof(uint32_t)) {
            return EINVAL;
        }
        uint32_t gid = *((uint32_t *) buf);
        state->fg_gid = gid;
        return 0;
    } else {
        return ENOIOCTL;
    }
}

errno_t tty_close(device_t *tty_dev)
{
    tty_state_t *state = tty_dev->aux;
    device_t *base_dev = &state->base_dev->dev;
    return base_dev->close(base_dev);
}

errno_t tty_dup(device_t *tty_dev)
{
    tty_state_t *state = tty_dev->aux;
    device_t *base_dev = &state->base_dev->dev;
    return base_dev->dup(base_dev);
}

void wrap_tty(chardev_t *tty_dev, const char *name, chardev_t *base_dev, tty_config_t *tty_config)
{
    tty_state_t *state = pmalloc(sizeof(tty_state_t));
    panic_if_not(state, "out of memory\n");

    const uint64_t BUF_SIZE = 4096;
    uint8_t *buf = pmalloc(BUF_SIZE);
    panic_if_not(buf, "out of memory\n");

    state->base_dev = base_dev;
    create_lock(&state->tty_lock);
    state->config = *tty_config;
    state->line_len = 0;
    state->tty_buf.size = BUF_SIZE;
    state->tty_buf.buf = buf;
    state->tty_buf.fill = 0;
    create_lock(&state->tty_buf.buf_lock);
    create_sema(&state->tty_buf.read_avail, 0);
    state->fg_gid = 0;

    *tty_dev = (chardev_t) {
        .dev.id = 0,
        .dev.type = DEVICE_TTY,
        .dev.name = name,
        .dev.ioctl = &tty_ioctl,
        .dev.dup = &tty_dup,
        .dev.close = &tty_close,
        .dev.aux = state,
        .read = &tty_read,
        .write = &tty_write
    };
}
