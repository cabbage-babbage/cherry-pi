#include <device/chardev.h>
#include <string.h>
#include <dbg.h>

chardev_t full_device;

errno_t full_ioctl(device_t *dev, uint64_t ioctl_nr,
        uint8_t *buf, uint64_t buf_len)
{
    return ENOIOCTL;
}

errno_t full_dup(device_t *dev)
{
    /* Nothing required; this device is always open. */
    return 0;
}

errno_t full_close(device_t *dev)
{
    /* Nothing required; this device is always open. */
    return 0;
}

errno_t full_read(chardev_t *dev, uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_read)
{
    memset(buf, buf_size, 0);
    *bytes_read = buf_size;
    return 0;
}

errno_t full_write(chardev_t *dev, const uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_written)
{
    *bytes_written = 0;
    return EFULL;
}

void initialize_full_device()
{
    full_device = (chardev_t) {
        .dev.id = 0,
        .dev.type = DEVICE_CHARDEV,
        .dev.name = "full",
        .dev.ioctl = &full_ioctl,
        .dev.dup = &full_dup,
        .dev.close = &full_close,
        .dev.aux = NULL,
        .read = &full_read,
        .write = &full_write
    };

    errno_t errno = device_register(&full_device.dev);
    panic_if(errno, "could not register full device: %e\n", errno);
}
