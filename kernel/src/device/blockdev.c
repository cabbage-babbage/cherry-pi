#include <device/blockdev.h>
#include <dbg.h>

INLINE errno_t blockdev_read(blockdev_t *dev, uint64_t offset,
        uint8_t *buf, uint64_t bytes, uint32_t flags, uint64_t *bytes_read)
{
    errno_t errno = dev->read(dev, offset, buf, bytes, flags, bytes_read);
    panic_if(*bytes_read > bytes,
            "blockdev "device_id_spec" (%qs) read more bytes than requested\n",
            dev->dev.id, dev->dev.name);
    return errno;
}

INLINE errno_t blockdev_write(blockdev_t *dev, uint64_t offset,
        const uint8_t *buf, uint64_t bytes, uint32_t flags, uint64_t *bytes_written)
{
    errno_t errno = dev->write(dev, offset, buf, bytes, flags, bytes_written);
    panic_if(*bytes_written > bytes,
            "blockdev "device_id_spec" (%qs) wrote more bytes than requested\n",
            dev->dev.id, dev->dev.name);
    return errno;
}

INLINE uint64_t blockdev_size(blockdev_t *dev)
{
    if (!dev->size) {
        return 0;
    }

    uint64_t size;
    errno_t errno = dev->size(dev, &size);
    if (errno) {
        printf("blockdev_size(): could not get size of blockdev "device_id_spec" (%qs): %e\n",
                dev->dev.id, dev->dev.name, errno);
        return 0;
    } else {
        return size;
    }
}
