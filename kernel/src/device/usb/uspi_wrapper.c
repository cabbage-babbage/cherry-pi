#include <std.h>
#include <dbg.h>
#include <heap.h>
#include <stdops.h>
#include <virtual_memory.h>
#include <vc_mailbox.h>
#include <string.h>
#include <irq.h>
#include <timer.h>
#include <device/usb.h>
#include <device/usb/uspi/uspios.h>

// malloc/free must work from IRQ context.
// malloc result must be 4-byte aligned.
void *malloc (unsigned nSize)
{
    //printf("uspi malloc(size = 0x%u32)", nSize);
    void *mem = pmalloc(nSize);
    //printf(" => 0x%p\n", mem);
    return mem;
}
void free (void *pBlock)
{
    //printf("uspi free(block = 0x%p)\n", pBlock);
    pfree(pBlock);
}

void delay_loop(uint64_t microseconds)
{
    // busy loop :(
    // TODO: do this with timer interrupts and wfi (with interrupts masked!)
    uint64_t timer_ticks = microseconds * timer_frequency() / (uint64_t) 1000000;
    uint64_t timer_start = timer_counter();
    while (timer_counter() < timer_start + timer_ticks) {
        asm volatile ("nop");
    }
}

void MsDelay (unsigned nMilliSeconds)
{
    //printf("uspi MsDelay(millis = %u32)\n", nMilliSeconds);
    delay_loop((uint64_t) nMilliSeconds * 1000);
}
void usDelay (unsigned nMicroSeconds)
{
    //printf("uspi usDelay(micros = %u32)\n", nMicroSeconds);
    delay_loop((uint64_t) nMicroSeconds);
}

// Returns a timer handle.
unsigned StartKernelTimer (unsigned	        nHzDelay,	// in HZ units (see "system configuration" above)
			   TKernelTimerHandler *pHandler,
			   void *pParam, void *pContext)	// handed over to the timer handler
{
    panic("uspi StartKernelTimer(delay-ticks = %u32, handler = 0x%p, param = 0x%p, context = 0x%p)\n",
            nHzDelay, pHandler, pParam, pContext);
}

void CancelKernelTimer (unsigned hTimer)
{
    panic("uspi CancelKernelTimer(timer index = %u32)\n",
            hTimer);
}

void ConnectInterrupt (unsigned nIRQ, TInterruptHandler *pHandler, void *pParam)
{
    panic_if_not(nIRQ == 9, "expected IRQ 9\n");

    usb_set_irq_handler((usb_irq_handler_t) pHandler, pParam);

    //printf("now usb_irq_handler = 0x%p\n", usb_irq_handler);

    *IRQ_DISABLE_1 &= ~((uint32_t) 1 << 9);
    *IRQ_ENABLE_1 |= (uint32_t) 1 << 9;

    asm volatile("dsb sy");
}

// 0 on failure.
int SetPowerStateOn (unsigned nDeviceId)	// "set power state" to "on", wait until completed
{
    const int SUCCESS = 1, FAILURE = 0;

    //printf("uspi SetPowerStateOn(device id = %u32)\n", nDeviceId);

    uint32_t buf[8] __attribute__((aligned(16)));
    uint32_t *tag_buf = buf + 5;
    tag_buf[0] = nDeviceId;
    tag_buf[1] = 0x3; /* set on (0b01), and wait (0b10) */

    uint32_t response_length;
    bool success = mailbox_call_property_tag(buf, 8,
            TAG_ID_SET_POWER_STATE, NULL, 2, 2,
            &response_length);

    if (!success) {
        printf("mailbox call failed\n");
        return FAILURE;
    }
    if (response_length != 8) {
        printf("response length was %u32 instead of 8\n", response_length);
        return FAILURE;
    }

    /* Make sure it's still the right device id. */
    if (tag_buf[0] != nDeviceId) {
        printf("VC responded with wrong device id (%u32)\n", tag_buf[0]);
        return FAILURE;
    }
    /* Make sure that the device is on (0b01) and exists (0b10). */
    if ((tag_buf[1] & 0x3) != 0x1) {
        printf("device was not turned on (code 0x%u32{16})\n", tag_buf[1]);
        return FAILURE;
    }

    return SUCCESS;
}

// 0 on failure.
int GetMACAddress (unsigned char Buffer[6])	// "get board MAC address"
{
    const int SUCCESS = 1, FAILURE = 0;

    //printf("uspi GetMACAddress(buffer = 0x%p)\n", Buffer);

    uint32_t buf[8] __attribute__((aligned(16)));

    uint32_t response_length;
    bool success = mailbox_call_property_tag(buf, 8,
            TAG_ID_GET_BOARD_MAC_ADDRESS, NULL, 2, 0,
            &response_length);

    if (!success) {
        return FAILURE;
    }
    if (response_length != 6) {
        return FAILURE;
    }

    memcpy(Buffer, &buf[5], 6);
    //printf("uspi GetMACAddress() => %u16{16}:%u16{16}:%u16{16}:%u16{16}:%u16{16}:%u16{16}\n",
    //        Buffer[0], Buffer[1], Buffer[2],
    //        Buffer[3], Buffer[4], Buffer[5]);

    return SUCCESS;
}

void LogWrite (const char *pSource,		// short name of module
	       unsigned	   Severity,		// see above
	       const char *pMessage, ...)	// uses printf format options
{
    printf("[uspi %s] %s: ", pSource,
            Severity == LOG_ERROR ? "ERROR" :
            Severity == LOG_WARNING ? "warning" :
            Severity == LOG_NOTICE ? "notice" :
            Severity == LOG_DEBUG ? "debug" :
            "(unknown severity)");

    // TODO: actually vprintf. problem is that we don't use standard printf() formatting specifiers...
    printf("%s\n", pMessage);
    //va_list args;
    //va_start(args, pMessage);
    //vprintf(fmt, args);
    //va_end(args);
}

void uspi_assertion_failed (const char *pExpr, const char *pFile, unsigned nLine)
{
    panic("uspi assertion failed:\n"
          "  %s\n"
          "  (at %s:%u32)\n",
          pExpr, pFile, nLine);
}

// display hex dump (pSource can be 0)
void DebugHexdump (const void *pBuffer, unsigned nBufLen, const char *pSource /* = 0 */)
{
    panic("uspi DebugHexDump(...)\n");
}
