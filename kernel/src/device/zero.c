#include <device/chardev.h>
#include <string.h>
#include <dbg.h>

chardev_t zero_device;

errno_t zero_ioctl(device_t *dev, uint64_t ioctl_nr,
        uint8_t *buf, uint64_t buf_len)
{
    return ENOIOCTL;
}

errno_t zero_dup(device_t *dev)
{
    /* Nothing required; this device is always open. */
    return 0;
}

errno_t zero_close(device_t *dev)
{
    /* Nothing required; this device is always open. */
    return 0;
}

errno_t zero_read(chardev_t *dev, uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_read)
{
    memset(buf, buf_size, 0);
    *bytes_read = buf_size;
    return 0;
}

errno_t zero_write(chardev_t *dev, const uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_written)
{
    *bytes_written = buf_size;
    return 0;
}

void initialize_zero_device()
{
    zero_device = (chardev_t) {
        .dev.id = 0,
        .dev.type = DEVICE_CHARDEV,
        .dev.name = "zero",
        .dev.ioctl = &zero_ioctl,
        .dev.dup = &zero_dup,
        .dev.close = &zero_close,
        .dev.aux = NULL,
        .read = &zero_read,
        .write = &zero_write
    };

    errno_t errno = device_register(&zero_device.dev);
    panic_if(errno, "could not register zero device: %e\n", errno);
}
