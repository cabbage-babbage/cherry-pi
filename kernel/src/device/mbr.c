#include <device/mbr.h>
#include <heap.h>
#include <dbg.h>

#define SECTOR_SIZE 512
#define MAX_PARTITIONS 4

/* This file constructs 'partition' devices that wrap a block device
 * which uses a Master Boot Record. */

/* Parse a (16-byte) partition entry from the given buffer, and write
 * the resulting partition information to 'partition'. */
void parse_partition(mbr_partition_t *partition, uint8_t *buf)
{
    partition->boot_flags = buf[0];
    partition->type = buf[4];
    partition->start_sector = *((uint32_t *) &buf[8]);
    partition->num_sectors = *((uint32_t *) &buf[12]);

    partition->valid = partition->type != 0;
    partition->active = (partition->boot_flags & 0x80) != 0;

    if (0x01 <= partition->boot_flags && partition->boot_flags <= 0x7F) {
        /* Invalid boot flags. */
        partition->valid = false;
    }
}

errno_t parse_mbr(blockdev_t *dev, mbr_partition_t *partitions)
{
    errno_t errno;
    uint8_t buf[SECTOR_SIZE];

    uint64_t bytes_read;
    errno = blockdev_read(dev, 0, buf, SECTOR_SIZE, IO_BLOCKING, &bytes_read);
    if (errno) {
        return errno;
    }
    if (bytes_read != SECTOR_SIZE) {
        printf("wrong number of bytes read\n");
        return EIO;
    }

    /* Look for the boot signature. */
    if (!(buf[510] == 0x55 && buf[511] == 0xAA)) {
        return ENOENT;
    }

    parse_partition(&partitions[0], buf + 446);
    parse_partition(&partitions[1], buf + 462);
    parse_partition(&partitions[2], buf + 478);
    parse_partition(&partitions[3], buf + 494);

    return 0;
}

typedef struct {
    /* The underlying device. */
    blockdev_t *dev;
    /* Partition information. */
    mbr_partition_t partition;
} partition_device_aux_t;

#define IOCTL_GET_PARTITION_BOOT_FLAGS  ((uint64_t) 1)
#define IOCTL_GET_PARTITION_TYPE        ((uint64_t) 2)
#define IOCTL_GET_PARTITION_RANGE       ((uint64_t) 3)

errno_t partition_device_ioctl(device_t *dev, uint64_t ioctl_nr,
        uint8_t *buf, uint64_t buf_len)
{
    partition_device_aux_t *aux = (partition_device_aux_t *) dev->aux;

    if (ioctl_nr == IOCTL_GET_PARTITION_BOOT_FLAGS) {
        if (buf_len != 1) {
            return EINVAL;
        }
        *buf = aux->partition.boot_flags;
        return 0;
    } else if (ioctl_nr == IOCTL_GET_PARTITION_TYPE) {
        if (buf_len != 1) {
            return EINVAL;
        }
        *buf = aux->partition.type;
        return 0;
    } else if (ioctl_nr == IOCTL_GET_PARTITION_RANGE) {
        if (buf_len != 2 * sizeof(uint32_t)) {
            return EINVAL;
        }
        uint32_t *buf32 = (uint32_t *) buf;
        buf32[0] = aux->partition.start_sector;
        buf32[1] = aux->partition.num_sectors;
        return 0;
    } else {
        return ENOIOCTL;
    }
}

errno_t partition_device_dup(device_t *dev)
{
    // TODO: pass along to underlying device?
    // otherwise, nothing to do
    return 0;
}

errno_t partition_device_close(device_t *dev)
{
    // TODO: pass along to underlying device?
    // otherwise, nothing to do
    return 0;
}

errno_t partition_device_read(blockdev_t *dev, uint64_t offset,
        uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_read)
{
    partition_device_aux_t *aux = (partition_device_aux_t *) dev->dev.aux;

    /* If offset + buf_size wraps around, it's definitely too far to the right. */
    if (offset + buf_size < offset) {
        return ERANGE;
    }
    /* Check the access is within the partition. */
    if (offset + buf_size > (uint64_t) aux->partition.num_sectors * SECTOR_SIZE) {
        return ERANGE;
    }

    uint64_t partition_offset = (uint64_t) aux->partition.start_sector * SECTOR_SIZE;
    return blockdev_read(aux->dev,
            partition_offset + offset, buf, buf_size,
            flags, bytes_read);
}

errno_t partition_device_write(blockdev_t *dev, uint64_t offset,
        const uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_written)
{
    partition_device_aux_t *aux = (partition_device_aux_t *) dev->dev.aux;

    /* If offset + buf_size wraps around, it's definitely too far to the right. */
    if (offset + buf_size < offset) {
        return ERANGE;
    }
    /* Check the access is within the partition. */
    if (offset + buf_size > (uint64_t) aux->partition.num_sectors * SECTOR_SIZE) {
        return ERANGE;
    }

    uint64_t partition_offset = (uint64_t) aux->partition.start_sector * SECTOR_SIZE;
    return blockdev_write(aux->dev,
            partition_offset + offset, buf, buf_size,
            flags, bytes_written);
}

errno_t partition_device_size(blockdev_t *dev, uint64_t *size)
{
    partition_device_aux_t *aux = (partition_device_aux_t *) dev->dev.aux;
    *size = (uint64_t) aux->partition.num_sectors * SECTOR_SIZE;
    return 0;
}

errno_t make_mbr_partition_device(blockdev_t *dev, mbr_partition_t *partition,
        blockdev_t **partition_device)
{
    partition_device_aux_t *aux = pmalloc(sizeof(partition_device_aux_t));
    if (!aux) {
        return ENOMEM;
    }
    aux->dev = dev;
    aux->partition = *partition;

    blockdev_t *pdev = pmalloc(sizeof(blockdev_t));
    if (!pdev) {
        pfree(aux);
        return ENOMEM;
    }
    *pdev = (blockdev_t) {
        .dev.id = 0,
        .dev.type = DEVICE_BLOCKDEV,
        .dev.name = "YOU FORGOT TO NAME A MBR PARTITION DEVICE YA DINGUS", // TODO: change? :P
        .dev.ioctl = &partition_device_ioctl,
        .dev.dup = &partition_device_dup,
        .dev.close = &partition_device_close,
        .dev.aux = aux,
        .read = &partition_device_read,
        .write = &partition_device_write,
        .size = &partition_device_size
    };

    *partition_device = pdev;
    return 0;
}

errno_t make_mbr_partition_devices(blockdev_t *dev, mbr_partition_t *partitions,
        blockdev_t **partition_devices)
{
    errno_t errno = parse_mbr(dev, partitions);
    if (errno) {
        printf("could not parse MBR: %e\n", errno);
        return errno;
    }

    for (uint8_t i = 0; i < MAX_PARTITIONS; i++) {
        if (partitions[i].valid) {
            blockdev_t *partition_device;
            errno = make_mbr_partition_device(dev, &partitions[i],
                    &partition_device);
            if (errno) {
                /* Free the devices we already created. */
                for (uint8_t j = 0; j < i; j++) {
                    if (partition_devices[j]) {
                        pfree(partition_devices[j]->dev.aux);
                        pfree(partition_devices[j]);
                    }
                }
                return errno;
            }
            partition_devices[i] = partition_device;
        } else {
            partition_devices[i] = NULL;
        }
    }

    return 0;
}
