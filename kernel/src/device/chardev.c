#include <device/chardev.h>
#include <dbg.h>

INLINE errno_t chardev_read(chardev_t *dev, uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_read)
{
    errno_t errno = dev->read(dev, buf, bytes, flags, bytes_read);
    panic_if(*bytes_read > bytes,
            "chardev "device_id_spec" (%s) read more bytes than requested\n",
            dev->dev.id, dev->dev.name);
    return errno;
}

INLINE errno_t chardev_write(chardev_t *dev, const uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_written)
{
    errno_t errno = dev->write(dev, buf, bytes, flags, bytes_written);
    panic_if(*bytes_written > bytes,
            "chardev "device_id_spec" (%s) wrote more bytes than requested\n",
            dev->dev.id, dev->dev.name);
    return errno;
}
