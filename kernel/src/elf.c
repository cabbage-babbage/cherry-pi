#include <elf.h>
#include <stdops.h>
#include <console.h>
#include <dbg.h>

/* Checks if the first interval contains the second. */
USE_VALUE INLINE bool interval_contained(
        uint64_t start1, uint64_t length1,
        uint64_t start2, uint64_t length2)
{
    //printf("ic(0x%p, 0x%u64{16} contains 0x%p, 0x%u64{16})\n",
    //        start1, length1, start2, length2);

    // TODO: does this work even if intervals overlap the end-of-64-bit
    // boundary? Probably, but check...
    return start2 >= start1 &&
        start2 - start1 + length2 >= start2 - start1 && /* check overflow */
        start2 - start1 + length2 <= length1;
}

elf_header_t * check_elf(ptr_t file_start, uint64_t file_length)
{
    uint8_t *file_bytes = file_start;

/* Note: each of these evaluates `pointer` twice. */
#define array_in_file(pointer, num) \
    interval_contained( \
            (uint64_t) file_start, file_length, \
            (uint64_t) pointer, (uint64_t) (num) * sizeof(*(pointer)))
#define in_file(pointer) array_in_file(pointer, 1)

    elf_header_t *e_header = file_start;
    if (!in_file(e_header)) {
        dbg_abort_with(NULL, "ELF header cannot fit in file\n");
    }

    /* Check the ELF header. */
    if (!(e_header->ei_magic[0] == 0x7f &&
          e_header->ei_magic[1] == 'E' &&
          e_header->ei_magic[2] == 'L' &&
          e_header->ei_magic[3] == 'F')) {
        dbg_abort_with(NULL, "wrong ELF magic number\n");
    }
    if (e_header->ei_class != 2) {
        dbg_abort_with(NULL, "only 64-bit ELF format allowed\n");
    }
    if (e_header->ei_data != 1) {
        dbg_abort_with(NULL, "only little-endian ELF format allowed\n");
    }
    if (e_header->ei_os_abi != 0) {
        dbg_abort_with(NULL, "expected e_ident[EI_OSABI] == 0\n");
    }
    if (!(e_header->ei_pad[0] == 0 && e_header->ei_pad[1] == 0 &&
          e_header->ei_pad[2] == 0 && e_header->ei_pad[3] == 0 &&
          e_header->ei_pad[4] == 0 && e_header->ei_pad[5] == 0 &&
          e_header->ei_pad[6] == 0 && e_header->ei_pad[7] == 0)) {
        dbg_abort_with(NULL, "e_ident[EI_PAD] should be filled with zeros\n");
    }
    if (!(e_header->e_type == ET_NONE || e_header->e_type == ET_REL ||
          e_header->e_type == ET_EXEC || e_header->e_type == ET_DYN ||
          e_header->e_type == ET_CORE ||
          (ET_LOOS <= e_header->e_type && e_header->e_type <= ET_HIOS) ||
          (ET_LOPROC <= e_header->e_type && e_header->e_type <= ET_HIPROC))) {
        dbg_abort_with(NULL, "invalid e_type\n");
    }
    /* No check on e_machine. */
    if (e_header->e_version != 1) {
        dbg_abort_with(NULL, "only ELF version 1 allowed\n");
    }
    /* No check on e_entry. */
    if (e_header->e_machine == EM_AARCH64) {
        /* No processor-specific flags, so e_flags must be zero. */
        if (e_header->e_flags != 0) {
            dbg_abort_with(NULL, "e_flags must be 0 for AArch64 target\n");
        }
    }
    if (e_header->e_ehsize != sizeof(elf_header_t)) {
        dbg_abort_with(NULL, "unexpected e_ehsize\n");
    }
    if (e_header->e_phentsize != sizeof(elf_ph_entry_t)) {
        dbg_abort_with(NULL, "unexpected e_phentsize\n");
    }
    if (e_header->e_shentsize != sizeof(elf_sh_entry_t)) {
        dbg_abort_with(NULL, "unexpected e_shentsize\n");
    }
    if (e_header->e_shstrndx >= e_header->e_shnum) {
        dbg_abort_with(NULL,
                "e_shstrndx is too large to refer to a section header table entry\n");
    }

    /* Start and length of the program header table array. */
    elf_ph_entry_t *ph = (elf_ph_entry_t *) &file_bytes[e_header->e_phoff];
    uint16_t phnum = e_header->e_phnum;
    //printf("ph_off = 0x%p, ph_num = %u64, ph_entsize = 0x%u64{16}\n",
    //        ph, ph_num, (uint64_t) sizeof(elf_ph_entry_t));
    if (!array_in_file(ph, phnum)) {
        dbg_abort_with(NULL, "program header table cannot fit in file\n");
    }

    /* Start and length of the section header table. */
    elf_sh_entry_t *sh = (elf_sh_entry_t *) &file_bytes[e_header->e_shoff];
    uint16_t shnum = e_header->e_shnum;
    if (!array_in_file(sh, shnum)) {
        dbg_abort_with(NULL, "section header table cannot fit in file\n");
    }

    /* Now let's check each program header entry. */
    for (uint16_t i = 0; i < phnum; i++) {
        elf_ph_entry_t *entry = &ph[i];

        if (!(entry->p_type == PT_NULL || entry->p_type==PT_LOAD ||
              entry->p_type == PT_DYNAMIC || entry->p_type==PT_INTERP ||
              entry->p_type == PT_NOTE || entry->p_type==PT_SHLIB ||
              entry->p_type == PT_PHDR || entry->p_type==PT_TLS ||
              (PT_LOOS <= entry->p_type && entry->p_type <= PT_HIOS) ||
              (PT_LOPROC <= entry->p_type && entry->p_type <= PT_HIPROC))) {
            dbg_abort_with(NULL, "program header entry %u16: invalid p_type\n", i);
        }

        if (entry->p_type == PT_NULL) {
            /* All the other fields of a PT_NULL entry are undefined, and we
             * should not check them. */
            continue;
        }

        /* Check that only (at most) the known flags are set. */
        if ((entry->p_flags & ~(PF_X | PF_W | PF_R | PF_MASKPROC)) != 0) {
            dbg_abort_with(NULL, "program header entry %u16: p_flags has unknown flags set\n", i);
        }

        uint8_t *segment = &file_bytes[entry->p_offset];
        if (!array_in_file(segment, entry->p_filesz)) {
            dbg_abort_with(NULL, "program header entry %u16: segment cannot fit in file\n", i);
        }

        if (entry->p_memsz < entry->p_filesz) {
            dbg_abort_with(NULL, "program header entry %u16: p_memsz must be at least p_filesz\n", i);
        }

        if (entry->p_align != 0) {
            /* Check it's a power of two. */
            uint32_t bits = alignment_bits(entry->p_align);
            if (entry->p_align != (uint64_t) 1 << bits) {
                dbg_abort_with(NULL, "program header entry %u16: p_align is not 0 or a power of two\n", i);
            }
            /* Check p_vaddr and p_paddr alignment. */
            if ((entry->p_vaddr - entry->p_paddr) % entry->p_align != 0) {
                dbg_abort_with(NULL, "program header entry %u16: p_vaddr and p_paddr are not co-aligned\n", i);
            }
        }
    }

    elf_sh_entry_t *shstrtab = &sh[e_header->e_shstrndx];

    /* Finally, let's check each section header entry. */
    for (uint16_t i = 0; i < shnum; i++) {
        elf_sh_entry_t *entry = &sh[i];

        if (entry->sh_name >= shstrtab->sh_size) {
            dbg_abort_with(NULL, "section header entry %u16: sh_name is too large to be the offset "
                    "of a string in the .shstrtab section\n", i);
        }
        if (!(entry->sh_type == SHT_NULL || entry->sh_type == SHT_PROGBITS ||
              entry->sh_type == SHT_SYMTAB || entry->sh_type == SHT_STRTAB ||
              entry->sh_type == SHT_RELA || entry->sh_type == SHT_HASH ||
              entry->sh_type == SHT_DYNAMIC || entry->sh_type == SHT_NOTE ||
              entry->sh_type == SHT_NOBITS || entry->sh_type == SHT_REL ||
              entry->sh_type == SHT_SHLIB || entry->sh_type == SHT_DYNSYM ||
              entry->sh_type == SHT_INIT_ARRAY || entry->sh_type == SHT_FINI_ARRAY ||
              entry->sh_type == SHT_PREINIT_ARRAY || entry->sh_type == SHT_GROUP ||
              entry->sh_type == SHT_SYMTAB_SHNDX || entry->sh_type == SHT_NUM ||
              entry->sh_type >= SHT_LOOS)) {
            dbg_abort_with(NULL, "section header entry %u16: invalid sh_type\n", i);
        }

        // TODO: check sh_flags, at least that there unknown flags aren't set; see Figure 1-11 in ELF spec

        uint8_t *section = &file_bytes[entry->sh_offset];
        if (!array_in_file(section, entry->sh_size)) {
            dbg_abort_with(NULL, "section header entry %u16: section does not fit in file\n", i);
        }

        // TODO: check sh_link and sh_info (Figure 1-12 in ELF spec)

        /* Check sh_addralign is a power of two. */
        if (entry->sh_addralign != 0) {
            uint32_t bits = alignment_bits(entry->sh_addralign);
            if (entry->sh_addralign != (uint64_t) 1 << bits) {
                dbg_abort_with(NULL, "section header entry %u16: sh_addralign is not 0 or a power of two\n", i);
            }
            /* Check sh_addr alignment. */
            if (entry->sh_addr % entry->sh_addralign != 0) {
                dbg_abort_with(NULL, "section header entry %u16: sh_addr is not aligned\n", i);
            }
        }

        // TODO: check sh_entsize? (ELF spec doesn't say much about it...)

        // TODO: check entry-0-specific things (see Figure 1-10, ELF spec)
    }

    // TODO:
    // ELF spec just says that "an object file segment comprises one or more sections";
    // maybe we should check that each section is contained in a segment and that the
    // segments do not overlap. The Wikipedia entry says that orphan bytes are allowed
    // (bytes not owned by any section), but the ELF spec makes no mention of this.

    /* Verify that loadable (PT_LOAD) segments appear in the program header table
     * already sorted by their p_addr. Th ELF spec does not mention if segments
     * can overlap or not, but we should require no overlap as a sanity check. */
    bool first_loadable_segment = true;
    uint64_t last_p_vaddr, last_p_memsz;
    for (uint16_t i = 0; i < phnum; i++) {
        elf_ph_entry_t *entry = &ph[i];
        if (entry->p_type != PT_LOAD) {
            continue;
        }

        if (!first_loadable_segment) {
            /* Compare against the previous one. */
            if (last_p_vaddr > entry->p_vaddr) {
                dbg_abort_with(NULL, "loadable segments must be sorted by p_vaddr\n");
            }
            if (entry->p_vaddr - last_p_vaddr < last_p_memsz) {
                dbg_abort_with(NULL, "loadable segments must not overlap (in virtual addresses)\n");
            }
        }

        first_loadable_segment = false;
        last_p_vaddr = entry->p_vaddr;
        last_p_memsz = entry->p_memsz;
    }

    /* Verify that there is at most one PT_PHDR segment in the file, and if present,
     * that it preceeds any loadable (PT_LOAD) segment (in the program header table). */
    bool saw_loadable_segment = false;
    bool saw_phdr_segment = false;
    for (uint16_t i = 0; i < phnum; i++) {
        elf_ph_entry_t *entry = &ph[i];
        if (entry->p_type == PT_LOAD) {
            saw_loadable_segment = true;
        } else if (entry->p_type == PT_PHDR) {
            if (saw_phdr_segment) {
                dbg_abort_with(NULL, "a PT_PHDR segment must occur at most once\n");
            }
            if (saw_loadable_segment) {
                dbg_abort_with(NULL, "PT_PHDR segment must preceed any loadable (PT_LOAD) segments "
                        "in the program header table\n");
            }
            saw_phdr_segment = true;
        }
    }

    return e_header;
}

void dump_elf_headers(ptr_t file_start, const char *prefix)
{
    uint8_t *file_bytes = file_start;

    elf_header_t *e_header = file_start;

    printf("%se_type: %u16 (%s)\n",
            prefix,
            e_header->e_type,
            e_header->e_type == ET_NONE ? "ET_NONE" :
            e_header->e_type == ET_REL ? "ET_REL" :
            e_header->e_type == ET_EXEC ? "ET_EXEC" :
            e_header->e_type == ET_DYN ? "ET_DYN" :
            e_header->e_type == ET_CORE ? "ET_CORE" :
            (ET_LOOS <= e_header->e_type && e_header->e_type <= ET_HIOS) ? "OS specific" :
            (ET_LOPROC <= e_header->e_type && e_header->e_type <= ET_HIPROC) ? "processor specific" :
                "unknown!");
    printf("%se_machine: %u16 (%s)\n",
            prefix,
            e_header->e_machine,
            e_header->e_machine == EM_AARCH64 ? "EM_AARCH64" :
                "unknown!");
    printf("%se_version: %u32\n",
            prefix, e_header->e_version);
    printf("%se_entry: 0x%p\n",
            prefix, e_header->e_entry);
    printf("%se_flags: 0b%u32{2}\n",
            prefix, e_header->e_flags);
    printf("%se_phnum: %u16\n",
            prefix, e_header->e_phnum);
    printf("%se_shnum: %u16\n",
            prefix, e_header->e_shnum);
    printf("%se_shstrndx: %u16\n",
            prefix, e_header->e_shstrndx);

    /* Start and length of the program header table array. */
    elf_ph_entry_t *ph = (elf_ph_entry_t *) &file_bytes[e_header->e_phoff];
    uint64_t phnum = e_header->e_phnum;

    ///* Start and length of the section header table. */
    //elf_sh_entry_t *sh = (elf_sh_entry_t *) &file_bytes[e_header->e_shoff];
    //uint64_t shnum = e_header->e_shnum;

    for (uint64_t i = 0; i < phnum; i++) {
        elf_ph_entry_t *entry = &ph[i];

        printf("%sprogram header entry %u64:\n",
                prefix, i);
        printf("%s    p_type: %u32 (%s)\n",
                prefix, entry->p_type,
                entry->p_type == PT_NULL ? "PT_NULL" :
                entry->p_type == PT_LOAD ? "PT_LOAD" :
                entry->p_type == PT_DYNAMIC ? "PT_DYNAMIC" :
                entry->p_type == PT_INTERP ? "PT_INTERP" :
                entry->p_type == PT_NOTE ? "PT_NOTE" :
                entry->p_type == PT_SHLIB ? "PT_SHLIB" :
                entry->p_type == PT_PHDR ? "PT_PHDR" :
                entry->p_type == PT_TLS ? "PT_TLS" :
                (PT_LOOS <= entry->p_type && entry->p_type <= PT_HIOS) ? "OS specific" :
                (PT_LOPROC <= entry->p_type && entry->p_type <= PT_HIPROC) ? "processor specific" :
                    "unknown!");
        printf("%s    p_flags: 0b%u64{2} (%c%c%c%s)\n",
                prefix, entry->p_flags,
                (entry->p_flags & PF_R) ? 'R' : 'r',
                (entry->p_flags & PF_W) ? 'W' : 'w',
                (entry->p_flags & PF_X) ? 'X' : 'x',
                (entry->p_flags & PF_MASKPROC) ? " and processor specific flags" : "");
        printf("%s    p_offset: 0x%u64{16}\n",
                prefix, entry->p_offset);
        printf("%s    p_filesz: 0x%u64{16}\n",
                prefix, entry->p_filesz);
        printf("%s    p_memsz: 0x%u64{16}\n",
                prefix, entry->p_memsz);
        printf("%s    p_vaddr: 0x%u64{16}\n",
                prefix, entry->p_vaddr);
        printf("%s    p_paddr: 0x%u64{16}\n",
                prefix, entry->p_paddr);
        printf("%s    p_align: 0x%u64{16}\n",
                prefix, entry->p_align);
    }

    // Don't print section headers for now; not so interested in them.
}
