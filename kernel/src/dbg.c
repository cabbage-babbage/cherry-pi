#include <dbg.h>

#define HEXDUMP_LIKE_XXD false

void zero_pad_uint8(uint8_t val)
{
    put_uint8((val >> 4) & 0xF, 16);
    put_uint8((val >> 0) & 0xF, 16);
}

void zero_pad_uint32(uint32_t val)
{
    put_uint8((val >> 28) & 0xF, 16);
    put_uint8((val >> 24) & 0xF, 16);
    put_uint8((val >> 20) & 0xF, 16);
    put_uint8((val >> 16) & 0xF, 16);
    put_uint8((val >> 12) & 0xF, 16);
    put_uint8((val >> 8) & 0xF, 16);
    put_uint8((val >> 4) & 0xF, 16);
    put_uint8((val >> 0) & 0xF, 16);
}

void hexdump(ptr_t buffer, uint64_t length)
{
    hexdump_ext(buffer, 0, length);
}

void hexdump_ext(ptr_t buffer, uint64_t display_offset, uint64_t length)
{
    uint8_t *buf = buffer;
    uint64_t index = 0;
    while (index < length) {
        zero_pad_uint32((uint32_t) (index + display_offset));
        puts(":");

        for (uint64_t offset = 0; offset < 16; offset++) {
            if (HEXDUMP_LIKE_XXD) {
                if (offset % 2 == 0) {
                    puts(" ");
                }
            } else {
                puts(" ");
                if (offset == 8) {
                    puts(" ");
                }
            }
            if (index + offset < length) {
                zero_pad_uint8(buf[index + offset]);
            } else {
                puts("  ");
            }
        }

        if (HEXDUMP_LIKE_XXD) {
            puts("  ");
        } else {
            puts("  |");
        }

        for (uint64_t offset = 0; offset < 16; offset++) {
            if (index + offset < length) {
                char c = (char) buf[index + offset];
                if (32 <= c && c <= 126) {
                    putc(c);
                } else {
                    putc('.');
                }
            } else {
                break;
            }
        }

        if (HEXDUMP_LIKE_XXD) {
            puts("\n");
        } else {
            puts("|\n");
        }

        index += 16;
    }
}
