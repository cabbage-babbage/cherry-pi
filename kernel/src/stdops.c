#include <stdops.h>
#include <dbg.h>

INLINE uint64_t max_uint64(uint64_t a, uint64_t b)
{
    return a > b ? a : b;
}
INLINE uint64_t min_uint64(uint64_t a, uint64_t b)
{
    return a < b ? a : b;
}

INLINE uint64_t round_up_div(uint64_t numerator, uint64_t denominator)
{
    panic_if(denominator == 0, "denominator must be nonzero\n");
    if (numerator % denominator == 0) {
        return numerator / denominator;
    } else {
        return numerator / denominator + 1;
    }
}

INLINE uint64_t align_down(uint64_t val, uint64_t alignment)
{
    panic_if(alignment == 0, "alignment must be nonzero\n");
    return val - (val % alignment);
}
INLINE uint64_t align_up(uint64_t val, uint64_t alignment)
{
    panic_if(alignment == 0, "alignment must be nonzero\n");
    return round_up_div(val, alignment) * alignment;
}

INLINE uint32_t alignment_bits(uint64_t val)
{
    uint64_t reversed;
    uint32_t trailing_zeros;
    asm volatile("rbit %[reversed], %[val]" : [reversed] "=r" (reversed) : [val] "r" (val));
    asm volatile("clz %[zeros], %[reversed]" : [zeros] "=r" (trailing_zeros) : [reversed] "r" (reversed));
    return trailing_zeros;
}

INLINE bool is_aligned_at_bits(uint64_t val, uint32_t alignment_bits)
{
    const uint64_t alignment_mask = ~(0xFFFFFFFFFFFFFFFF << alignment_bits);
    return (val & alignment_mask) == 0;
}

INLINE uint128_t make_uint128(uint64_t hi, uint64_t lo)
{
    uint128_t large = { .hi = hi, .lo = lo };
    return large;
}

INLINE uint128_t uint128_from_uint64(uint64_t val)
{
    return make_uint128(0, val);
}

INLINE uint64_t uint64_from_uint128(uint128_t val)
{
    panic_if(val.hi != 0, "value does not fit in a uint64\n");
    return val.lo;
}

INLINE bool lte_uint128(uint128_t a, uint128_t b)
{
    if (a.hi < b.hi) {
        return true;
    } else {
        return a.hi == b.hi && a.lo <= b.lo;
    }
}

INLINE uint128_t add_uint128(uint128_t a, uint128_t b)
{
    bool lo_overflow = a.lo + b.lo < a.lo;
    return make_uint128(
            a.hi + b.hi + (lo_overflow ? 1 : 0),
            a.lo + b.lo);
}

INLINE uint128_t sub_uint128(uint128_t a, uint128_t b)
{
    bool lo_underflow = a.lo < b.lo;
    return make_uint128(
            a.hi - b.hi - (lo_underflow ? 1 : 0),
            a.lo - b.lo);
}

INLINE uint128_t max_uint128(uint128_t a, uint128_t b)
{
    if (a.hi < b.hi) {
        return b;
    } else if (a.hi == b.hi) {
        return make_uint128(a.hi, max_uint64(a.lo, b.lo));
    } else {
        return a;
    }
}

INLINE uint128_t min_uint128(uint128_t a, uint128_t b)
{
    if (a.hi < b.hi) {
        return a;
    } else if (a.hi == b.hi) {
        return make_uint128(a.hi, min_uint64(a.lo, b.lo));
    } else {
        return b;
    }
}
