#!/usr/bin/python3

import sys
import os
import time

def wait():
    #os.system("/bin/bash -c \"read -n 1 -s -r\"")
    time.sleep(0.1);

def write_byte(f, b):
    f.write(b.to_bytes(1, byteorder='big'))

# usage: ./send.py file print-offset [device]
if len(sys.argv) not in [3, 4]:
    print("usage: ./send.py file print-offset [device]")
    sys.exit(1)

img_path = sys.argv[1]
print_offset = int(sys.argv[2])
dev_path = sys.argv[3] if len(sys.argv) > 3 else "/dev/ttyUSB0"

img = None
with open(img_path, "rb") as f:
    img = f.read()

# img is 'bytes' object, which we can treat as an array of integers

with open(dev_path, "wb") as f:
    for i, c in enumerate(img):
        off = print_offset + i
        if off >= 0 and off % 16 == 0:
            print("%08x  " % off, end='')
        sys.stdout.flush()

        wait()

        print("%02x" % c, end='')
        if off % 16 == 15:
            print()
        elif off % 16 == 7:
            print("  ", end='')
        else:
            print(" ", end='')
        sys.stdout.flush()

        write_byte(f, c)
        f.flush()
    print()
