.global _start
_start:
    // No need to set up SP; the kernel has already done that for us.
    bl main
    svc #12347 // Exit syscall. (TODO: don't hard-code this)

// Fall through to infinite loop. Definitely shouldn't get here.
loop:
    b loop
