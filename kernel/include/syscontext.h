#ifndef SYSCONTEXT_H
#define SYS_CONTEXT_H

#include <std.h>

typedef enum {
    /* Still booting up; the main process loop has not begun.
     * Interrupts may or may not be disabled, depending on how far
     * we are along the boot setup. Execution is non-preemptible,
     * since no processes are running yet. */
    SYSCONTEXT_BOOT,
    /* Currently running a process (user-mode or syscall).
     * Interrupts are enabled, and execution is preemptible. */
    SYSCONTEXT_PREEMPTIBLE,
    /* Currently executing an IRQ handler.
     * Interrupts are disabled, and execution is (therefore)
     * non-preemptible.*/
    SYSCONTEXT_IRQ
} syscontext_t;

syscontext_t current_syscontext();
void set_syscontext(syscontext_t context);

bool in_context_boot();
bool in_context_preemptible();
bool in_context_irq();

#endif
