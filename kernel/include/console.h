#ifndef CONSOLE_H
#define CONSOLE_H

#include <std.h>

void console_init();

void putc(char c);
/* Replaces \n with \n\r. */
void puts(const char *s);
/*
 * Like regular `puts`, but ensures that the output
 * appears at the start of the next line, regardless
 * of whether the last `putc` or `puts` finished with
 * a newline.
 */
void puts_nl(const char *s);

void put_int8(int8_t val, int32_t base);
void put_uint8(uint8_t val, int32_t base);
void put_int16(int16_t val, int32_t base);
void put_uint16(uint16_t val, int32_t base);
void put_int32(int32_t val, int32_t base);
void put_uint32(uint32_t val, int32_t base);
void put_int64(int64_t val, int32_t base);
void put_uint64(uint64_t val, int32_t base);
void put_ptr(ptr_t val);

void printf(const char *fmt, ...);
void printf_nl(const char *fmt, ...);
/*#define printf_nl(...) \
    do { \
        puts_nl(""); \
        printf(__VA_ARGS__); \
    } while (0)*/

#endif
