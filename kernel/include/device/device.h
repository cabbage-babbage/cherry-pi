#ifndef DEVICE_H
#define DEVICE_H

#include <std.h>
#include <uapi/errno.h>
#include <uapi/device.h>

#define IO_BLOCKING ((uint32_t) 1 << 0)
#define IO_RAW      ((uint32_t) 1 << 1)

typedef enum {
    DEVICE_CHARDEV,
    DEVICE_BLOCKDEV,
    DEVICE_TTY
} device_type_t;

struct device;

typedef errno_t (*device_ioctl_fn_t)(struct device *dev, uint64_t ioctl_nr,
        uint8_t *buf, uint64_t buf_len);

/* Closing really should not ever fail; avoid as much as possible. Failures on closure are
 * logged and then promptly ignored by control flow.
 *
 * Note that duplicating does not produce a new device; closing and duplicating are
 * essentially notifications to the interface to decrement/increment reference counts,
 * and once a device is successfully duplicated, it will simply be referenced by one more
 * entry in a process descriptor table. Closing and duplicating should each be atomic;
 * if duplication fails, there should be no substantive change to the device. */
typedef errno_t (*device_dup_fn_t)(struct device *dev);
typedef errno_t (*device_close_fn_t)(struct device *dev);

typedef struct device {
    device_id_t id;
    device_type_t type;
    const char *name;

    device_ioctl_fn_t ioctl;
    device_dup_fn_t dup;
    device_close_fn_t close;

    /* Internal data for the callbacks. */
    void *aux;
} device_t;

USE_VALUE errno_t device_ioctl(device_t *dev, uint64_t ioctl_nr,
        uint8_t *buf, uint64_t buf_len);

USE_VALUE errno_t device_dup(device_t *dev);

void device_close(device_t *dev);

/* Must be called before registering or looking up any device. */
void initialize_devices();

/* Initialize (and register) standard devices (for example 'null', 'zero', 'full'). */
void initialize_standard_devices();

/* Add all registered devices to the filesystem, in the specified directory. */
void add_devices(const char *dirname);

/* Register a new device. This assigns the device an id (the old value is overwritten,
 * and should be 0). */
USE_VALUE errno_t device_register(device_t *dev);

/* Returns NULL if not found. */
USE_VALUE device_t *find_device_by_id(device_id_t id);

#endif
