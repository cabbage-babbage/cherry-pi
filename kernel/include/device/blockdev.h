#ifndef BLOCKDEV_H
#define BLOCKDEV_H

#include <device/device.h>

struct blockdev;

/* Generic IO read/write functions. Each takes a buffer to read into or to write from,
 * a requested number of bytes to read or write, an offset at which to read or write,
 * and IO_* flags which specify the precise behavior requested. The return value must
 * be 0 on success or an error number on failure; furthermore, the number of bytes
 * successfully read or written into/from the buffer must be returned in (respectively)
 * the bytes_read or bytes_written out parameter, and also must be at most the buf_size.
 * The default flags behavior (flags=0) is to perform a nonblocking and possibly
 * pre-/post-processed (e.g. for ttys) read or write.
 *
 * TODO: update these?
 * Only some error numbers are expected, and thus allowed:
 *   ENOTSUP        if the operation (reading or writing) is not supported by the device.
 *   EIO            if there is an I/O error while performing the read or write
 *   EAGAIN         if the read or write is non-blocking but no data can be read or
 *                  written without blocking. (In this case, the bytes_read or bytes_written
 *                  should be zero.) */
typedef errno_t (*blockdev_read_fn_t)(struct blockdev *dev, uint64_t offset,
        uint8_t *buf, uint64_t buf_size, uint32_t flags, uint64_t *bytes_read);
typedef errno_t (*blockdev_write_fn_t)(struct blockdev *dev, uint64_t offset,
        const uint8_t *buf, uint64_t buf_size, uint32_t flags, uint64_t *bytes_written);

/* Get the size of a block device, or 0 if unknown. (If an error is returned, userspace
 * will be told that the size is 0 anyway; likewise if the function pointer is NULL.) */
typedef errno_t (*blockdev_size_fn_t)(struct blockdev *dev, uint64_t *size);

/* A blockdev_t* can be cast to and from a device_t* to convert to/from the character-specific
 * device type and the base device type; this is because the 'device_t dev' is the first field. */
typedef struct blockdev {
    device_t dev;

    blockdev_read_fn_t read;
    blockdev_write_fn_t write;

    blockdev_size_fn_t size;
} blockdev_t;

USE_VALUE errno_t blockdev_read(blockdev_t *dev, uint64_t offset,
        uint8_t *buf, uint64_t bytes, uint32_t flags, uint64_t *bytes_read);
USE_VALUE errno_t blockdev_write(blockdev_t *dev, uint64_t offset,
        const uint8_t *buf, uint64_t bytes, uint32_t flags, uint64_t *bytes_written);

// TODO maybe size == 0 should not represent "unknown size"...
/* Returns 0 (default size) if the device's size() function does not exist or fails. */
USE_VALUE uint64_t blockdev_size(blockdev_t *dev);

#endif
