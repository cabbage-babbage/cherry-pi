#ifndef CHARDEV_H
#define CHARDEV_H

#include <device/device.h>

struct chardev;

/* Generic IO read/write functions. Each takes a buffer to read into or to write from,
 * a requested number of bytes to read or write, and IO_* flags which specify the
 * precise behavior requested. The return value must be 0 on success or an error number
 * on failure; furthermore, the number of bytes successfully read or written into/from
 * the buffer must be returned in (respectively) the bytes_read or bytes_written out
 * parameter, and also must be at most the buf_size.
 * The default flags behavior (flags=0) is to perform a nonblocking and possibly
 * pre-/post-processed (e.g. for ttys) read or write.
 *
 * Only some error numbers are expected, and thus allowed:
 *   ENOTSUP        if the operation (reading or writing) is not supported by the device.
 *   EIO            if there is an I/O error while performing the read or write
 *   EAGAIN         if the read or write is non-blocking but no data can be read or
 *                  written without blocking. (In this case, the bytes_read or bytes_written
 *                  should be zero.) */
typedef errno_t (*chardev_read_fn_t)(struct chardev *dev, uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_read);
typedef errno_t (*chardev_write_fn_t)(struct chardev *dev, const uint8_t *buf, uint64_t buf_size,
        uint32_t flags, uint64_t *bytes_written);

/* A chardev_t* can be cast to and from a device_t* to convert to/from the character-specific
 * device type and the base device type; this is because the 'device_t dev' is the first field. */
typedef struct chardev {
    device_t dev;

    chardev_read_fn_t read;
    chardev_write_fn_t write;
} chardev_t;

USE_VALUE errno_t chardev_read(chardev_t *dev, uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_read);
USE_VALUE errno_t chardev_write(chardev_t *dev, const uint8_t *buf, uint64_t bytes,
        uint32_t flags, uint64_t *bytes_written);

#endif
