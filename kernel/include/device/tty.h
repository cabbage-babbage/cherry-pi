#ifndef TTY_H
#define TTY_H

#include <device/chardev.h>
#include <uapi/tty.h>

/* Wraps the base device (base_dev) with a TTY driver, using a given initial TTY
 * configuration. Places the wrapper device in tty_dev. Note that the tty_dev is
 * left unregistered (with id == 0). */
void wrap_tty(chardev_t *tty_dev, const char *name, chardev_t *base_dev,
        tty_config_t *tty_config);

#endif
