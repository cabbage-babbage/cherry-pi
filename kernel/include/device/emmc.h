#ifndef DEVICE_EMMC_H
#define DEVICE_EMMC_H

#include <std.h>
#include <uapi/errno.h>

#define EMMC_BLOCK_SIZE 512

/* EMMC errnos:
 *   0          success
 *   ETIMEDOUT  timed out
 *   EIO        other error */

errno_t initialize_emmc();

/* For both of these, the buf must be at least block_count * EMMC_BLOCK_SIZE in size. */
errno_t emmc_read_blocks(uint32_t block_nr, uint32_t block_count, uint8_t *buf);
errno_t emmc_write_blocks(uint32_t block_nr, uint32_t block_count, const uint8_t *buf);

#endif
