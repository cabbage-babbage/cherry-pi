#ifndef UART_H
#define UART_H

#include <mmio.h>
#include <device/chardev.h>

#define GPFSEL0         mmio(0x00200000)
#define GPFSEL1         mmio(0x00200004)
#define GPFSEL2         mmio(0x00200008)
#define GPFSEL3         mmio(0x0020000C)
#define GPFSEL4         mmio(0x00200010)
#define GPFSEL5         mmio(0x00200014)
#define GPSET0          mmio(0x0020001C)
#define GPSET1          mmio(0x00200020)
#define GPCLR0          mmio(0x00200028)
#define GPLEV0          mmio(0x00200034)
#define GPLEV1          mmio(0x00200038)
#define GPEDS0          mmio(0x00200040)
#define GPEDS1          mmio(0x00200044)
#define GPHEN0          mmio(0x00200064)
#define GPHEN1          mmio(0x00200068)
#define GPPUD           mmio(0x00200094)
#define GPPUDCLK0       mmio(0x00200098)
#define GPPUDCLK1       mmio(0x0020009C)

/* Auxilary mini UART registers */
#define AUX_ENABLE      mmio(0x00215004)
#define AUX_MU_IO       mmio(0x00215040)
#define AUX_MU_IER      mmio(0x00215044)
#define AUX_MU_IIR      mmio(0x00215048)
#define AUX_MU_LCR      mmio(0x0021504C)
#define AUX_MU_MCR      mmio(0x00215050)
#define AUX_MU_LSR      mmio(0x00215054)
#define AUX_MU_MSR      mmio(0x00215058)
#define AUX_MU_SCRATCH  mmio(0x0021505C)
#define AUX_MU_CNTL     mmio(0x00215060)
#define AUX_MU_STAT     mmio(0x00215064)
#define AUX_MU_BAUD     mmio(0x00215068)

extern chardev_t uart_device;

/* Initialize the UART driver. This includes setting the baud rate
 * and UART characteristics (115200 8N1), mapping to GPIO, and also
 * setting up the uart_device. */
void uart_init();

/* Both of these busy-wait to actually perform blocking, so should
 * generally be avoided. */
void uart_blocking_send(char c);
USE_VALUE char uart_blocking_getc();

void uart_enable_interrupts();
USE_VALUE bool uart_irq_firing();
void uart_handle_irq(ptr_t saved_regs_base);

USE_VALUE bool uart_tx_full();
void uart_tx_enqueue(char c);
USE_VALUE bool uart_rx_empty();
USE_VALUE char uart_rx_dequeue();

#endif
