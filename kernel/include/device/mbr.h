#ifndef MBR_H
#define MBR_H

#include <std.h>
#include <device/blockdev.h>
#include <uapi/errno.h>

/* This keeps a cache of processed data from a MBR partition entry. */
typedef struct {
    /* Specifies whether or not the entry is valid (and in use). */
    bool valid;

    uint8_t boot_flags;
    bool active; /* Really, bootable. */

    uint8_t type;

    uint32_t start_sector;
    uint32_t num_sectors;
} mbr_partition_t;

/* Parse a Master Boot Record from a block device and write the partition information
 * to the 'partitions' array (which must have 4 entries).
 * On success, returns 0; on device read failure, returns a suitable errno; and if
 * otherwise the first sector was successfully read but does not actually contain a
 * MBR, returns ENOENT. */
errno_t parse_mbr(blockdev_t *dev, mbr_partition_t *partitions);

/* This does the same work as parse_mbr(), but then also creates up to four block
 * devices to place in partition_devices, one per valid partition. (If a partition
 * is invalid, NULL is written instead of a device pointer.) Note that each device
 * is freshly allocated, and is left unregistered and also without a name.
 * On failure, none of the partition_devices should be used; if they were created
 * in the first place, they were destroyed before return. */
errno_t make_mbr_partition_devices(blockdev_t *dev, mbr_partition_t *partitions,
        blockdev_t **partition_devices);

#endif
