#ifndef DEVICE_USB_H
#define DEVICE_USB_H

#include <std.h>

typedef void (*usb_irq_handler_t)(void *aux);

/* Set the callback to be executed during a USB IRQ. The 'aux' parameter
 * is passed directly to the callback. A NULL handler indicates no callback,
 * so USB IRQs will effectively be ignored. */
void usb_set_irq_handler(usb_irq_handler_t handler, void *aux);

bool usb_irq_firing();

void usb_handle_irq();

#endif
