#ifndef IRQ_H
#define IRQ_H

#include <std.h>
#include <mmio.h>

/* IRQ registers base offset from the MMIO_BASE. */
#define IRQ_BASE 0x0000B000

#define IRQ_PEND_BASE       mmio(IRQ_BASE + 0x200)
#define GPU_PENDING_1       mmio(IRQ_BASE + 0x204)
#define GPU_PENDING_2       mmio(IRQ_BASE + 0x208)
#define FIQ                 mmio(IRQ_BASE + 0x20C)
#define IRQ_ENABLE_1        mmio(IRQ_BASE + 0x210)
#define IRQ_ENABLE_2        mmio(IRQ_BASE + 0x214)
#define BASE_IRQ_ENABLE     mmio(IRQ_BASE + 0x218)
#define IRQ_DISABLE_1       mmio(IRQ_BASE + 0x21C)
#define IRQ_DISABLE_2       mmio(IRQ_BASE + 0x220)
#define BASE_IRQ_DISABLE    mmio(IRQ_BASE + 0x224)

/*
 * The DAIF system register has the following layout:
 *       63    10   9   8   7   6   5      0
 *     | reserved | D | A | I | F | reserved |
 * Also, the DAIF bits are _mask_ bits, so if a bit is on, that kind of
 * exception is masked, i.e. disabled; off is enabled.
 * D : debug exceptions
 * A : system errors
 * I : IRQs
 * F : FIQ (Fast IRQ)
 */
#define DAIF_D ((uint64_t) 0x200)
#define DAIF_A ((uint64_t) 0x100)
#define DAIF_I ((uint64_t) 0x080)
#define DAIF_F ((uint64_t) 0x040)

USE_VALUE bool interrupts_enabled();

void set_interrupts_state(bool enabled);

void enable_interrupts();

void disable_interrupts();

typedef bool irq_state_t;

USE_VALUE irq_state_t push_interrupts_state(bool enabled);
USE_VALUE irq_state_t push_interrupts_disabled();
void restore_interrupts_state(irq_state_t state);

/*
 * Assigns x0-x30, and SP_EL0 to the values pointed to by
 * the argument.
 */
void return_to_process_el0(ptr_t regs);
/*
 * Assigns x0-x30, and SP (== SP_EL1) to the values pointed to by
 * the argument.
 */
void return_to_process_el1(ptr_t regs);

#endif
