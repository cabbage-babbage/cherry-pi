#ifndef VC_MAILBOX_H
#define VC_MAILBOX_H

#include <std.h>

#define CHANNEL_POWER_MANAGEMENT    ((uint8_t) 0)
#define CHANNEL_FRAMEBUFFER         ((uint8_t) 1)
#define CHANNEL_VIRTUAL_UART        ((uint8_t) 2)
#define CHANNEL_VCHIQ               ((uint8_t) 3)
#define CHANNEL_LEDS                ((uint8_t) 4)
#define CHANNEL_BUTTONS             ((uint8_t) 5)
#define CHANNEL_TOUCH_SCREEN        ((uint8_t) 6)
#define CHANNEL_ARM_TO_VC           ((uint8_t) 8) /* used for property tags */
#define CHANNEL_VC_TO_ARM           ((uint8_t) 9)

/* Blocking read from mailbox 1. Result is always 4-bit aligned,
 * i.e. the low 4 bits are zero. */
uint32_t mailbox_read(uint8_t channel);
/* Blocking write to mailbox 2. The data must be 4-bit aligned,
 * i.e. the low 4 bits must be zero. */
void mailbox_write(uint8_t channel, uint32_t data);

/* Format for mailbox request buffer:
 * (each element is a uint32_t)
 *   buffer[0]: buffer size (in bytes, including the header)
 *   buffer[1]: request/response code (MAILBOX_REQUEST*)
 *   buffer[2...]: concatenated tags
 *   buffer[-1]: zero, indicating no more tags
 */

#define MAILBOX_REQUEST             ((uint32_t) 0)
#define MAILBOX_REQUEST_SUCCESSFUL  ((uint32_t) 0x80000000)
#define MAILBOX_REQUEST_ERROR       ((uint32_t) 0x80000001)

/* Format for tag sent via mailbox call:
 * (each element is a uint32_t)
 *   tag[0]: tag identifier
 *   tag[1]: value length (i.e. tag buffer size, bytes)
 *   tag[2]: request/response code
 *      bit 31: 0 indicates it's a request, 1 indicates a response
 *      bits 30-0: response value length (again, tag buffer size);
 *                 should be 0 for a request
 *   tag[3...]: value buffer, padded to a multiple of 4 bytes
 * Simplified format:
 *   tag header
 *   tag value, padded to 4-byte alignment
 */

/* For channel CHANNEL_PROPERTY_TAGS_OUT. */
#define TAG_ID_GET_FIRMWARE_REVISION    ((uint32_t) 0x00000001)
#define TAG_ID_GET_BOARD_MODEL          ((uint32_t) 0x00010001)
#define TAG_ID_GET_BOARD_REVISION       ((uint32_t) 0x00010002)
#define TAG_ID_GET_BOARD_MAC_ADDRESS    ((uint32_t) 0x00010003)
#define TAG_ID_GET_BOARD_SERIAL_NUM     ((uint32_t) 0x00010004)
#define TAG_ID_GET_ARM_MEMORY           ((uint32_t) 0x00010005)
#define TAG_ID_GET_VC_MEMORY            ((uint32_t) 0x00010006)
#define TAG_ID_GET_CLOCKS               ((uint32_t) 0x00010007)
#define TAG_ID_GET_COMMAND_LINE         ((uint32_t) 0x00050001)
#define TAG_ID_GET_DMA_CHANNELS         ((uint32_t) 0x00060001)
#define TAG_ID_GET_POWER_STATE          ((uint32_t) 0x00020001)
#define TAG_ID_GET_TIMING               ((uint32_t) 0x00020002)
#define TAG_ID_SET_POWER_STATE          ((uint32_t) 0x00028001)
#define TAG_ID_GET_CLOCK_STATE          ((uint32_t) 0x00030001)
#define TAG_ID_SET_CLOCK_STATE          ((uint32_t) 0x00038001)
#define TAG_ID_GET_CLOCK_RATE           ((uint32_t) 0x00030002)
#define TAG_ID_SET_CLOCK_RATE           ((uint32_t) 0x00038002)
// TODO: there are a bunch more...

#define TAG_CODE_RESPONSE   ((uint32_t) 1 << 31)
#define TAG_CODE_LENGTH     (~((uint32_t) 1 << 31))

typedef struct {
    uint32_t id;
    uint32_t value_length;
    uint32_t code;
} __attribute__((packed)) mailbox_tag_header_t;

/* Blocking mailbox call. The buffer must be 16-byte aligned,
 * and contains a message for the VideoCore. The VideoCore replies to
 * the message using the same buffer, i.e. the buffer is overwritten.
 * Returns true on success, false on failure. */
bool mailbox_call(uint8_t channel, uint32_t *buffer);

/* A streamlined version of the mailbox_call() function which generates
 * and passes a single property tag on the PROPERTY_TAGS_OUT channel.
 * The given buffer (which contains buf_len elements, i.e. 4 times that
 * many bytes) must be large enough to hold a message header (2 uint32's),
 * the tag header (3 uint32's), and the tag buffer (i.e. tag_buf_len), along
 * along with a final end-of-tags suffix (1 uint32).
 * That is, buf_len must be at least 6 + tag_buf_len.
 *
 * The VideoCore will respond to the tag request (the tag id and tag buffer)
 * by writing data back into the tag buffer and also generating a response
 * length. The response length might be larger than the tag buffer length;
 * in that case, the VideoCore truncates its own output.
 *
 * The tag_buf can be NULL, indicating that it should simply be drawn from
 * the correct region of buf (i.e. starting at buf + 5).
 *
 * Note also that the buf must be 16-byte aligned.
 *
 * Returns true on success, false on failure. On success, the response_length
 * is written with the response length from the VideoCore.
 * */
bool mailbox_call_property_tag(uint32_t *buf, uint32_t buf_len,
        uint32_t tag_id, uint32_t *tag_buf, uint32_t tag_buf_len,
        uint32_t tag_buf_input_len, uint32_t *response_length);

#endif
