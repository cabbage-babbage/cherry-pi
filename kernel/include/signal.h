#ifndef SIGNAL_H
#define SIGNAL_H

#include <std.h>
#include <uapi/signal.h>

typedef struct signal {
    struct signal *prev, *next;
    /* The kind of signal. */
    signal_nr_t nr;
    /* Any extra data passed via the signal. If buf_len is 0, buf
     * may be NULL. */
    uint8_t buf_len;
    uint8_t *buf;
} signal_t;

/* Allocate and initialize a new signal_t with the given data. Ownership
 * of the buffer is transferred (on success) to the new signal; the buffer
 * will be freed with the signal itself. Returns NULL on failure.
 * Note that the buf may be NULL, but only if buf_len is 0. */
signal_t *alloc_signal(signal_nr_t nr, uint8_t buf_len, uint8_t *buf);
/* Free a signal and its buffer. */
void free_signal(signal_t *signal);

typedef struct {
    signal_t *head;
    signal_t *tail;
    /* If the list is closed, no signals can be added, and enqueue_signal()
     * will simply free the given signal instead of adding it to the list. */
    bool closed;
} signal_list_t;

/* Produce an empty (and open) signals list. */
signal_list_t empty_signals_list();

/* Note that these functions are all unsynchronized. */

/* Enqueue a signal into the given signals list. The signal becomes owned by the list,
 * and may be freed at (essentially) any time. */
void enqueue_signal(signal_list_t *list, signal_t *signal);

/* Returns the head of the signals list, or NULL if the list is empty. */
signal_t *peek_signal(signal_list_t *list);

/* Dequeue a signal and return it, if the list is nonempty. The signal becomes owned
 * by the caller. If the list is empty, this returns NULL. */
signal_t *dequeue_signal(signal_list_t *list);

/* Frees all signals in the given list and marks the list as closed, so no signals
 * will be added in the future. */
void free_signals(signal_list_t *list);

#endif
