#ifndef PROC_LIST_H
#define PROC_LIST_H

#include <std.h>

/* Forward declare. */
struct proc;

typedef struct {
    uint64_t len;
    struct proc *head;
    struct proc *tail;
} proc_list_t;

/*
 * Process list utility functions.
 * We can generally treat lists as queues or stacks if we
 * are consistent about prepending/appending and removing
 * from the head or tail:
 * - for a queue: enqueue to the tail, dequeue from the head
 * - for a stack: push to the tail, pop from the tail
 */

USE_VALUE proc_list_t empty_process_list();

// TODO: find a name that includes 'process', so we don't have
// to deconflict with other list types in the future.
USE_VALUE bool is_empty(proc_list_t *list);

/* The process must not be in any list. */
void prepend_process(proc_list_t *list, struct proc *proc);

/* The process must not be in any list. */
void append_process(proc_list_t *list, struct proc *proc);
#define enqueue_process append_process
#define push_process append_process

/* The process must not be in any list, and is inserted just
 * before the specified place, which must in the given list. */
void insert_process_before(proc_list_t *list, struct proc *proc, struct proc *place);

/* The process must be in the given list. (But this cannot be checked.)
 * Panics if the list is empty. */
void remove_process(proc_list_t *list, struct proc *proc);

/* Panics if the list is empty. */
struct proc *remove_head_process(proc_list_t *list);
#define dequeue_process remove_head_process

/* Panics if the list is empty. */
struct proc *remove_tail_process(proc_list_t *list);
#define pop_process remove_tail_process

/* Do not modify the list while iterating through it! */
#define forall_processes(iter, list) \
    for (struct proc *iter = (list).head; iter != NULL; iter = iter->next)

#endif
