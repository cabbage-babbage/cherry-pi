#ifndef PIPE_H
#define PIPE_H

#include <std.h>
#include <synch/lock.h>
#include <uapi/errno.h>

typedef struct {
    /* Protects open_readers and open_writers. However, if all that's required
     * is to check if open_readers or open_writers is zero, and false negatives
     * are acceptable (i.e. they're actually zero but we think they're still
     * nonzero), then the refs_lock need not be acquired.
     * In other words, if we want to check if there are any open readers or
     * open writers, but false positives are acceptable (i.e. there aren't any
     * readers/writers but we think there are some), then the refs_lock need
     * not be held. */
    rw_lock_t refs_lock;
    /* Number of open file descriptors referring to the read and write halves,
     * respectively, of this pipe.
     * Once either value becomes 0, it can never again become nonzero. */
    uint64_t open_readers, open_writers;

    /* The read_lock ensures that only one process has read access to the buf,
     * and the write_lock ensures that only one process has write access. This
     * way we can ensure that all read/writes are atomic, even if we're reading
     * or writing many more bytes than the pipe size.
     *
     * The read_lock must be held to sema_down(read_avail), and the write_lock
     * must be held to sema_down(write_avail). */
    lock_t read_lock, write_lock;

    /* 0-1 semaphores which indicate if there is any data available to read, or
     * any space available to write. */
    semaphore_t read_avail, write_avail;

    /* The number of allocated bytes. This cannot change after initialization. */
    uint64_t size;
    /* Offsets for head and tail of the circular buffer, maintaining the
     * invariant 0 <= (head and tail) < buf_size. When head == tail the buffer
     * is empty, not full; and we always ensure that the buffer is not completely
     * filled, so that we can always unambiguously determine the fill level. */
    uint64_t head, tail;
    uint8_t *buf;
} pipe_t;

/* Returns NULL on out-of-memory. */
pipe_t *alloc_pipe();

void pipe_dup_half(pipe_t *pipe, bool read_half);
/* Note that this will free the pipe if there are no longer any readers and writers,
 * so pipe may be invalid on return. */
void pipe_close_half(pipe_t *pipe, bool read_half);

errno_t pipe_read(pipe_t *pipe, uint8_t *buf, uint64_t buf_size,
        bool blocking, uint64_t *bytes_read);

errno_t pipe_write(pipe_t *pipe, const uint8_t *buf, uint64_t buf_size,
        bool blocking, uint64_t *bytes_written);

#endif
