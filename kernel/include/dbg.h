#ifndef DBG_H
#define DBG_H

#include <console.h>
#include <irq.h>
#include <stacktrace.h>

void hexdump(ptr_t buffer, uint64_t length);
void hexdump_ext(ptr_t buffer, uint64_t display_offset, uint64_t length);

/*
 * If a file wants to have debug messages sent
 * somewhere other than the console, they can
 * override these macros before including this
 * header.
 */
#ifndef DEBUG_PRINTF
#define DEBUG_PRINTF printf
#endif
#ifndef DEBUG_PRINTF_NL
#define DEBUG_PRINTF_NL printf_nl
#endif

#define DEBUG_PRINTF_LEVEL(level, ...) \
    do { \
        DEBUG_PRINTF_NL("[%s] ", #level); \
        DEBUG_PRINTF(__VA_ARGS__); \
    } while (0)

/* Prefixes with [ERROR], and always starts on a new line. */
#define DEBUG_ERROR(...) DEBUG_PRINTF_LEVEL(ERROR, __VA_ARGS__)
/* Prefixes with [WARNING], and always starts on a new line. */
#define DEBUG_WARNING(...) DEBUG_PRINTF_LEVEL(WARNING, __VA_ARGS__)
/* Prefixes with [LOG], and always starts on a new line. */
#define DEBUG_LOG(...) DEBUG_PRINTF_LEVEL(LOG, __VA_ARGS__)

/*
 * Usage:
 *    dbg_error(fmt, arg, arg, ...)
 * Prints file and line information, along with the given printf specification.
 */
#define dbg_error(...)  \
    do { \
        DEBUG_ERROR("at %s:%i32: ", __FILE__, (int32_t) __LINE__); \
        DEBUG_PRINTF(__VA_ARGS__); \
    } while (0)

/*
 * Usage:
 *   dbg_abort(fmt, arg, arg, ...)
 * Prints a debug message and returns from the current function.
 */
#define dbg_abort(...) \
    do { \
        dbg_error(__VA_ARGS__); \
        return; \
    } while (0)

/*
 * Usage:
 *   dbg_abort_with(value, fmt, arg, arg, ...)
 * Prints a debug message and returns from the current function with the specified value.
 */
#define dbg_abort_with(value, ...) \
    do { \
        dbg_error(__VA_ARGS__); \
        return (value); \
    } while (0)

/*
 * Usage:
 *   panic(fmt, arg, arg, ...)
 * Prints a debug message and then logs a kernel panic; never returns
 * and stops all execution.
 * TODO: make sure that print_stacktrace uses the same printf_nl and printf functions
 * that we're given here...
 */
#define panic(...) \
    do { \
        disable_interrupts(); \
        dbg_error(__VA_ARGS__); \
        DEBUG_ERROR("kernel panic\n"); \
        print_stacktrace("ERROR"); \
        while (1) asm volatile("wfi"); \
    } while (0)

/*
 * Usage:
 *   panic_if(value, fmt, arg, arg, ...)
 * If the value is true/nonzero, panics with the specified format string and
 * format arguments.
 */
#define panic_if(value, ...) \
    do { \
        if (value) { \
            panic(__VA_ARGS__); \
        } \
    } while (0)

/*
 * Usage:
 *   panic_if(value, fmt, arg, arg, ...)
 * If the value is false/zero, panics with the specified format string and
 * format arguments.
 */
#define panic_if_not(value, ...) \
    do { \
        if (!(value)) { \
            panic(__VA_ARGS__); \
        } \
    } while (0)

#endif
