#ifndef SYSREG_H
#define SYSREG_H

#define load_sysreg(destination, system_reg) \
    asm volatile("mrs %[tmp], " #system_reg : [tmp] "=r" (destination))
#define set_sysreg(system_reg, source) \
    asm volatile("msr " #system_reg ", %[tmp]" : : [tmp] "r" (source))

/*
 * Files that include this header must define PRINT_SYSREG if they
 * want console.h included and the print_sysreg macro defined.
 */
#ifdef PRINT_SYSREG
#include <console.h>
#define print_sysreg(log_name, system_reg, type, format_specifier) \
    do { \
        type _tmp; \
        load_sysreg(_tmp, system_reg); \
        printf("%s: " format_specifier "\n", log_name, _tmp); \
    } while (0)
#endif // PRINT_SYSREG

#endif
