#ifndef STDARG_H
#define STDARG_H

#define va_list __builtin_va_list
#define va_start(args, anchor) __builtin_va_start(args, anchor)
#define va_arg(args, type) __builtin_va_arg(args, type)
#define va_end(args) __builtin_va_end(args)

#endif
