#ifndef PROC_H
#define PROC_H

#include <std.h>
#include <proc_list.h>
#include <synch/semaphore.h>
#include <synch/lock.h>
#include <synch/monitor.h>
#include <fs/filesys.h>
#include <pipe.h>
#include <signal.h>

#define REG_FP 29
#define REG_LR 30
#define REG_SP 31

typedef struct {
    /* note: x29 is FP, x30 is LR, and x31 is SP (which is not a GPR in aarch64) */
    uint64_t x[32]; /* registers x0-x31 */
    uint64_t pc; /* next instruction to be executed */
    uint64_t pstate; /* ARM processor state system register */
    uint64_t ttbr0; /* translation table base register (no need for ttbr1; it doesn't change) */
} proc_regs_t;

typedef enum {
    CONTEXT_PROCESS, /* User code running. */
    CONTEXT_SYSCALL, /* Kernel code running on behalf of the process. */
    CONTEXT_EXITED   /* Process has finished, and should not be scheduled again. Many of
                        the process resources may have been freed already. */
} proc_context_t;

#define FD_READABLE         ((uint32_t) 1 << 0)
#define FD_WRITEABLE        ((uint32_t) 1 << 1)
#define FD_APPEND           ((uint32_t) 1 << 2)
#define FD_CLOSE_ON_EXEC    ((uint32_t) 1 << 3)
#define FD_NONBLOCKING      ((uint32_t) 1 << 4)
#define FD_IS_TTY           ((uint32_t) 1 << 5)

typedef struct {
    bool valid;

    /* type may be any of the INODE_* values, or FILE_PIPE to indicate that this
     * is an open pipe half instead of an inode-backed open file. */
    inode_type_t type;
    /* file and offset are only valid if this is not a pipe half. */
    open_file_t file;
    uint64_t offset;
    /* This auxiliary state/info is type-specific. */
    union {
        submount_t *submount; /* For iterating with readdir(). */
        device_t *dev; /* Cached device, for character or block device files. */
        pipe_t *pipe; /* If this is a pipe descriptor. */
    } aux;

    /* Flags for behaviorial control: options are given by FD_*. */
    uint32_t flags;
} file_descriptor_t;

errno_t dup_file_desc(file_descriptor_t *desc);
void close_file_desc(file_descriptor_t *desc);

typedef void (*interrupt_fn_t)(struct proc *proc);

#define MAX_FDS 8

typedef struct proc {
    /* Doubly-linked list pointers. Each process can be in one linked list using this
     * mechanism. NULL indicates no previous or next process. */
    struct proc *prev;
    struct proc *next;

    uint32_t id; /* Guaranteed to be 0 if this control block is not allocated for a
                    process, and nonzero if allocated. */
    uint32_t gid; /* Group id; processes with the same gid are in the same process group,
                     and the process whose id == gid is the group leader. */
    char name[128]; /* must be null-terminated, i.e. max 127 characters */
    bool is_kernel; /* Kernel process vs. user process. Kernel processes do not have
                       any lower-half virtual memory assigned, and their code must
                       live in high virtual addresses. */

    proc_context_t context; /* Current execution context. */
    /* A stopped process does not resume normal execution until receiving a SIGCONT signal.
     * (A SIGKILL can still kill it, though.) */
    bool is_stopped;

    /* Specifies how to interrupt current operation of the process. Must be called with interrupts
     * disabled. May be NULL, indicating that the current process cannot be interrupted. */
    interrupt_fn_t interrupt_fn;
    /* This is set to true in interrupt_process() and reset via was_interrupted(), and indicates
     * to the process that it should abort its current operation with EINTR, if possible. */
    bool is_interrupted;
    /* Auxiliary data that the interrupt_fn may freely use. */
    union {
        struct {
            semaphore_t *sema;
        } sema_waiting;
        struct {
            cond_var_t *var;
        } cond_var_waiting
    } interrupt_aux;

    proc_regs_t regs; /* Saved process registers during normal execution. */
    proc_regs_t syscall_regs; /* Saved process registers while executing a syscall. */

    /* Current working directory. NULL for kernel processes. */
    open_file_t cwd;

    bool is_fork;
    /* proc_memory: text and data segments of the process image.
     * heap_memory: user heap region.
     * stack_memory: user stack region.
     * aux_memory: an auxiliary data segment; currently only used for large initial
     *      program arguments (which would put too much pressure on the stack
     *      otherwise). May be NULL if unused.
     * These are all NULL if is_fork (so the physical memory is not necessary
     * contiguous, and is solely referenced by the page table).
     * Furthermore, if is_kernel then only the stack_memory is allocated; the image
     * should be in the kernel virtual address space, and the process uses the kernel
     * heap instead of a per-process heap. */
    ptr_t proc_memory, heap_memory, stack_memory, aux_memory;

    ptr_t heap_start; /* User virtual address. */
    uint64_t heap_pages; /* Size of heap. */

    /* Stack space to be used when handling a syscall for the given process. This
     * space will _not_ be freed when the process exits; it should be statically
     * allocated. */
    ptr_t syscall_stack;

    /* Only guaranteed to be valid if this process is in the processes_waiting list.
     * In that case, this specifies the earliest timer_counter() at which the process
     * may be awoken. */
    uint64_t wakeup_time;

    /* The process which forked the current one, or NULL if no parent exists (i.e.
     * this is the root process, or the current process is orphaned). */
    struct proc *parent;

    /* Only guaranteed to be valid once this process's context is CONTEXT_EXITED. */
    uint32_t exit_code;

    /* Process id of the child process we're waiting on, 0 if waiting on any child
     * process to exit, and -1 if not waiting for any exiting. */
    int64_t wait_id;

    signal_handler_t signal_handler[NUM_SIG];
    /* Protects the signals list, as well as usermode_signals_masked. */
    lock_t signals_lock;
    /* List of signals that have been delivered to the current process but not yet
     * processed. */
    signal_list_t signals;
    /* True if the current process cannot accept any more usermode signals currently,
     * i.e. because a usermode signal was delivered but the process has not yet made
     * a sigreturn() syscall. */
    bool usermode_signals_masked;
    /* True if the process needs to briefly return to the kernel for signal processing
     * before next running in usermode.
     * This may be freely modified externally, but should only be set to true. */
    bool signal_processing_pending;

    /* File descriptors available for reading/writing.
     * We don't need a lock protecting this array, since these file descriptors are
     * modified only by the running process itself; there is only a single reader
     * and writer, and all operations are naturally serialized. */
    file_descriptor_t fds[MAX_FDS];
} proc_t;

/* This lock must be acquired during any modification to the following lists. */
extern irq_lock_t process_lists_lock;

/* When a process is unallocated, the only fields guaranteed to be valid are
 * the prev/next pointers (since it's in the processes_unallocated list) and
 * the id, which is 0. */
extern proc_list_t processes_unallocated;
/* Processes that are ready to run, and are either currently running or waiting
 * for the scheduler to provide some run time. */
extern proc_list_t processes_ready;
/* Processes that are sleeping until their wakeup_time. This list must remain
 * sorted by each process's wakeup_time (in increasing order). */
extern proc_list_t processes_sleeping;

/*
 * Must be called before any other function in this header.
 */
void initialize_processes();

/*
 * Begins the execution cycle of all processes in the system.
 * Must be called after initialize_processes(), and at least one process must have been created.
 * Does not return.
 */
void start_processes();

/* Determines (from proc->is_kernel and proc->context) whether or not the process should
 * be currently executing with kernel privileges (true) or user privileges (false). */
USE_VALUE bool has_privileged_execution(proc_t *proc);

/* Allocates a previously unallocated process slot and returns it.
 * This does *not* set the id of the freshly allocated process.
 * (Returns NULL if there are no slots available.) */
USE_VALUE proc_t *allocate_process();

/* Assign a guaranteed fresh process id to the given process.
 * Fresh simply means that no currently allocated process has that
 * process id. */
void assign_fresh_process_id(proc_t *proc);

/* Free the resources referenced by the given process. This does _not_ free
 * the syscall_stack. This also doesn't move the process into the unallocated
 * queue.
 *
 * Important operational behavior: if the process is currently executing with
 * CONTEXT_SYSCALL, the process can still succesfully be scheduled, and run
 * properly, as long as it does not attempt to access any user-space data,
 * i.e. the image, data, or user-space stack. The freed process can still use
 * its kernel-space stack.
 *
 * In short, it is safe to free even the currently running process, as long as
 * the process only uses its kernel-space stack (and any kernel data) afterwards. */
void free_process(proc_t *proc);

/* Move the process to the deaellocated queue. (No synchronization used.)
 * (This does not free any of the process's resources. This also assumes that
 * the process is not currently in any list.) */
void deallocate_process_unsync(proc_t *proc);

/* Move the process to the deallocated queue.
 * (This does not free any of the process's resources. This also assumes that
 * the process is not currently in any list.) */
void deallocate_process(proc_t *proc);

/* Check if the process is actually allocated or not. */
USE_VALUE bool process_is_allocated(proc_t *proc);

/* Kills the given process, i.e. frees its resources and deallocates the control block.
 * This function must be executed from a syscall context, and the process must be ready
 * to run, i.e. in the ready-queue.
 * This function is safe to call on the current process, in which case it naturally
 * does not return. */
void kill_ready_process(proc_t *proc);

/* This assumes that the given process has been freshly allocated, i.e. has no resources that must be
 * freed before overwriting.
 * On success, the given process is initialized and ready to run the executable image indicated by the
 * filename. Specifically, the following process fields are initialized:
 * - prev, next (process is placed in the ready queue) [if make_ready]
 * - name
 * - is_kernel
 * - context
 * - is_stopped
 * - interrupt_fn
 * - is_interrupted
 * - regs
 * - cwd
 * - is_fork
 * - proc_memory, heap_memory, stack_memory, aux_memory
 * - heap_start, heap_pages
 * - wakeup_time
 * - parent
 * - wait_id
 * - signal_handler
 * - signals_lock
 * - signals
 * - usermode_signals_masked
 * - signal_processing_pending
 * - fds
 * (Notably, the id is not assigned, and neither is the syscall_stack. Less importantly, the following
 * are also not assigned: gid, syscall_regs, exit_code.)
 * The file descriptors given are simply copied, without calling dup_file_desc() on each file descriptor;
 * duplicating these descriptors before calling setup_user_process() is the responsibility of the caller.
 * Likewise, duplicating the cwd is the responsibility of the caller.
 * On failure, the given process is placed in the unallocated-processes queue [if deallocate_on_failure],
 * and all its resources are freed, including all the given file descriptors [if close_fds_on_failure] and
 * the given cwd [if close_cwd_on_failure].
 *
 * args may be NULL, but only if args_length is 0.
 *
 * If make_ready or deallocate_on_failure are not true, those responsibilities lie with the caller.
 *
 * Returns 0 on success, and -errno on failure. */
USE_VALUE int64_t setup_user_process(proc_t *proc, const char *name,
        open_file_t cwd, const char *filepath,
        ptr_t args, uint64_t args_length,
        proc_t *parent,
        file_descriptor_t *fds, uint32_t num_fd, bool close_fds_on_failure,
        bool make_ready, bool close_cwd_on_failure, bool deallocate_on_failure);

typedef uint32_t (*kernel_process_fn_t)(void *aux);

/* Like setup_user_process(), this assumes that the given process has been freshly allocated.
 * On success, the given process is initialized and ready to run the given function. The same
 * fields as in setup_user_process() are initialized on success, except that the cwd is set to
 * an invalid open_file_t (all NULL); the make_ready and deallocate_on_failure behavior is also
 * the same.
 * Returns 0 on success, and -errno on failure. */
USE_VALUE int64_t setup_kernel_process(proc_t *proc, const char *name, kernel_process_fn_t proc_main,
        void *aux, proc_t *parent,
        bool make_ready, bool deallocate_on_failure);

/*
 * Replace the given process with a new image, and restart its execution. Only the following
 * process state is preserved from the original process to the fresh process:
 * - the process id
 * - the process gid
 * - the current working directory
 * - the parent process
 * - any open file descriptors which do not have the FD_CLOSE_ON_EXEC flag set (the rest
 *   are closed)
 * On success, the original process is fully freed and the new process, using the original
 * control block, is fully initialized and is in the ready queue.
 * On failure, the original process continues running with no changes, and no new process is
 * initialized or even allocated.
 *
 * @param original      The original process. This process must be in the ready queue. (This
 *                      is not verified, but will likely cause strange bugs if that's not the
 *                      case.)
 * @param name          The new process's name.
 * @param filepath      The location of the process's image, relative to the original process's.
 *                      current working directory.
 * @param args          Pointer to a buffer of data to pass to the new process. May be NULL
 *                      if args_length is 0.
 * @param args_length   The length of the args buffer, in bytes.
 *
 * @return              0 if the new process was successfully created; -errno on failure.
 */
USE_VALUE int64_t replace_process(proc_t *original, const char *name, const char *filepath,
        ptr_t args, uint64_t args_length);

/*
 * Fork an existing process. On success, returns 0 and puts a pointer to the newly
 * forked process in the 'forked' parameter. On failure, returns -errno and sets 'forked'
 * to NULL instead.
 * The forked process has exactly the same state as the original process, with the
 * following exceptions:
 * - the forked process is assigned distinct physical memory regions (TODO: until we copy-on-write instead)
 *   (that is, the virtual memory map is new for the forked process)
 * - the forked process has its x0 register set to 0.
 */
USE_VALUE int64_t fork_process(proc_t *original, proc_t **forked);

/* Fork the current (kernel!) process and cause the child to execute the given
 * user program (whose filepath is relative to the cwd). On success, sets child
 * to the forked process and returns 0; on failure, sets child to NULL and returns
 * -errno. This function is equivalent to the userspace fork-and-then-exec pattern,
 * so the child only gets file descriptors _not_ marked as FD_CLOSE_ON_EXEC.
 * Note that this function does _not_ duplicate the cwd, but it does close the cwd
 * on failure. */
USE_VALUE int64_t fork_user(const char *name, open_file_t cwd, const char *filepath,
        ptr_t args, uint64_t args_length,
        proc_t **child);

/* Fork the current (kernel!) process and cause the child to execute the given
 * function as a kernel process. On success, sets child to the forked process
 * and returns 0; on failure, sets child to NULL and returns -errno. The child
 * is created with no open file descriptors; that is, the FD_CLOSE_ON_EXEC flag
 * is ignored for parent descriptors, and is implicitly taken to be set for each
 * descriptor. */
USE_VALUE int64_t fork_kernel(const char *name, kernel_process_fn_t proc_main,
        void *aux,
        proc_t **child);

/*
 * Returns (a pointer to) the currently running process.
 */
USE_VALUE proc_t *current_process();

/* True to continue, false to break. */
typedef bool (*process_action_t)(proc_t *proc, void *aux);
/* Loop through all processes in the system, whether allocated or not.
 * Passes aux to each call to action. */
void foreach_process(process_action_t action, void *aux);
/* Loop through all allocated processes in the system.
 * Passes aux to each call to action. */
void foreach_allocated_process(process_action_t action, void *aux);
/* Loop through all (allocated) processes in the given group.
 * Passes aux to each call to action. */
void foreach_process_in_group(uint32_t gid, process_action_t action, void *aux);

/*
 * Finds the process with the given id, if any; otherwise
 * returns NULL.
 */
USE_VALUE proc_t *process_with_id(uint32_t id);

/*
 * Runs the process scheduler, potentially switching to another process,
 * particularly if the current process is no longer enabled.
 * This can be called from an interrupt context, in which case the scheduler
 * is run after the interrupt handler returns.
 */
void yield_process();

/* These two functions cause the current process to sleep until wakeup_irq() is called
 * on it. Specifically, any call to wakeup_irq() (on the current process) made after
 * prepare_irq_wait() causes irq_wait() to return; if wakeup_irq() is done even before
 * irq_wait() is called, the irq_wait() invocation immediately returns without sleeping
 * at all.
 *
 * That is, these functions are intended to be used in the following manner, to
 * synchronize preemptible and non-preemptible (such as IRQ) execution:
 *    preemptible code:
 *        prepare_irq_wait();
 *        ... enable future wakeup ...
 *        irq_wait()
 *        ... disable future wakeup ...
 *    other code (e.g. IRQ handler):
 *        once enabled for future wakeup:
 *          if should_cause_wakeup:
 *            wakeup_irq(...); */
void prepare_irq_wait();
/* irq_wait() returns as soon as the process is interrupted by any mechanism, commonly
 * via wakeup_irq() or interrupt_process(). The former does not set is_interrupted,
 * while the latter does. */
void irq_wait();
/* Wake up a process that is irq-waiting. Can be called from an interrupt
 * context. */
void wakeup_irq(proc_t *proc);

/* If possible, interrupt a process from its current operation and make it ready to run. */
void interrupt_process(proc_t *proc);

/* If the current process was interrupted (since this was last called), returns true;
 * otherwise, returns false. In either case, the is_interrupted status is cleared. */
bool was_interrupted();

/* Continue execution of the given process, with the specified registers (an
 * array of 32 uint8_t's). If regs is NULL, use process regs (or process
 * syscall_regs, depending on the process's execution context). */
void return_to_process(proc_t *proc, ptr_t regs);

/* Cause the signal to be processed by the current process. If the process's signal
 * handler for the given signal is SIG_DFL or SIG_IGN, this processes the signal
 * immediately; otherwise, this modifies the process's stack and registers so that
 * when it resumes execution in user-mode it executes the signal handler for the
 * given signal. Note that this does not free the signal or its buffer.
 *
 * Returns 0 on success, or a nonzero errno on failure. */
void deliver_signal(proc_t *proc, signal_t *signal);
/* Deliver a signal (with empty buffer) to every process in the specified group. */
void deliver_group_signal(uint32_t gid, signal_nr_t signal_nr);

/* Process as many pending signals for the current process as possible. */
void process_signals();

// TODO: maybe define some opaque struct for the saved regs base,
// instead of just a pointer? Will make it easier to change how we
// save regs coming into an exception handler, if necessary.

/*
 * Handle a service call exception.
 * @param irq_saved_regs_base  Points to the IRQ-saved registers on the stack.
 */
void handle_process_syscall(ptr_t irq_saved_regs_base);

/*
 * Handle a timer interrupt.
 * @param irq_saved_regs_base  Points to the IRQ-saved registers on the stack.
 * @param el0              Specifies if the interrupt occured while executing
 *                         in EL0 (true) or EL1 (false).
 */
void handle_timer_irq(ptr_t irq_saved_regs_base, bool el0);

#endif
