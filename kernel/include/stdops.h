#ifndef STDOPS_H
#define STDOPS_H

#include <std.h>

USE_VALUE uint64_t max_uint64(uint64_t a, uint64_t b);
USE_VALUE uint64_t min_uint64(uint64_t a, uint64_t b);

USE_VALUE uint64_t round_up_div(uint64_t numerator, uint64_t denominator);

USE_VALUE uint64_t align_down(uint64_t val, uint64_t alignment);
USE_VALUE uint64_t align_up(uint64_t val, uint64_t alignment);

/* Returns the number of least-significant 0 bits in the given value. */
USE_VALUE uint32_t alignment_bits(uint64_t val);

USE_VALUE bool is_aligned_at_bits(uint64_t val, uint32_t alignment_bits);

typedef struct {
    uint64_t hi, lo;
} uint128_t;

USE_VALUE uint128_t uint128_from_uint64(uint64_t val);
USE_VALUE uint64_t uint64_from_uint128(uint128_t val);

USE_VALUE bool lte_uint128(uint128_t a, uint128_t b);
//USE_VALUE bool lt_uint128(uint128_t a, uint128_t b); TODO
//USE_VALUE bool eq_uint128(uint128_t a, uint128_t b); TODO
//USE_VALUE bool gt_uint128(uint128_t a, uint128_t b); TODO
//USE_VALUE bool gte_uint128(uint128_t a, uint128_t b); TODO
/* These will silently overflow/underflow. */
USE_VALUE uint128_t add_uint128(uint128_t a, uint128_t b);
USE_VALUE uint128_t sub_uint128(uint128_t a, uint128_t b);
/* These cannot (by definition) overflow/underflow. */
USE_VALUE uint128_t max_uint128(uint128_t a, uint128_t b);
USE_VALUE uint128_t min_uint128(uint128_t a, uint128_t b);

#endif
