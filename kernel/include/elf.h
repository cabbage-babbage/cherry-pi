#ifndef ELF_H
#define ELF_H

#include <std.h>

/* expected values for e_machine */
#define EM_AARCH64  ((uint16_t) 0xB7)

/* expected values for e_type */
#define ET_NONE     ((uint16_t) 0x0000)
#define ET_REL      ((uint16_t) 0x0001)
#define ET_EXEC     ((uint16_t) 0x0002)
#define ET_DYN      ((uint16_t) 0x0003)
#define ET_CORE     ((uint16_t) 0x0004)
#define ET_LOOS     ((uint16_t) 0xFe00)
#define ET_HIOS     ((uint16_t) 0xFEFF)
#define ET_LOPROC   ((uint16_t) 0xFF00)
#define ET_HIPROC   ((uint16_t) 0xFFFF)

/* expected values for p_type */
#define PT_NULL     ((uint32_t) 0x00000000)
#define PT_LOAD     ((uint32_t) 0x00000001)
#define PT_DYNAMIC  ((uint32_t) 0x00000002)
#define PT_INTERP   ((uint32_t) 0x00000003)
#define PT_NOTE     ((uint32_t) 0x00000004)
#define PT_SHLIB    ((uint32_t) 0x00000005)
#define PT_PHDR     ((uint32_t) 0x00000006)
#define PT_TLS      ((uint32_t) 0x00000007)
#define PT_LOOS     ((uint32_t) 0x60000000)
#define PT_HIOS     ((uint32_t) 0x6FFFFFFF)
#define PT_LOPROC   ((uint32_t) 0x70000000)
#define PT_HIPROC   ((uint32_t) 0x7FFFFFFF)

/* expected flags for p_flags */
#define PF_X        ((uint32_t) 0x00000001)
#define PF_W        ((uint32_t) 0x00000002)
#define PF_R        ((uint32_t) 0x00000004)
#define PF_MASKPROC ((uint32_t) 0xF0000000) /* allows for four processor-specific flags */

/* expected values for sh_type */
#define SHT_NULL            ((uint32_t) 0x00000000)
#define SHT_PROGBITS        ((uint32_t) 0x00000001)
#define SHT_SYMTAB          ((uint32_t) 0x00000002)
#define SHT_STRTAB          ((uint32_t) 0x00000003)
#define SHT_RELA            ((uint32_t) 0x00000004)
#define SHT_HASH            ((uint32_t) 0x00000005)
#define SHT_DYNAMIC         ((uint32_t) 0x00000006)
#define SHT_NOTE            ((uint32_t) 0x00000007)
#define SHT_NOBITS          ((uint32_t) 0x00000008)
#define SHT_REL             ((uint32_t) 0x00000009)
#define SHT_SHLIB           ((uint32_t) 0x0000000A)
#define SHT_DYNSYM          ((uint32_t) 0x0000000B)
#define SHT_INIT_ARRAY      ((uint32_t) 0x0000000E)
#define SHT_FINI_ARRAY      ((uint32_t) 0x0000000F)
#define SHT_PREINIT_ARRAY   ((uint32_t) 0x00000010)
#define SHT_GROUP           ((uint32_t) 0x00000011)
#define SHT_SYMTAB_SHNDX    ((uint32_t) 0x00000012)
#define SHT_NUM             ((uint32_t) 0x00000013)
#define SHT_LOOS            ((uint32_t) 0x60000000)
// TODO: this doesn't match with ELF spec, Figure 1-9
// (from SHT_LOOS on)

/* 64-bit ELF header */
typedef struct {
    char ei_magic[4];
    uint8_t ei_class;
    uint8_t ei_data;
    uint8_t ei_version;
    uint8_t ei_os_abi;
    uint8_t ei_pad[8]; /* we can ignore abi_version */
    uint16_t e_type; /* one of ET_* (or ET_LOOS to ET_HIOS, or ET_LOPROC to ET_HIPROC) */
    uint16_t e_machine;
    uint32_t e_version;
    uint64_t e_entry;
    uint64_t e_phoff;
    uint64_t e_shoff;
    uint32_t e_flags;
    uint16_t e_ehsize;
    uint16_t e_phentsize;
    uint16_t e_phnum;
    uint16_t e_shentsize;
    uint16_t e_shnum;
    uint16_t e_shstrndx;
} __attribute__((packed)) elf_header_t;

/* 64-bit ELF program header entry */
typedef struct {
    uint32_t p_type; /* one of PT_* (or PT_LOOS to PT_HIOS, or PT_LOPROC to PT_HIPROC) */
    uint32_t p_flags;
    uint64_t p_offset;
    uint64_t p_vaddr;
    uint64_t p_paddr;
    uint64_t p_filesz;
    uint64_t p_memsz;
    uint64_t p_align;
} __attribute__((packed)) elf_ph_entry_t;

/* 64-bit ELF section header entry */
typedef struct {
    uint32_t sh_name;
    uint32_t sh_type; /* one of SHT_* (or at least SHT_LOOS) */
    uint64_t sh_flags;
    uint64_t sh_addr;
    uint64_t sh_offset;
    uint64_t sh_size;
    uint32_t sh_link;
    uint32_t sh_info;
    uint64_t sh_addralign;
    uint64_t sh_entsize;
} __attribute__((packed)) elf_sh_entry_t;

/* Check that the given file (in memory) is of ELF format. In particular,
 * check also that all pointers point to valid locations in the file, so
 * that later stages can freely derereference data from the ELF file
 * without worry.
 * Returns a reference to the ELF file header on success, and returns NULL
 * if the file is not of ELF format. */
// NOTE: if caller wants to execute the ELF, they should check:
// - e_machine == EM_AARCH64
// - e_entry (required to be 0 if there's no entry point, but we can require an entry point)
// - e_flags (must contain 0 for aarch64)
// - that segment offsets/sizes make sense (e.g. are not too big)
//   (though this check will ensure taht the segments at least lie within in the file)
USE_VALUE elf_header_t *check_elf(ptr_t file_start, uint64_t file_length);

//elf_ph_entry_t *elf_ph_start(ptr_t file_start, elf_header_t e_header);
//uint16_t elf_ph_num(ptr_t file_start, elf_header_t e_header);

/* For debugging purposes. Assumes that check_elf() has already been called, and
 * may fault otherwise (if the ELF is not in fact valid). */
void dump_elf_headers(ptr_t file_start, const char *prefix);

#endif
