#ifndef MUTEX_H
#define MUTEX_H

#include <std.h>
#include <synch/semaphore.h>

/* A mutex is just a 0-1 semaphore. */
typedef struct {
    semaphore_t sema;
} mutex_t;

/* Creates a mutex in the memory specified. */
void create_mutex(mutex_t *mutex);

void acquire_mutex(mutex_t *mutext);

void release_mutex(mutex_t *mutex);

bool try_acquire_mutex(mutex_t *mutex);

bool acquire_mutex_interruptible(mutex_t *mutex);

#endif
