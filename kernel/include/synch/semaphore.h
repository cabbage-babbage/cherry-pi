#ifndef SEMAPHORE_H
#define SEMAPHORE_H

#include <std.h>
#include <proc_list.h>

typedef struct {
    uint64_t state;
    /* The processes waiting for the state to become nonzero. */
    proc_list_t processes_waiting;
} semaphore_t;

/*
 * Creates a semaphore in the memory specified, with the
 * given initial state.
 */
void create_sema(semaphore_t *sema, uint64_t state);

/*
 * Wait until the semaphore has a nonzero value,
 * and then decrement its value.
 */
void sema_down(semaphore_t *sema);

/* If the semaphore has a nonzero value, atomically decrement
 * its value and return true; otherwise, return false.
 * That is, the return value indicates whether or not the
 * semaphore's value was successfully decremented. */
bool sema_try_down(semaphore_t *sema);

/* Wait until the semaphore has a nonzero value, and then
 * decrement its value. The waiting is interruptible, and
 * if the process is (or was already) interrupted while waiting
 * for the nonzero value, this function returns false, with no
 * change to the semaphore (but with the interrupt status cleared).
 * A return value of true indicates that the process was not
 * interrupted, and the semaphore was successfully decremented. */
bool sema_down_interruptible(semaphore_t *sema);

/*
 * Increment the semaphore (thus possibly unblocking
 * another process.)
 */
void sema_up(semaphore_t *sema);

/* Increment the semaphore while interrupts are disabled. */
void sema_up_noirq(semaphore_t *sema);

/* Increment the semaphore (thus possible unblocking another
 * process), but limit the resulting semaphore state to a
 * maximum value of 'max'. Note that 'max' must be nonzero.
 * If you're using this, consider using a latch or condition
 * variable instead. */
void sema_limit_up(semaphore_t *sema, uint64_t max);

#endif
