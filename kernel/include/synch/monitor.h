#ifndef MONITOR_H
#define MONITOR_H

#include <std.h>
#include <proc_list.h>
#include <synch/lock.h>

typedef struct {
    lock_t *monitor_lock;
    proc_list_t processes_waiting;
} cond_var_t;

void create_cond_var(cond_var_t *var, lock_t *monitor_lock);

/* Wait until we're notified, and return (some time later) with the
 * monitor lock re-acquired. The monitor lock must be held on entry. */
void cond_var_wait(cond_var_t *var);

/* Wait until we're notified, or interrupted; if we are interrupted,
 * the monitor lock is _not_ held on exit. Again, the monitor lock
 * must be held on entry. */
bool cond_var_wait_interruptible(cond_var_t *var);

/* Wake up a single waiter. */
void cond_var_notify(cond_var_t *var);

/* Wake up all waiters. */
void cond_var_broadcast(cond_var_t *var);

#endif
