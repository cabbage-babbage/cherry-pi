#ifndef LATCH_H
#define LATCH_H

#include <std.h>
#include <synch/semaphore.h>

typedef struct {
    semaphore_t complete;
} latch_t;

/* Create an unset latch. */
void create_latch(latch_t *latch);

/* Set the latch, waking up all waiters. */
void latch_set(latch_t *latch);

/* Wait on a latch to be set. */
void latch_wait(latch_t *latch);

/* Wait, but non-blocking. */
bool latch_try_wait(latch_t *latch);

/* Wait, but interruptible. */
bool latch_wait_interruptible(latch_t *latch);

#endif
