#ifndef LOCK_H
#define LOCK_H

#include <synch/semaphore.h>
#include <irq.h>

/* Forward declare instead of including proc.h. */
struct proc;

/* Standard implementation of a lock via a 1-0 semaphore. */
typedef struct {
    semaphore_t sema;
    /* The process currently holding the lock, or NULL if
     * this lock is not currently acquired. */
    struct proc *owner;
} lock_t;

/*
 * Creates a lock in the memory specified.
 */
void create_lock(lock_t *lock);

/*
 * Wait until no other process owns the lock, and then
 * acquire it.
 */
void acquire_lock(lock_t *lock);

/* Try to acquire the lock: if no process owns the lock,
 * this atomically acquires it and returns true; otherwise,
 * this returns false.
 * That is, the return value indicates whether or not
 * this function successfully acquired the lock. */
bool try_acquire_lock(lock_t *lock);

/* Wait until no other process owns the lock, and then
 * acquire it. The waiting is interruptible, and if it is
 * (or was already) interrupted, the lock is unchanged and
 * the interrupt status is cleared. This returns true if the
 * lock was successfully acquired, or false if an interruption
 * occured. */
bool acquire_lock_interruptible(lock_t *lock);

/*
 * Release the lock. Must be called by the same process
 * that acquired the lock.
 */
void release_lock(lock_t *lock);

/* Release the lock while interrupts are disabled. */
void release_lock_noirq(lock_t *lock);

/*
 * Check if the lock is currently acquired, by any process.
 * Most useful in an interrupt context (i.e. interrupts disabled);
 * otherwise it gives no guarantee that the lock will not _soon_
 * be acquired by some process.
 */
USE_VALUE bool lock_is_acquired(lock_t *lock);


typedef struct {
    irq_state_t pushed_state;
} irq_lock_t;

/* Acquire/release the "IRQ" lock, i.e. disable interrupts on
 * acquiring and reenable interrupts on releasing. The lock
 * only needs to exist for the length of the acquire...release
 * sequence. */
void acquire_irq_lock(irq_lock_t *lock);
void release_irq_lock(irq_lock_t *lock);


typedef struct {
    bool prefer_writers;

    /* Protects readers and writers_waiting. */
    lock_t counts_lock;
    uint32_t readers;
    uint32_t writers_waiting; /* This also includes the currently
                                 active writer, if any. */

    /* A mutex, but not owned by any single process. */
    semaphore_t writers_allowed;

    /* For sanity checking, we can keep track of the current
     * writer and make sure that acquire/release write is
     * always called by the same process. */
    struct proc *writer;
} rw_lock_t;

/* If prefer_writers is true, as soon as a writer is waiting for
 * access no readers may gain access (even if there were already
 * readers). This ensures that writers cannot be starved.
 * If prefer_writers is false, then new readers may gain access
 * while a writer is waiting for access (i.e. waiting for all
 * readers to finish). */
void create_rw_lock(rw_lock_t *lock, bool prefer_writers);

/* Blocks until all writers have released the rw lock, and then
 * blocks future writers until we release_read() the rw lock. */
void acquire_read(rw_lock_t *lock);
/* Indicates that reading is complete. */
void release_read(rw_lock_t *lock);
/* Wait until writers have released the rw lock, and then acquire
 * the lock as a reader. The waiting is interruptible, and if it is
 * (or was already) interrupted, the lock is unchanged, the
 * interrupt status is cleared, and this returns false. Otherwise,
 * i.e. if the lock was successfully acquired, this returns true. */
bool acquire_read_interruptible(rw_lock_t *lock);

/* Blocks until all readers and all other writers have released
 * the rw lock, and then blocks all future readers and writers
 * until we release_write() the rw lock. If prefer_writers is
 * true, this even blocks any acquire_read() call until we
 * release_write(). */
void acquire_write(rw_lock_t *lock);
/* Indicates that writing is complete. */
void release_write(rw_lock_t *lock);
/* Wait until readers and other writers have released the rw lock,
 * and then acquire the lock as a writer. The waiting is interruptible,
 * and if it is (or was already) interrupted, the lock is unchanged,
 * the interrupt status is cleared, and this returns false. Otherwise,
 * if the lock was successfully acquired, this returns true. */
bool acquire_write_interruptible(rw_lock_t *lock);

#endif
