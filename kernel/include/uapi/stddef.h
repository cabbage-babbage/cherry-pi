#ifndef UAPI_STDDEF_H
#define UAPI_STDDEF_H

/* Request that a function be inlined. Must come before the function
 * declaration. */
#define INLINE __attribute__((always_inline)) inline
/* Make it a warning to ignore the return value of a function. Must
 * come before the function declaration. */
#define USE_VALUE __attribute__((warn_unused_result))
/* Explicitly ignore a value, for example returned by calling a function
 * defined with the USE_VALUE attribute. */
#define ignore_value(val) do { if (val) ; } while (0)
/* If val is false/0, report the error_msg as a compiler error. */
#define STATIC_ASSERT(val, error_msg) _Static_assert(val, error_msg)
/* Packed-struct attribute. */
#define PACKED __attribute__((packed))

typedef char bool;

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long uint64_t;

typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;
typedef signed long int64_t;

typedef void* ptr_t;

#define UINT8_MAX   ((uint8_t) 0xFF)
#define UINT16_MAX  ((uint16_t) 0xFFFF)
#define UINT32_MAX  ((uint32_t) 0xFFFFFFFF)
#define UINT64_MAX  ((uint64_t) 0xFFFFFFFFFFFFFFFF)

STATIC_ASSERT(sizeof(uint8_t) == 1, "sizeof(uint8_t) must be 1");
STATIC_ASSERT(sizeof(uint16_t) == 2, "sizeof(uint16_t) must be 2");
STATIC_ASSERT(sizeof(uint32_t) == 4, "sizeof(uint32_t) must be 4");
STATIC_ASSERT(sizeof(uint64_t) == 8, "sizeof(uint64_t) must be 8");
STATIC_ASSERT(sizeof(int8_t) == 1, "sizeof(int8_t) must be 1");
STATIC_ASSERT(sizeof(int16_t) == 2, "sizeof(int16_t) must be 2");
STATIC_ASSERT(sizeof(int32_t) == 4, "sizeof(int32_t) must be 4");
STATIC_ASSERT(sizeof(int64_t) == 8, "sizeof(int64_t) must be 8");
STATIC_ASSERT(sizeof(ptr_t) == 8, "sizeof(ptr_t) must be 8");

#define true (-1)
#define false 0

#define NULL ((void *) 0)

#endif
