#ifndef UAPI_SIGNAL_H
#define UAPI_SIGNAL_H

typedef uint8_t signal_nr_t;
#define signal_nr_spec "%u8"

#define SIGINT  ((signal_nr_t) 0)
#define SIGTERM ((signal_nr_t) 1)
#define SIGKILL ((signal_nr_t) 2)
#define SIGSTOP ((signal_nr_t) 3)
#define SIGCONT ((signal_nr_t) 4)
#define SIGCHLD ((signal_nr_t) 5)
#define SIGTTIN ((signal_nr_t) 6)
#define SIGTTOU ((signal_nr_t) 7)
#define NUM_SIG 8

typedef void (*signal_handler_t)(signal_nr_t sig, uint8_t buf_len, uint8_t* buf, void *context);
#define SIG_DFL ((signal_handler_t) 1)
#define SIG_IGN ((signal_handler_t) 2)

/* Standard SIGCHLD payloads:
 * - child exited:
 *     <EXITED>(1) <child id>(4) <exit code>(4)
 * - child stopped:
 *     <STOPPED>(1) <child id>(4)
 * - child resumed:
 *     <RESUMED>(1) <child id>(4)
 * - acquired new child (other than by fork(), of course):
 *     <NEWCHILD>(1) <child id>(4) <state>(1) */

#endif
