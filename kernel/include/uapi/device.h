#ifndef UAPI_DEVICE_H
#define UAPI_DEVICE_H

#include <uapi/stddef.h>

typedef uint64_t device_id_t;
#define device_id_spec "%u64"

#endif
