#ifndef UAPI_TTY_H
#define UAPI_TTY_H

typedef struct tty_config {
    bool in_convert_cr_to_lf;
    bool in_convert_ctrl_c_to_signal;
    bool in_line_mode;
    bool in_echo;

    bool out_convert_lf_to_crlf;
    bool out_stop;
} tty_config_t;

// copy the tty cfg to buf
#define TTY_IOCTL_GET_CONFIG 0
// copy the buf to tty cfg (also return the original cfg in the buf)
#define TTY_IOCTL_SET_CONFIG 1
// set the foreground process gid
#define TTY_IOCTL_SET_FG 2

#endif
