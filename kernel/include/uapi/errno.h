#ifndef UAPI_ERRNO_H
#define UAPI_ERRNO_H

#include <uapi/stddef.h>

typedef uint32_t errno_t;

/* Invalid pointer/buffer argument given. */
#define EFAULT      1
/* Any other invalid argument given. */
#define EINVAL      2
/* Operation not permitted. */
#define EPERM       3
/* Operation not supported. */
#define ENOTSUP     4
/* No such entity. */
#define ENOENT      5
/* Out of memory. */
#define ENOMEM      6
/* Input/output error. */
#define EIO         7
/* Requested resource is full. */
#define EFULL       8
/* Resource is temporarily unavailable. */
#define EAGAIN      9
/* Exec file format error. */
#define EEXEC       10
///* Mount depth too high. */
//#define EMDEPTH     11
/* File name too long. */
#define EFNAME      12
/* File system entity is not a directory. */
#define ENOTDIR     13
/* File system entity is a directory. */
#define EISDIR      14
/* File system entity is not a regular file. */
#define ENOTFILE    15
/* Entity already exists and cannot be created. */
#define EEXIST      16
/* File system entity is not a device. */
#define ENOTDEV     17
/* Is a pipe. */
#define EISPIPE     18
/* Other side of the pipe is closed. */
#define EPIPE       19
/* Operation interrupted. */
#define EINTR       20
/* No such syscall number. */
#define ENOCALL     21
/* No such ioctl number. */
#define ENOIOCTL    22
/* Got into a routing loop. */
#define ERTLOOP     23
/* Resource is unavailable. (Longer timeline than EAGAIN.) */
#define EUNAVAIL    24
/* Is a socket. */
#define EISSOCK     25
/* Not a socket. */
#define ENOTSOCK    26
/* Too big. */
#define E2BIG       27
/* Socket is unbound. */
#define EUNBOUND    28
/* Socket is already bound. */
#define EISBOUND    29
/* Socket has no remote. */
#define ENOREMOTE   30
/* Port is already in use. */
#define EPORTINUSE  31
/* Operation timed out. */
#define ETIMEDOUT   32
/* Out of range/bounds. */
#define ERANGE      33

#define FOREACH_ERRNO(macro_fn) \
    macro_fn("EFAULT", EFAULT) \
    macro_fn("EINVAL", EINVAL) \
    macro_fn("EPERM", EPERM) \
    macro_fn("ENOTSUP", ENOTSUP) \
    macro_fn("ENOENT", ENOENT) \
    macro_fn("ENOMEM", ENOMEM) \
    macro_fn("EIO", EIO) \
    macro_fn("EFULL", EFULL) \
    macro_fn("EAGAIN", EAGAIN) \
    macro_fn("EEXEC", EEXEC) \
    macro_fn("EFNAME", EFNAME) \
    macro_fn("ENOTDIR", ENOTDIR) \
    macro_fn("EISDIR", EISDIR) \
    macro_fn("ENOTFILE", ENOTFILE) \
    macro_fn("EEXIST", EEXIST) \
    macro_fn("ENOTDEV", ENOTDEV) \
    macro_fn("EISPIPE", EISPIPE) \
    macro_fn("EPIPE", EPIPE) \
    macro_fn("EINTR", EINTR) \
    macro_fn("ENOCALL", ENOCALL) \
    macro_fn("ENOIOCTL", ENOIOCTL) \
    macro_fn("ERTLOOP", ERTLOOP) \
    macro_fn("EUNAVAIL", EUNAVAIL) \
    macro_fn("EISSOCK", EISSOCK) \
    macro_fn("ENOTSOCK", ENOTSOCK) \
    macro_fn("E2BIG", E2BIG) \
    macro_fn("EUNBOUND", EUNBOUND) \
    macro_fn("EISBOUND", EISBOUND) \
    macro_fn("ENOREMOTE", ENOREMOTE) \
    macro_fn("EPORTINUSE", EPORTINUSE) \
    macro_fn("ETIMEDOUT", ETIMEDOUT) \
    macro_fn("ERANGE", ERANGE)

#endif
