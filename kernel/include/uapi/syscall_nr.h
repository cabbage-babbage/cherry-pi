#ifndef SYSCALL_NR_H
#define SYSCALL_NR_H

/* Note that the SYSCALL_EXIT number must be kept in sync with
 * processes/process_entry.s. All other syscall numbers are
 * arbitrary distinct 64-bit values. */

#define SYSCALL_EXIT            0
#define SYSCALL_WAIT            1
#define SYSCALL_SLEEP           2
#define SYSCALL_GET_DESC_CFG    3
#define SYSCALL_SET_DESC_CFG    4
#define SYSCALL_OPEN            5
#define SYSCALL_OPEN_AT         6
#define SYSCALL_CLOSE           7
#define SYSCALL_DUP             8
#define SYSCALL_READ            9
#define SYSCALL_WRITE           10
#define SYSCALL_FORK            11
#define SYSCALL_EXEC            12
#define SYSCALL_GET_HEAP        13
#define SYSCALL_SET_HEAP        14
#define SYSCALL_GET_PID         15
#define SYSCALL_CHDIR           16
#define SYSCALL_GET_CWD         17
#define SYSCALL_IOCTL           18
#define SYSCALL_READDIR         19
#define SYSCALL_STAT            20
#define SYSCALL_STAT_AT         21
#define SYSCALL_DUP_INTO        22
#define SYSCALL_MKPIPE          23
#define SYSCALL_SIGACTION       24
#define SYSCALL_SIGNAL          25
#define SYSCALL_SIGRETURN       26
#define SYSCALL_GET_PGID        27
#define SYSCALL_SET_PGID        28
#define SYSCALL_GET_TIME        29
#define SYSCALL_SEEK            30

/* Special syscall used by the kernel. Does nothing when called from
 * usermode. */
#define SYSCALL_KERN            99

#define SYSCALL_PANIC           100

#endif
