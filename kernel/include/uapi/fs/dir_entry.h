#ifndef UAPI_FS_DIR_ENTRY_H
#define UAPI_FS_DIR_ENTRY_H

#include <uapi/fs/def.h>

typedef struct {
    inode_nr_t inode_nr;

    /* Not null-terminated. Unused entries have empty names (name_len == 0), and used entries
     * must have nonempty names. */
    filename_len_t name_len;
    char name[FILE_NAME_MAX_LEN];
} __attribute__((packed)) dir_entry_t;

#endif
