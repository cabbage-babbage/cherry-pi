#ifndef UAPI_FS_INODE_H
#define UAPI_FS_INODE_H

#include <uapi/fs/def.h>

typedef enum {
    INODE_NONE     = 0, /* Unallocated/free inode. */
    INODE_FILE     = 1, /* Regular file. */
    INODE_DIR      = 2, /* Directory file. */
    INODE_SYMLINK  = 3, /* Symbolic link. */
    INODE_CHARDEV  = 4, /* Character device. */
    INODE_BLOCKDEV = 5, /* Block device. */
} inode_type_t;

typedef uint32_t inode_perm_t;
#define INODE_PERM_R ((inode_perm_t) 1 << 0)
#define INODE_PERM_W ((inode_perm_t) 1 << 1)
#define INODE_PERM_X ((inode_perm_t) 1 << 2)

#endif
