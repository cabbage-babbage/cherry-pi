#ifndef UAPI_FS_STAT_H
#define UAPI_FS_STAT_H

#include <uapi/stddef.h>
#include <uapi/fs/def.h>
#include <uapi/device.h>

typedef struct {
    uint64_t filesys_nr;
    inode_nr_t inode_nr;
    uint64_t refs;
    uint64_t size;
    inode_type_t type;
    inode_perm_t perm;
    device_id_t device_id;
    uint64_t time_created;
    uint64_t time_modified;
} stat_t;

#endif
