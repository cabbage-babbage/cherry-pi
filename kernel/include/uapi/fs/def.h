#ifndef UAPI_FS_DEF_H
#define UAPI_FS_DEF_H

#include <uapi/stddef.h>

typedef uint64_t inode_nr_t;

#define FILE_NAME_MAX_LEN 128
typedef uint8_t filename_len_t;
STATIC_ASSERT((filename_len_t) FILE_NAME_MAX_LEN == (uint64_t) FILE_NAME_MAX_LEN,
        "need to change filename_len_t to larger uint");

#define inode_nr_spec "%u64"
#define filename_len_spec "%u8"

#endif
