#ifndef VIRTUAL_MEMORY_H
#define VIRTUAL_MEMORY_H

#include <std.h>

/* Values that are most likely to be used externally.
 * ================================================= */
/* Number of bytes per page. */
#define PAGE_SIZE ((uint64_t) 4096)

/* Number of (least-significant) bits used from a virtual address for any page table walk. */
#define PTW_VA_BITS KERNEL_VA_BITS
STATIC_ASSERT(12 <= PTW_VA_BITS && PTW_VA_BITS <= 48, "PTW_VA_BITS must be between 12 and 48");

/* Starting level of page table walks.
 * Page table entries referred to by TTBR0_EL1 and TTBR1_EL1 must be entries of this level. */
#define PTW_START_LEVEL ((48 - PTW_VA_BITS) / 9)

/* The maximum possible size of the virtual address range covered by each of TTBR0_EL1
 * and TTBR1_EL1. */
#define VA_RANGE ((uint64_t) 1 << PTW_VA_BITS)

/* TTBR0 covers [0, TTBR0_END]. */
#define TTBR0_END (VA_RANGE - 1)
/* TTBR1 covers [TTBR1_START, MAX_UINT64]. */
#define TTBR1_START ((uint64_t) -VA_RANGE)
/* ================================================= */


/* TTBR sysreg attributes/flags */
#define TTBR_CnP ((uint64_t) 1)

/* page table entry attributes/flags: */
/* first, PTE attributes */
#define PTE_XN_EL0 ((uint64_t) 1 << 54) /* disallow EL0 execution */
#define PTE_XN_EL1 ((uint64_t) 1 << 53) /* disallow EL1 execution */
#define PTE_CONTIGUOUS ((uint64_t) 1 << 52)
#define PTE_DBM ((uint64_t) 1 << 51)
#define PTE_nG ((uint64_t) 1 << 11) /* not-global (ASID matching) */
#define PTE_AF ((uint64_t) 1 << 10) /* access flag */
#define PTE_NO_SH ((uint64_t) 0 << 8) /* non-shareable */
#define PTE_OUT_SH ((uint64_t) 2 << 8) /* outer-shareable */
#define PTE_INN_SH ((uint64_t) 3 << 8) /* inner-shareable */
#define PTE_RW ((uint64_t) 0 << 7) /* read/write */
#define PTE_RO ((uint64_t) 1 << 7) /* read-only */
#define PTE_KERNEL ((uint64_t) 0 << 6) /* EL0 cannot access for data */
#define PTE_USER ((uint64_t) 1 << 6) /* EL0 can access for data */

/* second, PTE memory types */
#define PTE_MEM ((uint64_t) 0 << 2)
#define PTE_MMIO ((uint64_t) 1 << 2)
#define PTE_NOCACHE ((uint64_t) 2 << 2)

/* third, PTE types */
#define PTE_UNMAPPED ((uint64_t) 0) /* invalid PTE at any level */
#define PTE_BLOCK ((uint64_t) 1)    /* block entry, levels 1-2 */
#define PTE_TABLE ((uint64_t) 3)    /* table entry, levels 0-2 */
#define PTE_PAGE ((uint64_t) 3)     /* block entry, level 3 */

/* translation control register attributes/flags */
#define TCR_TCMA1 ((uint64_t) 1 << 58)
#define TCR_TCMA0 ((uint64_t) 1 << 57)
#define TCR_EOPD1 ((uint64_t) 1 << 56)
#define TCR_EOPD0 ((uint64_t) 1 << 55)
#define TCR_NFD1 ((uint64_t) 1 << 54)
#define TCR_NFD0 ((uint64_t) 1 << 53)
#define TCR_TBID1 ((uint64_t) 1 << 52)
#define TCR_TBID0 ((uint64_t) 1 << 51)
#define TCR_HWU162 ((uint64_t) 1 << 50)
#define TCR_HWU161 ((uint64_t) 1 << 49)
#define TCR_HWU160 ((uint64_t) 1 << 48)
#define TCR_HWU159 ((uint64_t) 1 << 47)
#define TCR_HWU062 ((uint64_t) 1 << 46)
#define TCR_HWU061 ((uint64_t) 1 << 45)
#define TCR_HWU060 ((uint64_t) 1 << 44)
#define TCR_HWU059 ((uint64_t) 1 << 43)
#define TCR_HPD1 ((uint64_t) 1 << 42)
#define TCR_HPD0 ((uint64_t) 1 << 41)
#define TCR_HD ((uint64_t) 1 << 40)
#define TCR_HA ((uint64_t) 1 << 39)
#define TCR_TBI1 ((uint64_t) 1 << 38)
#define TCR_TBI0 ((uint64_t) 1 << 37)
#define TCR_AS ((uint64_t) 1 << 36)
#define TCR_IPS(val) ((uint64_t) (val) << 32)
#define TCR_TG1(val) ((uint64_t) (val) << 30)
#define TCR_SH1(val) ((uint64_t) (val) << 28)
#define TCR_ORGN1(val) ((uint64_t) (val) << 26)
#define TCR_IRGN1(val) ((uint64_t) (val) << 24)
#define TCR_EPD1 ((uint64_t) 1 << 23)
#define TCR_A1 ((uint64_t) 1 << 22)
#define TCR_T1SZ(val) ((uint64_t) (val) << 16)
#define TCR_TG0(val) ((uint64_t) (val) << 14)
#define TCR_SH0(val) ((uint64_t) (val) << 12)
#define TCR_ORGN0(val) ((uint64_t) (val) << 10)
#define TCR_IRGN0(val) ((uint64_t) (val) << 8)
#define TCR_EPD0 ((uint64_t) 1 << 7)
#define TCR_T0SZ(val) ((uint64_t) (val) << 0)

/* some helper macros */
#define BIT_OFF(n) (~((uint64_t) 1 << (n)))
#define BIT_ON(n) ((uint64_t) 1 << (n))

/* Initialize virtual memory in the most basic way possible: flat-map
 * (in a 1:1 way) both low and high virtual address ranges to physical
 * memory. Used by the kernel entrypoint (entry.s), and should not be
 * called from anywhere else. */
void initialize_basic_virtual_memory();

/* Initialize virtual memory with finer-grained control over kernel/user
 * privileges, read/write access, etc. */
void initialize_virtual_memory();

typedef uint64_t pte_t;

/* Specify this as the physical address to unmap a virtual address range. */
#define UNMAPPED ((uint64_t) 0xFFFFFFFFFFFFFFFF)

/* Fail if in the process of mapping the given virtual address range to the
 * given physical address range, we would have to overwrite an existing block
 * or page entry. That is, only allow replacing unmapped entries.
 * If used with UNMAPPED, can (incidentally) be used to check if any part of
 * the given virtual address range is mapped. */
#define MAP_CREATE ((uint64_t) 1 << 0)
/* Do not check alignment of the virtual address range and physical address;
 * instead, automatically align the ranges to the block size at the given
 * target level. */
#define MAP_ALIGN ((uint64_t) 1 << 1)
///* While performing the mapping, invalidate only the TLB entries whose associated
// * PTEs are changing. */
//#define MAP_TLBI_RANGE ((uint64_t) 1 << 2)
///* After performing the mapping, invalidate the entire TLB.
// * MAP_TLBI_RANGE is ignored if it is also specified; we only need to invalidate once. */
//#define MAP_TLBI_ALL ((uint64_t) 1 << 3)

#define MAP_RES_SUCCESS     ((int32_t) 0)
#define MAP_RES_OOM         ((int32_t) 1)
#define MAP_RES_OVERWRITE   ((int32_t) 2)
#define MAP_RES_ALIGNMENT   ((int32_t) 3)

/*
 * Update the page table tree starting with the given page table and level (0-3)
 * so that we end up with blocks at the given target level (table_level to 3) in the page tree
 * referring to the given virtual address range and specifying a mapping to the given physical
 * address range, with the specified access attributes and memory type.
 * Note that the virtual address range must lie within the virtual address range owned
 * by the page table specified.
 * If the phys_addr_start has the value UNMAPPED, then the attribs and mem_type are ignored
 * and we will unmap the specified virtual address range instead of mapping it.
 * Note that on any failure, the given page table may have been updated into an inconsistent
 * state. It is guaranteed to be well-formed enough to be able to free the table, but should
 * not be relied upon for use in an actual virtual memory map (i.e. TTBR0 or TTBR1).
 *
 * Returns an error code describing any of the following results:
 * - MAP_RES_SUCCESS  : success
 * - MAP_RES_OOM      : out of memory when allocating new page tables
 * - MAP_RES_OVERWRITE: MAP_CREATE specified, but we would have had to overwrite an existing block or page entry
 * - MAP_RES_ALIGNMENT: invalid alignment of virtual or physical addresses, and MAP_ALIGN was not specified
 */
USE_VALUE int32_t map_table_virt_to_phys(
        /* the page table to update */
        pte_t *page_table,
        /* start of the VA range which (if the MMU were walking the PTEs)
         * would bring us to the given page table */
        uint64_t page_table_base,
        /* the level of the table in the page table tree (0-3) */
        uint32_t page_table_level,
        /* the target level of blocks we will generate to cover the virtual
         * address range; must be in the range 1-3 (unless phys_addr_start is UNMAPPED,
         * in which case the target_level can be 0), but also must be at
         * least the table_level */
        uint32_t target_level,
        /* the virtual address range to map; the start and end must be aligned to the
         * correct block size depending on the target level (i.e. 1GB for level 1, 2MB
         * for level 2, 4KB for level 3) unless MAP_ALIGN is specified */
        uint64_t virt_addr_start, uint64_t length,
        /* the physical address to assign to the start of the virtual address range;
         * must be either the value UNMAPPED, or be aligned to the correct block size
         * depending on the target level (i.e. 1GB for level 1, 2MB for level 2, 4KB
         * for level 3) unless MAP_ALIGN is specified */
        uint64_t phys_addr_start,
        /* the attributes and memory type to apply to the new block PTEs;
         * ignored if phys_addr_start is UNMAPPED */
        uint64_t attribs, uint64_t mem_type,
        /* mapping options, using the MAP_** flags */
        uint64_t mapping_flags);

USE_VALUE int32_t map_level_virt_to_phys(
        pte_t *ttbr0, pte_t *ttbr1,
        /* the target level of blocks we will generate to cover the virtual
         * address range; must be in the range 1-3 */
        uint32_t target_level,
        /* the virtual address range to map; like above, must be aligned to the target
         * level block size */
        uint64_t virt_addr_start, uint64_t length,
        /* the physical address to assign to the start of the virtual address range;
         * like above, must be aligned to the target level block size */
        uint64_t phys_addr_start,
        /* the attributes and memory type to apply to the new block PTEs */
        uint64_t attribs, uint64_t mem_type,
        /* mapping options, using the MAP_** flags */
        uint64_t mapping_flags);

USE_VALUE int32_t map_virt_to_phys(
        /* If null, use the page tables referenced by the system registers TTBR0/1_EL1. */
        pte_t *ttbr0, pte_t *ttbr1,
        /* the virtual address range to map; like above, must be aligned to the target
         * level block size */
        uint64_t virt_addr_start, uint64_t length,
        /* the physical address to assign to the start of the virtual address range;
         * like above, must be aligned to the target level block size */
        uint64_t phys_addr_start,
        /* the attributes and memory type to apply to the new block PTEs */
        uint64_t attribs, uint64_t mem_type,
        /* mapping options, using the MAP_** flags */
        uint64_t mapping_flags);

USE_VALUE int32_t map_flat_kernel_virt_to_phys(
        /* If null, use the page tables referenced by the system register TTBR1_EL1. */
        pte_t *ttbr1,
        /* the virtual address range to map; like above, must be aligned to the target
         * level block size */
        uint64_t virt_addr_start, uint64_t length,
        /* the attributes and memory type to apply to the new block PTEs */
        uint64_t attribs, uint64_t mem_type,
        /* mapping options, using the MAP_** flags */
        uint64_t mapping_flags);

/* Recursively free a page table (existing at a particular level).
 * Optionally free the blocks of memory pointed to by the page table.
 * (In this case, each block PTE must have allocated by a different
 * kmalloc call.) */
void free_page_table(pte_t *page_table, uint32_t level, bool free_referenced_blocks);

/* Invalidate a particular TLB entry. */
void invalidate_tlb_entry(uint64_t virt_addr);
/* Invalidate the entire TLB. */
void invalidate_tlb();

USE_VALUE uint64_t kernel_virt_to_phys(ptr_t virt_addr);
USE_VALUE ptr_t phys_to_kernel_virt(uint64_t phys_addr);

USE_VALUE bool is_user_virt_addr(ptr_t virt_addr);
USE_VALUE bool is_kernel_virt_addr(ptr_t virt_addr);

// Checks in the current virtual map (i.e. current TTBR0/TTBR1)
USE_VALUE bool virt_addr_range_has_attribs(ptr_t virt_addr_start, uint64_t length,
        bool require_read, bool require_write,
        bool require_user_access, bool require_kernel_access,
        bool require_user_executable, bool require_kernel_executable);

/* Find the minimum target level whose blocks contain the given virtual address range,
 * and so that no lower target level also has blocks which can contain the given
 * range but with less "wasted space" on the sides. */
// TODO: explain this way better lol
// idea is that if the range is already aligned on (say) a 2MB boundary,
// we should return "level 2", but if (say) the end doesn't lie on a 2MB
// boundary but instead on a 4KB boundary, we have to return "level 3".
USE_VALUE uint32_t best_target_level(uint64_t virt_addr_start, uint64_t length);

/* Encode a PTE that references a next-level table residing at the given address. */
USE_VALUE pte_t pte_encode_table(uint64_t phys_addr);

/* Encode a block/page PTE that references a block of physical memory,
 * applying the given attributes and memory type, and intended to be placed
 * in a particular given level of page table.
 * Note that level 0 PTEs cannot be block entries, and only levels up to 3
 * even exist. (Level 3 technically doesn't have block entries, but rather
 * page entries; it's essentially the same thing, but with a slightly different
 * encoding.) */
USE_VALUE pte_t pte_encode_block(uint32_t level, uint64_t attribs, uint64_t mem_type,
        uint64_t phys_addr);

/* Encode a PTE that does not reference any memory, causing an exception if a
 * virtual address is accessed in the PTE's range. */
USE_VALUE pte_t pte_encode_unmapped();

USE_VALUE bool pte_is_table(pte_t entry, uint32_t level);
USE_VALUE bool pte_is_block(pte_t entry, uint32_t level);
USE_VALUE bool pte_is_unmapped(pte_t entry);

USE_VALUE uint64_t table_pte_phys_addr(pte_t entry);

USE_VALUE uint64_t block_pte_phys_addr(pte_t entry, uint64_t level);
USE_VALUE uint64_t block_pte_attribs(pte_t entry, uint64_t level);
USE_VALUE uint64_t block_pte_mem_type(pte_t entry);

#endif
