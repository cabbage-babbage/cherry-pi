#ifndef HEAP_H
#define HEAP_H

#include <std.h>

void initialize_heap();

/* Allocate a physical page and return a kernel virtual address pointing to
 * the start of the page. Returns NULL on out-of-memory. */
USE_VALUE ptr_t pmalloc_page();
/* Allocate a physically contiguous sequence of pages and return a kernel virtual
 * address pointing to the start of the first page. Returns NULL on out-of-memory.
 * num_pages must not be 0. */
USE_VALUE ptr_t pmalloc_pages(uint64_t num_pages);
/* Free pages allocated by pmalloc_page() or pmalloc_pages(). */
void pfree_pages(ptr_t start_page);

USE_VALUE ptr_t pmalloc(uint64_t bytes);
void pfree(ptr_t mem);

// TODO: vmalloc -- allocate memory and map it to new kernel virtual addresses,
//       and does not have to be physically contiguous
// of course also vfree -- deallocates the memory, and unmaps the virtual addresses

#endif
