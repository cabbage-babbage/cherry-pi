#ifndef INODE_H
#define INODE_H

#include <std.h>
#include <fs/def.h>
#include <device/device.h>
#include <uapi/fs/inode.h>
#include <uapi/fs/dir_entry.h>

/* Inode 0 does not exist, so can always be used as the number of an invalid/missing
 * inode. */

/* We treat this as another inode_type_t, but it should never appear as the type of
 * an actual inode in a file system. Instead, this is the 'type' of the read or write
 * half of an anonymous pipe. This number just has to be distinct from all the INODE_*
 * types from <uapi/fs/inode.h>. */
#define FILE_PIPE ((inode_type_t) 0x123456)

#define INODE_EMBEDDED_CONTENT_SIZE 256
#define INODE_NUM_DIRECT 21

typedef struct inode {
    inode_nr_t nr;
    /* Counts how many references there are to this inode.
     * This includes, in particular, the root-directory reference as well as
     * directory entries referring to this inode. It does _not_ include the
     * . and .. entries of each directory. */
    uint64_t refs;
    /* Inode linked list prev/next, used for the free list. These references
     * are not counted by the refs field. */
    inode_nr_t prev, next;

    /* Entry size, in bytes. */
    uint64_t size;
    /* Entry type. */
    inode_type_t type;

    /* Specified by INODE_PERM_* flags. */
    inode_perm_t perm;

    /* Timestamps. */
    // TODO: make this not just relative to system startup...
    uint64_t time_created;
    uint64_t time_modified;

    /* If any of these block numbers are 0, when according to the inode size
     * they should not be, it means that the file was extended by a write that
     * did not fill in the intermediate blocks: it is a sparse file. */
    block_nr_t blocks_direct[INODE_NUM_DIRECT];
    block_nr_t block_indirect;
    block_nr_t block_double_indirect;
    block_nr_t block_triple_indirect;

    union {
        /* Embedded data, for types: INODE_FILE, INODE_DIR, INODE_SYMLINK. */
        uint8_t embedded[INODE_EMBEDDED_CONTENT_SIZE];

        /* Device id, for types: INODE_CHARDEV, INODE_BLOCKDEV. */
        device_id_t device_id;
    } content;
} __attribute__((packed)) inode_t;

STATIC_ASSERT(sizeof(inode_t) <= BLOCK_SIZE, "inode_t must fit into a single block");

/* A directory is simply a file containing an array of dir_entry_t. The file size (stored in
 * the directory's inode) must therefore be a multiple of sizeof(dir_entry_t), and indicates
 * how many entries there are. */
STATIC_ASSERT(sizeof(dir_entry_t) == 137, "dir_entry_t has changed; go update make_cpfs and then reset this");

#endif
