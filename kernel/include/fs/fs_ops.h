#ifndef INODE_OPS_H
#define INODE_OPS_H

#include <fs/inode.h>
//#include <fs/block_ops.h>

typedef struct fs_ops {
    const char (*name)(struct fs_ops *ops);

    /* Allocate and initialize a new inode with 0 length. NULL if not supported. */
    int64_t (*alloc_inode)(struct fs_ops *ops, inode_nr_t *inode_nr,
            inode_type_t type, inode_perm_t perm);
    /* Free the inode and all of its referenced file data. NULL if not supported. */
    int64_t (*free_inode)(struct fs_ops *ops, inode_nr_t inode_nr);

    /* Read an inode into the dst. */
    int64_t (*read_inode)(struct fs_ops *ops, inode_nr_t inode_nr, inode_t *dst);
    /* Write an inode from the src. */
    int64_t (*write_inode)(struct fs_ops *ops, inode_nr_t inode_nr, const inode_t *src);

    /* These may assume that there is some external reader/writer synchronization occuring,
     * so multiple reads may be requested in parallel but all writes are serialized (along
     * with interleaved reads).
     * NULL if not supported. */
    int64_t (*file_read)(struct fs_ops *ops, inode_t *inode,
            uint64_t offset,
            uint8_t *buf, uint64_t buf_size,
            bool blocking, uint64_t *bytes_read);
    int64_t (*file_write)(struct fs_ops *ops, inode_t *inode,
            uint64_t offset,
            const uint8_t *buf, uint64_t buf_size,
            bool blocking, uint64_t *bytes_written);

    /* Internal callback data. */
    void *aux;
} fs_ops_t;

#endif
