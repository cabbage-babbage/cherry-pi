#ifndef BLOCK_OPS_H
#define BLOCK_OPS_H

#include <std.h>
#include <uapi/errno.h>
#include <fs/def.h>

typedef struct block_ops {
    //errno_t (*init)(struct block_ops *ops);

    //errno_t (*alloc_inode_block)(struct block_ops *ops, inode_nr_t *inode_nr);
    //errno_t (*free_inode_block)(struct block_ops *ops, inode_nr_t inode_nr);

    //errno_t (*read_inode_block)(struct block_ops *ops, inode_nr_t inode_nr, inode_t *dst);
    //errno_t (*write_inode_block)(struct block_ops *ops, inode_nr_t inode_nr, const inode_t *src);

    //errno_t (*alloc_data_block)(struct block_ops *ops, block_nr_t *block_nr);
    //errno_t (*free_data_block)(struct block_ops *ops, block_nr_t block_nr);

    //errno_t (*read_data_block)(struct block_ops *ops, block_nr_t *block_nr);
    //errno_t (*write_data_block)(struct block_ops *ops, block_nr_t block_nr, const uint8_t *src);

    errno_t (*read_block)(struct block_ops *ops, block_nr_t block_nr, uint8_t *dst);
    errno_t (*write_block)(struct block_ops *ops, block_nr_t block_nr, const uint8_t *src);

    /* Internal callback data. */
    void *aux;
} block_ops_t;

#endif
