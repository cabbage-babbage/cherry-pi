#ifndef INITRD_H
#define INITRD_H

#include <std.h>
#include <fs/block_ops.h>

typedef struct {
    ptr_t initrd_start;
    uint64_t num_blocks;
} initrd_block_ops_aux_t;
/* The aux structure need not be initialized (but aux must be a valid pointer). */
void get_initrd_block_ops(block_ops_t *dst, initrd_block_ops_aux_t *aux);

#endif
