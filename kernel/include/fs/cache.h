#ifndef FS_CACHE_H
#define FS_CACHE_H

#include <std.h>
#include <fs/def.h>
#include <fs/device.h>

/* Must be called before any other functions in this header. */
void initialize_fs_cache();

/* Blocking read. buf must be a least BLOCK_SIZE byes. */
int64_t fs_cache_read(fs_device_t *dev, block_nr_t block_nr, uint8_t *buf);

/* Blocking write. buf must be a least BLOCK_SIZE byes. */
int64_t fs_cache_write(fs_device_t *dev, block_nr_t block_nr, const uint8_t *buf);

#endif
