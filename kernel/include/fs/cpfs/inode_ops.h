#ifndef CPFS_INODE_OPS
#define CPFS_INODE_OPS

#include <std.h>
#include <fs/def.h>
#include <fs/inode_ops.h>
#include <fs/block_ops.h>
#include <synch/lock.h>

typedef struct {
    uint64_t num_blocks;
    uint64_t num_inodes;
    inode_nr_t free_inodes_head;
    block_nr_t free_data_head;
} __attribute__((packed)) cpfs_header_t;

typedef struct {
    block_ops_t *block_ops;
    /* Protects access to the metadata block (block 0). */
    rw_lock_t cpfs_metadata_lock;
    /* Cached copy of the header/metadata fields. */
    cpfs_header_t header;
} cpfs_inode_ops_aux_t;
/* The aux parameter must have block_ops assigned; all other fields are overwritten,
 * so it is useless to initialize them. */
void get_cpfs_inode_ops(inode_ops_t *dst, cpfs_inode_ops_aux_t *aux);

#endif
