#ifndef INODE_OPS_H
#define INODE_OPS_H

#include <std.h>
#include <fs/inode.h>
#include <device/device.h>
#include <uapi/errno.h>

typedef struct inode_ops {
    //errno_t (*init)(struct inode_ops *ops);

    inode_nr_t root_nr;

    /* Allocate and initialize a new inode with 0 length and 1 reference. If the type
     * is INODE_CHARDEV or INODE_BLOCKDEV, the new inode should have content.device_id
     * set to the given device id; otherwise, device_id can be ignored. NULL if not
     * supported. */
    errno_t (*alloc_inode)(struct inode_ops *ops, inode_nr_t *inode_nr,
            inode_type_t type, inode_perm_t perm, device_id_t device_id);
    /* Free the inode and all of its referenced file data. NULL if not supported. */
    errno_t (*free_inode)(struct inode_ops *ops, inode_nr_t inode_nr);

    /* Read an inode into the dst. */
    errno_t (*read_inode)(struct inode_ops *ops, inode_nr_t inode_nr, inode_t *dst);
    /* Write an inode from the src. */
    errno_t (*write_inode)(struct inode_ops *ops, inode_nr_t inode_nr, const inode_t *src);

    /* These may assume that there is some external reader/writer synchronization occuring,
     * so multiple reads may be requested in parallel but all writes are serialized (along
     * with interleaved reads). */
    errno_t (*read_file)(struct inode_ops *ops, inode_nr_t inode_nr, uint64_t offset,
            uint8_t *buf, uint64_t buf_size,
            uint64_t *bytes_read);
    /* If append is set, offset should be is ignored. */
    errno_t (*write_file)(struct inode_ops *ops, inode_nr_t inode_nr, uint64_t offset,
            bool append,
            const uint8_t *buf, uint64_t buf_size,
            uint64_t *bytes_written);

    /* Internal callback data. */
    void *aux;
} inode_ops_t;

#endif
