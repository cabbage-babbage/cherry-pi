#ifndef FILESYS_H
#define FILESYS_H

#include <std.h>
#include <fs/def.h>
#include <fs/file_ops.h>
#include <device/device.h>
#include <uapi/errno.h>

typedef struct {
    const char *name;
    uint64_t id; /* Unique, and may change between reboots. */
    file_ops_t ops;
    uint64_t mount_count;
} filesys_t;

typedef struct {
    inode_nr_t dir;
    const char *name;
} mount_point_t;

struct submount;

typedef struct mount {
    struct mount *parent;
    /* Specifies where in the parent we're mounted. */
    mount_point_t point;

    filesys_t *filesys;
    struct submount *submounts_head;
    uint32_t depth;
} mount_t;

typedef struct submount {
    struct submount *next;
    mount_point_t point;
    mount_t mount;
} submount_t;

/* Uniquely describes a file location among all existing mounts. */
typedef struct {
    mount_t *mount;
    inode_nr_t inode_nr;
} fs_locator_t;

typedef struct {
    mount_t *mount;
    open_inode_t *file;
} open_file_t;

void initialize_filesys();

/* This may only be called once. */
USE_VALUE errno_t filesys_mount_root(filesys_t *filesys);

/* Since we can only mount filesys_t's and not mount_t's, it is impossible to produce
 * a mount cycle. */
USE_VALUE errno_t filesys_mount(mount_t *mount, mount_point_t point, filesys_t *filesys);
/* Unmount the given mount, and all its submounts (recursively). The sync flag
 * specifies whether or not all dirty blocks in the mounted filesystem should be
 * synced to the associated device. (Usually we want syncing.) */
USE_VALUE errno_t filesys_unmount(mount_t *mount, bool sync);

USE_VALUE fs_locator_t open_file_locator(open_file_t file);

/* filesys_open(...) is just filesys_open_at(filesys root dir, ...).
 * On error, file is undefined and need not (indeed, should not) be closed. */
USE_VALUE errno_t filesys_open(const char *path, open_file_t *file);
USE_VALUE errno_t filesys_open_at(fs_locator_t dir, const char *path, open_file_t *file);

/* Starting at the given dir, walk the path and write the final location to dst.
 * On error, the value of dst is undefined. */
USE_VALUE errno_t filesys_walk_at(fs_locator_t dir, const char *path, fs_locator_t *dst);
/* Same, but start at the root directory instead if the specified dir. */
USE_VALUE errno_t filesys_walk(const char *path, fs_locator_t *dst);

USE_VALUE errno_t filesys_dup(open_file_t file);

void filesys_close(open_file_t file);

USE_VALUE errno_t filesys_find_entry(open_file_t dir, const char *name, inode_nr_t *entry_nr);

/* Create an entry with given type and permissions at the specified path relative to a
 * known directory. If keep_open is true, this opens the new (or existing) entry and writes
 * it to file; otherwise, file is ignored (and may be NULL). is_new is set according to
 * whether this function created a new entry or the path referred to an already-existing
 * entry.
 * device_id is used only for inode types INODE_CHARDEV and INODE_BLOCKDEV. */
USE_VALUE errno_t filesys_create_at(fs_locator_t dir, const char *path, bool keep_open,
            inode_type_t type, inode_perm_t perm, device_id_t device_id,
            bool *is_new, open_file_t *file);
USE_VALUE errno_t filesys_create(const char *path, bool keep_open,
            inode_type_t type, inode_perm_t perm, device_id_t device_id,
            bool *is_new, open_file_t *file);

USE_VALUE errno_t filesys_delete_entry(open_file_t dir, inode_nr_t entry_nr);

USE_VALUE errno_t filesys_get_inode(open_file_t file, inode_t *inode);

USE_VALUE errno_t filesys_read_file(open_file_t file, uint64_t offset,
        uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_read);

USE_VALUE errno_t filesys_write_file(open_file_t file, uint64_t offset,
        bool append,
        const uint8_t *buf, uint64_t buf_size,
        uint64_t *bytes_written);

#endif
