#ifndef FILE_OPS_H
#define FILE_OPS_H

#include <std.h>
#include <fs/inode.h>
#include <fs/inode_ops.h>
#include <synch/lock.h>
#include <uapi/errno.h>

typedef struct {
    /* This protects the open_count, the inode, and all the file content. */
    rw_lock_t lock;

    inode_nr_t inode_nr;
    uint64_t open_count;
} open_inode_t;

typedef struct file_ops {
    inode_nr_t root_nr;

    /* This must return the same pointer, given the same inode_nr.
     * open_file() automatically duplicates the file if it is
     * already open (and otherwise opens it anew, with open_count
     * set to 1).
     * The inode_nr should be either root_nr, or an inode number
     * obtained from a find_entry() or create_entry() call; the behavior
     * is undefined otherwise, as this function may not be able to detect
     * that the inode number is invalid */
    errno_t (*open_file)(struct file_ops *ops, inode_nr_t inode_nr, open_inode_t **file);
    /* Atomically increments the open_count. */
    errno_t (*dup_file)(struct file_ops *ops, open_inode_t *file);
    /* Atomically decrements the open_count, and if the file has no
     * accessors, can deallocate associated memory. That is, the file
     * should not be used at all once close_file() returns.
     * This should not fail; failures are logged and then ignored. */
    errno_t (*close_file)(struct file_ops *ops, open_inode_t *file);

    /* Find an entry's inode_nr by name in a directory. */
    // TODO: this should _not_ release_read() the dir, and leave that to the caller
    // such as filesys_open_at, so that it can walk the fs with the right synchronization
    errno_t (*find_entry)(struct file_ops *ops, open_inode_t *dir, const char *name, inode_nr_t *entry_nr);
    /* Create a new entry in a directory. On success, is_new is set to true if the entry
     * is actually new, and false if there is already an entry with the given name;
     * furthermore entry_nr is set to the inode number of the new (or existing) entry with
     * the given name.
     * If the new entry is a directory (type == INODE_DIR), its . and .. entries must be
     * added by this function; if the new entry is a character or block device (type is
     * INODE_CHARDEV or INODE_BLOCKDEV), then the new entry's content.device_id must be
     * set to the given device_id; and otherwise, the new entry must be empty. */
    // TODO: this should optionally open the entry, atomically.
    errno_t (*create_entry)(struct file_ops *ops, open_inode_t *dir, const char *name,
            inode_type_t type, inode_perm_t perm, device_id_t device_id,
            bool *is_new, inode_nr_t *entry_nr);
    /* Delete an entry from a directory. */
    errno_t (*delete_entry)(struct file_ops *ops, open_inode_t *dir, inode_nr_t entry_nr);

    /* Load the inode itself into the 'inode' destination. */
    errno_t (*get_inode)(struct file_ops *ops, open_inode_t *file, inode_t *inode);

    errno_t (*read_file)(struct file_ops *ops, open_inode_t *file, uint64_t offset,
            uint8_t *buf, uint64_t buf_size,
            uint64_t *bytes_read);
    /* If append is set, offset is ignored. */
    errno_t (*write_file)(struct file_ops *ops, open_inode_t *file, uint64_t offset,
            bool append,
            const uint8_t *buf, uint64_t buf_size,
            uint64_t *bytes_written);

    /* Internal callback data. */
    void *aux;
} file_ops_t;

typedef struct {
    inode_ops_t *inode_ops;
    void *open_files;
} default_file_ops_aux_t;
/* The aux parameter must have inode_ops assigned, but open_files will be overwritten
 * (so there's no point initializing it to anything). */
void get_default_file_ops(file_ops_t *dst, default_file_ops_aux_t *aux);

#endif
