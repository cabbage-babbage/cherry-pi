#ifndef FS_DEVICE_H
#define FS_DEVICE_H

#include <fs/def.h>
#include <uapi/errno.h>

typedef struct fs_device {
    /* The device number is unique in a running system, but has no particular
     * ordering or persistence across reboots. */
    fs_device_nr_t device_nr;

    /* Specifies if reads/writes should be cached in the block cache, or always
     * passed through. */
    bool cacheable;

    /* Performs a (blocking) read from the given block into the buf (which may be
     * assumed to be a least BLOCK_SIZE bytes in size). Should return 0 on success,
     * and -errno on failure. */
    errno_t (*read)(struct fs_device *dev, block_nr_t block_nr, uint8_t *buf);
    /* Performs a (blocking) write from the buf into the given block. (The buf may
     * be assumed to have at least BLOCK_SIZE bytes.). Should return 0 on success,
     * and -errno on failure. */
    errno_t (*write)(struct fs_device *dev, block_nr_t block_nr, const uint8_t *buf);

    /* Internal callback data. */
    void *aux;
} fs_device_t;

#endif
