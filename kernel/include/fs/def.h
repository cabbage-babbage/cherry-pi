#ifndef FS_DEF_H
#define FS_DEF_H

#include <std.h>
#include <uapi/fs/def.h>

#define BLOCK_SIZE 512

typedef uint64_t block_nr_t;
typedef uint32_t fs_device_nr_t;

STATIC_ASSERT(BLOCK_SIZE % sizeof(block_nr_t) == 0,
        "a block must be able to hold an array of block_nr's without padding");

#define BLOCK_NRS_PER_BLOCK (BLOCK_SIZE / sizeof(block_nr_t))

#define block_nr_spec "%u64"
#define fs_device_nr_spec "%u32"

#endif
