#ifndef TMPFS_INODE_OPS
#define TMPFS_INODE_OPS

#include <fs/inode_ops.h>

void get_tmpfs_inode_ops(inode_ops_t *dst);

#endif
