#ifndef EXCEPTION_LEVEL_H
#define EXCEPTION_LEVEL_H

#include <std.h>
#include <irq.h> /* For DAIF_D, ..., DAIF_F. */

void switch_from_el1_to_el0(uint64_t daif_mask);

void initialize_el0_and_el1();

USE_VALUE uint32_t current_exception_level();

#endif
