#ifndef STD_H
#define STD_H

#include <uapi/stddef.h>

/*
 * Use this to define a variable whose address corresponds
 * to an externally-defined label. The address of the variable
 * has type uint8_t*.
 */
#define extern_symbol extern uint8_t

/*
 * STRINGIFY(...) macro-expands the argument
 * and then places it in a string.
 */
#define STRINGIFY(code) STRINGIFY_BASE(code)
#define STRINGIFY_BASE(text) #text

#define OFFSET_OF(struct_name, field) ((uint64_t) &((struct_name *) 0)->field)

#endif
