#ifndef SYSCALL_H
#define SYSCALL_H

#include <std.h>

/* Handles a syscall. This is executed in the CONTEXT_SYSCALL context of the calling
 * process, so in partular it is executed with interrupts enabled, and with amenities
 * like locks and memory allocation available.
 * The argN parameters are those arguments passed by the process, and if required
 * can go up to arg7.
 * The return value of this function is passed back to the process as the return value
 * of the syscall itself (or should be ignored, depending on the has_return_value output). */
USE_VALUE uint64_t syscall_base(uint64_t syscall_nr,
        uint64_t arg0, uint64_t arg1, uint64_t arg2, uint64_t arg3,
        bool *has_return_value);

/* Make a syscall from a kernel process. */
int64_t _syscall0(uint64_t syscall_nr);
int64_t _syscall1(uint64_t syscall_nr, uint64_t arg0);
int64_t _syscall2(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1);
int64_t _syscall3(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                  uint64_t arg2);
int64_t _syscall4(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                  uint64_t arg2, uint64_t arg3);
int64_t _syscall5(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                  uint64_t arg2, uint64_t arg3, uint64_t arg4);
int64_t _syscall6(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                  uint64_t arg2, uint64_t arg3, uint64_t arg4,
                  uint64_t arg5);
int64_t _syscall7(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                  uint64_t arg2, uint64_t arg3, uint64_t arg4,
                  uint64_t arg5, uint64_t arg6);

#endif
