#ifndef TIMER_H
#define TIMER_H

#include <std.h>
//#include <mmio.h>
//
///* Timer registers base offset from the MMIO_BASE. */
//#define TIMER_BASE 0x00003000
//
//#define TIMER_CS    mmio(TIMER_BASE + 0x00)
//#define TIMER_CLO   mmio(TIMER_BASE + 0x04)
//#define TIMER_CHI   mmio(TIMER_BASE + 0x08)
//#define TIMER_C0    mmio(TIMER_BASE + 0x0C)
//#define TIMER_C1    mmio(TIMER_BASE + 0x10)
//#define TIMER_C2    mmio(TIMER_BASE + 0x14)
//#define TIMER_C3    mmio(TIMER_BASE + 0x18)

/*
 * Obtains the timer frequency, in Hz.
 */
USE_VALUE uint64_t timer_frequency();

/*
 * Obtains the current timer value.
 */
USE_VALUE uint64_t timer_counter();

/*
 * Initializes the timer module; must be called before any of the
 * functions below.
 */
void initialize_timer();

/*
 * Enable timer interrupts.
 */
void enable_timer_interrupts();

/*
 * Disable timer interrupts
 */
void disable_timer_interrupts();

/*
 * Set up the timer so that at when the timer_counter() reaches
 * the given number of ticks, we receive a timer interrupt (if
 * enabled).
 */
void timer_interrupt_absolute(uint64_t ticks);

/*
 * Set up the timer so that in `ticks` number of timer ticks from
 * now, we receive a timer interrupt (if enabled).
 */
void timer_interrupt_relative(uint64_t ticks);

USE_VALUE bool timer_irq_firing();

USE_VALUE uint64_t ticks_to_ns(uint64_t ticks);
USE_VALUE uint64_t ticks_to_ms(uint64_t ticks);

USE_VALUE uint64_t ns_to_ticks(uint64_t ns);
USE_VALUE uint64_t ms_to_ticks(uint64_t ms);

#define BILLION ((uint64_t) 1000000000)

#endif
