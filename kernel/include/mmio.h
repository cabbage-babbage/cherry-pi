#ifndef MMIO_H
#define MMIO_H

#include <std.h>
#include <virtual_memory.h>

#ifdef MMIO_USE_PHYS_ADDR
#define MMIO_BASE ((uint8_t*) 0x3F000000)
#else
#define MMIO_BASE ((uint8_t*) (TTBR1_START + 0x3F000000))
#endif

/*
 * Return a volatile pointer that can be used for reading to or writing from
 * the MMIO location specified by the given offset relative to the MMIO_BASE.
 */
#define mmio(offset) ((volatile uint32_t*) (MMIO_BASE + (offset)))

#endif
