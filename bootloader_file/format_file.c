#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>

int format_file(FILE *in)
{
    if (fseek(in, 0, SEEK_END) < 0) {
        fprintf(stderr, "could not find length of file: %s\n",
                strerror(errno));
        return 1;
    }
    long tell = ftell(in);
    if (tell < 0) {
        fprintf(stderr, "could not find length of file: %s\n",
                strerror(errno));
        return 1;
    }
    if (fseek(in, 0, SEEK_SET) < 0) {
        fprintf(stderr, "could not find length of file: %s\n",
                strerror(errno));
        return 1;
    }
    if (tell > UINT32_MAX) {
        fprintf(stderr, "input file too large: length must fit in a uint32_t\n");
    }
    uint32_t file_len = (uint32_t) tell;

    putchar((file_len >> 0) & 0xFF);
    putchar((file_len >> 8) & 0xFF);
    putchar((file_len >> 16) & 0xFF);
    putchar((file_len >> 24) & 0xFF);

    uint32_t checksum = 0;
    int cur;
    while ((cur = fgetc(in)) != EOF) {
        uint8_t b = (uint8_t) cur;
        checksum = 7 * checksum + b;
    }

    putchar((checksum >> 0) & 0xFF);
    putchar((checksum >> 8) & 0xFF);
    putchar((checksum >> 16) & 0xFF);
    putchar((checksum >> 24) & 0xFF);

    if (fseek(in, 0, SEEK_SET) < 0) {
        fprintf(stderr, "could not seek to start of file: %s\n",
                strerror(errno));
    }

    while ((cur = fgetc(in)) != EOF) {
        putchar((uint8_t) cur);
    }

    return 0;
}

int main(int argc, char **argv)
{
    if (argc != 2) {
        fprintf(stderr, "usage: format_file file\n");
        return 2;
    }

    FILE *in = fopen(argv[1], "r");
    if (!in) {
        fprintf(stderr, "could not open '%s' for reading: %s\n",
                argv[1], strerror(errno));
        return 1;
    }

    return format_file(in);
}
