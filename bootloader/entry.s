.globl _start
_start:
    //mov sp, 0x40000000
    mov sp, 0x00400000
    bl bootloader_start

// fallthrough to hang:
hang:
    b hang
