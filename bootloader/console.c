#include "console.h"

#include "uart.h"
#include "string.h"

console_setting_t fg(color_t color)
{
    console_setting_t setting;

    setting.reset = false;

    setting.fg_bold = KEEP;
    setting.fg_underline = KEEP;
    setting.fg_intense = KEEP;
    setting.change_fg_color = true;
    setting.fg_color = color;

    setting.bg_intense = KEEP;
    setting.change_bg_color = false;

    return setting;
}
console_setting_t bg(color_t color)
{
    console_setting_t setting;

    setting.reset = false;

    setting.fg_bold = KEEP;
    setting.fg_underline = KEEP;
    setting.fg_intense = KEEP;
    setting.change_fg_color = true;

    setting.bg_intense = KEEP;
    setting.change_bg_color = true;
    setting.bg_color = color;

    return setting;
}

console_setting_t bold(console_setting_t setting)
{
    setting.fg_bold = ON;
    return setting;
}
console_setting_t no_bold(console_setting_t setting)
{
    setting.fg_bold = OFF;
    return setting;
}
console_setting_t underline(console_setting_t setting)
{
    setting.fg_underline = ON;
    return setting;
}
console_setting_t no_underline(console_setting_t setting)
{
    setting.fg_underline = OFF;
    return setting;
}
console_setting_t intense(console_setting_t setting)
{
    setting.fg_intense = ON;
    return setting;
}
console_setting_t no_intense(console_setting_t setting)
{
    setting.fg_intense = OFF;
    return setting;
}

console_setting_t bg_intense(console_setting_t setting)
{
    setting.bg_intense = ON;
    return setting;
}
console_setting_t bg_no_intense(console_setting_t setting)
{
    setting.bg_intense = OFF;
    return setting;
}

static color_t color_stack[MAX_COLOR_STACK];
static int color_stack_depth = 0;
static color_t current_color = RESET;

typedef struct {
    /* Buffer for the history line. */
    char line[MAX_LINE_LENGTH];
    /* Length of the line. */
    int len;
    /* Stored cursor position for line editing. */
    int pos;
} line_state_t;

/* Acts like a queue. */
line_state_t history[HISTORY_LINES];
int history_len = 0;

void console_init()
{
    /* Nothing for color setup. */

    /* Nothing for history setup. */

    /* Set a default keymap. */
    set_keymap("default");
}

static console_setting_t update_setting(console_setting_t old, console_setting_t new)
{
    if (old.reset)
        return new;

    if (new.fg_bold == KEEP)
        new.fg_bold = old.fg_bold;
    if (new.fg_underline == KEEP)
        new.fg_underline = old.fg_underline;
    if (new.fg_intense == KEEP)
        new.fg_intense = old.fg_intense;
    if (!new.change_fg_color)
        new.fg_color = old.fg_color;

    if (new.bg_intense == KEEP)
        new.bg_intense = old.bg_intense;
    if (!new.change_bg_color)
        new.bg_color = old.bg_color;

    return new;
}

/* TODO: add any feature more than just fg color */
static void change_to_color(color_t color)
{
    if (color == RESET) {
        puts("\x1b[0m");
        return;
    }

    switch (color)
    {
        case BLACK:
            puts("\x1b[0;30m");
            break;
        case RED:
            puts("\x1b[0;31m");
            break;
        case GREEN:
            puts("\x1b[0;32m");
            break;
        case YELLOW:
            puts("\x1b[0;33m");
            break;
        case BLUE:
            puts("\x1b[0;34m");
            break;
        case PURPLE:
            puts("\x1b[0;35m");
            break;
        case CYAN:
            puts("\x1b[0;36m");
            break;
        case WHITE:
            puts("\x1b[0;37m");
            break;
    }
}

void push_color(color_t color)
{
    if (color_stack_depth < MAX_COLOR_STACK) {
        color_stack[color_stack_depth] = current_color;
    }
    color_stack_depth++;
    //current_color = update_setting(current_color, color);
    current_color = color;
    change_to_color(current_color);
}
void pop_color()
{
    if (color_stack_depth == 0) {
        return;
    }

    color_stack_depth--;
    if (color_stack_depth >= 0 && color_stack_depth < MAX_COLOR_STACK) {
        current_color = color_stack[color_stack_depth];
        change_to_color(current_color);
    }
}

void putc(char c)
{
    uart_send(c);
}

/* Replaces \n with \n\r. */
void puts(const char *s)
{
    while (*s) {
        putc(*s);
        if (*s == '\n') {
            putc('\r');
        }
        s++;
    }
}

void put_int(int val)
{
    if (val < 0)
    {
        putc('-');
        val = -val; /* TODO: what about MIN_INT? */
    } else if (val == 0) {
        putc('0');
        return;
    }
    char buf[10];
    int digits = 0;
    while (val != 0) {
        buf[digits++] = '0' + (val % 10);
        val /= 10;
    }
    for (int d = digits - 1; d >= 0; d--) {
        putc(buf[d]);
    }
}

void put_ptr(const void *ptr)
{
    unsigned long n = (unsigned long) ptr;
    if (n == 0) {
        putc('0');
        return;
    }
    char buf[60];
    int hexits = 0;
    while (n != 0) {
        int hex = n % 16;
        buf[hexits++] = hex < 10
            ? ('0' + hex)
            : ('a' + (hex - 10));
        n /= 16;
    }
    for (int h = hexits - 1; h >= 0; h--) {
        putc(buf[h]);
    }
}

/* Basically, any command/input to the bootloader for
 * which different terminal emulators (such as PuTTY
 * or MiniCom) send different special sequences, or
 * for which multiple characters are sent. */
typedef enum {
    NONE = 0,
    BACKSPACE,
    ARROW_LEFT,
    ARROW_RIGHT,
    ARROW_UP,
    ARROW_DOWN,
    HOME,
    DELETE,
    END,
    PG_UP,
    PG_DOWN,
    F1,
    F2,
    F3,
    F4,
    F5,
    F6,
    F7,
    F8,
    F9,
    F10,
    F11,
    F12
} special_input_t;

typedef struct {
    special_input_t input; /* the special input which we want to map */
    const char *sequence; /* the sequence of characters that a particular terminal sends for the special input */
} special_sequence_t;

typedef struct {
    const char *name; /* user-selectable name, to specify that the console should use this keymap */
    const special_sequence_t *mappings; /* the keymapping data. ends in a 0 struct */
    const int max_sequence_len; /* maximum length of any sequence used in the mappings */
} keymap_t;

static const char *get_special_sequence(const special_sequence_t *mappings, special_input_t input) {
    for (const special_sequence_t *mapping = mappings; mapping->input; mapping++) {
        if (mapping->input == input) {
            return mapping->sequence;
        }
    }
    return "";
}

special_sequence_t mappings_default[] = {
    { 0, NULL }
};

special_sequence_t mappings_putty[] = {
    { BACKSPACE, "\x7F" },
    { ARROW_LEFT, "\x1B\x5B\x44" },
    { ARROW_RIGHT, "\x1B\x5B\x43" },
    { ARROW_UP, "\x1B\x5B\x41" },
    { ARROW_DOWN, "\x1B\x5B\x42" },
    { HOME, "\x1B\x5B\x31\x7E" },
    { DELETE, "\x1B\x5B\x33\x7E" },
    { END, "\x1B\x5B\x34\x7E" },
    { PG_UP, "\x1B\x5B\x35\x7E" },
    { PG_DOWN, "\x1B\x5B\x36\x7E" },
    { F1, "\x1B\x5B\x31\x31\x7E" },
    { F2, "\x1B\x5B\x31\x32\x7E" },
    { F3, "\x1B\x5B\x31\x33\x7E" },
    { F4, "\x1B\x5B\x31\x34\x7E" },
    { F5, "\x1B\x5B\x31\x35\x7E" },
    { F6, "\x1B\x5B\x31\x37\x7E" },
    { F7, "\x1B\x5B\x31\x38\x7E" },
    { F8, "\x1B\x5B\x31\x39\x7E" },
    { F9, "\x1B\x5B\x32\x30\x7E" },
    { F10, "\x1B\x5B\x32\x31\x7E" },
    { F11, "\x1B\x5B\x32\x33\x7E" },
    { F12, "\x1B\x5B\x32\x34\x7E" },
    { 0, NULL }
};

special_sequence_t mappings_minicom[] = {
    { BACKSPACE, "\x08" },
    { ARROW_LEFT, "\x1B\x5B\x44" },
    { ARROW_RIGHT, "\x1B\x5B\x43" },
    { ARROW_UP, "\x1B\x5B\x41" },
    { ARROW_DOWN, "\x1B\x5B\x42" },
    { HOME, "\x1B\x5B\x31\x7E" },
    { DELETE, "\x1B\x5B\x33\x7E" },
    { END, "\x1B\x4f\x46" },
    { PG_UP, "\x1B\x5B\x35\x7E" },
    { PG_DOWN, "\x1B\x5B\x36\x7E" },
    { F1, "\x1B\x4f\x50" },
    { F2, "\x1B\x4f\x51" },
    { F3, "\x1B\x4f\x52" },
    { F4, "\x1B\x4f\x53" },
    { F5, "\x1B\x5B\x31\x36\x7E" },
    { F6, "\x1B\x5B\x31\x37\x7E" },
    { F7, "\x1B\x5B\x31\x38\x7E" },
    { F8, "\x1B\x5B\x31\x39\x7E" },
    { F9, "\x1B\x5B\x32\x30\x7E" },
    { F10, "\x1B\x5B\x32\x31\x7E" },
    { F11, "\x1B\x5B\x32\x33\x7E" },
    { F12, "\x1B\x5B\x32\x34\x7E" },
    { 0, NULL }
};

keymap_t keymaps[] = {
    { "default", mappings_default, 1 },
    { "putty", mappings_putty, 5 },
    { "minicom", mappings_minicom, 5 },
    { NULL, NULL, -1 }
};

static const keymap_t *current_keymap;

bool set_keymap(const char *name)
{
    for (const keymap_t *keymap = keymaps; keymap->name; keymap++) {
        if (streq(name, keymap->name)) {
            current_keymap = keymap;
            return true;
        }
    }
    return false;
}

void print_keymaps()
{
    for (keymap_t *keymap = keymaps; keymap->name; keymap++) {
        puts(keymap->name);
        puts("\n");
    }
}

void print_current_keymap()
{
    puts(current_keymap->name);
    puts("\n");
}

static bool check_max_sequence_length(const keymap_t *map)
{
    int max_len = 0;
    for (const special_sequence_t *spec = map->mappings; spec->input != NONE; spec++) {
        int seq_len = strlen(spec->sequence);
        if (seq_len > max_len) {
            max_len = seq_len;
        }
    }
    return max_len <= map->max_sequence_len;
}

static bool sequence_is_special_prefix(const keymap_t *map, const char *seq)
{
    int len_seq = strlen(seq);
    for (const special_sequence_t *spec = map->mappings; spec->input != NONE; spec++) {
        int len_spec = strlen(spec->sequence);
        if (len_seq <= len_spec && memeq(seq, spec->sequence, len_seq)) {
            return true;
        }
    }
    return false;
}

static bool sequence_is_special(const keymap_t *map, const char *seq, special_input_t *input)
{
    for (const special_sequence_t *spec = map->mappings; spec->input != NONE; spec++) {
        if (streq(seq, spec->sequence)) {
            *input = spec->input;
            return true;
        }
    }
    return false;
}

/* Which line of history are we editing? */
static int edit_line;

/* Handle left-arrow. */
static void read_line_left()
{
}

/* Handle right-arrow. */
static void read_line_right()
{
}

/* Handle up-arrow. */
static void read_line_up()
{
    return;
    if (edit_line + 1 < history_len) {
        edit_line++;
        /* TODO: replace line by using backspace chars */
    }
}

/* Handle down-arrow. */
static void read_line_down()
{
    return;
    if (edit_line > 0) {
        edit_line--;
        /* TODO: replace line by using backspace chars */
    }
}

/* Handle backspace. */
static void read_line_backspace()
{
    line_state_t *state = &history[edit_line];
    /* TODO */
    /* for now, only delete from end */
    if (state->len > 0) {
        state->len--;
        puts(get_special_sequence(current_keymap->mappings, BACKSPACE));
    }
}

/* Handle delete. */
static void read_line_delete()
{
    return;
    /* TODO */
}

/* Handle any other input. */
static void read_line_put(char c)
{
    /* Just convert tabs to spaces. */
    if (c == '\t') {
        c = ' ';
    }
    /* Only accept characters in the printable ASCII range. */
    if (32 <= c && c <= 126) {
        /* TODO */
        /* for now, only append char */
        line_state_t *state = &history[edit_line];
        if (state->len < MAX_LINE_LENGTH) {
            state->line[state->len++] = c;
            putc(c);
        }
    }
}

/*
 * 'line' should be at least max_len+1 bytes long, to leave room
 * for the \0 terminator.
 * Returns: -1 on error, length of line read on success.
 */
int read_line(char *line)
{
    if (history_len == HISTORY_LINES) {
        /* Shift history up, to make room for new line. */
        for (int i = HISTORY_LINES - 1; i >= 1; i--) {
            history[i] = history[i - 1];
        }
    }

    history[0].len = 0;
    history[0].pos = 0;
    edit_line = 0;

    /* we will always keep it as a null-terminated string */
    char input_buf[current_keymap->max_sequence_len + 1];
    memset(input_buf, current_keymap->max_sequence_len + 1, 0);
    char *current_input = input_buf;

    while (1) {
        char in = uart_getc();
        *current_input++ = in;
        special_input_t spec_in;
        if (!sequence_is_special_prefix(current_keymap, input_buf)) {
            /* just deal with each character */
            for (current_input = input_buf; *current_input; current_input++) {
                switch (*current_input)
                {
                    case '\r':
                        /* Copy the history line to the given buffer,
                         * and null-terminate the buffer. */
                        putc('\n');
                        putc('\r');
                        int len = history[edit_line].len;
                        memcpy(line, history[edit_line].line, len);
                        line[len] = '\0';
                        return len;
                    default:
                        read_line_put(in);
                        break;
                }
            }

            /* clear the input buffer */
            memset(input_buf, current_keymap->max_sequence_len + 1, 0);
            current_input = input_buf;
        } else if (sequence_is_special(current_keymap, input_buf, &spec_in)) {
            /* we got a special input sequence */
            switch (spec_in) {
                case BACKSPACE:
                    read_line_backspace();
                    break;
                case ARROW_LEFT:
                    read_line_left();
                    break;
                case ARROW_RIGHT:
                    read_line_right();
                    break;
                case ARROW_UP:
                    read_line_up();
                    break;
                case ARROW_DOWN:
                    read_line_down();
                    break;
                case DELETE:
                    read_line_delete();
                    break;
                case HOME:
                    /* TODO */
                    break;
                case END:
                    /* TODO */
                    break;
                case PG_UP:
                case PG_DOWN:
                case F1:
                case F2:
                case F3:
                case F4:
                case F5:
                case F6:
                case F7:
                case F8:
                case F9:
                case F10:
                case F11:
                case F12:
                case NONE:
                    /* do nothing */
                    break;
            }
 
            /* clear the input buffer */
            memset(input_buf, current_keymap->max_sequence_len + 1, 0);
            current_input = input_buf;
        }
    }
}

int prompt_line(const char *prompt, char *line)
{
    puts(prompt);
    return read_line(line);
}
