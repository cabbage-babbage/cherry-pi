#ifndef STRING_H
#define STRING_H

#include "std.h"

int strlen(const char *s);

/* <0 if s1<s2, =0 if s1==s2, >0 if s1>s2 */
int strcmp(const char *s1, const char *s2);

bool streq(const char *s1, const char *s2);

/* undefined behavior on region overlap */
void memcpy(void *dst, const void *src, int len);

/* <0 if src1<src2, =0 if src1==src2, >0 if src1>src2 */
int memcmp(const void *src1, const void *src2, int len);

bool memeq(const void *src1, const void *src2, int len);

void memset(void *dst, int len, uint8_t val);

#endif
