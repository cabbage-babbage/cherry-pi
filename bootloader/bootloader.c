#include "string.h"
#include "uart.h"
#include "console.h"
#include "fileload.h"

extern char _bss_start, _bss_end;

/* Zero bss. */
static void zero_bss()
{
    for (char *bss = &_bss_start; bss < &_bss_end; bss++)
        *bss = 0;
}

/* TODO: allow quoted arguments */
/* This splits a line into parts, placing a pointer to
 * each part in the 'parts' array. Each part is null
 * terminated, which is why 'line' must be mutable. */
static int split_line(char *line, char* *parts)
{
    int num_parts = 0;
    while (true) {
        while (*line == ' ') {
            line++;
        }
        if (!*line) {
            return num_parts;
        }
        parts[num_parts++] = line;
        while (*line != ' ' && *line) {
            line++;
        }
        if (*line) {
            *line++ = '\0';
        } else {
            return num_parts;
        }
    }
}

typedef enum {
    OK,
    FAIL,
    SHOW_USAGE
} command_result_t;

typedef struct {
    const char *name;
    const char *description;
    const char *usage;
    command_result_t (*handler)(int argc, char **argv);
} command_t;

static command_result_t handle_help(int argc, char **argv);
static command_result_t handle_read(int argc, char **argv);
static command_result_t handle_write(int argc, char **argv);
static command_result_t handle_dump(int argc, char **argv);
static command_result_t handle_load(int argc, char **argv);
static command_result_t handle_jump(int argc, char **argv);
static command_result_t handle_status(int argc, char **argv);
static command_result_t handle_kb_hex(int argc, char **argv);
static command_result_t handle_keymap(int argc, char **argv);

static command_result_t last_result = OK;

static command_t bootloader_commands[] = {
    { "help",
        "give a list of commands",
        "help [command-name]\n"
            "Options: [command-name]    if specified, show extended information about the named command\n"
            "                           if not specified, show information about all commands\n",
        handle_help },
    { "rb",
        "read a byte from memory",
        "rb address\n"
            "Options: address    memory address to read the byte from\n",
        handle_read },
    { "rh",
        "read a half-word (two bytes) from memory",
        "rh address\n"
            "Options: address    starting memory address to read the half-word from; must be aligned on a 2-byte boundary\n",
        handle_read },
    { "rw",
        "read a word (four bytes) from memory",
        "rw address\n"
            "Options: address    starting memory address to read the word from; must be aligned on a 4-byte boundary\n",
        handle_read },
    { "rd",
        "read a double-word (eight bytes) from memory",
        "rd address\n"
            "Options: address    starting memory address to read the double-word from; must be aligned on an 8-byte boundary\n",
        handle_read },
    { "wb",
        "write a byte to memory",
        "wb address value\n"
            "Options: address    memory address at which to write the byte\n"
            "         value      a 1-byte integer value to write\n",
        handle_write },
    { "wh",
        "write a half-word (two bytes) to memory",
        "wh address value\n"
            "Options: address    starting memory address at which to write the half-word; must be aligned on a 2-byte boundary\n"
            "         value      a 2-byte integer value to write\n",
        handle_write },
    { "ww",
        "write a word (four bytes) to memory",
        "ww address value\n"
            "Options: address    starting memory address at which to write the word; must be aligned on a 4-byte boundary\n"
            "         value      a 4-byte integer value to write\n",
        handle_write },
    { "wd",
        "write a double-word (eight bytes) to memory",
        "wd address value\n"
            "Options: address    starting memory address at which to write the double-word; must be aligned on a 8-byte boundary\n"
            "         value      a 8-byte integer value to write\n",
        handle_write },
    { "dump",
        "like 'hexdump -C', print a formatted display of a range of bytes",
        "dump start-addr nbytes\n"
            "Options: start-addr    starting memory address to read bytes from\n"
            "         nbytes        number of bytes to read\n",
        handle_dump },
    { "load",
        "load a file to memory over UART1",
        "load addr\n"
            "Options: addr    starting address at which to load the file\n",
        handle_load },
    { "jump",
        "jump to an address",
        "jump addr\n"
            "Options: addr    jump target address\n",
        handle_jump },
    { "status",
        "see the status of the last command",
        "status\n",
        handle_status },
    { "kb_hex",
        "print all recieved characters in hex",
        "kb_hex\n"
            "Type 'quit' at any point to return to bootloader.\n",
        handle_kb_hex },
    { "keymap",
        "get or set the current keymap used",
        "keymap [name | -l]\n"
            "Options: name    name of a keymap to use\n"
            "         -l      list all existing keymaps\n",
        handle_keymap },
    { NULL, NULL, NULL, NULL }
};

static command_result_t handle_help(int argc, char **argv)
{
    if (argc == 1) {
        /* Print help about all the commands. */
        int longest_name_len = 0;
        for (command_t *cmd = bootloader_commands; cmd->name; cmd++) {
            int len = strlen(cmd->name);
            if (len > longest_name_len) {
                longest_name_len = len;
            }
        }
        for (command_t *cmd = bootloader_commands; cmd->name; cmd++) {
            puts(cmd->name);
            for (int i = longest_name_len - strlen(cmd->name); i >= 0; i--) {
                putc(' ');
            }
            puts("    ");
            puts(cmd->description);
            puts("\n");
        }
        return OK;
    } else if (argc == 2) {
        /* Print help about a particular command. */
        for (command_t *cmd = bootloader_commands; cmd->name; cmd++) {
            if (streq(argv[1], cmd->name)) {
                puts(cmd->name);
                puts("    ");
                puts(cmd->description);
                puts("\n");
                puts("Usage: ");
                puts(cmd->usage);
                return OK;
            }
        }

        /* No command found with that name. */
        puts("There is no command '");
        push_color(RED);
        puts(argv[1]);
        pop_color();
        puts("'.\n");

        puts("Use '");
        push_color(GREEN);
        puts("help");
        pop_color();
        puts("' to see a list of all commands.\n");

        return FAIL;
    } else {
        return SHOW_USAGE;
    }
}

/* Returns -1 if the char is not numeric in the given base, or
 *    if the base is not in the range [1, 36]
 * Otherwise, returns the unsigned char value, in the given base.
 * Examples:
 *    parse_numeric_char('b', 16) == 11
 *    parse_numeric_char('z', 16) == -1
 *    parse_numeric_char('3', 2) == -1
 *    parse_numeric_char('?', 100000) == -1 */
static int8_t parse_numeric_char(char c, int base)
{
    if (base < 1 || base > 36) {
        return -1;
    }

    uint8_t num = 0;
    if ('0' <= c && c <= '9') {
        num = c - '0';
    } else if ('a' <= c && c <= 'z') {
        num = (c - 'a') + 10;
    } else if ('A' <= c && c <= 'Z') {
        num = (c - 'A') + 10;
    } else {
        return -1;
    }

    if (num > base) {
        return -1;
    } else {
        return num;
    }
}

/* Parse a uint{bytes} from the string, and put it in 'val'.
 * The entire input string is required to be numeric; this doesn't
 * read only a prefix.
 * 'bytes' must be 1, 2, 4, or 8.
 * Allowed prefixes:
 *    0b: binary
 *    0o: octal
 *    0d: decimal (default)
 *    0x: hexadecimal
 * Returns:
 *    0 on success.
 *    1 if the string could not be read as an integer.
 *    2 if the numeric value overflowed the specified number of bytes.
 *    3 if 'bytes' is invalid. */
static int parse_uint(const char *str, void *val, int bytes)
{
    if (bytes != 1 && bytes != 2 && bytes != 4 && bytes != 8) {
        return 3; /* Invalid 'bytes' argument. */
    }

    int base = 10;
    if (strlen(str) > 2 && str[0] == '0') {
        switch (str[1]) {
            case 'b':
                base = 2;
                str += 2;
                break;
            case 'o':
                base = 8;
                str += 2;
                break;
            case 'd':
                base = 10;
                str += 2;
                break;
            case 'x':
                base = 16;
                str += 2;
                break;
        }
    }
    /* Now we have our base. Let's get the limit. */
    uint64_t limit = 0;
    switch (bytes) {
        case 1:
            limit = (uint64_t)1 << 8;
            break;
        case 2:
            limit = (uint64_t)2 << 8;
            break;
        case 4:
            limit = (uint64_t)4 << 8;
            break;
    }

    uint64_t num = 0;
    while (*str) {
        int8_t char_num_res = parse_numeric_char(*str, base);
        if (char_num_res < 0) {
            return 1; /* Invalid numeric string. */
        }
        uint8_t char_num = (uint8_t) char_num_res;
        if (bytes == 8) {
            /* Special case overflow checking. */
            if (num > 0 && num * base + char_num <= num) {
                return 2; /* Overflow. */
            }
        } else {
            if (num * base + char_num >= limit) {
                return 2; /* Overflow. */
            }
        }
        num = num * base + char_num;
        str++;
    }

    /* Now we have our number. */
    switch (bytes) {
        case 1:
            *(uint8_t*) val = (uint8_t) num;
            break;
        case 2:
            *(uint16_t*) val = (uint16_t) num;
            break;
        case 4:
            *(uint32_t*) val = (uint32_t) num;
            break;
        case 8:
            *(uint64_t*) val = (uint64_t) num;
            break;
    }
    return 0;
}

static int parse_uint8(const char *str, uint8_t *val)
{
    return parse_uint(str, val, 1);
}
static int parse_uint16(const char *str, uint16_t *val)
{
    return parse_uint(str, val, 2);
}
static int parse_uint32(const char *str, uint32_t *val)
{
    return parse_uint(str, val, 4);
}
static int parse_uint64(const char *str, uint64_t *val)
{
    return parse_uint(str, val, 8);
}

/* Expect 0 <= val < 16. */
static void put_hexit(uint8_t val)
{
    if (val < 10) {
        putc('0' + val);
    } else if (val < 16) {
        putc('a' + (val - 10));
    } else {
        putc('^');
    }
}

static command_result_t handle_read(int argc, char **argv)
{
    /* rb|rh|rw|rd address */
    if (argc != 2) {
        return SHOW_USAGE;
    }

    uint64_t addr = 0;
    int result = parse_uint64(argv[1], &addr);
    if (result > 0) {
        puts("Could not read address '");
        push_color(RED);
        puts(argv[1]);
        pop_color();
        puts("': ");
        if (result == 1) {
            puts("value is in an invalid format.\n");
        } else if (result == 2) {
            puts("value is too large to fit in a uint64.\n");
        }
        return FAIL;
    }

    /* TODO: remove this once it's certain that overflow checking works */
    puts("Address: 0x");
    put_ptr((void*) addr);
    puts("\n");

    if (streq(argv[0], "rb")) {
        uint8_t val = *((uint8_t*) addr);
        put_hexit((val >> 4) & 0x0F);
        put_hexit((val >> 0) & 0x0F);
        puts("\n");
        return OK;
    } else if (streq(argv[0], "rh")) {
        if ((addr & 0x1) != 0) {
            puts("Address must be aligned to a 2-byte boundary.\n");
            return FAIL;
        }
        uint16_t val = *((uint16_t*) addr);
        put_hexit((val >> 12) & 0x0F);
        put_hexit((val >> 8) & 0x0F);
        put_hexit((val >> 4) & 0x0F);
        put_hexit((val >> 0) & 0x0F);
        puts("\n");
        return OK;
    } else if (streq(argv[0], "rw")) {
        if ((addr & 0x3) != 0) {
            puts("Address must be aligned to a 4-byte boundary.\n");
            return FAIL;
        }
        uint32_t val = *((uint32_t*) addr);
        put_hexit((val >> 28) & 0x0F);
        put_hexit((val >> 24) & 0x0F);
        put_hexit((val >> 20) & 0x0F);
        put_hexit((val >> 16) & 0x0F);
        put_hexit((val >> 12) & 0x0F);
        put_hexit((val >> 8) & 0x0F);
        put_hexit((val >> 4) & 0x0F);
        put_hexit((val >> 0) & 0x0F);
        puts("\n");
        return OK;
    } else if (streq(argv[0], "rd")) {
        if ((addr & 0x7) != 0) {
            puts("Address must be aligned to an 8-byte boundary.\n");
            return FAIL;
        }
        uint64_t val = *((uint64_t*) addr);
        put_hexit((val >> 60) & 0x0F);
        put_hexit((val >> 56) & 0x0F);
        put_hexit((val >> 52) & 0x0F);
        put_hexit((val >> 48) & 0x0F);
        put_hexit((val >> 44) & 0x0F);
        put_hexit((val >> 40) & 0x0F);
        put_hexit((val >> 36) & 0x0F);
        put_hexit((val >> 32) & 0x0F);
        put_hexit((val >> 28) & 0x0F);
        put_hexit((val >> 24) & 0x0F);
        put_hexit((val >> 20) & 0x0F);
        put_hexit((val >> 16) & 0x0F);
        put_hexit((val >> 12) & 0x0F);
        put_hexit((val >> 8) & 0x0F);
        put_hexit((val >> 4) & 0x0F);
        put_hexit((val >> 0) & 0x0F);
        puts("\n");
        return OK;
    }

    return FAIL;
}

static command_result_t handle_write(int argc, char **argv)
{
    if (argc != 3) {
        return SHOW_USAGE;
    }

    uint64_t addr = 0;
    int result = parse_uint64(argv[1], &addr);
    if (result > 0) {
        puts("Could not read address '");
        push_color(RED);
        puts(argv[1]);
        pop_color();
        puts("': ");
        if (result == 1) {
            puts("value is in an invalid format.\n");
        } else if (result == 2) {
            puts("value is too large to fit in a uint64.\n");
        }
        return FAIL;
    }

    if (streq(argv[0], "wb")) {
        uint8_t val;
        result = parse_uint8(argv[2], &val);
        if (result > 0) {
            puts("Could not read value '");
            push_color(RED);
            puts(argv[2]);
            pop_color();
            puts("': ");
            if (result == 1) {
                puts("value is in an invalid format.\n");
            } else if (result == 2) {
                puts("value is too large to fit in a uint8.\n");
            }
            return FAIL;
        }

        *((uint8_t*) addr) = val;
        return OK;
    } else if (streq(argv[0], "wh")) {
        if ((addr & 0x1) != 0) {
            puts("Address must be aligned to a 2-byte boundary.\n");
            return FAIL;
        }

        uint16_t val;
        result = parse_uint16(argv[2], &val);
        if (result > 0) {
            puts("Could not read value '");
            push_color(RED);
            puts(argv[2]);
            pop_color();
            puts("': ");
            if (result == 1) {
                puts("value is in an invalid format.\n");
            } else if (result == 2) {
                puts("value is too large to fit in a uint16.\n");
            }
            return FAIL;
        }

        *((uint16_t*) addr) = val;
        return OK;
    } else if (streq(argv[0], "ww")) {
        if ((addr & 0x3) != 0) {
            puts("Address must be aligned to a 4-byte boundary.\n");
            return FAIL;
        }

        uint32_t val;
        result = parse_uint32(argv[2], &val);
        if (result > 0) {
            puts("Could not read value '");
            push_color(RED);
            puts(argv[2]);
            pop_color();
            puts("': ");
            if (result == 1) {
                puts("value is in an invalid format.\n");
            } else if (result == 2) {
                puts("value is too large to fit in a uint32.\n");
            }
            return FAIL;
        }

        *((uint32_t*) addr) = val;
        return OK;
    } else if (streq(argv[0], "wd")) {
        if ((addr & 0x7) != 0) {
            puts("Address must be aligned to an 8-byte boundary.\n");
            return FAIL;
        }

        uint64_t val;
        result = parse_uint64(argv[2], &val);
        if (result > 0) {
            puts("Could not read value '");
            push_color(RED);
            puts(argv[2]);
            pop_color();
            puts("': ");
            if (result == 1) {
                puts("value is in an invalid format.\n");
            } else if (result == 2) {
                puts("value is too large to fit in a uint64.\n");
            }
            return FAIL;
        }

        *((uint64_t*) addr) = val;
        return OK;
    }

    return FAIL;
}

static command_result_t handle_dump(int argc, char **argv)
{
    if (argc != 3) {
        return SHOW_USAGE;
    }

    uint64_t addr, len;

    int result = parse_uint64(argv[1], &addr);
    if (result > 0) {
        puts("Could not read start address '");
        push_color(RED);
        puts(argv[1]);
        pop_color();
        puts("': ");
        if (result == 1) {
            puts("value is in an invalid format.\n");
        } else if (result == 2) {
            puts("value is too large to fit in a uint64.\n");
        }
        return FAIL;
    }

    result = parse_uint64(argv[2], &len);
    if (result > 0) {
        puts("Could not read dump length '");
        push_color(RED);
        puts(argv[2]);
        pop_color();
        puts("': ");
        if (result == 1) {
            puts("value is in an invalid format.\n");
        } else if (result == 2) {
            puts("value is too large to fit in a uint64.\n");
        }
        return FAIL;
    }

    if ((addr & 0xF) != 0) {
        puts("Address must be aligned to a 16-byte boundary.\n");
        return FAIL;
    }

    /* TODO: we just assume that the entire range of bytes is in the 32-bit address space.
     * Formatting needs to change otherwise. */
    for (uint64_t offset = 0; offset < len; offset += 0x10) {
        /* format: 0xnnnnnnnn  bb bb bb bb bb bb bb bb  bb bb bb bb bb bb bb bb  |cccccccccccccccc| */
        uint64_t row_addr = addr + offset;
        put_hexit((row_addr >> 28) & 0xF);
        put_hexit((row_addr >> 24) & 0xF);
        put_hexit((row_addr >> 20) & 0xF);
        put_hexit((row_addr >> 16) & 0xF);
        put_hexit((row_addr >> 12) & 0xF);
        put_hexit((row_addr >> 8) & 0xF);
        put_hexit((row_addr >> 4) & 0xF);
        put_hexit((row_addr >> 0) & 0xF);
        puts("  ");
        for (uint8_t col = 0; col < 0x10; col++) {
            if (offset + col < len) {
                put_hexit((*((uint8_t*) row_addr + col) >> 4) & 0xF);
                put_hexit((*((uint8_t*) row_addr + col) >> 0) & 0xF);
            } else {
                puts("  ");
            }
            if (col == 7 || col == 15) {
                puts("  ");
            } else {
                puts(" ");
            }
        }
        puts("|");
        for (uint8_t col = 0; col < 0x10 && offset + col < len; col++) {
            char c = *((char*) row_addr + col);
            if (32 <= c && c <= 126) {
                putc(c);
            } else {
                putc('.');
            }
        }
        puts("|\n");
    }

    return OK;
}

static command_result_t handle_load(int argc, char **argv)
{
    if (argc != 2) {
        return SHOW_USAGE;
    }

    uint64_t load_addr;
    int result = parse_uint64(argv[1], &load_addr);
    if (result > 0) {
        puts("Could not read load address '");
        push_color(RED);
        puts(argv[1]);
        pop_color();
        puts("': ");
        if (result == 1) {
            puts("value is in an invalid format.\n");
        } else if (result == 2) {
            puts("value is too large to fit in a uint64.\n");
        }
        return FAIL;
    }

    if (load_file_from_uart((void*) load_addr)) {
        return OK;
    } else {
        return FAIL;
    }
}

void _jump_to(uint64_t addr);

static command_result_t handle_jump(int argc, char **argv)
{
    if (argc != 2) {
        return SHOW_USAGE;
    }

    uint64_t addr;
    int result = parse_uint64(argv[1], &addr);
    if (result > 0) {
        puts("Could not read destination address '");
        push_color(RED);
        puts(argv[1]);
        pop_color();
        puts("': ");
        if (result == 1) {
            puts("value is in an invalid format.\n");
        } else if (result == 2) {
            puts("value is too large to fit in a uint64.\n");
        }
        return FAIL;
    }

    _jump_to(addr);

    return OK;
}

static command_result_t handle_status(int argc, char **argv)
{
    if (argc != 1) {
        return SHOW_USAGE;
    }

    switch (last_result) {
        case OK:
            push_color(GREEN);
            puts("successful\n");
            pop_color();
            break;
        case FAIL:
            push_color(RED);
            puts("unsuccessful\n");
            pop_color();
            break;
        case SHOW_USAGE:
            push_color(YELLOW);
            puts("invalid invocation\n");
            pop_color();
            break;
    }

    return OK;
}

static command_result_t handle_kb_hex(int argc, char **argv)
{
    if (argc != 1) {
        return SHOW_USAGE;
    }

    const int QUIT_CMD_LEN = 4;
    const char *QUIT_CMD = "quit";
    char buf[QUIT_CMD_LEN];
    for (int i = 0; i < QUIT_CMD_LEN; i++) {
        buf[i] = 0;
    }
    while (!memeq(buf, QUIT_CMD, QUIT_CMD_LEN))
    {
        char in = uart_getc();
        for (int i = 0; i < QUIT_CMD_LEN - 1; i++) {
            buf[i] = buf[i + 1];
        }
        buf[QUIT_CMD_LEN - 1] = in;

        put_int((int) in);
        puts("\n");
    }

    return OK;
}

static command_result_t handle_keymap(int argc, char **argv)
{
    if (argc == 1) {
        print_current_keymap();
        return OK;
    } else if (argc == 2) {
        if (streq(argv[1], "-l")) {
            print_keymaps();
            return OK;
        } else {
            if (set_keymap(argv[1])) {
                puts("keymap set to '");
                push_color(GREEN);
                puts(argv[1]);
                pop_color();
                puts("'\n");
                return OK;
            } else {
                puts("there is no keymap named '");
                push_color(RED);
                puts(argv[1]);
                pop_color();
                puts("': use '");
                push_color(GREEN);
                puts("keymap -l");
                pop_color();
                puts("' to see a list of all keymaps\n");
                return FAIL;
            }
        }
    } else {
        return SHOW_USAGE;
    }
}

static void run_command(int num_parts, char **parts)
{
    /* Find a command to run. */
    for (command_t *cmd = bootloader_commands; cmd->name; cmd++) {
        if (streq(parts[0], cmd->name)) {
            command_result_t res = (cmd->handler)(num_parts, parts);
            last_result = res;
            switch (res) {
                case OK:
                    return;
                case FAIL:
                    push_color(RED);
                    puts("Command execution failed.\n");
                    pop_color();
                    return;
                case SHOW_USAGE:
                    push_color(RED);
                    puts("Wrong command invocation.\n");
                    pop_color();
                    puts("Usage: ");
                    puts(cmd->usage);
                    return;
                default:
                    push_color(RED);
                    puts("Command did not return a valid result.\n");
                    pop_color();
                    last_result = FAIL;
                    return;
            }
        }
    }

    /* No command of that name was found. */
    puts("There is no command '");
    push_color(RED);
    puts(parts[0]);
    pop_color();
    puts("'.\n");

    puts("Use '");
    push_color(GREEN);
    puts("help");
    pop_color();
    puts("' to see a list of all commands.\n");
}

void bootloader_start()
{
    zero_bss();
    uart_init();
    console_init();

    char buf[MAX_LINE_LENGTH + 1];
    char* parts[MAX_LINE_LENGTH];

    while (1) {
        push_color(PURPLE);
        puts("bootloader");
        pop_color();
        push_color(YELLOW);
        puts("$ ");
        pop_color();
        int len = read_line(buf);
        if (len < 0) {
            push_color(RED);
            puts("error: could not read line\n");
            pop_color();
        }
        else {
            int num_parts = split_line(buf, parts);
            if (num_parts != 0) {
                run_command(num_parts, parts);
            }
        }
    }
}
