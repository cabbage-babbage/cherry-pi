#include "fileload.h"

#include "std.h"
#include "uart.h"
#include "console.h"

/* Expect 0 <= val < 16. */
static void put_hexit(uint8_t val)
{
    if (val < 10) {
        putc('0' + val);
    } else if (val < 16) {
        putc('a' + (val - 10));
    } else {
        putc('^');
    }
}

static uint32_t read_bin_uint32()
{
    uint32_t val = 0;
    val |= uart_getc();
    val |= uart_getc() << 8;
    val |= uart_getc() << 16;
    val |= uart_getc() << 24;
    return val;
}

bool load_file_from_uart(void *load_addr)
{
    puts("File load addr: 0x");
    put_ptr(load_addr);
    puts("\n");

    uint8_t *mem = load_addr;
    uint32_t file_len = read_bin_uint32();

    puts("File length: 0x");
    put_ptr((void*) (uint64_t) file_len);
    puts("\n");

    uint32_t file_checksum = read_bin_uint32();

    puts("File checksum: ");
    put_ptr((void*) (uint64_t) file_checksum);
    puts("\n");

    puts("Loading file...\n");

    uint32_t checksum = 0;
    for (uint32_t offset = 0; offset < file_len; offset++) {
        if (offset % 16 == 0) {
            put_hexit((offset >> 28) & 0xF);
            put_hexit((offset >> 24) & 0xF);
            put_hexit((offset >> 20) & 0xF);
            put_hexit((offset >> 16) & 0xF);
            put_hexit((offset >> 12) & 0xF);
            put_hexit((offset >> 8) & 0xF);
            put_hexit((offset >> 4) & 0xF);
            put_hexit((offset >> 0) & 0xF);
            puts("  ");
        }

        uint8_t in = uart_getc();
        mem[offset] = in;
        put_hexit((in >> 4) & 0xF);
        put_hexit((in >> 0) & 0xF);
        if (offset % 16 == 15) {
            puts("\n");
        } else if (offset % 16 == 7) {
            puts("  ");
        } else {
            puts(" ");
        }
        checksum = 7 * checksum + in;
    }

    puts("Computed checksum: ");
    if (checksum == file_checksum) {
        push_color(GREEN);
    } else {
        push_color(RED);
    }
    puts("0x");
    put_ptr((void*) (uint64_t) checksum);
    pop_color();
    puts("\n");

    return checksum == file_checksum;
}
