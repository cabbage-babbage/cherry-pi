#ifndef STD_H
#define STD_H

typedef char bool;

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long uint64_t;

typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;
typedef signed long int64_t;

//#define UINT8_MAX 
//#define UINT16_MAX
//#define UINT32_MAX 
//#define UINT64_MAX 

_Static_assert(sizeof(uint8_t) == 1, "sizeof(uint8_t) must be 1");
_Static_assert(sizeof(uint16_t) == 2, "sizeof(uint16_t) must be 2");
_Static_assert(sizeof(uint32_t) == 4, "sizeof(uint32_t) must be 4");
_Static_assert(sizeof(uint64_t) == 8, "sizeof(uint64_t) must be 8");

#define true (-1)
#define false 0

#define NULL 0

#endif
