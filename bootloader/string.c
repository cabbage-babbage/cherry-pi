#include "string.h"

int strlen(const char *s)
{
    int len = 0;
    while (*s) {
        s++;
        len++;
    }
    return len;
}

int strcmp(const char *s1, const char *s2)
{
    while (*s1 && *s2)
    {
        if (*s1 < *s2) {
            return -1;
        } else if (*s1 > *s2) {
            return 1;
        } else {
            s1++;
            s2++;
        }
    }
    if (*s1) {
        return 1;
    } else if (*s2) {
        return -1;
    } else {
        return 0;
    }
}

bool streq(const char *s1, const char *s2)
{
    return strcmp(s1, s2) == 0;
}

void memcpy(void *dst, const void *src, int len)
{
    char *_dst = dst;
    const char *_src = src;
    while (len-- > 0) {
        *_dst++ = *_src++;
    }
}

int memcmp(const void *src1, const void *src2, int len)
{
    const char *_src1 = src1;
    const char *_src2 = src2;
    while (len-- > 0) {
        if (*_src1 < *_src2) {
            return -1;
        } else if (*_src1 > *_src2) {
            return 1;
        }
        _src1++;
        _src2++;
    }
    return 0;
}

bool memeq(const void *src1, const void *src2, int len)
{
    return memcmp(src1, src2, len) == 0;
}

void memset(void *dst, int len, uint8_t val)
{
    uint8_t *_dst = dst;
    while (len-- > 0) {
        *_dst++ = val;
    }
}
