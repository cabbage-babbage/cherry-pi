#ifndef CONSOLE_H
#define CONSOLE_H

#include "std.h"

#define HISTORY_LINES 20
#define MAX_LINE_LENGTH 80

#define MAX_COLOR_STACK 10

void print_int(int val);
void print_ptr(void *val);

typedef enum {
    ON, /* Turn the setting on. */
    OFF, /* Turn the setting off. */
    KEEP /* Keep the old setting. */
} setting_t;

typedef enum {
    BLACK = 0,
    RED = 1,
    GREEN = 2,
    YELLOW = 3,
    BLUE = 4,
    PURPLE = 5,
    CYAN = 6,
    WHITE = 7,

    RESET
} color_t;

typedef struct console_setting {
    bool reset;

    setting_t fg_bold;
    setting_t fg_underline;
    setting_t fg_intense;
    bool change_fg_color;
    color_t fg_color;

    setting_t bg_intense;
    bool change_bg_color;
    color_t bg_color;
} console_setting_t;

console_setting_t fg(color_t color);
console_setting_t bg(color_t color);

console_setting_t bold(console_setting_t setting);
console_setting_t no_bold(console_setting_t setting);
console_setting_t underline(console_setting_t setting);
console_setting_t no_underline(console_setting_t setting);
console_setting_t intense(console_setting_t setting);
console_setting_t no_intense(console_setting_t setting);

console_setting_t bg_intense(console_setting_t setting);
console_setting_t bg_no_intense(console_setting_t setting);

void console_init(void);

//void push_color(console_setting_t color);
void push_color(color_t color);
void pop_color(void);

char getc();

void putc(char c);
/* Replaces \n with \n\r. */
void puts(const char *s);
void put_int(int val);
void put_ptr(const void *ptr);

/* true if keymap found; false if no keymap exists with the
 * specified name */
bool set_keymap(const char *name);

void print_keymaps();

void print_current_keymap();

/* 'line' must be at least MAX_LINE_LENGTH + 1 bytes. */
int read_line(char *line);
int prompt_line(const char *prompt, char *line);

#endif
