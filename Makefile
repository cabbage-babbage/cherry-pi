QEMU_SERIAL_PORT=12345

.PHONY: clean-kernel
clean-kernel:
	cd kernel && make clean

.PHONY: clean-all
clean-all:
	cd bootloader && make clean
	cd bootloader_file && make clean
	cd kernel && make clean

.PHONY: bootloader
bootloader:
	cd bootloader && make all

.PHONY: kernel
kernel:
	cd kernel && make all

# NOTE: requires changing kernel/src/kernel.ld to load the entrypoint
# at 0x80000 instead of any other value specified.
.PHONY: kernel-as-bootloader
kernel-as-bootloader:
	make kernel
	rm -f bootloader/boot/kernel8.img
	cp kernel/bin/kernel.img bootloader/boot/kernel8.img

.PHONY: run-bootloader
run-bootloader:
	cd bootloader && make run

.PHONY: trace-bootloader
trace-bootloader:
	cd bootloader && make trace

.PHONY: connect-bootloader
connect-bootloader:
	qemu_serial -p $(QEMU_SERIAL_PORT)

# NOTE: this requires the bootloader to already be running
.PHONY: run-kernel
run-kernel:
	cd kernel && make run

.PHONY: run-all
run-all:
	make bootloader
	make run-bootloader &
	sleep 1
	make run-kernel

.PHONY: run-kernel-as-bootloader
run-kernel-as-bootloader:
	make kernel-as-bootloader
	make run-bootloader &
	sleep 1
	make connect-bootloader

.PHONY: trace-kernel-as-bootloader
trace-kernel-as-bootloader:
	make kernel-as-bootloader
	make trace-bootloader &
	sleep 1
	make connect-bootloader

.PHONY: dump-kernel
dump-kernel:
	cd kernel && make bin/dump-elf
