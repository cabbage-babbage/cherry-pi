/* We cannot call this just ERRNO_H since the kernel errno.h uses
 * that #define. */
#ifndef STD_ERRNO_H
#define STD_ERRNO_H

#include <uapi/errno.h>

extern errno_t errno;

#endif
