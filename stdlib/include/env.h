#ifndef ENV_H
#define ENV_H

#include <std.h>
#include <errno.h>

/* Returns a NULL-terminated list of strings holding the environment
 * variables. */
char *const *environment();

/* Obtain the value of a given environment variable, by name. Returns
 * NULL if the variable does not exist. */
char *getenv(const char *name);

/* Set an environment variable's value. If the variable already exists,
 * the overwrite argument determines whether the existing value is overwritten
 * or left alone; in either case, the result is success. If the variable does
 * not already exist, it is created.
 *
 * Returns 0 on success, with errno set to 0; returns a negative value on failure,
 * with errno set to appropriate nonzero value.
 *
 * Errors:
 *   EINVAL     if the name is empty or contains '='.
 *   ENOMEM     if there was not enough memory to set the environment variable. */
int64_t setenv(const char *name, const char *value, bool overwrite);

/* Delete an environment variable. If the variable does not exist, the
 * environment is unchanged; the result is still success.
 *
 * Returns 0 on success, with errno set to 0; returns a negative value on failure,
 * with errno set to appropriate nonzero value.
 *
 * Errors:
 *   EINVAL     if the name is empty or contains '='. */
int64_t unsetenv(const char *name);

#endif
