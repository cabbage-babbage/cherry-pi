#ifndef STRING_H
#define STRING_H

#include <std.h>

USE_VALUE int strlen(const char *s);

/* <0 if s1<s2, =0 if s1==s2, >0 if s1>s2 */
USE_VALUE int strcmp(const char *s1, const char *s2);

USE_VALUE bool streq(const char *s1, const char *s2);

USE_VALUE bool startswith(const char *str, const char *prefix);

/* dst must be large enough to hold all the srcs, i.e. must be
 * at least strlen(srcs[0]) + ... + strlen(srcs[n-1]) + 1, where
 * n is the index of the srcs null terminator. */
void strcat_array(char *dst, const char **srcs);

void strcat(char *dst, const char *s1, const char *s2);

/* undefined behavior on region overlap */
void memcpy(void *dst, const void *src, int len);

/* <0 if src1<src2, =0 if src1==src2, >0 if src1>src2 */
USE_VALUE int memcmp(const void *src1, const void *src2, int len);

USE_VALUE bool memeq(const void *src1, const void *src2, int len);

void memset(void *dst, int len, uint8_t val);

#endif
