#ifndef STDIO_H
#define STDIO_H

#include <std.h>

void putc(char c);
void puts(const char *s);

void fputc(uint32_t fd, char c);
void fputs(uint32_t fd, const char *s);

#define FD_STDIN    0
#define FD_STDOUT   1
#define FD_STDERR   2

/*
 * Like standard C printf. Allowable format specifiers: (N can be 8, 16, 32, or 64; B can be any decimal number 2-36)
 *     %iN     intN_t, in decimal
 *     %iN{B}  intN_t, in base B  [TODO]
 *     %uN     uintN_t, in decimal
 *     %uN{B}  uintN_t, in base B [TODO]
 *     %p      ptr_t, in hexadecimal
 *     %[q]s   char*, directly printed (or quoted, if 'q' is specified)
 *     %[q]c   char, directly printed (or quoted, if 'q' is specified)
 *     %b      bool, printed as 'true' or 'false'
 *     %%      prints '%'
 *     %{      prints '{'
 *     %}      prints '}' (not really as necessary as %{)
 *
 * Returns the number of characters written.
 */
uint64_t printf(const char *fmt, ...);
/* Print to a particular descriptor instead of FD_STDOUT. */
uint64_t fprintf(uint32_t fd, const char *fmt, ...);

/* These return the length of the string written to dst; that is, they actually write
 * (return value) + 1 characters to dst, since a null terminator is included. */
uint64_t sprintf(char *dst, const char *fmt, ...);
uint64_t snprintf(char *dst, uint64_t buf_size, const char *fmt, ...);

#define EOF ((int64_t) 256)
/* Returns +char on success, -errno on failure, and EOF if there's no more input. */
USE_VALUE int64_t getc();

#endif
