#ifndef HEAP_H
#define HEAP_H

#include <std.h>

/* Must be called before using any heap functions. */
void init_heap();

/* Basic malloc. Returns NULL if there is not enough memory to allocate the
 * specified number of bytes. */
USE_VALUE ptr_t malloc(uint64_t size);
/* Extended malloc, with more options:
 * - alignment_bits specifies the minimum alignment of the allocated memory block,
 *   in terms of the number of least-significant bits that must be zero. */
USE_VALUE ptr_t malloc_ext(uint64_t size, uint64_t alignment_bits);

/* Basic realloc. If alloc is NULL, same as malloc(new_size). On failure, returns
 * NULL and does not free the original allocation. */
USE_VALUE ptr_t realloc(ptr_t alloc, uint64_t new_size);
/* Extended realloc, with more options. */
USE_VALUE ptr_t realloc_ext(ptr_t alloc, uint64_t new_size, uint64_t alignment_bits);

void free(ptr_t alloc);

#endif
