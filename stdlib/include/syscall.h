#ifndef SYSCALL_H
#define SYSCALL_H

#include <std.h>
#include <uapi/fs/def.h>
#include <uapi/fs/inode.h>
#include <uapi/fs/dir_entry.h>
#include <uapi/fs/stat.h>
#include <uapi/signal.h>

/* NOTE:
 * A negative result from any syscall _always_ indicates (the negation of) an error
 * number. The expected error codes for any given syscall are documented here.
 * These functions wrap the actual syscalls, however, so on error -1, false, NULL,
 * or some other error indicator will be returned, with the actual error number stored
 * in errno (see <errno.h>).
 *
 * In fact, the following is guaranteed: every function in this header sets errno to 0
 * on success, and to an appropriate (nonzero) value on failure. */

/* Kill the current process, and return the specified exit code to the parent. */
void exit(uint32_t exit_code) __attribute__((noreturn));

/* Wait for a child process to exit, then assign child_id to the child's id and return
 * its exit code.
 * If WAIT_ANY is not specified, wait for the child with the specified process id to
 * exit; if WAIT_ANY is specified, instead wait for any child process to exit (the
 * original value of child_id is ignored).
 *
 * The WAIT_NONBLOCKING flag specifies that the syscall should not block for a child
 * process to exit, but instead return immediately. If no targeted child has exited,
 * child_id is set to 0 and the return value is 0.
 *
 * Note that once a child's status has been returned by this syscall, that child is
 * deallocated and cannot be waited on again.
 *
 * Returns +exit_code if a targeted child has exited with exit_code, 0 if WAIT_ANY and
 * WAIT_NONBLOCKING were specified but no child has exited, or a negative value on
 * failure.
 *
 * Errors:
 *   ENOENT     if not WAIT_ANY, and there is either no process with the given id or
 *              there is one but it is not our child;
 *              or if no targeted child has exited and WAIT_NONBLOCKING is specified.
 *   EFAULT     if the child_id pointer is not valid. */
#define WAIT_ANY            ((uint64_t) 1 << 0)
#define WAIT_NONBLOCKING    ((uint64_t) 1 << 1)
int64_t wait(uint32_t *child_id, uint64_t flags);

/* Sleep until (at least) the given number of milliseconds have elapsed. On success,
 * returns 0; on failure, returns a negative value.
 *
 * Errors:
 *   EINTR      if the sleep was interrupted. */
int64_t sleep(uint64_t ms);

#define CFG_READABLE        ((uint32_t) 1 << 0)
#define CFG_WRITEABLE       ((uint32_t) 1 << 1)
#define CFG_APPEND          ((uint32_t) 1 << 2)
#define CFG_CLOSE_ON_EXEC   ((uint32_t) 1 << 3)
#define CFG_NONBLOCKING     ((uint32_t) 1 << 4)
#define CFG_IS_TTY          ((uint32_t) 1 << 5)

/* Obtain the configuration flags of a file descriptor. On success, returns
 * a uint32_t bitfield indicating the following flags:
 *
 *   CFG_CLOSE_ON_EXEC      if the descriptor is automatically closed when
 *                          switching process images via the exec() syscall.
 *   CFG_NONBLOCKING        if all reads/writes via the descriptor should be
 *                          performed in a non-blocking manner.
 *   CFG_IS_TTY             if the descriptor is backed by a TTY.
 *
 * On failure, returns a negative value.
 *
 * Errors:
 *   EINVAL     if fd is not a valid file descriptor index.
 *   ENOENT     if there is no such file descriptor open. */
int64_t get_desc_cfg(uint32_t fd);

/* Set the configuration flags of a file descriptor. Only some of the flags
 * supported by get_desc_cfg() may be modified via set_desc_cfg():
 *
 *   CFG_CLOSE_ON_EXEC
 *   CFG_NONBLOCKING
 *
 * Any other bit positions in the cfg bitfield must be exactly as obtained
 * from get_desc_cfg(), as otherwise they would indicate an unsupported
 * configuration modification.
 *
 * Returns 0 on success, and a negative value on failure.
 *
 * Errors:
 *   EINVAL     if fd is not a valid file descriptor index, or if the cfg
 *              bitfield indicates invalid/unsupported modifications.
 *   ENOENT     if there is no such file descriptor open. */
int64_t set_desc_cfg(uint32_t fd, uint32_t cfg);

/* Require read permission. */
#define O_READ          ((uint32_t) 1 << 0)
/* Require write permission. */
#define O_WRITE         ((uint32_t) 1 << 1)
/* Open in append mode, where all writes occur at the end of the file. */
#define O_APPEND        ((uint32_t) 1 << 2)
/* Require the path to refer to a directory; fail if it's a file
 * instead. (The only other flags that may be specified are O_CLOSE_EXEC,
 * O_CREATE, O_NEW, and O_CLOSE.) */
#define O_DIR           ((uint32_t) 1 << 3)
/* Request that later reads/writes are done in nonblocking mode. */
#define O_NONBLOCK      ((uint32_t) 1 << 4)
/* Close the descriptor on exec. */
#define O_CLOSE_EXEC    ((uint32_t) 1 << 5)
/* Create the file if it didn't already exist. (If O_DIR is specified,
 * creates a directory instead.) */
#define O_CREATE        ((uint32_t) 1 << 6)
/* Create the file, and require that it did not already exist. (If O_DIR
 * is specified, creates a directory instead.) O_CREATE need not be given
 * as well. */
#define O_NEW           ((uint32_t) 1 << 7)
/* Immediately close the file or directory after opening it. Requires that
 * O_CREATE or O_NEW is specified. */
#define O_CLOSE         ((uint32_t) 1 << 8)

// TODO: comment
int64_t open(const char *path, uint32_t flags);
int64_t open_at(uint32_t dir_fd, const char *path, uint32_t flags);

/* Close the given file descriptor. Returns 0 on success, or a negative value
 * on failure.
 *
 * Errors:
 *   EINVAL     if fd is not a valid file descriptor index.
 *   ENOENT     if there is no such file descriptor open. */
int64_t close(uint32_t fd);

/* Duplicate the given file descriptor. The 'dup' variant has the kernel find an
 * empty file descriptor and copy into that descriptor. The 'dup_into' instead
 * specifies a file descriptor new_fd to use; the kernel closes the new_fd (if
 * it is currently open) and then duplicates fd into the new_fd. If fd and new_fd
 * are equal, no changes are made.
 *
 * On success, returns the new file descriptor index; on failure, returns a
 * negative value.
 *
 * Errors:
 *   EINVAL     if fd or new_fd is not a valid file descriptor index.
 *   ENOENT     if fd does not refer to an open file descriptor.
 *   EFULL      if there are no available file descriptors (due to already having
 *              too many open). */
int64_t dup(uint32_t fd);
int64_t dup_into(uint32_t fd, uint32_t new_fd);

/* Read the requested number of bytes from a file descriptor into the specified buffer.
 *
 * By default, the read blocks until the requested number of bytes is available (or
 * until there is no more input, or there is an error). The CFG_NONBLOCKING flag, if
 * set, indicates that the read should be performed in a non-blocking manner, so the
 * number of bytes read may be less than the number requested. In non-blocking mode,
 * the data is read "best-effort", and if it is impossible to read any data without
 * blocking, -EAGAIN is returned. (The CFG_NONBLOCKING flag may be set using the
 * set_desc_cfg() syscall.)
 *
 * On success, if the number of bytes read is 0, it indicates an end-of-input
 * condition: no more data will ever be read from the file descriptor, and all future
 * reads will return 0 bytes read. This holds in blocking and non-blocking modes.
 *
 * Returns the number of bytes read on success, or a negative value on failure.
 *
 * Errors:
 *   EFAULT     if the buffer is not user-writeable.
 *   EINVAL     if the fd is not a valid file descriptor index.
 *   ENOTSUP    if the given file descriptor is write-only.
 *   ENOENT     if fd does not refer to an open file descriptor.
 *   EIO        if there is an I/O error while reading.
 *   EAGAIN     if CFG_NONBLOCKING is set, but no data can be read without blocking. */
int64_t read(uint32_t fd, uint8_t *buf, uint64_t bytes);

/* Write the requested number of bytes from the specified buffer to a file descriptor.
 *
 * By default, the write blocks until the requested number of bytes has been written
 * (or the underlying device has been closed, or there is an error). The CFG_NONBLOCKING
 * flag, if set, indicates that the write should be performed in a non-blocking manner,
 * so the number of bytes written may be less than the number requested. In non-blocking
 * mode, the write is "best-effort", and if it is impossible to write any data without
 * blocking, -EAGAIN is returned. (The CFG_NONBLOCKING flag may be set using the
 * set_desc_cfg() syscall.)
 *
 * On success, if the number of bytes written is 0, it indicates an end-of-output
 * condition: no more data can ever be written to the file descriptor, and all future writes
 * will also return 0 bytes written. This holds in blocking and non-blocking modes.
 *
 * Returns the number of bytes written on success, or a negative value on failure.
 *
 * Errors:
 *   EFAULT     if the buffer is not user-readable.
 *   EINVAL     if the fd is not a valid file descriptor index.
 *   ENOTSUP    if the given file descriptor is read-only.
 *   ENOENT     if fd does not refer to an open file descriptor.
 *   EIO        if there is an I/O error while writing.
 *   EAGAIN     if CFG_NONBLOCKING is set but no data can be written without blocking. */
int64_t write(uint32_t fd, const uint8_t *buf, uint64_t bytes);

/* Fork the current process.
 *
 * In the parent process, this returns +child_id on success and a negative value on
 * failure. In the child process (and only on success), this returns 0. Note that the
 * child_id is guaranteed to be nonzero, so it is possible to distinguish (on success)
 * between the parent and child processes by testing whether or not the return value is
 * positive or zero.
 *
 * Errors:
 *   EFULL      if no more process slots are available.
 *   ENOMEM     if out of memory. */
int64_t fork();

/* Replace the current process with a new process image. An optional argument buffer may be
 * passed to the fresh process. If args_length is 0, args is unused and may be NULL.
 *
 * This only returns on failure.
 *
 * Errors:
 *   EFAULT     if name, filename, or args (together with the args_length) do not point to
 *              user-readable memory regions.
 *   EINVAL     if the name is too long.
 *   ENOENT     if the filename does not refer to an existing file.
 *   EEXEC      if the file exists, but is not a valid executable ELF file.
 *   ENOMEM     if out of memory. */
void exec(const char *name, const char *filename, ptr_t args, uint64_t args_length);

/* These variants of 'exec' takes an array of strings, along with an explicitly or implicitly given
 * environment, instead of an opaque binary blob as the argument.
 * Each packs the strings into a blob, with the simple format:
 *      argc envc
 *      argv[0]-offset ... argv[argc-1]-offset
 *      envp[0]-offset ... envp[envc-1]-offset 0
 *      argv[0]-content argv[1]-content ... argv[argc-1]-content
 *      envp[0]-content envp[1]-content ... envp[envc-1]-content
 * where envp is either given (execve) or is the current program environment (execv), and where envc
 * is the number of entries in the envp.
 * No padding is included, and each argv and envp content is left null-terminated. Each offset is
 * relative to the start of the buffer (i.e. offset 0 would refer to argc).
 *
 * Note that these functions allocate the blob on the stack, so the arguments should not be too
 * large that the stack overflows.
 *
 * Like 'exec', these variants only return on failure, and the possible errors are the same. */
void execv(const char *name, const char *filename, uint64_t argc, const char **argv);
void execve(const char *name, const char *filename, uint64_t argc, const char **argv, const char **envp);

/* Get the current heap parameters, i.e. a pointer to the start of the heap and the heap size
 * (in bytes).
 *
 * Returns 0 on success, or a negative value on failure.
 *
 * Errors:
 *   EFAULT     if either argument is not a user-writeable pointer. */
int64_t get_heap(ptr_t *heap_start, uint64_t *heap_size);

/* Set the current heap parameters, which specifically is the current heap size. The resulting
 * heap size may be more than the requested heap size, but will never be less (except in an
 * out-of-memory condition).
 *
 * Returns 0 on success, or a negative value on failure.
 *
 * Errors:
 *   ENOMEM     if the requested heap size is too large to be satisfied. */
int64_t set_heap(uint64_t heap_size);

/* Get the current process's id. */
uint32_t get_pid();

/* Set the current working directory to the directory referenced by path (relative, of course,
 * to the current working directory).
 *
 * Returns 0 on success, or a negative value on failure.
 *
 * Errors:
 *   EFAULT     if the path is not user-readable.
 *   ENOENT     if the path does not refer to an existing file system entry.
 *   ENOTDIR    if the path refers to a non-directory file system entry. */
int64_t chdir(const char *path);

// TODO: comment
int64_t ioctl(uint32_t fd, uint64_t ioctl_nr, void *buf, uint64_t buf_len);

// TODO: comment
int64_t readdir(uint32_t fd, dir_entry_t *buf, uint64_t buf_entries,
        uint64_t *entries_read);

// TODO: comment
int64_t stat(const char *path, stat_t *buf);
int64_t stat_at(uint32_t fd, const char *path, stat_t *buf);

// TODO: comment
// note that 'flags' is a union of O_* flags, specifically O_NONBLOCK or O_CLOSE_EXEC
// ENOMEM: not enough memory to allocate pipe
// EFULL: too many file descriptors already open
int64_t mkpipe(uint32_t *rfd, uint32_t *wfd, uint32_t flags);

// TODO: comment
// note that SIGKILL, SIGSTOP, SIGCONT cannot be switched away from their default handlers,
// and produce EINVAL
int64_t sigaction(signal_nr_t signal_nr, signal_handler_t handler);

// TODO: comment
// note that only some signals actually allow a buffer; all others must have buf_len == 0.
// signals that allow buffers: SIGINT, SIGTERM, SIGKILL
int64_t signal(uint32_t pid, signal_nr_t signal_nr, uint8_t buf_len, const uint8_t *buf);

// TODO: comment
// note that this does not overwrite errno
// note that this parameter is precisely the argument passed into the signal handler
// which is calling sigreturn
void sigreturn(void *context);

// For debug only. Cause a kernel panic with the specified message.
void panic(const char *msg) __attribute__((noreturn));

#endif
