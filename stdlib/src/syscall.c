#include <syscall.h>
#include <errno.h>
#include <uapi/syscall_nr.h>

/* These functions are defined in syscall_asm.s. */
int64_t _syscall0(uint64_t syscall_nr);
int64_t _syscall1(uint64_t syscall_nr, uint64_t arg0);
int64_t _syscall2(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1);
int64_t _syscall3(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                  uint64_t arg2);
int64_t _syscall4(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                  uint64_t arg2, uint64_t arg3);
int64_t _syscall5(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                  uint64_t arg2, uint64_t arg3, uint64_t arg4);
int64_t _syscall6(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                  uint64_t arg2, uint64_t arg3, uint64_t arg4,
                  uint64_t arg5);
int64_t _syscall7(uint64_t syscall_nr, uint64_t arg0, uint64_t arg1,
                   uint64_t arg2, uint64_t arg3, uint64_t arg4,
                   uint64_t arg5, uint64_t arg6);

INLINE USE_VALUE int64_t wrap_err(int64_t errcode)
{
    if (errcode >= 0) {
        errno = 0;
        return errcode;
    } else {
        errno = (errno_t) -errcode;
        return -1;
    }
}

void exit(uint32_t exit_code)
{
    _syscall1(SYSCALL_EXIT, (uint64_t) exit_code);
    __builtin_unreachable();
}

int64_t wait(uint32_t *child_id, uint64_t flags)
{
    return wrap_err(_syscall2(SYSCALL_WAIT, (uint64_t) child_id, flags));
}

int64_t sleep(uint64_t ms)
{
    return wrap_err(_syscall1(SYSCALL_SLEEP, ms));
}

int64_t get_desc_cfg(uint32_t fd)
{
    return wrap_err(_syscall1(SYSCALL_GET_DESC_CFG, (uint64_t) fd));
}

int64_t set_desc_cfg(uint32_t fd, uint32_t cfg)
{
    return wrap_err(_syscall2(SYSCALL_SET_DESC_CFG, (uint64_t) fd, (uint64_t) cfg));
}

int64_t open(const char *path, uint32_t flags)
{
    return wrap_err(_syscall2(SYSCALL_OPEN, (uint64_t) path, (uint64_t) flags));
}

int64_t open_at(uint32_t dir_fd, const char *path, uint32_t flags)
{
    return wrap_err(_syscall3(SYSCALL_OPEN_AT,
                (uint64_t) dir_fd, (uint64_t) path, (uint64_t) flags));
}

int64_t close(uint32_t fd)
{
    return wrap_err(_syscall1(SYSCALL_CLOSE, (uint64_t) fd));
}

int64_t dup(uint32_t fd)
{
    return wrap_err(_syscall1(SYSCALL_DUP, (uint64_t) fd));
}

int64_t dup_into(uint32_t fd, uint32_t new_fd)
{
    return wrap_err(_syscall2(SYSCALL_DUP_INTO, (uint64_t) fd, (uint64_t) new_fd));
}

int64_t read(uint32_t fd, uint8_t *buf, uint64_t bytes)
{
    return wrap_err(_syscall3(SYSCALL_READ, (uint64_t) fd, (uint64_t) buf, bytes));
}

int64_t write(uint32_t fd, const uint8_t *buf, uint64_t bytes)
{
    return wrap_err(_syscall3(SYSCALL_WRITE, (uint64_t) fd, (uint64_t) buf, bytes));
}

int64_t fork()
{
    return wrap_err(_syscall0(SYSCALL_FORK));
}

void exec(const char *name, const char *filename, ptr_t args, uint64_t args_length)
{
    ignore_value(wrap_err(_syscall4(SYSCALL_EXEC,
            (uint64_t) name, (uint64_t) filename, (uint64_t) args, args_length)));
}

int64_t get_heap(ptr_t *heap_start, uint64_t *heap_size)
{
    return wrap_err(_syscall2(SYSCALL_GET_HEAP,
                (uint64_t) heap_start, (uint64_t) heap_size));
}

int64_t set_heap(uint64_t heap_size)
{
    return wrap_err(_syscall1(SYSCALL_SET_HEAP, heap_size));
}

uint32_t get_pid()
{
    // TODO: what if there is an error...
    errno = 0;
    return (uint32_t) _syscall0(SYSCALL_GET_PID);
}

int64_t chdir(const char *path)
{
    return wrap_err(_syscall1(SYSCALL_CHDIR, (uint64_t) path));
}

int64_t ioctl(uint32_t fd, uint64_t ioctl_nr, void *buf, uint64_t buf_len)
{
    return wrap_err(_syscall4(SYSCALL_IOCTL,
                (uint64_t) fd, ioctl_nr, (uint64_t) buf, buf_len));
}

int64_t readdir(uint32_t fd, dir_entry_t *buf, uint64_t buf_entries,
        uint64_t *entries_read)
{
    return wrap_err(_syscall4(SYSCALL_READDIR,
                (uint64_t) fd, (uint64_t) buf, buf_entries, (uint64_t) entries_read));
}

int64_t stat(const char *path, stat_t *buf)
{
    return wrap_err(_syscall2(SYSCALL_STAT, (uint64_t) path, (uint64_t) buf));
}

int64_t stat_at(uint32_t fd, const char *path, stat_t *buf)
{
    return wrap_err(_syscall3(SYSCALL_STAT_AT,
                (uint64_t) fd, (uint64_t) path, (uint64_t) buf));
}

int64_t mkpipe(uint32_t *rfd, uint32_t *wfd, uint32_t flags)
{
    return wrap_err(_syscall3(SYSCALL_MKPIPE,
                (uint64_t) rfd, (uint64_t) wfd, (uint64_t) flags));
}

int64_t sigaction(signal_nr_t signal_nr, signal_handler_t handler)
{
    return wrap_err(_syscall2(SYSCALL_SIGACTION,
                (uint64_t) signal_nr, (uint64_t) handler));
}

int64_t signal(uint32_t pid, signal_nr_t signal_nr, uint8_t buf_len, const uint8_t *buf)
{
    return wrap_err(_syscall4(SYSCALL_SIGNAL,
                (uint64_t) pid, (uint64_t) signal_nr, (uint64_t) buf_len, (uint64_t) buf));
}

void sigreturn(void *context)
{
    _syscall1(SYSCALL_SIGRETURN, (uint64_t) context);
}

void panic(const char *msg)
{
    _syscall1(SYSCALL_PANIC, (uint64_t) msg);
    __builtin_unreachable();
}
