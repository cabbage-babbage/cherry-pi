#include <std.h>
#include <stdio.h>
#include <syscall.h>
#include <stdarg.h>
#include <string.h>
#include <uapi/errno.h>

void putc(char c)
{
    char buf[1] = { c };
    write(1, (uint8_t *) buf, 1);
}

void puts(const char *s)
{
    uint64_t remaining = strlen(s);
    while (remaining) {
        int64_t result = write(1, (uint8_t *) s, remaining);
        if (result < 0) {
            // TODO: report the (unexpected!) error somehow
            break;
        }
        uint64_t written = (uint64_t) result;
        s += written;
        remaining -= written;
    }
}

void fputc(uint32_t fd, char c)
{
    char buf[1] = { c };
    write(fd, (uint8_t *) buf, 1);
}

void fputs(uint32_t fd, const char *s)
{
    uint64_t remaining = strlen(s);
    while (remaining) {
        int64_t result = write(fd, (uint8_t *) s, remaining);
        if (result < 0) {
            // TODO: report the (unexpected!) error somehow
            break;
        }
        uint64_t written = (uint64_t) result;
        s += written;
        remaining -= written;
    }
}

USE_VALUE char numeric_char(uint8_t val)
{
    if (val < 10) {
        return '0' + val;
    } else if (val < 36) {
        return 'a' + (val - 10);
    } else {
        //dbg_error("got val %u8; should be 0-35.\n", val);
        return '?';
    }
}

/* Standard callback to "put" a character. What "put" means is dependent on usage,
 * and is generally up to the callback. The aux data is internal to the callback.
 * The return value indicates if the character was actually printed or not.
 * The value written to cont generally indicates whether or not a printing operation
 * should continue after putting the given character. */
typedef bool (*putc_fn_t)(char c, void *aux, bool *cont);

/* Predeclare all these helper functions, since some of them call each other.
 * Each one returns true if printing operations should continue (according to
 * the putc_fn passed in) after doing the particular operation. */
USE_VALUE bool cputc(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        char c);
USE_VALUE bool cputs(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        const char *s);
USE_VALUE bool cputc_quoted(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        char c);
USE_VALUE bool cputs_quoted(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        const char *s);
USE_VALUE bool cput_int8(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        int8_t val, int32_t base);
USE_VALUE bool cput_uint8(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        uint8_t val, int32_t base);
USE_VALUE bool cput_int16(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        int16_t val, int32_t base);
USE_VALUE bool cput_uint16(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        uint16_t val, int32_t base);
USE_VALUE bool cput_int32(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        int32_t val, int32_t base);
USE_VALUE bool cput_uint32(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        uint32_t val, int32_t base);
USE_VALUE bool cput_int64(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        int64_t val, int32_t base);
USE_VALUE bool cput_uint64(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        uint64_t val, int32_t base);
USE_VALUE bool cput_ptr(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        ptr_t val);

INLINE bool cputc(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        char c)
{
    bool cont;
    if (putc_fn(c, aux, &cont)) {
        (*chars_written)++;
    }
    return cont;
}

INLINE bool cputs(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        const char *s)
{
    while (*s) {
        if (cputc(putc_fn, aux, chars_written, *s)) {
            s++;
        } else {
            return false;
        }
    }
    return true;
}

INLINE bool cputc_quoted(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        char c)
{
    uint8_t b = (uint8_t) c;
    switch (b) {
        case 0: return cputs(putc_fn, aux, chars_written, "\\0");
        case '\t': return cputs(putc_fn, aux, chars_written, "\\t");
        case '\r': return cputs(putc_fn, aux, chars_written, "\\r");
        case '\n': return cputs(putc_fn, aux, chars_written, "\\n");
        case '\\': return cputs(putc_fn, aux, chars_written, "\\\\");
        case '\'': return cputs(putc_fn, aux, chars_written, "\\'");
        case '"': return cputs(putc_fn, aux, chars_written, "\\\"");
    }
    /* Not a specially-escaped character. Either put it normally, or encode
     * in hex. */
    if (32 <= b && b <= 126) {
        return cputc(putc_fn, aux, chars_written, c);
    } else {
        if (!cputc(putc_fn, aux, chars_written, '\\')) {
            return false;
        }
        // TODO: fix this once we (finally!) have a zero-pad integer printing routine...
        if (b < 16) {
            if (!cputc(putc_fn, aux, chars_written, '0')) {
                return false;
            }
        }
        return cput_uint8(putc_fn, aux, chars_written, b, 16);
    }
}

INLINE bool cputs_quoted(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        const char *s)
{
    while (*s) {
        if (cputc_quoted(putc_fn, aux, chars_written, *s)) {
            s++;
        } else {
            return false;
        }
    }
    return true;
}

INLINE bool cput_int8(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        int8_t val, int32_t base)
{
    return cput_int64(putc_fn, aux, chars_written, (int64_t) val, base);
}

INLINE bool cput_uint8(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        uint8_t val, int32_t base)
{
    return cput_uint64(putc_fn, aux, chars_written, (uint64_t) val, base);
}

INLINE bool cput_int16(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        int16_t val, int32_t base)
{
    return cput_int64(putc_fn, aux, chars_written, (int64_t) val, base);
}

INLINE bool cput_uint16(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        uint16_t val, int32_t base)
{
    return cput_uint64(putc_fn, aux, chars_written, (uint64_t) val, base);
}

INLINE bool cput_int32(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        int32_t val, int32_t base)
{
    return cput_int64(putc_fn, aux, chars_written, (int64_t) val, base);
}

INLINE bool cput_uint32(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        uint32_t val, int32_t base)
{
    return cput_uint64(putc_fn, aux, chars_written, (uint64_t) val, base);
}

bool cput_uint64(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        uint64_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        //dbg_error("got base %i32; should be 2-36.\n", base);
        return cputs(putc_fn, aux, chars_written, "?");
    }

    if (val == 0) {
        return cputc(putc_fn, aux, chars_written, '0');
    }
    char buf[65];
    int digits = 0; /* May not be digits, but whatever. */
    while (val != 0) {
        buf[digits++] = numeric_char(val % base);
        val /= base;
    }
    for (int d = digits - 1; d >= 0; d--) {
        if (!cputc(putc_fn, aux, chars_written, buf[d])) {
            return false;
        }
    }
    return true;
}

bool cput_int64(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        int64_t val, int32_t base)
{
    if (base < 2 || base > 36)  {
        //dbg_error("got base %i32; should be 2-36.\n", base);
        return cputs(putc_fn, aux, chars_written, "?");
    }

    if (val < 0) {
        if (!cputc(putc_fn, aux, chars_written, '-')) {
            return false;
        }
        val = -val; /* TODO: what about MIN_INT64? */
    }
    return cput_uint64(putc_fn, aux, chars_written, (uint64_t) val, base);
}

INLINE bool cput_ptr(putc_fn_t putc_fn, void *aux, uint64_t *chars_written,
        ptr_t val)
{
    return cput_uint64(putc_fn, aux, chars_written, (uint64_t) val, 16);
}

/*
 * Here we only use puts, instead of using printf, in case a
 * bug in printf is causing this to be called. (We don't want
 * to end up in an infinite loop of calling printf to report
 * an error with a broken printf!) Therefore the `msg` must
 * be a literal string.
 */
#define INVALID_PRINTF_FORMAT(fmt, msg) \
    do { \
        puts("[PRINTF] invalid format string '"); \
        puts(fmt); \
        puts("': "); \
        puts(msg); \
        puts("\n"); \
        while (true) ; \
    } while (0)

/*
 * When fmt is just after a %iNN or %uNN specifier, this
 * checks for a {BB} base specifier. Return value is a pointer
 * to the format string _after_ such a base specifier (if it exists),
 * and this sets `base` to the parsed base (if valid), or -1 otherwise.
 * (If there is no base specifier, sets `base` to the default value 10.)
 */
USE_VALUE const char *read_base(const char *fmt, int32_t *base)
{
    // TODO: handle any base 2-36 that isn't just 2, 8, 10, or 16.
    if (startswith(fmt, "{2}")) {
        *base = 2;
        return fmt + 3;
    } else if (startswith(fmt, "{8}")) {
        *base = 8;
        return fmt + 3;
    } else if (startswith(fmt, "{10}")) {
        *base = 10;
        return fmt + 4;
    } else if (startswith(fmt, "{16}")) {
        *base = 16;
        return fmt + 4;
    } else if (*fmt == '{') {
        *base = -1;
        return fmt;
    } else {
        *base = 10;
        return fmt;
    }
}

/* The putc_fn is called once for each character "print"ed by vfprintf. For example,
 * printf() would supply a callback which actually prints the character to the screen,
 * while sprintf() would supply a callback which appends the character to a buffer.
 * The putc_fn return value specifies whether or not vcprintf() should continue
 * "print"ing characters, i.e. false is like a "break" statement.
 * The aux parameter is for the callback's internal use.
 *
 * Returns the number of characters written. (A format error is a programming
 * error, so is not reported via a return value but rather via a panic.)
 * Note the naming:
 *    v c print f
 *    | |   |   \-- use a format string for printing
 *    | |   \------ print a string (to some destination)
 *    | \---------- use a callback to print each character
 *    \------------ takes an explicit va_list argument */
INLINE uint64_t vcprintf(putc_fn_t putc_fn, void *aux, const char *fmt, va_list args)
{
    const char *save_fmt = fmt;
    uint64_t chars_written = 0;

    while (*fmt) {
        if (*fmt == '%') {
            fmt++;
            if (!*fmt) {
                INVALID_PRINTF_FORMAT(save_fmt, "must not end with '%'");
            }
            switch (*fmt) {
                case 'i': {
                    fmt++;
                    uint32_t size; // in bits
                    int32_t base;
                    if (startswith(fmt, "8")) {
                        fmt++;
                        size = 8;
                    } else if (startswith(fmt, "16")) {
                        fmt += 2;
                        size = 16;
                    } else if (startswith(fmt, "32")) {
                        fmt += 2;
                        size = 32;
                    } else if (startswith(fmt, "64")) {
                        fmt += 2;
                        size = 64;
                    } else {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid '%i' specifier");
                    }

                    fmt = read_base(fmt, &base);
                    if (base == -1) {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid base for '%i' specifier");
                    }

                    if (size == 8) {
                        if (!cput_int8(putc_fn, aux, &chars_written, (int8_t) va_arg(args, int), base)) {
                            return chars_written;
                        }
                    } else if (size == 16) {
                        if (!cput_int16(putc_fn, aux, &chars_written, (int16_t) va_arg(args, int), base)) {
                            return chars_written;
                        }
                    } else if (size == 32) {
                        if (!cput_int32(putc_fn, aux, &chars_written, va_arg(args, int32_t), base)) {
                            return chars_written;
                        }
                    } else if (size == 64) {
                        if (!cput_int64(putc_fn, aux, &chars_written, va_arg(args, int64_t), base)) {
                            return chars_written;
                        }
                    }

                    break;
                }
                case 'u': {
                    fmt++;
                    uint32_t size; // in bits
                    int32_t base;
                    if (startswith(fmt, "8")) {
                        fmt++;
                        size = 8;
                    } else if (startswith(fmt, "16")) {
                        fmt += 2;
                        size = 16;
                    } else if (startswith(fmt, "32")) {
                        fmt += 2;
                        size = 32;
                    } else if (startswith(fmt, "64")) {
                        fmt += 2;
                        size = 64;
                    } else {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid '%u' specifier");
                    }

                    fmt = read_base(fmt, &base);
                    if (base == -1) {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid base for '%u' specifier");
                    }

                    if (size == 8) {
                        if (!cput_uint8(putc_fn, aux, &chars_written, (uint8_t) va_arg(args, int), base)) {
                            return chars_written;
                        }
                    } else if (size == 16) {
                        if (!cput_uint16(putc_fn, aux, &chars_written, (uint16_t) va_arg(args, int), base)) {
                            return chars_written;
                        }
                    } else if (size == 32) {
                        if (!cput_uint32(putc_fn, aux, &chars_written, va_arg(args, uint32_t), base)) {
                            return chars_written;
                        }
                    } else if (size == 64) {
                        if (!cput_uint64(putc_fn, aux, &chars_written, va_arg(args, uint64_t), base)) {
                            return chars_written;
                        }
                    }

                    break;
                }
                case 'p':
                    fmt++;
                    ptr_t p = va_arg(args, ptr_t);
                    if (!cput_uint64(putc_fn, aux, &chars_written, (uint64_t) p, 16)) {
                        return chars_written;
                    }
                    break;
                case 's':
                    fmt++;
                    if (!cputs(putc_fn, aux, &chars_written, va_arg(args, const char*))) {
                        return chars_written;
                    }
                    break;
                case 'c':
                    fmt++;
                    if (!cputc(putc_fn, aux, &chars_written, (char) va_arg(args, int))) {
                        return chars_written;
                    }
                    break;
                case 'q':
                    fmt++;
                    if (*fmt == 'c') {
                        fmt++;
                        if (!cputc_quoted(putc_fn, aux, &chars_written, (char) va_arg(args, int))) {
                            return chars_written;
                        }
                    } else if (*fmt == 's') {
                        fmt++;
                        if (!cputs_quoted(putc_fn, aux, &chars_written, va_arg(args, const char*))) {
                            return chars_written;
                        }
                    } else {
                        INVALID_PRINTF_FORMAT(save_fmt, "invalid '%q' specifier");
                    }
                    break;
                case 'b':
                    fmt++;
                    bool b = (bool) va_arg(args, int);
                    if (!cputs(putc_fn, aux, &chars_written, b ? "true" : "false")) {
                        return chars_written;
                    }
                    break;
                case 'e':
                    fmt++;
                    errno_t errno = va_arg(args, errno_t);
                    switch (errno) {
#define CASE_ERRNO(errno_name, errno_val) \
                        case errno_val: \
                            if (!cputs(putc_fn, aux, &chars_written, errno_name)) { \
                                return chars_written; \
                            } \
                            break;

                        FOREACH_ERRNO(CASE_ERRNO)
                        default:
                            if (!cputs(putc_fn, aux, &chars_written, "errno ")) {
                                return chars_written;
                            }
                            if (!cput_uint32(putc_fn, aux, &chars_written, errno, 10)) {
                                return chars_written;
                            }
                            break;
                    }
                    break;
                case '%':
                    fmt++;
                    if (!cputc(putc_fn, aux, &chars_written, '%')) {
                        return chars_written;
                    }
                    break;
                case '{':
                    fmt++;
                    if (!cputc(putc_fn, aux, &chars_written, '{')) {
                        return chars_written;
                    }
                    break;
                case '}':
                    fmt++;
                    if (!cputc(putc_fn, aux, &chars_written, '}')) {
                        return chars_written;
                    }
                    break;
                default:
                    INVALID_PRINTF_FORMAT(save_fmt, "nonexistent specifier");
            }
        } else {
            if (!cputc(putc_fn, aux, &chars_written, *fmt)) {
                return chars_written;
            }
            fmt++;
        }
    }

    return chars_written;
}

bool printf_putc(char c, void *aux, bool *cont)
{
    putc(c);
    *cont = true;
    return true;
}
uint64_t printf(const char *fmt, ...)
{
    uint64_t chars_written;

    va_list args;
    va_start(args, fmt);
    chars_written = vcprintf(&printf_putc, NULL, fmt, args);
    va_end(args);

    return chars_written;
}

bool fprintf_putc(char c, void *aux, bool *cont)
{
    uint32_t fd = *((uint32_t *) aux);
    fputc(fd, c);
    *cont = true;
    return true;
}
uint64_t fprintf(uint32_t fd, const char *fmt, ...)
{
    uint64_t chars_written;

    va_list args;
    va_start(args, fmt);
    chars_written = vcprintf(&fprintf_putc, &fd, fmt, args);
    va_end(args);

    return chars_written;
}

typedef struct {
    char *dst;
    uint64_t index;
    uint64_t limit;
} sprintf_state_t;

bool sprintf_putc(char c, void *aux, bool *cont)
{
    sprintf_state_t *state = aux;
    state->dst[state->index++] = c;
    *cont = true;
    return true;
}
uint64_t sprintf(char *dst, const char *fmt, ...)
{
    uint64_t chars_written;
    sprintf_state_t state = {
        .dst = dst,
        .index = 0,
        .limit = 0 /* limit is unused. */
    };

    va_list args;
    va_start(args, fmt);
    chars_written = vcprintf(&sprintf_putc, &state, fmt, args);
    va_end(args);

    dst[chars_written] = 0;
    return chars_written;
}

bool snprintf_putc(char c, void *aux, bool *cont)
{
    sprintf_state_t *state = aux;
    if (state->index < state->limit) {
        state->dst[state->index++] = c;
        *cont = state->index < state->limit;
        return true;
    } else {
        *cont = false;
        return false;
    }
}
uint64_t snprintf(char *dst, uint64_t buf_size, const char *fmt, ...)
{
    if (buf_size == 0) {
        return 0;
    }

    uint64_t chars_written;
    sprintf_state_t state = {
        .dst = dst,
        .index = 0,
        .limit = buf_size - 1
    };

    va_list args;
    va_start(args, fmt);
    chars_written = vcprintf(&snprintf_putc, &state, fmt, args);
    va_end(args);

    dst[chars_written] = 0;
    return chars_written;
}

int64_t getc()
{
    char buf[1];
    int64_t count = read(0, (uint8_t *) buf, 1);
    if (count < 0) {
        return count;
    } else if (count == 0) {
        return EOF;
    } else {
        return buf[0];
    }
}
