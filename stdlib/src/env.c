#include <env.h>
#include <heap.h>
#include <syscall.h>
#include <string.h>

uint64_t envc, envp_len;
/* The envp array is always null terminated, even though we know the
 * number of valid elements (envc) and the total number of elements
 * (envp_len). */
char **envp;

void init_env(uint64_t stack_envc, char **stack_envp)
{
    envc = stack_envc;

    envp_len = envc + 1;
    envp = malloc(envp_len * sizeof(char *));
    if (!envp) {
        exit(129);
    }

    memcpy(envp, stack_envp, (envc + 1) * sizeof(char *));
}

char *const *environment()
{
    return envp;
}

/* Determines whether or not an environment entry records the value of the
 * given variable (by name). That is, returns true if the entry is of the
 * form 'name=value', and false otherwise. */
INLINE bool entry_has_var(char *entry, const char *name, uint64_t name_len)
{
    return startswith(entry, name) && entry[name_len] == '=';
}

char *getenv(const char *name)
{
    uint64_t name_len = strlen(name);
    for (uint64_t i = 0; i < envc; i++) {
        char *env = envp[i];
        if (entry_has_var(env, name, name_len)) {
            return &env[name_len + 1];
        }
    }
    return NULL;
}

/* Put an entry of the form 'name=value' into the environment. Returns
 * 0 on success, and an errno on failure. Note that the string is not
 * copied; if you modify it later, it will directly affect the environment.
 *
 * Errors:
 *   EINVAL     if the string is not of the form 'name=value', with a
 *              nonempty name.
 *   ENOMEM     if there was not enough memory to add the entry to the
 *              environment. */
errno_t putenv(char *string)
{
    bool splits = false;
    uint64_t name_len = 0;
    for (char *s = string; *s; s++) {
        if (*s == '=') {
            splits = true;
            break;
        }
        name_len++;
    }
    if (!splits || name_len == 0) {
        return EINVAL;
    }

    for (uint64_t i = 0; i < envc; i++) {
        char *env = envp[i];
        if (memcmp(env, string, name_len) == 0 && env[name_len] == '=') {
            /* An environment variable already exists with the desired name. Overwrite
             * this entry. */
            envp[i] = string;
            return 0;
        }
    }

    /* We must add a new entry. */
    if (envc + 1 == envp_len) {
        envp_len *= 2;
        char **new_envp = realloc(envp, envp_len * sizeof(char *));
        if (!new_envp) {
            return ENOMEM;
        }
        envp = new_envp;
    }
    envp[envc++] = string;
    envp[envc] = NULL;
    return 0;
}

int64_t setenv(const char *name, const char *value, bool overwrite)
{
    /* The name must be nonempty and cannot contain any '='. */
    if (!*name) {
        errno = EINVAL;
        return -1;
    }
    for (const char *s = name; *s; s++) {
        if (*s == '=') {
            errno = EINVAL;
            return -1;
        }
    }

    if (!overwrite && getenv(name)) {
        /* Already exists. */
        errno = 0;
        return 0;
    }

    char *env_str = malloc(strlen(name) + 1 /* '=' */ + strlen(value) + 1 /* null terminator */);
    if (!env_str) {
        errno = ENOMEM;
        return -1;
    }

    const char *strs[] = { name, "=", value, NULL };
    strcat_array(env_str, strs);
    errno = putenv(env_str);
    return errno ? -1 : 0;
}

int64_t unsetenv(const char *name)
{
    /* The name must be nonempty and cannot contain any '='. */
    if (!*name) {
        errno = EINVAL;
        return -1;
    }
    for (const char *s = name; *s; s++) {
        if (*s == '=') {
            errno = EINVAL;
            return -1;
        }
    }

    uint64_t name_len = strlen(name);
    for (uint64_t i = 0; i < envc; i++) {
        if (entry_has_var(envp[i], name, name_len)) {
            // TODO: clean up envp[i]! However, we can't necessarily just free() it...
            // need to fix this memory leak.

            /* Just shift the later entries down. */
            for (uint64_t j = i + 1; j < envc; j++) {
                envp[j - 1] = envp[j];
            }
            envc--;
            break;
        }
    }

    errno = 0;
    return 0;
}
