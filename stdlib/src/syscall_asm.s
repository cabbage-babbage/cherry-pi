// The kernel expects the syscall number in x0 and
// the syscall arguments in x1...x7, and will leave
// the result in x0 (like a normal function call).
// The `svc` immediate is required to be 0x1.

// We need to define a _syscallN function for each
// number of arguments, from 0 to 7, so that C knows
// how to call them. Plot twist: they're all the same
// function!
// Note: N specifies the number of _extra_ arguments
// given for the syscall; the first argument to _syscallN
// is always the syscall number.
.global _syscall0, _syscall1, _syscall2, _syscall3
.global _syscall4, _syscall5, _syscall6, _syscall7
_syscall0:
_syscall1:
_syscall2:
_syscall3:
_syscall4:
_syscall5:
_syscall6:
_syscall7:
    svc #1
    ret
