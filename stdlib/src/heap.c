#include <heap.h>
#include <syscall.h>
#include <string.h>

#include <stdio.h> // debug

uint8_t *heap_start;
uint64_t heap_size;

uint8_t *current;

void init_heap()
{
    if (get_heap((ptr_t) &heap_start, &heap_size) < 0) {
        /* Something is terribly wrong. */
        exit(128);
    }

    current = heap_start;
}

USE_VALUE bool should_request_larger_heap(uint64_t alloc_size)
{
    return alloc_size > heap_size ||
        (uint64_t) (current - heap_start) > heap_size - alloc_size;
}

ptr_t malloc(uint64_t size)
{
    return malloc_ext(size, 3);
}

ptr_t malloc_ext(uint64_t size, uint64_t alignment_bits)
{
    if (alignment_bits >= 64) {
        /* There's no way this request can be satisfied. */
        return NULL;
    }

    uint64_t alignment = (uint64_t) 1 << alignment_bits;
    uint64_t alignment_mask = ~((uint64_t) -1 << alignment_bits);

    uint64_t header_size = sizeof(uint64_t);
    current += header_size;

    uint64_t current_offset = alignment_mask & (uint64_t) current;
    uint64_t alignment_offset = current_offset == 0 ? 0 : alignment - current_offset;

    // TODO: do this in one go... no need for a whole loop
    while (should_request_larger_heap(alignment_offset + size)) {
        if (set_heap(heap_size * 3 / 2) < 0) {
            /* Out of memory. */
            return NULL;
        }

        if (get_heap(NULL, &heap_size) < 0) {
            /* Something has gone terribly wrong. */
            exit(128);
        }
    }

    current += alignment_offset;
    ptr_t alloc = current;
    current += size;
    //printf("malloc_ext(size=0x%u64{16}, align<<%u64) => 0x%p (alignment_offset %u64)\n",
    //        size, alignment_bits, alloc, alignment_offset);
    *((uint64_t *) alloc - 1) = size;
    return alloc;
}

ptr_t realloc(ptr_t alloc, uint64_t new_size)
{
    return realloc_ext(alloc, new_size, 3);
}

ptr_t realloc_ext(ptr_t alloc, uint64_t new_size, uint64_t alignment_bits)
{
    //printf("realloc_ext(0x%p, new size 0x%u64{16}, align<<%u64\n",
    //        alloc, new_size, alignment_bits);
    if (!alloc) {
        return malloc_ext(new_size, alignment_bits);
    }

    ptr_t new_alloc = malloc_ext(new_size, alignment_bits);
    if (!new_alloc) {
        return NULL;
    }

    uint64_t old_size = *((uint64_t *) alloc - 1);
    memcpy(new_alloc, alloc, new_size < old_size ? new_size : old_size);
    free(alloc);
    return new_alloc;
}

void free(ptr_t alloc)
{
    // TODO: actually do something
    //printf("free(0x%p): size=0x%u64{16}\n", alloc, *((uint64_t *) alloc - 1));
}
