#include <syscall.h>
#include <string.h>
#include <env.h>

void execv(const char *name, const char *filename, uint64_t argc, const char **argv)
{
    execve(name, filename, argc, argv, (const char **) environment());
}

void execve(const char *name, const char *filename, uint64_t argc, const char **argv,
        const char **envp)
{
    uint64_t envc = 0;
    for (const char **env = envp; *env; env++) {
        envc++;
    }

    uint64_t args_length = sizeof(uint64_t) /* argc */ + sizeof(uint64_t) /* envc */
                         + argc * sizeof(uint64_t) /* one offset per argv */
                         + envc * sizeof(uint64_t) /* one offset per envp */
                         + sizeof(uint64_t) /* terminal NULL pointer for envp */;
    for (uint64_t i = 0; i < argc; i++) {
        args_length += strlen(argv[i]) + 1 /* null terminator */;
    }
    for (uint64_t i = 0; i < envc; i++) {
        args_length += strlen(envp[i]) + 1 /* null terminator */;
    }

    uint8_t args[args_length];
    uint8_t *buf = args;
#define write_uint64(val) \
    do { *((uint64_t *) buf) = (val); buf += sizeof(uint64_t); } while (0)

    /* argc */
    write_uint64(argc);
    /* envc */
    write_uint64(envc);

    /* offsets */
    uint64_t offset = sizeof(uint64_t) /* argc */ + sizeof(uint64_t) /* envc */
                    + argc * sizeof(uint64_t) /* one offset per argv */
                    + envc * sizeof(uint64_t) /* one offset per envp */
                    + sizeof(uint64_t) /* terminal NULL pointer for envp */;
    for (uint64_t i = 0; i < argc; i++) {
        write_uint64(offset);
        offset += strlen(argv[i]) + 1 /* null terminator */;
    }
    for (uint64_t i = 0; i < envc; i++) {
        write_uint64(offset);
        offset += strlen(envp[i]) + 1 /* null terminator */;
    }
    write_uint64(0); /* envp null terminator */

    /* content */
    for (uint64_t i = 0; i < argc; i++) {
        uint64_t content_length = strlen(argv[i]) + 1 /* null terminator */;
        memcpy(buf, argv[i], content_length);
        buf += content_length;
    }
    for (uint64_t i = 0; i < envc; i++) {
        uint64_t content_length = strlen(envp[i]) + 1 /* null terminator */;
        memcpy(buf, envp[i], content_length);
        buf += content_length;
    }

    exec(name, filename, args, args_length);
}
