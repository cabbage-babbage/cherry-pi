#include <std.h>
#include <errno.h>
#include <heap.h>

/* This must be defined by the program that is linking in the stdlib. */
uint32_t main(uint64_t argc, char **argv);

extern void init_env(uint64_t envc, char **envp);

uint32_t _entry(uint8_t *args, uint64_t args_length)
{
    errno = 0;

    init_heap();

    if (args_length >= 2 * sizeof(uint64_t)) {
        uint64_t argc = *((uint64_t *) &args[0]);
        uint64_t envc = *((uint64_t *) &args[sizeof(uint64_t)]);

        uint64_t *offsets = (uint64_t *) &args[2 * sizeof(uint64_t)];
        for (uint64_t i = 0; i < argc + envc + 1; i++) {
        }

        /* We take over the uint64-sized string offsets as the string
         * pointers themselves, since sizeof(uint64_t) == sizeof(char *). */
        char **argv = (char **) &offsets[0];
        char **envp = (char **) &offsets[argc];

        for (uint64_t i = 0; i < argc; i++) {
            argv[i] = (char *) (args + offsets[i]);
        }
        for (uint64_t i = 0; i < envc; i++) {
            envp[i] = (char *) (args + offsets[argc + i]);
        }
        envp[envc] = NULL;

        init_env(envc, envp);

        return main(argc, argv);
    } else {
        char *envp[1] = { NULL };
        init_env(0, envp);

        /* Pass in argv == [""] (argc == 1) to the program. */
        char empty[1] = { 0 };
        char *argv[1] = { empty };
        return main(1, argv);
    }
}
