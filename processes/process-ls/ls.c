#include <std.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <errno.h>

#define CSI "\x1b["

#define RESET               "0"

#define FG_BLACK            "30"
#define FG_RED              "31"
#define FG_GREEN            "32"
#define FG_YELLOW           "33"
#define FG_BLUE             "34"
#define FG_MAGENTA          "35"
#define FG_CYAN             "36"
#define FG_WHITE            "37"
#define FG_GRAY             "90"
#define FG_BRIGHT_RED       "91"
#define FG_BRIGHT_GREEN     "92"
#define FG_BRIGHT_YELLOW    "93"
#define FG_BRIGHT_BLUE      "94"
#define FG_BRIGHT_MAGENTA   "95"
#define FG_BRIGHT_CYAN      "96"
#define FG_BRIGHT_WHITE     "97"

#define BG_BLACK            "40"
#define BG_RED              "41"
#define BG_GREEN            "42"
#define BG_YELLOW           "43"
#define BG_BLUE             "44"
#define BG_MAGENTA          "45"
#define BG_CYAN             "46"
#define BG_WHITE            "47"
#define BG_GRAY             "100"
#define BG_BRIGHT_RED       "101"
#define BG_BRIGHT_GREEN     "102"
#define BG_BRIGHT_YELLOW    "103"
#define BG_BRIGHT_BLUE      "104"
#define BG_BRIGHT_MAGENTA   "105"
#define BG_BRIGHT_CYAN      "106"
#define BG_BRIGHT_WHITE     "107"

void print_name(const char *name, inode_type_t type, inode_perm_t perm, bool is_mount_point)
{
    switch (type) {
        case INODE_NONE:
            printf(CSI FG_BRIGHT_RED ";" BG_GRAY "m"
                   "%s"
                   CSI RESET "m"
                   "\n",
                name);
            break;
        case INODE_FILE:
            if (perm & INODE_PERM_X) {
                printf(CSI BG_GREEN "m"
                       "%s"
                       CSI RESET "m"
                       "\n",
                   name);
            } else {
                printf("%s\n", name);
            }
            break;
        case INODE_DIR:
            if (is_mount_point) {
                printf(CSI BG_BLUE "m"
                       "%s/"
                       CSI RESET "m"
                       "\n",
                   name);
            } else {
                printf(CSI FG_BRIGHT_BLUE "m"
                       "%s/"
                       CSI RESET "m"
                       "\n",
                   name);
            }
            break;
        case INODE_SYMLINK: {
            // TODO: figure out where the symlink points
            const char *symlink_target = "(unknown)";
            printf(CSI FG_BRIGHT_GREEN "m"
                   "%s"
                   CSI RESET "m"
                   " -> "
                   CSI FG_GREEN "m"
                   "%s"
                   CSI RESET "m"
                   "\n",
               name, symlink_target);
            break;
        }
        case INODE_CHARDEV:
            printf(CSI FG_YELLOW ";" BG_BLUE "m"
                   "%s"
                   CSI RESET "m"
                   "\n",
               name);
            break;
        case INODE_BLOCKDEV:
            printf(CSI FG_BRIGHT_CYAN ";" BG_BLUE "m"
                   "%s"
                   CSI RESET "m"
                   "\n",
               name);
            break;
        default:
            printf(CSI FG_BRIGHT_WHITE ";" BG_BRIGHT_RED "m"
                   "%s"
                   CSI RESET "m"
                   "\n",
                name);
            break;
    }
}

bool print_entry(uint32_t fd, stat_t *dir_st, const char *name)
{
    stat_t st;
    if (stat_at(fd, name, &st) < 0) {
        printf("ls: could not stat %s: %e\n", name, errno);
        return false;
    }

    bool is_mount_point = st.filesys_nr != dir_st->filesys_nr;
    print_name(name, st.type, st.perm, is_mount_point);

    return true;
}

bool ls(const char *dirpath)
{
    int64_t fd = open(dirpath, O_DIR);
    if (fd < 0) {
        printf("ls: could not open directory %s: %e\n",
                dirpath, errno);
        return false;
    }

    stat_t st;
    if (stat_at((uint32_t) fd, "", &st) < 0) {
        printf("ls: could not stat directory %s: %e\n",
                dirpath, errno);
        return false;
    }

#define BUF_ENTRIES 10
    dir_entry_t entries[BUF_ENTRIES];
    char name_buf[FILE_NAME_MAX_LEN + 1];
    while (true) {
        uint64_t entries_read;
        readdir(fd, entries, BUF_ENTRIES, &entries_read);
        errno_t read_errno = errno;
        for (uint64_t i = 0; i < entries_read; i++) {
            dir_entry_t *entry = &entries[i];
            memcpy(name_buf, entry->name, entry->name_len);
            name_buf[entry->name_len] = 0;
            if (streq(name_buf, ".") || streq(name_buf, "..")) {
                continue;
            }
            print_entry(fd, &st, name_buf);
        }
        if (read_errno) {
            printf("ls: error reading directory: %e\n", read_errno);
            close(fd);
            return false;
        }
        if (entries_read == 0) {
            close(fd);
            return true;
        }
    }
}

uint32_t main(uint64_t argc, char **argv)
{
    bool success = true;
    if (argc == 1) {
        success = ls(".") && success;
    } else if (argc == 2) {
        success = ls(argv[1]) && success;
    } else {
        for (uint64_t i = 1; i < argc; i++) {
            printf("%s%s:\n", i == 1 ? "" : "\n", argv[i]);
            success = ls(argv[i]) && success;
        }
    }

    return success ? 0 : 1;
}

