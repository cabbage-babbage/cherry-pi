#include <std.h>
#include <stdio.h>
#include <string.h>
#include <heap.h>
#include <syscall.h>
#include <env.h>

INLINE bool same_file(stat_t stat1, stat_t stat2)
{
    return stat1.filesys_nr == stat2.filesys_nr
        && stat1.inode_nr == stat2.inode_nr;
}

/* Puts the name in 'name', and returns true if fd is at the root (in which
 * case the name is '.'). This does not close fd on exit, and furthermore opens
 * the parent_fd. The name buf must be at least FILE_NAME_MAX_LEN + 1 bytes. */
bool find_name(uint32_t dirfd, stat_t dirstat,
        char *name, uint32_t *parent_fd, stat_t *parent_stat)
{
    int64_t pfd = open_at(dirfd, "..", O_DIR);
    if (pfd < 0) {
        printf("pwd: could not open ..: %e\n", errno);
        exit(1);
    }
    *parent_fd = (uint32_t) pfd;

    if (stat_at((uint32_t) pfd, "", parent_stat) < 0) {
        printf("pwd: could not stat ..: %e\n", errno);
        exit(1);
    }

    if (same_file(*parent_stat, dirstat)) {
        /* We're at the root. */
        memcpy(name, ".", 2);
        return true;
    }

#define BUF_ENTRIES 10
    dir_entry_t entries[BUF_ENTRIES];
    while (true) {
        uint64_t entries_read;
        readdir(pfd, entries, BUF_ENTRIES, &entries_read);
        errno_t read_errno = errno;
        for (uint64_t i = 0; i < entries_read; i++) {
            dir_entry_t *entry = &entries[i];
            memcpy(name, entry->name, entry->name_len);
            name[entry->name_len] = 0;
            if (streq(name, ".") || streq(name, "..")) {
                continue;
            }

            stat_t stat;
            if (stat_at((uint32_t) pfd, name, &stat) < 0) {
                printf("pwd: could not stat: %e\n", errno);
                exit(1);
            }

            if (same_file(stat, dirstat)) {
                return false;
            }
        }
        if (read_errno) {
            printf("pwd: error reading directory: %e\n", read_errno);
            exit(1);
        }
        if (entries_read == 0) {
            break;
        }
    }

    /* Did not find the entry... */
    printf("pwd: could not find directory in ..\n");
    exit(1);
}

void pwd_physical()
{
    char **parts_rev;
    uint64_t parts_rev_size;
    uint64_t num_parts = 0;

    /* This should be (way) more than enough to start with. */
    parts_rev_size = 128;
    parts_rev = malloc(parts_rev_size * sizeof(char *));
    if (!parts_rev) {
        printf("pwd: out of memory\n");
        exit(1);
    }

    uint32_t cur_fd;
    stat_t cur_stat;

    int64_t fd = open(".", O_DIR);
    if (fd < 0) {
        printf("pwd: could not open .: %e\n", errno);
        exit(1);
    }
    cur_fd = (uint32_t) fd;

    if (stat_at(cur_fd, "", &cur_stat) < 0) {
        printf("pwd: could not stat .: %e\n", errno);
        exit(1);
    }

    while (true) {
        char *name = malloc(FILE_NAME_MAX_LEN + 1);
        if (!name) {
            printf("pwd: out of memory\n");
            exit(1);
        }

        uint32_t parent_fd;
        stat_t parent_stat;
        bool at_root = find_name(cur_fd, cur_stat, name, &parent_fd, &parent_stat);
        if (at_root) {
            break;
        }

        if (num_parts == parts_rev_size) {
            parts_rev_size *= 2;
            parts_rev = realloc(parts_rev, parts_rev_size * sizeof(char *));
            if (!parts_rev) {
                printf("pwd: out of memory\n");
                exit(1);
            }
        }
        parts_rev[num_parts++] = name;

        close(cur_fd);
        cur_fd = parent_fd;
        cur_stat = parent_stat;
    }

    if (num_parts == 0) {
        printf("/\n");
        return;
    }

    for (int64_t i = num_parts - 1; i >= 0; i--) {
        printf("/%s", parts_rev[i]);
    }
    printf("\n");
}

void pwd_logical()
{
    char *PWD = getenv("PWD");
    if (PWD) {
        printf("%s\n", PWD);
    } else {
        pwd_physical();
    }
}

__attribute__((noreturn)) void usage()
{
    printf("usage: pwd [-l|--logical] [-p|--physical]\n");
    exit(1);
}

uint32_t main(uint64_t argc, char **argv)
{
    bool logical = false;
    bool physical = false;

    for (uint64_t i = 1; i < argc; i++) {
        if (streq(argv[i], "-l") || streq(argv[i], "--logical")) {
            logical = true;
        } else if (streq(argv[i], "-p") || streq(argv[i], "--physical")) {
            physical = true;
        } else {
            printf("pwd: unknown argument '%s'\n", argv[i]);
            usage();
        }
    }

    if (logical && physical) {
        printf("pwd: -l and -p are mutually exclusive\n");
        return 1;
    }

    /* This prefers physical pwd if neither logical nor physical was specified. */
    if (logical) {
        pwd_logical();
    } else {
        pwd_physical();
    }
    return 0;
}
