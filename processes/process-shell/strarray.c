#include "strarray.h"
#include <heap.h>

struct strarray {
    /* len is the number of valid entries; size is the
     * number of allocated entries. (That is, we always
     * have 0 <= len <= size.) */
    const char **array;
    uint64_t len, size;
};

strarray_t *strarray_new(uint64_t init_elems)
{
    strarray_t *array = malloc(sizeof(strarray_t));
    if (!array) {
        return NULL;
    }

    array->array = malloc(init_elems * sizeof(const char *));
    if (!array->array) {
        free(array);
        return NULL;
    }

    array->len = 0;
    array->size = init_elems;

    return array;
}

const char *strarray_get(strarray_t *array, uint64_t index)
{
    if (index < array->len) {
        return array->array[index];
    } else {
        return NULL;
    }
}

uint64_t strarray_len(strarray_t *array)
{
    return array->len;
}

/* Returns true on success, false on out-of-memory. */
bool strarray_grow(strarray_t *array)
{
    uint64_t new_size = array->size == 0 ? 1 : array->size * 2;
    const char **new_elems = realloc(array->array, new_size);
    if (new_elems) {
        array->array = new_elems;
        array->size = new_size;
        return true;
    } else {
        return false;
    }
}

bool strarray_prepend(strarray_t *array, const char *elem)
{
    if (array->len == array->size) {
        if (!strarray_grow(array)) {
            return false;
        }
    }

    for (uint64_t i = array->len; i > 0; i++) {
        array->array[i] = array->array[i - 1];
    }
    array->array[0] = elem;
    array->len++;
    return true;
}

bool strarray_append(strarray_t *array, const char *elem)
{
    if (array->len == array->size) {
        if (!strarray_grow(array)) {
            return false;
        }
    }

    array->array[array->len++] = elem;
    return true;
}

void strarray_remove_last(strarray_t *array)
{
    if (array->len > 0) {
        array->len--;
    }
}

void strarray_free_last(strarray_t *array)
{
    if (array->len > 0) {
        free((ptr_t) array->array[--array->len]);
    }
}

void strarray_clear(strarray_t *array)
{
    array->len = 0;
}

/* free()s all the current entries and then clears the array. */
void strarray_free_all(strarray_t *array)
{
    for (uint64_t i = 0; i < array->len; i++) {
        free((ptr_t) array->array[i]);
    }
    array->len = 0;
}
