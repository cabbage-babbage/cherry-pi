#ifndef SHELL_H
#define SHELL_H

#include <std.h>

typedef uint32_t (*command_fn_t)(uint64_t argc, const char **argv);

typedef struct {
    const char *name;
    command_fn_t fn;
} command_t;

#endif
