#include "readline.h"
#include <stdio.h>
#include <string.h>
#include <heap.h>
#include <errno.h>
#include <syscall.h>
#include <uapi/tty.h>

#define CSI "\x1b["

/* === Memory functions === */

malloc_t rl_malloc;
realloc_t rl_realloc;
free_t rl_free;

/* === History === */

rl_append_history_predicate_t rl_append_input_to_history;

typedef struct {
    /* This is the original history element. */
    const char *history_text;

    /* This values are the result of editing the original history element, i.e.
     * by going up/down in history from readline(). The buffer itself may be
     * NULL (and the buf_size zero) if this history element has not yet been
     * edited. */

    /* The text buffer for editing. */
    char *buf;
    /* The size of the text buffer. */
    uint64_t buf_size;
    /* The length of text currently in the buffer. */
    uint64_t text_len;
    /* Current cursor index in the text buffer. */
    uint64_t cursor_index;
    /* Current cursor position, relative to the start of text, i.e. relative to
     * the start of the buffer in the terminal display. This may be different
     * from the cursor_index (and indeed larger) due to characters being displayed
     * with display width larger than 1. */
    uint64_t cursor_pos;
} rl_history_elem_t;

rl_history_elem_t *rl_history;
uint64_t rl_history_buf_size; /* 0 indicates that the buffer is not initialized */
uint64_t rl_history_len;

/* === Input === */

tty_config_t saved_tty_config;

/* We use a fake history element for the editing status of the newest input line.
 * This history element has no history_text. */
rl_history_elem_t rl_input_elem;

/* Index of history element we're currently editing, or negative if not editing
 * any history element (in which case we're editing rl_input_elem instead). */
int64_t rl_history_index;

/* Cursor position at the start of input, i.e. at the end of the prompt. */
uint64_t rl_start_cursor_row, rl_start_cursor_col;

#define RL_KEY_BUF_SIZE 32
/* Input key sequence buffer. This supports up to (RL_KEY_BUF_SIZE - 1) levels of
 * keymap recursion, i.e. input key sequences up to that length. */
char rl_key_buf[RL_KEY_BUF_SIZE];
uint64_t rl_key_seq_len;

/* Specifies if readline has detected EOF in the input stream or not. */
bool rl_eof;

/* === Keymap === */

/* The keymap in use. */
rl_keymap_t *rl_keymap;

/* === Character display ===*/

typedef struct {
    const char *text;
    uint64_t width;
} rl_char_display_t;

#define GRAY(text) (CSI "90m" text CSI "0m")
rl_char_display_t rl_char_displays[256] = {
    /*  0*/ { GRAY("<0>"), 3 },
    /*  1*/ { GRAY("^A"), 2 },
    /*  2*/ { GRAY("^B"), 2 },
    /*  3*/ { GRAY("^C"), 2 },
    /*  4*/ { GRAY("^D"), 2 },
    /*  5*/ { GRAY("^E"), 2 },
    /*  6*/ { GRAY("^F"), 2 },
    /*  7*/ { GRAY("^G"), 2 },
    /*  8*/ { GRAY("^H"), 2 },
    /*  9*/ { GRAY("^I"), 2 },
    /* 10*/ { GRAY("^J"), 2 },
    /* 11*/ { GRAY("^K"), 2 },
    /* 12*/ { GRAY("^L"), 2 },
    /* 13*/ { GRAY("^M"), 2 },
    /* 14*/ { GRAY("^N"), 2 },
    /* 15*/ { GRAY("^O"), 2 },
    /* 16*/ { GRAY("^P"), 2 },
    /* 17*/ { GRAY("^Q"), 2 },
    /* 18*/ { GRAY("^R"), 2 },
    /* 19*/ { GRAY("^S"), 2 },
    /* 20*/ { GRAY("^T"), 2 },
    /* 21*/ { GRAY("^U"), 2 },
    /* 22*/ { GRAY("^V"), 2 },
    /* 23*/ { GRAY("^W"), 2 },
    /* 24*/ { GRAY("^X"), 2 },
    /* 25*/ { GRAY("^Y"), 2 },
    /* 26*/ { GRAY("^Z"), 2 },
    /* 27*/ { GRAY("<Esc>"), 5 },
    /* 28*/ { GRAY("<28>"), 4 },
    /* 29*/ { GRAY("<29>"), 4 },
    /* 30*/ { GRAY("<30>"), 4 },
    /* 31*/ { GRAY("<31>"), 4 },
    /* 32*/ { " ", 1 },
    /* 33*/ { "!", 1 },
    /* 34*/ { "\"", 1 },
    /* 35*/ { "#", 1 },
    /* 36*/ { "$", 1 },
    /* 37*/ { "%", 1 },
    /* 38*/ { "&", 1 },
    /* 39*/ { "'", 1 },
    /* 40*/ { "(", 1 },
    /* 41*/ { ")", 1 },
    /* 42*/ { "*", 1 },
    /* 43*/ { "+", 1 },
    /* 44*/ { ",", 1 },
    /* 45*/ { "-", 1 },
    /* 46*/ { ".", 1 },
    /* 47*/ { "/", 1 },
    /* 48*/ { "0", 1 },
    /* 49*/ { "1", 1 },
    /* 50*/ { "2", 1 },
    /* 51*/ { "3", 1 },
    /* 52*/ { "4", 1 },
    /* 53*/ { "5", 1 },
    /* 54*/ { "6", 1 },
    /* 55*/ { "7", 1 },
    /* 56*/ { "8", 1 },
    /* 57*/ { "9", 1 },
    /* 58*/ { ":", 1 },
    /* 59*/ { ";", 1 },
    /* 60*/ { "<", 1 },
    /* 61*/ { "=", 1 },
    /* 62*/ { ">", 1 },
    /* 63*/ { "?", 1 },
    /* 64*/ { "@", 1 },
    /* 65*/ { "A", 1 },
    /* 66*/ { "B", 1 },
    /* 67*/ { "C", 1 },
    /* 68*/ { "D", 1 },
    /* 69*/ { "E", 1 },
    /* 70*/ { "F", 1 },
    /* 71*/ { "G", 1 },
    /* 72*/ { "H", 1 },
    /* 73*/ { "I", 1 },
    /* 74*/ { "J", 1 },
    /* 75*/ { "K", 1 },
    /* 76*/ { "L", 1 },
    /* 77*/ { "M", 1 },
    /* 78*/ { "N", 1 },
    /* 79*/ { "O", 1 },
    /* 80*/ { "P", 1 },
    /* 81*/ { "Q", 1 },
    /* 82*/ { "R", 1 },
    /* 83*/ { "S", 1 },
    /* 84*/ { "T", 1 },
    /* 85*/ { "U", 1 },
    /* 86*/ { "V", 1 },
    /* 87*/ { "W", 1 },
    /* 88*/ { "X", 1 },
    /* 89*/ { "Y", 1 },
    /* 90*/ { "Z", 1 },
    /* 91*/ { "[", 1 },
    /* 92*/ { "\\", 1 },
    /* 93*/ { "]", 1 },
    /* 94*/ { "^", 1 },
    /* 95*/ { "_", 1 },
    /* 96*/ { "`", 1 },
    /* 97*/ { "a", 1 },
    /* 98*/ { "b", 1 },
    /* 99*/ { "c", 1 },
    /*100*/ { "d", 1 },
    /*101*/ { "e", 1 },
    /*102*/ { "f", 1 },
    /*103*/ { "g", 1 },
    /*104*/ { "h", 1 },
    /*105*/ { "i", 1 },
    /*106*/ { "j", 1 },
    /*107*/ { "k", 1 },
    /*108*/ { "l", 1 },
    /*109*/ { "m", 1 },
    /*110*/ { "n", 1 },
    /*111*/ { "o", 1 },
    /*112*/ { "p", 1 },
    /*113*/ { "q", 1 },
    /*114*/ { "r", 1 },
    /*115*/ { "s", 1 },
    /*116*/ { "t", 1 },
    /*117*/ { "u", 1 },
    /*118*/ { "v", 1 },
    /*119*/ { "w", 1 },
    /*120*/ { "x", 1 },
    /*121*/ { "y", 1 },
    /*122*/ { "z", 1 },
    /*123*/ { "{", 1 },
    /*124*/ { "|", 1 },
    /*125*/ { "}", 1 },
    /*126*/ { "~", 1 },
    /*127*/ { GRAY("<Back>"), 6 },
    /*128*/ { GRAY("<128>"), 5 },
    /*129*/ { GRAY("<129>"), 5 },
    /*130*/ { GRAY("<130>"), 5 },
    /*131*/ { GRAY("<131>"), 5 },
    /*132*/ { GRAY("<132>"), 5 },
    /*133*/ { GRAY("<133>"), 5 },
    /*134*/ { GRAY("<134>"), 5 },
    /*135*/ { GRAY("<135>"), 5 },
    /*136*/ { GRAY("<136>"), 5 },
    /*137*/ { GRAY("<137>"), 5 },
    /*138*/ { GRAY("<138>"), 5 },
    /*139*/ { GRAY("<139>"), 5 },
    /*140*/ { GRAY("<140>"), 5 },
    /*141*/ { GRAY("<141>"), 5 },
    /*142*/ { GRAY("<142>"), 5 },
    /*143*/ { GRAY("<143>"), 5 },
    /*144*/ { GRAY("<144>"), 5 },
    /*145*/ { GRAY("<145>"), 5 },
    /*146*/ { GRAY("<146>"), 5 },
    /*147*/ { GRAY("<147>"), 5 },
    /*148*/ { GRAY("<148>"), 5 },
    /*149*/ { GRAY("<149>"), 5 },
    /*150*/ { GRAY("<150>"), 5 },
    /*151*/ { GRAY("<151>"), 5 },
    /*152*/ { GRAY("<152>"), 5 },
    /*153*/ { GRAY("<153>"), 5 },
    /*154*/ { GRAY("<154>"), 5 },
    /*155*/ { GRAY("<155>"), 5 },
    /*156*/ { GRAY("<156>"), 5 },
    /*157*/ { GRAY("<157>"), 5 },
    /*158*/ { GRAY("<158>"), 5 },
    /*159*/ { GRAY("<159>"), 5 },
    /*160*/ { GRAY("<160>"), 5 },
    /*161*/ { GRAY("<161>"), 5 },
    /*162*/ { GRAY("<162>"), 5 },
    /*163*/ { GRAY("<163>"), 5 },
    /*164*/ { GRAY("<164>"), 5 },
    /*165*/ { GRAY("<165>"), 5 },
    /*166*/ { GRAY("<166>"), 5 },
    /*167*/ { GRAY("<167>"), 5 },
    /*168*/ { GRAY("<168>"), 5 },
    /*169*/ { GRAY("<169>"), 5 },
    /*170*/ { GRAY("<170>"), 5 },
    /*171*/ { GRAY("<171>"), 5 },
    /*172*/ { GRAY("<172>"), 5 },
    /*173*/ { GRAY("<173>"), 5 },
    /*174*/ { GRAY("<174>"), 5 },
    /*175*/ { GRAY("<175>"), 5 },
    /*176*/ { GRAY("<176>"), 5 },
    /*177*/ { GRAY("<177>"), 5 },
    /*178*/ { GRAY("<178>"), 5 },
    /*179*/ { GRAY("<179>"), 5 },
    /*180*/ { GRAY("<180>"), 5 },
    /*181*/ { GRAY("<181>"), 5 },
    /*182*/ { GRAY("<182>"), 5 },
    /*183*/ { GRAY("<183>"), 5 },
    /*184*/ { GRAY("<184>"), 5 },
    /*185*/ { GRAY("<185>"), 5 },
    /*186*/ { GRAY("<186>"), 5 },
    /*187*/ { GRAY("<187>"), 5 },
    /*188*/ { GRAY("<188>"), 5 },
    /*189*/ { GRAY("<189>"), 5 },
    /*190*/ { GRAY("<190>"), 5 },
    /*191*/ { GRAY("<191>"), 5 },
    /*192*/ { GRAY("<192>"), 5 },
    /*193*/ { GRAY("<193>"), 5 },
    /*194*/ { GRAY("<194>"), 5 },
    /*195*/ { GRAY("<195>"), 5 },
    /*196*/ { GRAY("<196>"), 5 },
    /*197*/ { GRAY("<197>"), 5 },
    /*198*/ { GRAY("<198>"), 5 },
    /*199*/ { GRAY("<199>"), 5 },
    /*200*/ { GRAY("<200>"), 5 },
    /*201*/ { GRAY("<201>"), 5 },
    /*202*/ { GRAY("<202>"), 5 },
    /*203*/ { GRAY("<203>"), 5 },
    /*204*/ { GRAY("<204>"), 5 },
    /*205*/ { GRAY("<205>"), 5 },
    /*206*/ { GRAY("<206>"), 5 },
    /*207*/ { GRAY("<207>"), 5 },
    /*208*/ { GRAY("<208>"), 5 },
    /*209*/ { GRAY("<209>"), 5 },
    /*210*/ { GRAY("<210>"), 5 },
    /*211*/ { GRAY("<211>"), 5 },
    /*212*/ { GRAY("<212>"), 5 },
    /*213*/ { GRAY("<213>"), 5 },
    /*214*/ { GRAY("<214>"), 5 },
    /*215*/ { GRAY("<215>"), 5 },
    /*216*/ { GRAY("<216>"), 5 },
    /*217*/ { GRAY("<217>"), 5 },
    /*218*/ { GRAY("<218>"), 5 },
    /*219*/ { GRAY("<219>"), 5 },
    /*220*/ { GRAY("<220>"), 5 },
    /*221*/ { GRAY("<221>"), 5 },
    /*222*/ { GRAY("<222>"), 5 },
    /*223*/ { GRAY("<223>"), 5 },
    /*224*/ { GRAY("<224>"), 5 },
    /*225*/ { GRAY("<225>"), 5 },
    /*226*/ { GRAY("<226>"), 5 },
    /*227*/ { GRAY("<227>"), 5 },
    /*228*/ { GRAY("<228>"), 5 },
    /*229*/ { GRAY("<229>"), 5 },
    /*230*/ { GRAY("<230>"), 5 },
    /*231*/ { GRAY("<231>"), 5 },
    /*232*/ { GRAY("<232>"), 5 },
    /*233*/ { GRAY("<233>"), 5 },
    /*234*/ { GRAY("<234>"), 5 },
    /*235*/ { GRAY("<235>"), 5 },
    /*236*/ { GRAY("<236>"), 5 },
    /*237*/ { GRAY("<237>"), 5 },
    /*238*/ { GRAY("<238>"), 5 },
    /*239*/ { GRAY("<239>"), 5 },
    /*240*/ { GRAY("<240>"), 5 },
    /*241*/ { GRAY("<241>"), 5 },
    /*242*/ { GRAY("<242>"), 5 },
    /*243*/ { GRAY("<243>"), 5 },
    /*244*/ { GRAY("<244>"), 5 },
    /*245*/ { GRAY("<245>"), 5 },
    /*246*/ { GRAY("<246>"), 5 },
    /*247*/ { GRAY("<247>"), 5 },
    /*248*/ { GRAY("<248>"), 5 },
    /*249*/ { GRAY("<249>"), 5 },
    /*250*/ { GRAY("<250>"), 5 },
    /*251*/ { GRAY("<251>"), 5 },
    /*252*/ { GRAY("<252>"), 5 },
    /*253*/ { GRAY("<253>"), 5 },
    /*254*/ { GRAY("<254>"), 5 },
    /*255*/ { GRAY("<255>"), 5 },
};

void dump_keymap(rl_keymap_t *keymap, uint64_t indent)
{
    for (uint32_t i = 0; i < 256; i++) {
        rl_keymap_entry_t entry = keymap->entries[i];
        if (entry.type == ENTRY_EMPTY) {
            continue;
        }

        for (uint64_t i = 0; i < indent; i++) {
            puts("  ");
        }

        if (32 <= i && i <= 126) {
            printf("'%c': ", (char) i);
        } else {
            printf("<%u32>: ", i);
        }

        if (entry.type == ENTRY_KEYMAP) {
            puts("keymap\n");
            dump_keymap((rl_keymap_t *) entry.data, indent + 1);
        } else if (entry.type == ENTRY_COMMAND) {
            printf("cmd '%s'\n", (const char *) entry.data);
        } else {
            puts("???\n");
        }
    }
}

bool rl_init()
{
    rl_malloc = &malloc;
    rl_realloc = &realloc;
    rl_free = &free;

    rl_append_input_to_history = &rl_append_history_if_nonempty;

    /* Set up the default keymap. */
    rl_keymap = rl_keymap_new();
    if (!rl_keymap) {
        return false;
    }

#define TRY(expr) do { if (!(expr)) { return false; } } while (0)

    for (char c = 32; c <= 126; c++) {
        char seq[2];
        seq[0] = c;
        seq[1] = 0;
        TRY(rl_keymap_update(rl_keymap, seq, rl_keymap_entry_command("insert-char")));
    }

    TRY(rl_keymap_update(rl_keymap, "\n", rl_keymap_entry_command("input-complete")));

    TRY(rl_keymap_update(rl_keymap, CSI "A", rl_keymap_entry_command("up")));
    TRY(rl_keymap_update(rl_keymap, CSI "B", rl_keymap_entry_command("down")));
    TRY(rl_keymap_update(rl_keymap, CSI "C", rl_keymap_entry_command("right")));
    TRY(rl_keymap_update(rl_keymap, CSI "D", rl_keymap_entry_command("left")));
    TRY(rl_keymap_update(rl_keymap, "\x7F", rl_keymap_entry_command("backspace")));
    TRY(rl_keymap_update(rl_keymap, CSI "3~", rl_keymap_entry_command("delete")));

    TRY(rl_keymap_update(rl_keymap, "\x3", rl_keymap_entry_command("insert-char")));
    TRY(rl_keymap_update(rl_keymap, "\x4", rl_keymap_entry_command("input-eof")));
    TRY(rl_keymap_update(rl_keymap, "\x5", rl_keymap_entry_command("insert-char")));
    TRY(rl_keymap_update(rl_keymap, "\x6", rl_keymap_entry_command("insert-char")));
    TRY(rl_keymap_update(rl_keymap, "\x16", rl_keymap_entry_command("insert-char")));
    TRY(rl_keymap_update(rl_keymap, "\x1f", rl_keymap_entry_command("insert-char")));

    return true;
}

rl_keymap_t *rl_keymap_new()
{
    rl_keymap_t *keymap = rl_malloc(sizeof(rl_keymap_t));
    if (!keymap) {
        return NULL;
    }

    for (uint32_t i = 0; i < 256; i++) {
        keymap->entries[i] = rl_keymap_entry_empty();
    }
    return keymap;
}

void rl_free_keymap(rl_keymap_t *keymap)
{
    for (uint32_t i = 0; i < 256; i++) {
        rl_keymap_entry_t *entry = &keymap->entries[i];
        if (entry->type == ENTRY_KEYMAP) {
            rl_keymap_t *rec_keymap = (rl_keymap_t *) entry->data;
            rl_free_keymap(rec_keymap);
        }
    }
    rl_free(keymap);
}

rl_keymap_entry_t rl_keymap_entry_keymap(rl_keymap_t *keymap)
{
    rl_keymap_entry_t entry = { ENTRY_KEYMAP, (ptr_t) keymap };
    return entry;
}

rl_keymap_entry_t rl_keymap_entry_command(const char *command)
{
    rl_keymap_entry_t entry = { ENTRY_COMMAND, (ptr_t) command };
    return entry;
}

rl_keymap_entry_t rl_keymap_entry_empty()
{
    rl_keymap_entry_t entry = { ENTRY_EMPTY, NULL };
    return entry;
}

bool rl_keymap_update(rl_keymap_t *keymap, const char *input_sequence, rl_keymap_entry_t new_entry)
{
    if (!*input_sequence) {
        return false;
    }

    char index = input_sequence[0];
    rl_keymap_entry_t *cur_entry = &keymap->entries[(uint8_t) index];

    bool at_leaf = input_sequence[1] == 0;

    if (at_leaf) {
        /* We must update the current entry, i.e. free the old one (if applicable)
         * and then replace it. */
        if (cur_entry->type == ENTRY_KEYMAP) {
            rl_keymap_t *rec_keymap = (rl_keymap_t *) cur_entry->data;
            rl_free_keymap(rec_keymap);
        }

        *cur_entry = new_entry;
        return true;
    } else {
        /* We must recurse. If the entry was already a keymap entry, we can
         * continue into it; otherwise we must replace the current entry with
         * a keymap entry first. */
        if (cur_entry->type != ENTRY_KEYMAP) {
            rl_keymap_t *new_keymap = rl_keymap_new();
            if (!new_keymap) {
                return false;
            }
            *cur_entry = rl_keymap_entry_keymap(new_keymap);
        }

        rl_keymap_t *rec_keymap = (rl_keymap_t *) cur_entry->data;
        return rl_keymap_update(rec_keymap, input_sequence + 1, new_entry);
    }
}

const char *rl_keymap_get_command(rl_keymap_t *keymap, const char *input_sequence, bool *got_terminal)
{
    if (!*input_sequence) {
        *got_terminal = false;
        return NULL;
    }

    char index = input_sequence[0];
    rl_keymap_entry_t cur_entry = keymap->entries[(uint8_t) index];
    bool at_leaf = input_sequence[1] == 0;

    if (at_leaf) {
        *got_terminal = cur_entry.type != ENTRY_KEYMAP;

        if (cur_entry.type == ENTRY_COMMAND) {
            return (const char *) cur_entry.data;
        } else {
            return NULL;
        }
    } else {
        if (cur_entry.type == ENTRY_KEYMAP) {
            /* Recursive entry. */
            rl_keymap_t *rec_keymap = (rl_keymap_t *) cur_entry.data;
            const char *command = rl_keymap_get_command(
                    rec_keymap, input_sequence + 1, got_terminal);
            return command;
        } else {
            /* Terminal entry; can't go further! */
            *got_terminal = true;
            return NULL;
        }
    }
}

void rl_set_append_history_predicate(rl_append_history_predicate_t predicate)
{
    rl_append_input_to_history = predicate;
}

bool rl_never_append_history(char *text)
{
    return false;
}

bool rl_always_append_history(char *text)
{
    return true;
}

INLINE bool is_whitespace(char c)
{
    return c == ' ' || c == '\t';
}
bool rl_append_history_if_nonempty(char *text)
{
    while (*text) {
        if (!is_whitespace(*text)) {
            return true;
        }
        text++;
    }
    return false;
}

rl_history_elem_t *rl_current_elem()
{
    if (rl_history_index < 0) {
        return &rl_input_elem;
    } else {
        return &rl_history[rl_history_index];
    }
}

/* Get the actual text to display for a given character, along with the (visual) width
 * of the displayed text. The display_width may be different from strlen(display_text),
 * since the display may have ANSI escape sequences for e.g. coloring. For an example,
 * the character <22> (Ctrl-V) might have display text
 *     <CSI>90m^V<CSI>0m
 * which displays "^V" in gray coloring; the display_width in that case would be 2.
 * If an output argument is NULL, it is not written. */
void rl_get_display(char c, const char **display_text, uint64_t *display_width)
{
    rl_char_display_t display = rl_char_displays[(uint8_t) c];
    if (display_text) {
        *display_text = display.text;
    }
    if (display_width) {
        *display_width = display.width;
    }
}
/* Related helper methods. */
const char *rl_get_display_text(char c)
{
    return rl_char_displays[(uint8_t) c].text;
}
uint64_t rl_get_display_width(char c)
{
    return rl_char_displays[(uint8_t) c].width;
}

/* If length is negative, get the length of the whole string. */
uint64_t rl_get_display_width_str(const char *s, int64_t length)
{
    if (length < 0) {
        length = strlen(s);
    }
    uint64_t width = 0;
    for (uint64_t i = 0; i < length; i++) {
        width += rl_get_display_width(s[i]);
    }
    return width;
}

void rl_cursor_left()
{
    rl_history_elem_t *elem = rl_current_elem();
    if (elem->cursor_index > 0) {
        elem->cursor_index--;
        uint64_t width = rl_get_display_width(elem->buf[elem->cursor_index]);
        elem->cursor_pos -= width;
        printf(CSI "%u64D", width); /* Move cursor left. */
    }
}

void rl_cursor_right()
{
    rl_history_elem_t *elem = rl_current_elem();
    if (elem->cursor_index < elem->text_len) {
        uint64_t width = rl_get_display_width(elem->buf[elem->cursor_index]);
        elem->cursor_pos += width;
        elem->cursor_index++;
        printf(CSI "%u64C", width); /* Move cursor right. */
    }
}

/* Returns false on failure, indicating out-of-memory.
 * (In this case, the buf is not freed.)
 * The index is clamped if out-of-bounds. */
USE_VALUE bool rl_insert_char(uint64_t index, char c)
{
    rl_history_elem_t *elem = rl_current_elem();

    if (index > elem->text_len) {
        index = elem->text_len;
    }

    if (elem->text_len == elem->buf_size - 1) {
        /* We need to allocate more buffer space. */
        elem->buf_size *= 2;
        char *new_buf = rl_realloc(elem->buf, elem->buf_size);
        if (!new_buf) {
            return false;
        }
        elem->buf = new_buf;
    }

    /* Update the buffer. */
    for (uint64_t i = elem->text_len; i > index; i--) {
        elem->buf[i] = elem->buf[i - 1];
    }

    elem->buf[index] = c;
    elem->text_len++;

    /* Also update the display. */
    printf(CSI "s"); /* Save current cursor position. */
    printf(CSI "%u64G", rl_start_cursor_col +
                        rl_get_display_width_str(elem->buf, index)); /* Move to column. */
    printf(CSI "0K"); /* Clear to end of line. */
    /* Rewrite from 'index' on. */
    for (uint64_t i = index; i < elem->text_len; i++) {
        puts(rl_get_display_text(elem->buf[i]));
    }
    printf(CSI "u"); /* Restore old cursor position. */

    if (index <= elem->cursor_index) {
        rl_cursor_right();
    }

    return true;
}

/* If the index is out of bounds, no deletion is performed. */
void rl_delete_char(uint64_t index)
{
    rl_history_elem_t *elem = rl_current_elem();

    if (index >= elem->text_len) {
        return;
    }

    /* Update the buffer. */
    for (uint64_t i = index; i < elem->text_len; i++) {
        elem->buf[i] = elem->buf[i + 1];
    }
    elem->text_len--;

    /* Update the display. */
    printf(CSI "s"); /* Save current cursor position. */
    printf(CSI "%u64G", rl_start_cursor_col +
                        rl_get_display_width_str(elem->buf, index)); /* Move to column. */
    printf(CSI "0K"); /* Clear to end of line. */
    /* Rewrite from 'index' on. */
    for (uint64_t i = index; i < elem->text_len; i++) {
        puts(rl_get_display_text(elem->buf[i]));
    }
    printf(CSI "u"); /* Restore old cursor position. */

    if (index < elem->cursor_index) {
        rl_cursor_left();
    }
}

/* Returns false on failure, indicating out-of-memory. */
USE_VALUE bool rl_set_history_index(int64_t new_history_index)
{
    /* Check for no-ops first. */
    if ((rl_history_index < 0 && new_history_index < 0) ||
        (new_history_index == rl_history_index)) {
        return true;
    }

    uint64_t old_history_index = rl_history_index;
    rl_history_index = new_history_index;

    rl_history_elem_t *elem = rl_current_elem();
    if (!elem->buf) {
        /* We must allocate the editing buffer. */
        // TODO: do this later, only once we actually need to edit the buffer
        // (i.e. have each editing function call something like rl_elem_prepare())
        elem->text_len = strlen(elem->history_text);
        elem->cursor_index = elem->text_len;
        elem->cursor_pos = rl_get_display_width_str(elem->history_text, elem->cursor_index);

        /* Somewhat arbitrarily request an extra 32 bytes for editing, so we won't
         * have to reallocate the buffer until 31 (= 32 - null terminator) chars
         * have been added. */
        elem->buf_size = elem->text_len + 32;
        if (!(elem->buf = rl_malloc(elem->buf_size))) {
            rl_history_index = old_history_index;
            return false;
        }

        memcpy(elem->buf, elem->history_text, elem->text_len);
    }

    /* Reset the editing display. */
    printf(CSI "%u64G", rl_start_cursor_col); /* Move to column. */
    printf(CSI "0K"); /* Clear to end of line. */
    for (uint64_t i = 0; i < elem->text_len; i++) {
        puts(rl_get_display_text(elem->buf[i]));
    }
    printf(CSI "%u64G", rl_start_cursor_col + elem->cursor_pos); /* Move to column. */

    return true;
}

/* Returns false on failure, indicating out-of-memory. */
USE_VALUE bool rl_history_up()
{
    if (rl_history_index < 0 && rl_history_len > 0) {
        return rl_set_history_index(rl_history_len - 1);
    } else if (rl_history_index > 0) {
        return rl_set_history_index(rl_history_index - 1);
    } else {
        return true;
    }
}

/* Returns false on failure, indicating out-of-memory. */
USE_VALUE bool rl_history_down()
{
    if (rl_history_index < rl_history_len - 1) {
        return rl_set_history_index(rl_history_index + 1);
    } else if (rl_history_index == rl_history_len - 1) {
        return rl_set_history_index(-1);
    } else {
        return true;
    }
}

/* Returns 0 on success, 1 if we got input but couldn't understand it, EOF if we found
 * EOF while reading input, and -errno if we got an error while reading input. */
USE_VALUE int64_t rl_get_cursor_position(uint64_t *cursor_row, uint64_t *cursor_col)
{
    printf(CSI "6n");
    /* We expect a response of the form
     *    <CSI><r>;<c>R
     * where <r> is the row and <c> the column of the cursor. */

    // TODO: just do this with a basic scanf():
    //scanf(CSI "%u64;%u64R", cursor_row, cursor_col);

    int64_t in = getc();
    if (in < 0 || in == EOF) {
        return in;
    }

    while (in != CSI[0]) {
        in = getc();
        if (in < 0 || in == EOF) {
            return in;
        }
    }
    in = getc();
    if (in < 0 || in == EOF) {
        return in;
    }
    if (in != CSI[1]) {
        return 1;
    }
    in = getc();
    if (in < 0 || in == EOF) {
        return in;
    }

    // TODO: don't ignore overflow
    *cursor_row = 0;
    bool had_decimal = false;
    do {
        if ('0' <= in && in <= '9') {
            had_decimal = true;
            *cursor_row = *cursor_row * 10 + (in - '0');
            in = getc();
            if (in < 0 || in == EOF) {
                return in;
            }
        } else {
            break;
        }
    } while (true);
    if (!had_decimal) {
        return 1;
    }

    if (in != ';') {
        return 1;
    }
    in = getc();
    if (in < 0 || in == EOF) {
        return in;
    }

    // TODO: don't ignore overflow
    *cursor_col = 0;
    had_decimal = false;
    do {
        if ('0' <= in && in <= '9') {
            had_decimal = true;
            *cursor_col = *cursor_col * 10 + (in - '0');
            in = getc();
            if (in < 0 || in == EOF) {
                return in;
            }
        } else {
            break;
        }
    } while (true);
    if (!had_decimal) {
        return 1;
    }

    if (in != 'R') {
        return 1;
    }

    return 0;
}

void rl_free_buf(rl_history_elem_t *elem)
{
    if (elem->buf) {
        rl_free(elem->buf);
    }
    elem->buf = NULL;
    elem->buf_size = 0;
    elem->text_len = 0;
    elem->cursor_index = 0;
    elem->cursor_pos = 0;
}

/* Free the buffers in all history elements (and the rl_input_elem). */
void rl_free_bufs()
{
    rl_free_buf(&rl_input_elem);
    for (uint64_t i = 0; i < rl_history_len; i++) {
        rl_free_buf(&rl_history[i]);
    }
}

char *alloc_empty_string()
{
    char *empty = malloc(1);
    if (empty) {
        *empty = 0;
    }
    return empty;
}

void rl_set_tty_config()
{
    tty_config_t rl_tty_config = {
        .in_convert_cr_to_lf = true,
        .in_convert_ctrl_c_to_signal = true,
        .in_line_mode = false,
        .in_echo = false,
        .out_convert_lf_to_crlf = true,
        .out_stop = false,
    };
    saved_tty_config = rl_tty_config;
    if (ioctl(0, TTY_IOCTL_SET_CONFIG, &saved_tty_config, sizeof(saved_tty_config)) < 0) {
        char buf[100];
        snprintf(buf, 100, "could not set tty config: %e\n", errno);
        panic(buf);
    }
}

void rl_restore_tty_config()
{
    if (ioctl(0, TTY_IOCTL_SET_CONFIG, &saved_tty_config, sizeof(saved_tty_config)) < 0) {
        char buf[100];
        snprintf(buf, 100, "could not restore tty config: %e\n", errno);
        panic(buf);
    }
}

char *readline(const char *prompt, readline_status_t *result)
{
    // TODO: once there is kernel support, make sure to set stdin as blocking,
    //       check that it's a tty, and unset local echo and ^D processing.
    //       (also restore settings before returning)

    if (rl_eof) {
        *result = READLINE_EOF;
        char *empty = alloc_empty_string();
        if (!empty) {
            *result = READLINE_OUT_OF_MEMORY;
        }
        return empty;
    }

    puts(prompt);

    rl_set_tty_config();

    while (true) {
        int64_t errcode = rl_get_cursor_position(&rl_start_cursor_row, &rl_start_cursor_col);
        if (errcode == 0) {
            break;
        } else if (errcode < 0) {
            printf("[readline get_cursor_pos => %e]\n", (errno_t) -errcode);
            *result = READLINE_READ_FAILURE;
            rl_restore_tty_config();
            return NULL;
        } else if (errcode == EOF) {
            *result = READLINE_EOF;
            char *empty = alloc_empty_string();
            if (!empty) {
                *result = READLINE_OUT_OF_MEMORY;
            }
            rl_restore_tty_config();
            return empty;
        }
        /* In other cases, just continue trying to get the cursor position. */
    }

    rl_history_index = -1; /* Not editing any history. */

    rl_input_elem.history_text = NULL;
    rl_input_elem.buf_size = 128; /* Initial buffer size. Includes null terminator. */
    rl_input_elem.text_len = 0;
    rl_input_elem.cursor_index = 0;
    rl_input_elem.cursor_pos = 0;

    rl_key_seq_len = 0;

    if (!(rl_input_elem.buf = rl_malloc(rl_input_elem.buf_size))) {
        *result = READLINE_OUT_OF_MEMORY;
        rl_restore_tty_config();
        return NULL;
    }

    while (true) {
        int64_t in = getc();
        if (in == EOF) {
            rl_eof = true;
        } else if (in < 0) {
            *result = READLINE_READ_FAILURE;
            goto readline_failed;
        }

        const char *command;
        if (rl_eof) {
            command = "input-eof";
        } else {
            char c = (char) in;
            if (rl_key_seq_len == RL_KEY_BUF_SIZE - 1) {
                /* This is awkward... somehow we got a really long input sequence. */
                *result = READLINE_READ_FAILURE;
                goto readline_failed;
            }

            rl_key_buf[rl_key_seq_len++] = c;
            rl_key_buf[rl_key_seq_len] = 0; /* Null-terminate. */
            bool got_terminal;
            command = rl_keymap_get_command(rl_keymap, rl_key_buf, &got_terminal);
            if (!command) {
                if (got_terminal) {
                    /* We don't have a command assigned to the input. Let's clear the key sequence
                     * buffer and continue reading. */
                    printf(CSI "s"); /* Save current cursor position. */
                    printf("\n");
                    //printf(CSI "1E"); /* Move cursor down and to start of line. */
                    printf(CSI "0K"); /* Clear to end of line. */
                    printf("ignoring key sequence: ");
                    for (uint32_t i = 0; i < rl_key_seq_len; i++) {
                        char c = rl_key_buf[i];
                        if (32 <= c && c <= 126) {
                            printf("'%c' ", c);
                        } else {
                            printf("<%u8> ", (uint8_t) c);
                        }
                    }
                    printf(CSI "u"); /* Restore old cursor position. */
                    printf(CSI "1A"); /* Cursor up (since the old cursor position is now too low). */

                    rl_key_seq_len = 0;
                    continue;
                } else {
                    /* Not enough input yet. Let's keep reading. */
                    continue;
                }
            }
        }

        /* We have a command name to execute! */
        // TODO: check for all sorts of functions returning failure...
        if (streq(command, "input-complete") || streq(command, "input-eof")) {
            puts("\n");

            rl_history_elem_t *elem = rl_current_elem();

            /* Free the bufs of all history elements except the one
             * whose buf we're returning. (The caller has to free that
             * one.) */
            if (&rl_input_elem != elem) {
                rl_free_buf(&rl_input_elem);
            }
            for (uint64_t i = 0; i < rl_history_len; i++) {
                if (&rl_history[i] != elem) {
                    rl_free_buf(&rl_history[i]);
                }
            }

            char *return_buf = elem->buf;
            /* Apply null terminator so we can return the buffer as
             * a string. */
            return_buf[elem->text_len] = 0;

            if (rl_append_input_to_history(return_buf)) {
                if (!rl_append_history(elem->buf)) {
                    *result = READLINE_OUT_OF_MEMORY;
                    rl_free_buf(elem);
                    rl_restore_tty_config();
                    return NULL;
                }
            }

            elem->buf = NULL;
            elem->buf_size = 0;
            elem->text_len = 0;
            elem->cursor_index = 0;
            elem->cursor_pos = 0;

            //printf("[readline] history:\n");
            //for (uint64_t i = 0; i < rl_history_len; i++) {
            //    printf("  %u64: '%s'\n", i, rl_history[i].history_text);
            //}

            if (streq(command, "input-eof") && strlen(return_buf) == 0) {
                *result = READLINE_EOF;
            } else {
                *result = READLINE_SUCCESS;
            }
            rl_restore_tty_config();
            return return_buf;
        } else if (streq(command, "insert-char")) {
            char c = rl_key_buf[rl_key_seq_len - 1];
            if (!rl_insert_char(rl_current_elem()->cursor_index, c)) {
                *result = READLINE_OUT_OF_MEMORY;
                goto readline_failed;
            }
        } else if (streq(command, "backspace")) {
            if (rl_current_elem()->cursor_index > 0) {
                rl_cursor_left();
                rl_delete_char(rl_current_elem()->cursor_index);
            }
        } else if (streq(command, "delete")) {
            rl_delete_char(rl_current_elem()->cursor_index);
        } else if (streq(command, "left")) {
            rl_cursor_left();
        } else if (streq(command, "right")) {
            rl_cursor_right();
        } else if (streq(command, "up")) {
            if (!rl_history_up()) {
                *result = READLINE_OUT_OF_MEMORY;
                goto readline_failed;
            }
        } else if (streq(command, "down")) {
            if (!rl_history_down()) {
                *result = READLINE_OUT_OF_MEMORY;
                goto readline_failed;
            }
        } else {
            printf("[readline: unknown command '%s']\n",
                    command);
        }

        /* Clear the key sequence buffer and continue reading. */
        rl_key_seq_len = 0;
    }

readline_failed:
    rl_free_bufs();
    rl_restore_tty_config();
    return NULL;
}

bool rl_append_history(const char *text)
{
    if (rl_history_buf_size == 0) {
        rl_history_buf_size = 64;
        rl_history_len = 0;
        rl_history = rl_malloc(rl_history_buf_size * sizeof(rl_history_elem_t));
        if (!rl_history) {
            return false;
        }
    }

    if (rl_history_len == rl_history_buf_size) {
        rl_history_buf_size *= 2;
        rl_history_elem_t *new_history = rl_realloc(rl_history,
                rl_history_buf_size * sizeof(rl_history_elem_t));
        if (!new_history) {
            return false;
        }
        rl_history = new_history;
    }

    uint64_t text_len = strlen(text);
    char *text_copy = rl_malloc(text_len + 1);
    if (!text_copy) {
        return false;
    }
    memcpy(text_copy, text, text_len + 1);

    rl_history_elem_t elem;
    elem.history_text = text_copy;
    elem.buf = NULL;
    elem.buf_size = 0;
    elem.text_len = 0;
    elem.cursor_index = 0;
    elem.cursor_pos = 0;

    rl_history[rl_history_len++] = elem;

    return true;
}
