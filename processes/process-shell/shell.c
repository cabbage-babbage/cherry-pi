#include "shell.h"
#include <stdio.h>
#include <syscall.h>
#include <string.h>
#include <heap.h>
#include <env.h>
#include "readline.h"
#include "strarray.h"

extern command_t builtins;

uint32_t last_exit_code;

/* Components of the logical cwd; might have symlinks. The path
 * (whose components are the elements of this array) is always
 * assumed to be an absolute path. Each entry is copied from elsewhere
 * and is owned by this array, so each entry must be freed before being
 * overwritten. */
strarray_t *cwd;

uint32_t process_line(char *line);

void setup_cwd()
{
    cwd = strarray_new(5);
    if (!cwd) {
        printf("error: out of memory\n");
        exit(1);
    }

    /* If the environment PWD exists and is an absolute path, we try to chdir() to it.
     * If that fails, or if otherwise the environment PWD is nonexistent or is a relative
     * path, then we just set the environment PWD to the physical cwd. */
    bool env_pwd_ok;
    char *pwd = getenv("PWD");
    if (pwd && *pwd == '/') {
        env_pwd_ok = chdir(pwd) >= 0;
    } else {
        env_pwd_ok = false;
    }

    if (!env_pwd_ok) {
        // TODO: actually get cwd, instead of assuming it's /.
        const char *cwd = "/";
        setenv("PWD", cwd, true);
    }

    pwd = getenv("PWD");
    if (!pwd || *pwd != '/') {
        panic("error: PWD should exist and be an absolute path by now\n");
    }
    while (true) {
        while (*pwd == '/') {
            pwd++;
        }
        if (!*pwd) {
            break;
        }

        uint64_t part_len = 0;
        while (pwd[part_len] && pwd[part_len] != '/') {
            part_len++;
        }
        char *part_copy = malloc(part_len + 1);
        if (!part_copy) {
            printf("error: out of memory\n");
            exit(1);
        }
        memcpy(part_copy, pwd, part_len);
        part_copy[part_len] = 0;

        if (!strarray_append(cwd, part_copy)) {
            printf("error: out of memory\n");
            exit(1);
        }

        pwd += part_len;
    }
}

uint32_t main(uint64_t argc, char **argv)
{
    if (!rl_init()) {
        puts("error: could not init readline\n");
        return 1;
    }

    /* Give PATH and HOME some reasonable defaults if they don't exist. */
    setenv("PATH", "/bin", false);
    setenv("HOME", "/home", false);

    setup_cwd();

    last_exit_code = 0;

    while (true) {
        const char *prompt = getenv("prompt");
        if (prompt) {
            puts(prompt);
        } else {
            const char *cwd_base;

            const char *pwd = getenv("PWD");
            const char *home = getenv("HOME");
            if (pwd && home && streq(pwd, home)) {
                cwd_base = "~";
            } else if (strarray_len(cwd) == 0) {
                cwd_base = "/";
            } else {
                cwd_base = strarray_get(cwd, strarray_len(cwd) - 1);
            }

#define RESET   "\x1b[0m"
#define BLACK   "\x1b[30m"
#define RED     "\x1b[31m"
#define GREEN   "\x1b[32m"
#define YELLOW  "\x1b[33m"
#define BLUE    "\x1b[34m"
#define PURPLE  "\x1b[35m"
#define CYAN    "\x1b[36m"
#define WHITE   "\x1b[37m"
            puts(YELLOW "[");
            if (last_exit_code == 0) {
                puts(GREEN "(0) ");
            } else {
                printf(RED "(%u32) ", last_exit_code);
            }
            printf(PURPLE "%s" YELLOW "]$" RESET " ", cwd_base);
        }

        readline_status_t read_result;
        char *line = readline("", &read_result);
        switch (read_result) {
            case READLINE_SUCCESS:
            case READLINE_EOF:
                last_exit_code = process_line(line);
                free(line);
                if (read_result == READLINE_EOF) {
                    /* We're done. */
                    return 0;
                }
                break;
            case READLINE_CANCELLED:
                /* That's fine, just prompt again. */
                free(line);
                break;
            case READLINE_READ_FAILURE:
                printf("readline error: read failure\n");
                return 1;
            case READLINE_OUT_OF_MEMORY:
                printf("readline error: out of memory\n");
                return 1;
            default:
                printf("readline error: unknown error\n");
                return 1;
        }

        uint32_t child_id;
        while (true) {
            int64_t wait_result = wait(&child_id, WAIT_ANY | WAIT_NONBLOCKING);
            if (child_id) {
                printf("[PID %u32] exit %u32\n",
                        child_id, (uint32_t) wait_result);
            } else {
                break;
            }
        }
    }

    return 0;
}

bool is_whitespace(char c)
{
    return c == ' ' || c == '\t' || c == '\n' || c == '\r';
}

/* Read a line part from line, and null-terminate it.
 * Return a pointer to the part content, and set next
 * to a pointer to just after the parsed part.
 * (On error, returns NULL and does not set next.) */
char *process_part(char *line, char **next)
{
    bool single_quoted = false, double_quoted = false;
    if (*line == '\'') {
        single_quoted = true;
        line++;
    } else if (*line == '"') {
        double_quoted = true;
        line++;
    }

    char *start_part = line;
    char *part = line;

    /* Process until we see an end-of-part indicator. If single-quoted,
     * this means an unescaped single-quote ('); if double-quoted, this
     * means an unescaped double-quote ("); if unquoted, this means any
     * space character (space or tab). */
    while (*line) {
        if (*line == '\\') {
            line++;
            switch (*line) {
                case '\\':
                    *part++ = '\\';
                    line++;
                    break;
                case ' ':
                    *part++ = ' ';
                    line++;
                    break;
                case 't':
                    *part++ = '\t';
                    line++;
                    break;
                case 'n':
                    *part++ = '\n';
                    line++;
                    break;
                case 'r':
                    *part++ = '\r';
                    line++;
                    break;
                case '\'':
                    *part++ = '\'';
                    line++;
                    break;
                case '"':
                    *part++ = '"';
                    line++;
                    break;
                default:
                    /* Invalid escape sequence. */
                    return NULL;
            }
        } else if (is_whitespace(*line) && !single_quoted && !double_quoted) {
            /* End of the part, since it's not quoted. */
            break;
        } else if (*line == '\'' && single_quoted) {
            break;
        } else if (*line == '"' && double_quoted) {
            break;
        } else {
            *part++ = *line++;
        }
    }

    if (single_quoted) {
        /* Expect a single-quote. */
        if (*line != '\'') {
            return NULL;
        }
        line++;
        *part++ = '\0';
        *next = line;
        return start_part;
    } else if (double_quoted) {
        /* Expect a double-quote. */
        if (*line != '"') {
            return NULL;
        }
        line++;
        *part++ = '\0';
        *next = line;
        return start_part;
    } else {
        /* Expect a whitespace character, or EOL. */
        if (*line && !is_whitespace(*line)) {
            return NULL;
        }
        if (*line) {
            line++;
        }
        *part++ = '\0';
        *next = line;
        return start_part;
    }
}

uint32_t process_line(char *line)
{
    const uint64_t MAX_ARGC = 128;
    uint64_t argc = 0;
    char *argv[MAX_ARGC];

    /* Split the line into parts. */
    while (*line) {
        while (is_whitespace(*line)) {
            line++;
        }

        if (!*line) {
            break;
        }

        if (argc == MAX_ARGC) {
            printf("too many arguments; the line may have at most %u64 parts\n",
                    MAX_ARGC);
            return 127;
        }

        char *next;
        char *part = process_part(line, &next);
        if (!part) {
            printf("syntax error, starting around: %s\n", line);
            return 127;
        }

        argv[argc++] = part;
        line = next;
    }

    if (argc == 0) {
        /* Line had only whitespace. */
        return last_exit_code;
    }

    const char *cmd_name = argv[0];

    bool background = false;
    if (streq(argv[argc - 1], "&")) {
        background = true;
        argc--;
    }

    for (command_t *cmd = &builtins; cmd->name; cmd++) {
        if (streq(cmd->name, cmd_name)) {
            /* Found a builtin command. */
            if (background) {
                printf("shell: cannot execute builtin %s in the background\n", cmd_name);
                return 127;
            }
            return cmd->fn(argc, (const char **) argv);
        }
    }

    /* Not a built-in. */
    int64_t _pid = fork();
    if (_pid < 0) {
        printf("error: could not fork: %e\n", errno);
        return 127;
    } else if (_pid == 0) {
        /* Child process. */
        bool simple_name = true;
        for (const char *s = cmd_name; *s; s++) {
            if (*s == '/') {
                simple_name = false;
                break;
            }
        }

        if (simple_name) {
            const char *PATH = getenv("PATH");
            /* Split PATH by | into different directories to try. Skip empty entries. */
            while (true) {
                while (*PATH == '|') {
                    PATH++;
                }
                if (!*PATH) {
                    break;
                }

                uint64_t part_len = 0;
                while (PATH[part_len] && PATH[part_len] != '|') {
                    part_len++;
                }

                uint64_t cmd_name_len = strlen(cmd_name);
                char *cmd_path = malloc(part_len + 1 + cmd_name_len + 1);
                if (!cmd_path) {
                    printf("error: out of memory\n");
                    exit(128);
                }
                /* We must stitch together the PATH part, "/", and the cmd_name.
                 * Unfortunately the PATH part is not null-terminated, so we cannot
                 * use strcat_array(). */
                memcpy(cmd_path, PATH, part_len);
                cmd_path[part_len] = '/';
                memcpy(cmd_path + part_len + 1, cmd_name, cmd_name_len + 1);

                execv(cmd_name, cmd_path, argc, (const char **) argv);
                if (errno != ENOENT) {
                    printf("could not execute %s: %e\n", cmd_path, errno);
                    exit(127);
                }
                free(cmd_path);

                PATH += part_len;
            }

            printf("%s: command not found\n", cmd_name);
            exit(127);
        } else {
            execv(cmd_name, cmd_name, argc, (const char **) argv);
            printf("could not execute %s: %e\n", cmd_name, errno);
            exit(127);
        }
    } else {
        /* Parent process. */
        uint32_t pid = (uint32_t) _pid;

        if (background) {
            printf("[PID %u32]\n", pid);
            return 0;
        }

        int64_t code = wait(&pid, 0);
        if (code < 0) {
            printf("error: could not wait for exit: %e\n", errno);
            return 127;
        } else {
            return (uint32_t) code;
        }
    }
}
