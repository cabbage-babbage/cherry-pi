#include "shell.h"
#include <stdio.h>
#include <syscall.h>
#include <string.h>
#include <env.h>
#include <heap.h>
#include <uapi/tty.h>
#include "strarray.h"

/* Returns -1 if the char is not numeric in the given base, or
 *    if the base is not in the range [1, 36]
 * Otherwise, returns the unsigned char value, in the given base.
 * Examples:
 *    parse_numeric_char('b', 16) == 11
 *    parse_numeric_char('z', 16) == -1
 *    parse_numeric_char('3', 2) == -1
 *    parse_numeric_char('?', 100000) == -1 */
int8_t parse_numeric_char(char c, int base)
{
    if (base < 1 || base > 36) {
        return -1;
    }

    uint8_t num = 0;
    if ('0' <= c && c <= '9') {
        num = c - '0';
    } else if ('a' <= c && c <= 'z') {
        num = (c - 'a') + 10;
    } else if ('A' <= c && c <= 'Z') {
        num = (c - 'A') + 10;
    } else {
        return -1;
    }

    if (num > base) {
        return -1;
    } else {
        return num;
    }
}

#define SUCCESS 0
#define NOT_INT 1
#define TOO_BIG 2

/* Parse a uint{bytes} from the string, and put it in 'val'.
 * The entire input string is required to be numeric; this doesn't
 * read only a prefix.
 * 'bytes' must be 1, 2, 4, or 8.
 * Allowed prefixes:
 *    0b: binary
 *    0o: octal
 *    0d: decimal (default)
 *    0x: hexadecimal
 * Returns:
 *    0 on success.
 *    1 if the string could not be read as an integer.
 *    2 if the numeric value overflowed the specified number of bytes.
 *    3 if 'bytes' is invalid. */
int parse_uint(const char *str, void *val, int bytes)
{
    if (bytes != 1 && bytes != 2 && bytes != 4 && bytes != 8) {
        return 3; /* Invalid 'bytes' argument. */
    }

    int base = 10;
    if (strlen(str) > 2 && str[0] == '0') {
        switch (str[1]) {
            case 'b':
                base = 2;
                str += 2;
                break;
            case 'o':
                base = 8;
                str += 2;
                break;
            case 'd':
                base = 10;
                str += 2;
                break;
            case 'x':
                base = 16;
                str += 2;
                break;
        }
    }
    /* Now we have our base. Let's get the limit. */
    uint64_t limit = 0;
    switch (bytes) {
        case 1:
            limit = (uint64_t) 1 << 8;
            break;
        case 2:
            limit = (uint64_t) 2 << 8;
            break;
        case 4:
            limit = (uint64_t) 4 << 8;
            break;
    }

    uint64_t num = 0;
    while (*str) {
        int8_t char_num_res = parse_numeric_char(*str, base);
        if (char_num_res < 0) {
            return 1; /* Invalid numeric string. */
        }
        uint8_t char_num = (uint8_t) char_num_res;
        if (bytes == 8) {
            /* Special case overflow checking. */
            if (num > 0 && num * base + char_num <= num) {
                return 2; /* Overflow. */
            }
        } else {
            if (num * base + char_num >= limit) {
                return 2; /* Overflow. */
            }
        }
        num = num * base + char_num;
        str++;
    }

    /* Now we have our number. */
    switch (bytes) {
        case 1:
            *(uint8_t*) val = (uint8_t) num;
            break;
        case 2:
            *(uint16_t*) val = (uint16_t) num;
            break;
        case 4:
            *(uint32_t*) val = (uint32_t) num;
            break;
        case 8:
            *(uint64_t*) val = (uint64_t) num;
            break;
    }
    return 0;
}

int parse_uint8(const char *str, uint8_t *val)
{
    return parse_uint(str, val, 1);
}
int parse_uint16(const char *str, uint16_t *val)
{
    return parse_uint(str, val, 2);
}
int parse_uint32(const char *str, uint32_t *val)
{
    return parse_uint(str, val, 4);
}
int parse_uint64(const char *str, uint64_t *val)
{
    return parse_uint(str, val, 8);
}

uint32_t builtin_fork_bomb(uint64_t argc, const char **argv)
{
    while (true) {
        int64_t pid = fork();
        if (pid < 0) {
            printf("could not fork\n");
            exit(2);
        } else if (pid == 0) {
            printf("I am a fork\n");
        }
    }
}

uint32_t builtin_create_bomb(uint64_t argc, const char **argv)
{
    while (true) {
        int64_t pid = fork();
        if (pid == 0) {
            exec("bomb", "quick-exit", NULL, 0);
            printf("[shell] could not start process\n");
            exit(2);
        } else if (pid < 0) {
            printf("[shell] could not fork\n");
            sleep(0);
        } else {
            printf("[shell] created PID %u32\n", (uint32_t) pid);
        }
    }
}

uint32_t builtin_exit(uint64_t argc, const char **argv)
{
    if (argc == 1) {
        exit(0);
    } else if (argc == 2) {
        uint32_t exit_code;
        int status = parse_uint32(argv[1], &exit_code);
        if (status == SUCCESS) {
            exit(exit_code);
        }

        if (status == NOT_INT) {
            printf("exit: could not understand %s: numeric value required\n",
                    argv[1]);
        } else if (status == TOO_BIG) {
            printf("exit: exit code %s is too high\n", argv[1]);
        }
        exit(2);
    } else {
        printf("exit: too many arguments\n");
        exit(2);
    }
}

uint32_t builtin_pid(uint64_t argc, const char **argv)
{
    printf("%u32\n", get_pid());
    return 0;
}

uint32_t builtin_test_nonblocking(uint64_t argc, const char **argv)
{
    uint32_t cfg = (uint32_t) get_desc_cfg(0);
    set_desc_cfg(0, cfg | CFG_NONBLOCKING);

    for (int i = 0; i < 5; i++) {
        char buf[10];
        int64_t result = read(0, (uint8_t *) buf, 10);
        if (result < 0) {
            printf("got error: %e\n", errno);
        } else {
            uint64_t bytes_read = (uint64_t) result;
            printf("got %u64 in: ", bytes_read);
            for (uint64_t j = 0; j < bytes_read; j++) {
                putc(buf[j]);
            }
            printf("\n");
        }
        sleep(2000);
    }

    set_desc_cfg(0, cfg);

    return 0;
}

uint32_t builtin_cd(uint64_t argc, const char **argv)
{
    if (argc != 2) {
        printf("cd takes one argument\n");
        return 1;
    }
    const char *path = argv[1];
    if (chdir(path) < 0) {
        printf("cd: could not change directory: %e\n", errno);
        return 1;
    }

    /* We must update our logical cwd by 'walking' the given path;
     * if the component is . we do not change the cwd, if the
     * component is .. we pop the last component of the cwd, and
     * otherwise we just append the component to the cwd. */
    extern strarray_t *cwd;
    if (*path == '/') {
        strarray_free_all(cwd);
        path++;
    }

    while (true) {
        while (*path == '/') {
            path++;
        }
        if (!*path) {
            break;
        }

        uint64_t part_len = 0;
        while (path[part_len] && path[part_len] != '/') {
            part_len++;
        }

        char *part = malloc(part_len + 1);
        if (!part) {
            printf("error: out of memory\n");
            exit(128);
        }

        memcpy(part, path, part_len);
        part[part_len] = 0;

        path += part_len;

        if (streq(part, ".")) {
            /* Do nothing to cwd. */
        } else if (streq(part, "..")) {
            strarray_free_last(cwd);
        } else {
            if (!strarray_append(cwd, part)) {
                printf("error: out of memory\n");
                exit(128);
            }
        }
    }

    /* Finally, we need to update the environment PWD according
     * to our cwd array. */
    if (strarray_len(cwd) == 0) {
        if (setenv("PWD", "/", true) < 0) {
            printf("error: out of memory\n");
            exit(128);
        }
    } else {
        uint64_t pwd_len = 1 /* null terminator */;
        strarray_for(cwd, i) {
            pwd_len += 1 /* '/' */ + strlen(strarray_get(cwd, i));
        }
        char *new_pwd = malloc(pwd_len);
        if (!new_pwd) {
            printf("error: out of memory\n");
            exit(128);
        }
        char *cur = new_pwd;
        strarray_for(cwd, i) {
            const char *part = strarray_get(cwd, i);
            *cur++ = '/';
            uint64_t part_len = strlen(part);
            memcpy(cur, part, part_len);
            cur += part_len;
        }
        *cur = 0;

        if (setenv("PWD", new_pwd, true) < 0) {
            printf("error: out of memory\n");
            exit(128);
        }

        free(new_pwd);
    }

    return 0;
}

uint32_t builtin_wait(uint64_t argc, const char **argv)
{
    bool got_child_id = false;
    uint32_t child_id;
    bool wait_any = false;
    bool wait_nonblocking = false;

    for (uint64_t i = 1; i < argc; i++) {
        if (streq(argv[i], "-a") || streq(argv[i], "--any")) {
            wait_any = true;
        } else if (streq(argv[i], "-b") || streq(argv[i], "--nonblocking")) {
            wait_nonblocking = true;
        } else {
            int status = parse_uint32(argv[i], &child_id);
            if (status == SUCCESS) {
                if (got_child_id) {
                    printf("wait: only one child id may be specified\n");
                    return 1;
                }
                got_child_id = true;
            } else if (status == NOT_INT) {
                printf("wait: could not understand %s\n", argv[i]);
                return 1;
            } else /* TOO_BIG */ {
                printf("wait: child id %s is too large\n", argv[i]);
                return 1;
            }
        }
    }

    if (wait_any && got_child_id) {
        printf("wait: specifying --any and a child id are mutually exclusive\n");
        return 1;
    }

    int64_t code = wait(&child_id,
            (wait_any ? WAIT_ANY : 0) |
            (wait_nonblocking ? WAIT_NONBLOCKING : 0));
    if (code >= 0) {
        if (wait_nonblocking && child_id == 0) {
            printf("no child has exited\n");
        } else {
            printf("PID %u32 exited with %u32\n",
                    child_id, (uint32_t) code);
        }
    } else {
        printf("wait: %e\n", errno);
    }
    return 0;
}

uint32_t builtin_sleep(uint64_t argc, const char **argv)
{
    if (argc != 2) {
        printf("sleep takes one argument\n");
        return 1;
    }

    uint64_t ms;
    int status = parse_uint64(argv[1], &ms);
    if (status == SUCCESS) {
        sleep(ms);
        return 0;
    } else if (status == NOT_INT) {
        printf("sleep: could not understand %s: numeric value required\n",
                argv[1]);
        return 1;
    } else /* TOO_BIG */ {
        printf("sleep: number of milliseconds %s is too large\n", argv[1]);
        return 1;
    }
}

uint32_t builtin_desc_cfg(uint64_t argc, const char **argv)
{
    /* desc-cfg <fd> [flags] */
    if (argc <= 1) {
        printf("desc-cfg: fd is required\n");
        return 1;
    }

    uint32_t fd;
    int status = parse_uint32(argv[1], &fd);
    if (status != SUCCESS) {
        if (status == NOT_INT) {
            printf("desc-cfg: could not understand %s: numeric value required\n",
                    argv[1]);
        } else /* TOO_BIG */ {
            printf("desc-cfg: fd %s is too large\n", argv[1]);
        }
        return 1;
    }

    bool set_cfg = false;
    bool set_close_on_exec = false,
         reset_close_on_exec = false;
    bool set_nonblocking = false,
         reset_nonblocking = false;

    for (uint64_t i = 2; i < argc; i++) {
        if (streq(argv[i], "+e") || streq(argv[i], "+close-on-exec")) {
            set_cfg = true;
            set_close_on_exec = true;
        } else if (streq(argv[i], "-e") || streq(argv[i], "-close-on-exec")) {
            set_cfg = true;
            reset_close_on_exec = true;
        } else if (streq(argv[i], "+b") || streq(argv[i], "+blocking") || streq(argv[i], "-nonblocking")) {
            set_cfg = true;
            reset_nonblocking = true;
        } else if (streq(argv[i], "-b") || streq(argv[i], "-blocking") || streq(argv[i], "+nonblocking")) {
            set_cfg = true;
            set_nonblocking = true;
        } else {
            printf("desc-cfg: could not understand %s\n", argv[i]);
            return 1;
        }
    }

    if (set_close_on_exec && reset_close_on_exec) {
        printf("desc-cfg: cannot both set and reset close-on-exec\n");
        return 1;
    }
    if (set_nonblocking && reset_nonblocking) {
        printf("desc-cfg: cannot both set and reset nonblocking\n");
        return 1;
    }

    if (set_cfg) {
        int64_t result = get_desc_cfg(fd);
        if (result < 0) {
            printf("desc-cfg: could not get desc cfg: %e\n", errno);
            return 1;
        }

        uint32_t cfg = (uint32_t) result;

        if (set_close_on_exec) {
            cfg |= CFG_CLOSE_ON_EXEC;
        } else if (reset_close_on_exec) {
            cfg &= ~CFG_CLOSE_ON_EXEC;
        }

        if (set_nonblocking) {
            cfg |= CFG_NONBLOCKING;
        } else if (reset_nonblocking) {
            cfg &= ~CFG_NONBLOCKING;
        }

        if (set_desc_cfg(fd, cfg) < 0) {
            printf("desc-cfg: could not set desc cfg: %e\n", errno);
            return 1;
        }

        return 0;
    } else {
        int64_t result = get_desc_cfg(fd);
        if (result >= 0) {
            uint32_t cfg = (uint32_t) result;
            printf("%sread %swrite %sappend %sclose-on-exec %snonblocking %stty\n",
                    (cfg & CFG_READABLE) ? "+" : "-",
                    (cfg & CFG_WRITEABLE) ? "+" : "-",
                    (cfg & CFG_APPEND) ? "+" : "-",
                    (cfg & CFG_CLOSE_ON_EXEC) ? "+" : "-",
                    (cfg & CFG_NONBLOCKING) ? "+" : "-",
                    (cfg & CFG_IS_TTY) ? "+" : "-");
            return 0;
        } else {
            printf("desc-cfg: %e\n", errno);
            return 1;
        }
    }
}

uint32_t builtin_open(uint64_t argc, const char **argv)
{
    bool o_read = false,
         o_write = false,
         o_append = false,
         o_dir = false,
         o_nonblock = false,
         o_close_exec = false,
         o_create = false,
         o_new = false,
         o_close = false;

    if (argc == 1) {
        printf("open: path is required\n");
        return 1;
    }

    const char *path = argv[argc - 1];

    for (uint64_t i = 1; i < argc - 1; i++) {
        if (streq(argv[i], "-r") || streq(argv[i], "--read")) {
            o_read = true;
        } else if (streq(argv[i], "-w") || streq(argv[i], "--write")) {
            o_write = true;
        } else if (streq(argv[i], "-a") || streq(argv[i], "--append")) {
            o_append = true;
        } else if (streq(argv[i], "-d") || streq(argv[i], "--dir")) {
            o_dir = true;
        } else if (streq(argv[i], "-b") || streq(argv[i], "--nonblock")) {
            o_nonblock = true;
        } else if (streq(argv[i], "-e") || streq(argv[i], "--close-exec")) {
            o_close_exec = true;
        } else if (streq(argv[i], "-c") || streq(argv[i], "--create")) {
            o_create = true;
        } else if (streq(argv[i], "-n") || streq(argv[i], "--new")) {
            o_new = true;
        } else if (streq(argv[i], "-C") || streq(argv[i], "--close")) {
            o_close = true;
        } else {
            printf("open: unknown argument %s\n", argv[i]);
            return 1;
        }
    }

    int64_t fd = open(path,
            (o_read ? O_READ : 0) |
            (o_write ? O_WRITE : 0) |
            (o_append ? O_APPEND : 0) |
            (o_dir ? O_DIR : 0) |
            (o_nonblock ? O_NONBLOCK : 0) |
            (o_close_exec ? O_CLOSE_EXEC : 0) |
            (o_create ? O_CREATE : 0) |
            (o_new ? O_NEW : 0) |
            (o_close ? O_CLOSE : 0));
    if (fd >= 0) {
        if (o_close) {
            printf("open: %u32\n", (uint32_t) fd);
        } else {
            printf("opened fd %u32\n", (uint32_t) fd);
        }
    } else {
        printf("open: %e\n", errno);
    }

    return 0;
}

uint32_t builtin_open_at(uint64_t argc, const char **argv)
{
    bool o_read = false,
         o_write = false,
         o_append = false,
         o_dir = false,
         o_nonblock = false,
         o_close_exec = false,
         o_create = false,
         o_new = false,
         o_close = false;

    if (argc < 3) {
        printf("open-at: not enough arguments\n");
        return 1;
    }

    uint32_t dirfd;
    int status = parse_uint32(argv[1], &dirfd);
    if (status != SUCCESS) {
        if (status == NOT_INT) {
            printf("open-at: could not understand %s: numeric value required\n",
                    argv[1]);
        } else /* TOO_BIG */ {
            printf("open-at: fd %s is too large\n", argv[1]);
        }
        return 1;
    }

    const char *path = argv[argc - 1];

    for (uint64_t i = 2; i < argc - 1; i++) {
        if (streq(argv[i], "-r") || streq(argv[i], "--read")) {
            o_read = true;
        } else if (streq(argv[i], "-w") || streq(argv[i], "--write")) {
            o_write = true;
        } else if (streq(argv[i], "-a") || streq(argv[i], "--append")) {
            o_append = true;
        } else if (streq(argv[i], "-d") || streq(argv[i], "--dir")) {
            o_dir = true;
        } else if (streq(argv[i], "-b") || streq(argv[i], "--nonblock")) {
            o_nonblock = true;
        } else if (streq(argv[i], "-e") || streq(argv[i], "--close-exec")) {
            o_close_exec = true;
        } else if (streq(argv[i], "-c") || streq(argv[i], "--create")) {
            o_create = true;
        } else if (streq(argv[i], "-n") || streq(argv[i], "--new")) {
            o_new = true;
        } else if (streq(argv[i], "-C") || streq(argv[i], "--close")) {
            o_close = true;
        } else {
            printf("open: unknown argument %s\n", argv[i]);
            return 1;
        }
    }

    int64_t fd = open_at(dirfd, path,
            (o_read ? O_READ : 0) |
            (o_write ? O_WRITE : 0) |
            (o_append ? O_APPEND : 0) |
            (o_dir ? O_DIR : 0) |
            (o_nonblock ? O_NONBLOCK : 0) |
            (o_close_exec ? O_CLOSE_EXEC : 0) |
            (o_create ? O_CREATE : 0) |
            (o_new ? O_NEW : 0) |
            (o_close ? O_CLOSE : 0));
    if (fd >= 0) {
        if (o_close) {
            printf("open: %u32\n", (uint32_t) fd);
        } else {
            printf("opened fd %u32\n", (uint32_t) fd);
        }
    } else {
        printf("open: %e\n", errno);
    }

    return 0;
}

uint32_t builtin_close(uint64_t argc, const char **argv)
{
    if (argc <= 1) {
        printf("close: fd required\n");
        return 1;
    }

    bool success = true;
    for (uint64_t i = 1; i < argc; i++) {
        uint32_t fd;
        int status = parse_uint32(argv[i], &fd);
        if (status == SUCCESS) {
            if (close(fd) < 0) {
                printf("close: could not close fd %u32: %e\n", fd, errno);
                success = false;
            } else {
                printf("closed fd %u32\n", fd);
            }
        } else if (status == NOT_INT) {
            printf("close: could not understand %s: numeric value required\n",
                    argv[1]);
            success = false;
        } else /* TOO_BIG */ {
            printf("close: fd %s is too large\n", argv[1]);
            success = false;
        }
    }

    return success ? 0 : 1;
}

uint32_t builtin_dup(uint64_t argc, const char **argv)
{
    if (argc != 2) {
        printf("dup takes one argument\n");
        return 1;
    }

    uint32_t fd;
    int status = parse_uint32(argv[1], &fd);
    if (status != SUCCESS) {
        if (status == NOT_INT) {
            printf("dup: could not understand %s: numeric value required\n",
                    argv[1]);
        } else /* TOO_BIG */ {
            printf("dup: fd %s is too large\n", argv[1]);
        }
        return 1;
    }

    int64_t new_fd = dup(fd);
    if (new_fd >= 0) {
        printf("new fd %u32\n", (uint32_t) new_fd);
        return 0;
    } else {
        printf("dup: %e\n", errno);
        return 1;
    }
}

uint32_t builtin_dup_into(uint64_t argc, const char **argv)
{
    if (argc != 3) {
        printf("dup-into takes two argument\n");
        return 1;
    }

    uint32_t fd;
    int status = parse_uint32(argv[1], &fd);
    if (status != SUCCESS) {
        if (status == NOT_INT) {
            printf("dup: could not understand %s: numeric value required\n",
                    argv[1]);
        } else /* TOO_BIG */ {
            printf("dup: fd %s is too large\n", argv[1]);
        }
        return 1;
    }

    uint32_t new_fd;
    status = parse_uint32(argv[2], &new_fd);
    if (status != SUCCESS) {
        if (status == NOT_INT) {
            printf("dup: could not understand %s: numeric value required\n",
                    argv[2]);
        } else /* TOO_BIG */ {
            printf("dup: fd %s is too large\n", argv[2]);
        }
        return 1;
    }

    int64_t newer_fd = dup_into(fd, new_fd);
    if (newer_fd >= 0) {
        printf("duplicated into fd %u32\n", (uint32_t) newer_fd);
        return 0;
    } else {
        printf("dup-into: %e\n", errno);
        return 1;
    }
}

uint32_t builtin_ioctl(uint64_t argc, const char **argv)
{
    printf("ioctl: not implemented\n");
    return 1;
}

void print_stat(stat_t stat)
{
    bool r = stat.perm & INODE_PERM_R,
         w = stat.perm & INODE_PERM_W,
         x = stat.perm & INODE_PERM_X;

    printf("nr %u64:%u64, refs %u64, size %u64, type %s, perm %s%s%s%s%s%s%s%s, dev %u64, created %u64, modified %u64\n",
            stat.filesys_nr, (uint64_t) stat.inode_nr,
            stat.refs,
            stat.size,
            (stat.type == INODE_NONE ? "none" :
             stat.type == INODE_FILE ? "file" :
             stat.type == INODE_DIR ? "dir" :
             stat.type == INODE_SYMLINK ? "symlink" :
             stat.type == INODE_CHARDEV ? "chardev" :
             stat.type == INODE_BLOCKDEV ? "blockdev" :
             "???"),
            (r || w || x) ? "+" : "",
            r ? "r" : "", w ? "w" : "", x ? "x" : "",
            (!r || !w || !x) ? "-" : "",
            r ? "" : "r", w ? "" : "w", x ? "" : "x",
            stat.device_id,
            stat.time_created,
            stat.time_modified);
}

uint32_t builtin_stat(uint64_t argc, const char **argv)
{
    if (argc <= 1) {
        printf("stat: path is required\n");
        return 1;
    }

    if (argc == 2) {
        stat_t fstat;
        if (stat(argv[1], &fstat) >= 0) {
            print_stat(fstat);
            return 0;
        } else {
            printf("stat: %e\n", errno);
            return 1;
        }
    }

    bool success = true;
    for (uint64_t i = 1; i < argc; i++) {
        if (i != 1) {
            printf("\n");
        }
        stat_t fstat;
        if (stat(argv[i], &fstat) >= 0) {
            printf("%s:\n", argv[i]);
            print_stat(fstat);
        } else {
            printf("stat: could not stat %s: %e\n", argv[i], errno);
            success = false;
        }
    }

    return success ? 0 : 1;
}

uint32_t builtin_stat_at(uint64_t argc, const char **argv)
{
    if (argc != 3) {
        printf("stat-at takes two arguments\n");
        return 1;
    }

    uint32_t fd;
    int status = parse_uint32(argv[1], &fd);
    if (status != SUCCESS) {
        if (status == NOT_INT) {
            printf("stat-at: could not understand %s: numeric value required\n",
                    argv[1]);
        } else /* TOO_BIG */ {
            printf("stat-at: fd %s is too large\n", argv[1]);
        }
        return 1;
    }

    stat_t fstat;
    if (stat_at(fd, argv[2], &fstat) >= 0) {
        print_stat(fstat);
        return 0;
    } else {
        printf("stat-at: %e\n", errno);
        return 1;
    }
}

uint32_t builtin_env(uint64_t argc, const char **argv)
{
    if (argc == 1) {
        char *const *envp = environment();
        for (char *const *env = envp; *env; env++) {
            printf("%s\n", *env);
        }
        return 0;
    } else if (argc == 2) {
        char *value = getenv(argv[1]);
        if (value) {
            printf("%s\n", value);
        } else {
            printf("env: %s does not exist\n", argv[1]);
        }
        return 0;
    } else if (argc == 3) {
        if (setenv(argv[1], argv[2], true) < 0) {
            printf("env: could not setenv: %e\n", errno);
            return 1;
        }
        return 0;
    } else {
        printf("usage: env [<name> [<value>]]\n");
        return 1;
    }
}

uint32_t builtin_mkpipe(uint64_t argc, const char **argv)
{
    bool o_nonblock = false,
         o_close_exec = false;

    for (uint64_t i = 1; i < argc - 1; i++) {
        if (streq(argv[i], "-b") || streq(argv[i], "--nonblock")) {
            o_nonblock = true;
        } else if (streq(argv[i], "-e") || streq(argv[i], "--close-exec")) {
            o_close_exec = true;
        } else {
            printf("open: unknown argument %s\n", argv[i]);
            return 1;
        }
    }

    uint32_t flags =
            (o_nonblock ? O_NONBLOCK : 0) |
            (o_close_exec ? O_CLOSE_EXEC : 0);

    uint32_t rfd, wfd;
    if (mkpipe(&rfd, &wfd, flags) < 0) {
        printf("mkpipe: %e\n", errno);
        return 1;
    } else {
        printf("mkpipe: opened read fd %u32, write fd %u32\n",
                rfd, wfd);
        return 0;
    }
}

uint32_t builtin_read(uint64_t argc, const char **argv)
{
    /* read [-a|--all] [-b N | --buffer N] <fd> [<N>] */
    bool read_all = false;

    if (argc >= 2 && (streq(argv[1], "-a") || streq(argv[1], "--all"))) {
        read_all = true;
        argc--;
        argv++;
    }

    if (argc < 2) {
        printf("read: fd required\n");
        return 1;
    }
    uint32_t fd;
    int status = parse_uint32(argv[1], &fd);
    if (status != SUCCESS) {
        if (status == NOT_INT) {
            printf("read: could not understand %s: numeric value required\n",
                    argv[1]);
        } else /* TOO_BIG */ {
            printf("read: fd %s is too large\n", argv[1]);
        }
        return 1;
    }

    uint64_t read_required = 0;
    if (argc > 3) {
        printf("read: too many arguments\n");
        return 1;
    }

    if (argc == 3) {
        if (read_all) {
            printf("read: -a and a byte count are mutually exclusive\n");
            return 1;
        }
        int status = parse_uint64(argv[2], &read_required);
        if (status != SUCCESS) {
            if (status == NOT_INT) {
                printf("read: could not understand %s: numeric value required\n",
                        argv[2]);
            } else /* TOO_BIG */ {
                printf("read: byte count %s is too large\n", argv[2]);
            }
            return 1;
        }

        if (read_required == 0) {
            printf("read: byte count must be nonzero\n");
            return 1;
        }
    } else {
        if (!read_all) {
            printf("read: byte count is required (since -a is not specified)\n");
            return 1;
        }
    }

    if (read_all) {
        read_required = UINT64_MAX;
    }

    const uint64_t buf_size = 64;
    uint8_t buf[buf_size];

    uint64_t total_read = 0;
    int64_t result = 0;
    while ((result = read(fd, buf, buf_size < read_required ? buf_size : read_required)) > 0) {
        for (uint64_t i = 0; i < result; i++) {
            putc(buf[i]);
        }
        total_read += result;
        read_required -= result;
    }
    if (result == 0) {
        if (total_read < read_required) {
            printf("read: got EOF after reading %u64 bytes\n", total_read);
        }
        return 0;
    } else {
        printf("read: got %e after reading %u64 bytes\n", errno, total_read);
        return 1;
    }
}

uint32_t builtin_write(uint64_t argc, const char **argv)
{
    /* write <fd> <content> */
    if (argc < 3) {
        printf("write: not enough arguments\n");
        return 1;
    }
    uint32_t fd;
    int status = parse_uint32(argv[1], &fd);
    if (status != SUCCESS) {
        if (status == NOT_INT) {
            printf("write: could not understand %s: numeric value required\n",
                    argv[1]);
        } else /* TOO_BIG */ {
            printf("write: fd %s is too large\n", argv[1]);
        }
        return 1;
    }

    const uint8_t *buf = (const uint8_t *) argv[2];
    uint64_t buf_size = strlen(argv[2]);

    uint64_t total_written = 0;
    int64_t result;
    while ((result = write(fd, buf, buf_size)) > 0) {
        total_written += result;
        buf += result;
        buf_size -= result;
    }
    if (result == 0) {
        if (total_written < strlen(argv[2])) {
            printf("write: got EOF after writing %u64 bytes\n", total_written);
        }
        return 0;
    } else {
        printf("read: got %e after writing %u64 bytes\n", errno, total_written);
        return 1;
    }
}

command_t builtins[] = {
    { "fork-bomb", &builtin_fork_bomb },
    { "create-bomb", &builtin_create_bomb },
    { "exit", &builtin_exit },
    { "pid", &builtin_pid },
    { "test-nonblocking", &builtin_test_nonblocking },
    { "cd", &builtin_cd },
    { "wait", &builtin_wait },
    { "sleep", &builtin_sleep },
    { "desc-cfg", &builtin_desc_cfg },
    { "open", &builtin_open },
    { "open-at", &builtin_open_at },
    { "close", &builtin_close },
    { "dup", &builtin_dup },
    { "dup-into", &builtin_dup_into },
    { "ioctl", &builtin_ioctl },
    { "stat", &builtin_stat },
    { "stat-at", &builtin_stat_at },
    { "env", &builtin_env },
    { "mkpipe", &builtin_mkpipe },
    { "read", &builtin_read },
    { "write", &builtin_write },
    { NULL, NULL }
};
