#ifndef READLINE_H
#define READLINE_H

#include <std.h>

/* Set up defaults. Returns false on failure, likely out-of-memory. */
USE_VALUE bool rl_init();

/* Allocate memory. */
typedef ptr_t (*malloc_t)(uint64_t size);
/* Reallocate memory. */
typedef ptr_t (*realloc_t)(ptr_t alloc, uint64_t new_size);
/* Free memory. */
typedef void (*free_t)(ptr_t alloc);

void rl_set_malloc_fn(malloc_t malloc_fn);
void rl_set_realloc_fn(realloc_t realloc_fn);
void rl_set_free_fn(free_t free_fn);

typedef enum {
    /* Recursive keymap entry. */
    ENTRY_KEYMAP,
    /* The rest are 'terminal' entries. */
    ENTRY_COMMAND,
    ENTRY_EMPTY
} rl_keymap_entry_type_t;

typedef struct {
    rl_keymap_entry_type_t type;
    /* For ENTRY_KEYMAP, this is a pointer to another rl_keymap_t.
     * For ENTRY_COMMAND, this is a string indicating the command to run.
     * For ENTRY_EMPTY, this is NULL. */
    ptr_t data;
} rl_keymap_entry_t;

typedef struct {
    /* One entry per possible input character.
     * Note that entry 0 is always empty. */
    rl_keymap_entry_t entries[256];
} rl_keymap_t;

/* Allocate a new keymap. Must be freed (rl_free_keymap()) by the caller.
 * All entries by default are type 'empty'.
 * Returns NULL on out-of-memory. */
USE_VALUE rl_keymap_t *rl_keymap_new();

/* (Recursively) free a keymap. */
void rl_free_keymap(rl_keymap_t *keymap);

/* Constructors for the entry types. */
USE_VALUE rl_keymap_entry_t rl_keymap_entry_keymap(rl_keymap_t *keymap);
USE_VALUE rl_keymap_entry_t rl_keymap_entry_command(const char *command);
USE_VALUE rl_keymap_entry_t rl_keymap_entry_empty();

/* Update a keymap (possibly recursively) to specify that the input sequence is mapped
 * to the given entry. Usually the entry should be a terminal entry.
 * The input_sequence must be nonempty.
 * Returns false on failure, of which the likely cause is out-of-memory. */
USE_VALUE bool rl_keymap_update(rl_keymap_t *keymap, const char *input_sequence,
        rl_keymap_entry_t new_entry);

/* Returns the command associated to the command entry, if the input sequence in fact leads to a
 * command entry; otherwise returns NULL.
 * Sets got_terminal to true if the input sequence lead to any terminal entry, or false otherwise. */
USE_VALUE const char *rl_keymap_get_command(rl_keymap_t *keymap, const char *input_sequence,
        bool *got_terminal);

/* Specifies whether or not a completed input text should be appended to history. This can modify the
 * input, but usually there's no reason to. */
typedef bool (*rl_append_history_predicate_t)(char *text);
/* Set the predicate function which determines whether or not input text should be appended to
 * history when input is complete. Some basic predicates are available:
 *   &rl_never_append_history: always append to history
 *   &rl_always_append_history: never append to history
 *   &rl_append_history_if_nonempty: append to history if there's any non-whitespace character
 *      (this is the default)
 * */
void rl_set_append_history_predicate(rl_append_history_predicate_t predicate);
bool rl_never_append_history(char *text);
bool rl_always_append_history(char *text);
bool rl_append_history_if_nonempty(char *text);

// TODO: autosuggestion (inline) instead of autocompletion list (out-of-line)

/* Recommend a list of completions based on the current input and cursor index.
 * Returns a NULL-terminated array of strings to suggest to the user (in order).
 * The cursor index lies in the range [0, strlen(input)]. */
typedef const char **(*autocompletion_fn_t)(const char *input, uint64_t cursor_index);
/* Callback for the user accepting an autocomplete option. Provides the index
 * of the accepted option, along with the option text. */
typedef void (*autocompletion_accepted_fn_t)(uint64_t index, const char *text);

typedef enum {
    READLINE_SUCCESS,
    READLINE_CANCELLED,
    READLINE_READ_FAILURE,
    READLINE_EOF,
    READLINE_OUT_OF_MEMORY,
} readline_status_t;

/* Read a line from input. The return value (if not NULL) must be free()d by
 * the caller. This also sets the result, if not NULL.
 * The returned string is valid only if the result is READLINE_SUCCESS or
 * READLINE_EOF. Once this function returns a status of READLINE_EOF, it is
 * an error to call this function again. (It will, however, respond with the
 * READLINE_EOF and an empty string.) */
USE_VALUE char *readline(const char *prompt, readline_status_t *result);

/* The text is copied. Returns false on failure, i.e. out of memory. */
USE_VALUE bool rl_append_history(const char *text);

#endif
