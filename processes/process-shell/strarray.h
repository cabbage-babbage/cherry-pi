#ifndef STRARRAY_H
#define STRARRAY_H

#include <std.h>

struct strarray;
typedef struct strarray strarray_t;

/* Returns a fresh array on success, NULL on out-of-memory. */
strarray_t *strarray_new(uint64_t init_elems);

/* Returns NULL if the index is out of bounds. */
const char *strarray_get(strarray_t *array, uint64_t index);

uint64_t strarray_len(strarray_t *array);

/* Returns true on success, false on out-of-memory. */
bool strarray_prepend(strarray_t *array, const char *elem);
/* Returns true on success, false on out-of-memory. */
bool strarray_append(strarray_t *array, const char *elem);

/* Removes the last entry of the array. This does not free() the
 * removed entry. No change is made if the array is empty. */
void strarray_remove_last(strarray_t *array);

/* free(s) and removes the last entry of the array. No change is made
 * if the array is empty. */
void strarray_free_last(strarray_t *array);

/* Clears the array by setting the length to 0. This does not free()
 * any of the entries in the array. */
void strarray_clear(strarray_t *array);

/* free()s all the current entries and then clears the array. */
void strarray_free_all(strarray_t *array);

#define strarray_for(array, index_name) \
    for (uint64_t index_name = 0, __len = strarray_len(array); \
         index_name < __len; \
         index_name++)

#endif
