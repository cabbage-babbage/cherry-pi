include ../build_flags

# Make sure the linker does not add tons of padding in between sections.
# It's not entirely clear why these help, but it seems to work so far.
LFLAGS += -n -z max-page-size=4096

SUBDIRS=$(filter-out bin/,$(wildcard */))
PROCESSES=$(SUBDIRS:%/=%)

srcs_c_for_process=$(wildcard $(1)/*.c)
srcs_s_for_process=$(wildcard $(1)/*.s)
objs_c_for_process=$(patsubst %.c,bin/%.o,$(call srcs_c_for_process,$(1)))
objs_s_for_process=$(patsubst %.s,bin/%.o,$(call srcs_s_for_process,$(1)))
objs_for_process=$(call objs_c_for_process,$(1)) $(call objs_s_for_process,$(1))
bin_objs_for_process=$(patsubst bin/%,%,$(call objs_for_process,$(1)))

ALL_SRCS_C=$(wildcard */*.c) $(wildcard *.c)
ALL_SRCS_S=$(wildcard */*.s) $(wildcard *.s)
ALL_OBJS_C=$(ALL_SRCS_C:%.c=bin/%.o)
ALL_OBJS_S=$(ALL_SRCS_S:%.s=bin/%.o)
ALL_OBJS=$(ALL_OBJS_C) $(ALL_OBJS_S)

ALL_PROCESS_ELFS=$(PROCESSES:%=bin/%/process.elf)

.PHONY: all
all: $(PROCESSES:%=bin/%/process.elf)

.PHONY: clean
clean:
	rm -rf bin

.PHONY: save_intermediates
save_intermediates: $(ALL_OBJS) $(ALL_PROCESS_ELFS)

bin/%.o: %.s
	@if [ ! -d $(shell dirname $@) ]; then mkdir -p $(shell dirname $@); fi
	$(TOOLCHAIN)-as $(AFLAGS) $< -o $@

bin/%.o: %.c
	@if [ ! -d $(shell dirname $@) ]; then mkdir -p $(shell dirname $@); fi
	$(TOOLCHAIN)-gcc $(CFLAGS) -isystem ../stdlib/include -c $< -o $@

.SECONDEXPANSION:
bin/%/process.elf: ../stdlib/bin/stdlib.a bin/process_entry.o $$(call objs_for_process,$$*)
	@if [ ! -d bin/$* ]; then mkdir -p bin/$*; fi
	cd bin && $(TOOLCHAIN)-ld $(LFLAGS) process_entry.o $(call bin_objs_for_process,$*) ../../stdlib/bin/stdlib.a -o $*/process.elf

bin/%/process.dump: bin/%/process.elf
	$(TOOLCHAIN)-objdump -htrD $< > $@
