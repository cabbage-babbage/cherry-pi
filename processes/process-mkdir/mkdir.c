#include <std.h>
#include <stdio.h>
#include <syscall.h>
#include <errno.h>
#include <string.h>

/* Return the index of the last path separator ('/') in the path
 * which actually splits path components, or -1 if no such separator
 * exists.
 * For example,
 *   last_split('/') == -1,
 *   last_split('a/b/c/') == 3,
 *   last_split('/a') == -1. */
int64_t last_split(const char *path)
{
    uint64_t offset = 0;
    while (*path == '/') {
        path++;
        offset++;
    }
    uint64_t len = strlen(path);
    while (len > 0 && path[len - 1] == '/') {
        len--;
    }
    /* Now we have a string of length 'len' with no '/' at the start
     * or end. */
    for (int64_t i = len - 1; i >= 0; i--) {
        if (path[i] == '/') {
            return offset + i;
        }
    }
    return -1;
}

uint32_t mkdir(char *path)
{
    uint64_t full_path_len = strlen(path);

    while (open(path, O_DIR | O_CREATE | O_CLOSE) < 0 && errno == ENOENT) {
        /* Reset path to be the parent directory path, by null-terminating
         * in the middle of the existing path. */
        int64_t split = last_split(path);
        if (split < 0) {
            /* No parent directory. */
            break;
        }

        /* Sanity check. */
        if (split >= full_path_len || path[split] != '/') {
            panic("bug\n");
        }
        path[split] = 0;
    }

    if (errno) {
        fprintf(FD_STDERR, "mkdir: could not create %s: %e\n",
                path, errno);
        return 1;
    }

    /* We were able to create a directory! Now try going back to the full path,
     * adding one component at a time. If anything fails here, we're not going
     * to try the parents again. */
    uint64_t path_len;
    while ((path_len = strlen(path)) < full_path_len) {
        path[path_len] = '/';
        if (open(path, O_DIR | O_CREATE | O_CLOSE) < 0) {
            fprintf(FD_STDERR, "mkdir: could not create %s: %e\n",
                    path, errno);
            return 1;
        }
    }

    return 0;
}

__attribute__((noreturn))
void usage_and_exit(uint32_t fd, uint32_t exit_code)
{
    fprintf(fd, "usage: mkdir [-p] <path>\n");
    exit(exit_code);
}

uint32_t main(uint64_t argc, char **argv)
{
    if (argc < 2) {
        fprintf(FD_STDERR, "not enough arguments\n");
        usage_and_exit(FD_STDERR, 1);
    }

    bool make_parents = false;
    char *path = NULL;
    for (uint64_t i = 1; i < argc; i++) {
        if (streq(argv[i], "-p")) {
            make_parents = true;
        } else {
            if (path) {
                fprintf(FD_STDERR, "path may only be specified once\n");
                usage_and_exit(FD_STDERR, 1);
            }
            path = argv[i];
        }
    }

    if (!path) {
        fprintf(FD_STDERR, "path is required\n");
        usage_and_exit(FD_STDERR, 1);
    }

    if (!make_parents) {
        if (open(path, O_DIR | O_CREATE | O_CLOSE) >= 0) {
            return 0;
        } else {
            fprintf(FD_STDERR, "mkdir: could not create %s: %e\n",
                    path, errno);
            return 1;
        }
    }

    return mkdir(path);
}
