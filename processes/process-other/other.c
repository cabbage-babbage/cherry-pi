#include <std.h>
#include <console.h>
#include <proc.h>
#include <userspace_syscall.h>
#include <dbg.h>

void main()
{
    syscall_puts("[other] was set up properly!\n");

    printf("[other] sleeping for a few seconds\n");
    syscall_sleep_ms(3000);

    printf("[other] looping\n");
    for (int i = 0; i < 5; i++) {
        syscall_sleep_ms(1000);
        putc('.');
    }
}
