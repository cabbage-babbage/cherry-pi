#include <std.h>
#include <stdio.h>
#include <syscall.h>
#include <errno.h>

uint32_t main(uint64_t argc, char **argv)
{
    // TODO: usage: wc [-c|-w|-l|-p] [<file> ...]

    if (argc != 1) {
        fprintf(FD_STDERR, "wc: no arguments expected\n");
        return 1;
    }

    const uint64_t buf_size = 1024;
    uint8_t buf[buf_size];

    uint64_t total_read = 0;
    int64_t result;
    while ((result = read(FD_STDIN, buf, buf_size)) > 0) {
        total_read += result;
    }

    if (result < 0) {
        fprintf(FD_STDERR, "wc: error reading stdin: %e\n", errno);
        return 1;
    }

    printf("%u64\n", total_read);
    return 0;
}
