.global _start
_start:
    // No need to set up SP; the kernel has already done that for us.
    // Also, the kernel set up our initial arguments buffer in x0 and x1,
    // so we can just leave those for the program to use.
    bl _entry
    // Make an exit syscall (#0).
    mov x1, x0
    mov x0, #0
    svc #1

// Fall through to infinite loop. Definitely shouldn't get here.
loop:
    b loop
