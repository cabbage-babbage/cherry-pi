#include <std.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <errno.h>

#define FD_STDIN  0
#define FD_STDOUT 1
#define FD_STDERR 2

bool cat_chardev(uint32_t fd)
{
    uint8_t in;
    int64_t bytes_read;
    while ((bytes_read = read((uint32_t) fd, &in, 1)) > 0 || (bytes_read < 0 && errno == EINTR)) {
        if (bytes_read < 0) continue;
        if (write(FD_STDOUT, &in, bytes_read) < 0) {
            fprintf(FD_STDERR, "cat: error writing file: %e\n", errno);
            return false;
        }
    }
    if (bytes_read < 0) {
        fprintf(FD_STDERR, "cat: error reading file: %e\n", errno);
        return false;
    }
    return true;
}

bool cat_buffered(uint32_t fd)
{
    uint8_t buf[128];
    int64_t bytes_read;
    while ((bytes_read = read((uint32_t) fd, buf, 128)) > 0) {
        if (write(FD_STDOUT, buf, bytes_read) < 0) {
            fprintf(FD_STDERR, "cat: error writing file: %e\n", errno);
            return false;
        }
    }
    if (bytes_read < 0) {
        fprintf(FD_STDERR, "cat: error reading file: %e\n", errno);
        return false;
    }
    return true;
}

uint32_t main(uint64_t argc, char **argv)
{
    bool success = true;
    if (argc == 1) {
        success = cat_chardev(FD_STDIN) && success;
    } else {
        for (uint64_t i = 1; i < argc; i++) {
            if (streq(argv[i], "-")) {
                success = cat_chardev(FD_STDIN) && success;
            } else {
                int64_t fd = open(argv[i], O_READ);
                if (fd >= 0) {
                    stat_t st;
                    stat_at((uint32_t) fd, "", &st);
                    if (st.type == INODE_CHARDEV) {
                        success = cat_chardev((uint32_t) fd) && success;
                    } else {
                        success = cat_buffered((uint32_t) fd) && success;
                    }
                    close((uint32_t) fd);
                } else {
                    fprintf(FD_STDERR, "cat: could not open file '%s': %e\n", argv[i], errno);
                    success = false;
                }
            }
        }
    }

    return success ? 0 : 1;
}
