#include <std.h>
#include <stdio.h>
#include <syscall.h>
#include <errno.h>

uint32_t main()
{
    /* As the init process, we actually have no file descriptors open at startup;
     * we need to do that ourselves. */
    int64_t fd = open("/dev/tty", O_READ);
    if (fd < 0) {
        char msg[128];
        sprintf(msg, "could not open /dev/tty for reading: %e\n", errno);
        panic(msg);
    }
    if (fd != 0) {
        char msg[128];
        sprintf(msg, "expected to open /dev/tty as fd 0: got fd %u32 instead\n",
                (uint32_t) fd);
        panic(msg);
    }

    fd = open("/dev/tty", O_WRITE);
    if (fd < 0) {
        char msg[128];
        sprintf(msg, "could not open /dev/tty for writing: %e\n", errno);
        panic(msg);
    }
    if (fd != 1) {
        char msg[128];
        sprintf(msg, "expected to open /dev/tty as fd 1: got fd %u32 instead\n",
                (uint32_t) fd);
        panic(msg);
    }

    fd = dup_into(1, 2);
    if (fd < 0) {
        char msg[128];
        sprintf(msg, "could not dup /dev/tty: %e\n", errno);
        panic(msg);
    }
    if (fd != 2) {
        char msg[128];
        sprintf(msg, "expected to dup /dev/tty as fd 2: got fd %u32 instead\n",
                (uint32_t) fd);
        panic(msg);
    }

    /* Now stdin, stdout, and stderr are set up. */

    printf("[init] starting shell\n");

    int64_t pid = fork();
    if (pid < 0) {
        printf("[init] could not fork shell: %e\n", errno);
    } else if (pid == 0) {
        exec("shell", "/bin/shell", NULL, 0);
        printf("[init] could not start shell: %e\n", errno);
    } else {
        printf("[init] forked shell (PID %u32)\n", (uint32_t) pid);
    }

    while (true) {
        uint32_t child_id;
        int64_t code = wait(&child_id, WAIT_ANY);
        if (code < 0) {
            printf("[init] could not wait(): %e\n", errno);
        } else {
            printf("[init] PID %u32 exited with code %u32\n",
                child_id, (uint32_t) code);
        }
    }
}
