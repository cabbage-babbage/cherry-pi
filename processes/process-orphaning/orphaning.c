#include <std.h>
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <errno.h>

uint32_t main(uint64_t argc, char **argv)
{
    /* orphaning -l
     *    this process will live longer than the processes it creates */
    bool should_sleep = argc >= 2 && streq(argv[1], "-l");

    if (chdir("/bin") < 0) {
        printf("could not chdir to /bin: %e\n", errno);
    }

    for (uint32_t i = 0; i < 30; i++) {
        if (fork() == 0) {
            exec("long-running", "long-running", NULL, 0);
            exit(1);
        }
    }

    if (should_sleep) {
        printf("sleeping!\n");
        sleep(1000 * 5);
    }

    printf("exiting!\n");

    return 0;
}
