#include <std.h>
#include <stdio.h>
#include <syscall.h>

uint32_t main()
{
    printf("[long-running] starting sleep\n");
    sleep(1000 * 3);
    printf("[long-running] finished sleep\n");

    return 0;
}
