#include <std.h>
#include <stdio.h>
#include <string.h>

uint32_t main(uint64_t argc, char **argv)
{
    bool print_newline = true;

    argc--;
    argv++;
    if (argc && streq(argv[0], "-n")) {
        print_newline = false;
        argc--;
        argv++;
    }

    for (uint64_t i = 0; i < argc; i++) {
        if (i != 0) {
            puts(" ");
        }
        puts(argv[i]);
    }

    if (print_newline) {
        puts("\n");
    }

    return 0;
}
